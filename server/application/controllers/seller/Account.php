<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('seller/account_model', 'main');
    }

    public function get_company_profile(){
        $result = $this->main->get_company_profile($this->input->post('agentID'));
        echo json_encode($result);
    }

    public function get_staff_profile(){
        $result = $this->main->get_staff_profile($this->input->post('staffID'));
        echo json_encode($result);
    }

    public function update_company_profile(){
        $result = $this->main->update_company_profile($this->input->post('agent'));
        echo json_encode($result);
    }

    public function update_staff_profile(){
        $result = $this->main->update_staff_profile($this->input->post('staff'));
        echo json_encode($result);
    }

    public function get_staff(){
        $result = $this->main->get_staff($this->input->post('agentID'));
        echo json_encode($result);
    }

    public function get_staff_id(){
        $result = $this->main->get_staff_id($this->input->post('id'));
        echo json_encode($result);
    }

    public function save_staff(){
        $result = $this->main->save_staff($this->input->post('agentID'));
        echo json_encode($result);
    }

    public function reset_staff(){
        $result = $this->main->reset_staff($this->input->post('agentID'));
        echo json_encode($result);
    }

    public function delete_staff(){
        $result = $this->main->delete_staff($this->input->post('id'));
        echo json_encode($result);
    }

}