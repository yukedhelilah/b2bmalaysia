<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reservation extends CI_Controller {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('seller/reservation_model', 'main');
    }

    public function get_reservation(){
        $result = $this->main->get_reservation();
        echo json_encode($result);
    }

    public function set_status(){
        $result = $this->main->set_status($this->input->post());
        echo json_encode($result);
    }   

    public function get_details(){
        $result = $this->main->get_details($this->input->post('id'));
        echo json_encode($result);
    }

    public function get_departure(){
        $result = $this->main->get_departure($this->input->post('id'));
        echo json_encode($result);
    }

    public function get_pickup(){
        $result = $this->main->get_pickup($this->input->post('ID'));
        echo json_encode($result);
    }

    public function set_pickup(){
        if($this->input->post('actionPickup')=='add'){
	        $result = $this->main->set_pickup($this->input->post());
	        echo json_encode($result);
        }
        else if ($this->input->post('actionPickup')=='edit') {
	        $result = $this->main->update_pickup($this->input->post());
	        echo json_encode($result);
        }  
    }	
}