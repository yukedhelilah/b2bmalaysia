<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Utils extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('buyer/product_model', 'mainmodul');
        $this->load->model('buyer/utils_model', 'utilsmodul');
    }

    public function index_get()
    {
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function kurs_post()
    {
        $data = $this->utilsmodul->kurs();
        $this->response([
            'code' => 200,
            'status' => 'Success',
            'data' => $data,
        ], 200);
    }

    public function reservation_post()
    {
        if(!empty($this->input->post('user'))){
            $token = $this->utilsmodul->token($this->input->post('user'));
            $data = $this->utilsmodul->reservation($token['id']);
            $this->response([
                'code' => 200,
                'status' => 'Success',
                'data' => $data,
            ], 200);
        }
    }

}