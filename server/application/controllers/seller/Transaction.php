<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('buyer/product_model', 'mainmodul');
        $this->load->model('buyer/utils_model', 'utilsmodul');
        $this->load->model('buyer/transaction_model', 'trsmodul');

    }

    public function index_get()
    {
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function save_post()
    {
        if(!empty($this->input->post('user'))){
            $getToken = $this->utilsmodul->token($this->input->post('user'));
            if (isset($getToken['error'])) {
                $this->response([
                    'code' => 500,
                    'status' => 'Internal Server Error',
                    'info' => $getToken['error'],
                ], 200);
            }else{
                if($getToken['status']=='false'){
                    $this->response([
                        'code' => 500,
                        'status' => 'Internal Server Error',
                        'info' => 'Invalid token',
                    ], 200);
                }else{
                    $product    = $this->mainmodul->fne_package_detil($this->input->post('id'));
                    $inDate     = $this->input->post('depdate_yy').'-'.$this->input->post('depdate_mm').'-'.$this->input->post('depdate_dd');
                    $outDate    = date('Y-m-d', strtotime("+".$product['noofdays']." day", strtotime($inDate)));

                    $data = [
                        'bookingCode'   => strtoupper($this->generateRandomString()),
                        'bookingDate'   => date('Y-m-d'),
                        'bookingTime'   => date('H:i:s'),
                        'agentID'       => $getToken['id'],
                        'productID'     => $product['_'],
                        'inDate'        => $inDate,
                        'outDate'       => $outDate,
                        'noofpax'       => count($this->input->post('g_category')),
                        'contactName'   => strtoupper($this->input->post('cp_name')),
                        'contactMobile' => $this->input->post('cp_mobile'),
                    ];

                    $sql = $this->trsmodul->add_reservation($data);

                    if($sql)
                    {
                        $g_category = $this->input->post('g_category');
                        $g_fname    = $this->input->post('g_fname');
                        $g_lname    = $this->input->post('g_lname');
                        $g_passport = $this->input->post('g_passport');
                        $g_expired  = $this->input->post('g_expired');
                        $g_dob      = $this->input->post('g_dob');
                        foreach ($g_category as $key => $value) {
                            if($value=='single'){ $OpriceNett = $product['pSingle']; $priceNett = $product['pSingleReal']; }
                            if($value=='twin'){ $OpriceNett = $product['pAdultTwin']; $priceNett = $product['pAdultTwinReal']; }
                            if($value=='triple'){ $OpriceNett = $product['pAdultTriple']; $priceNett = $product['pAdultTripleReal']; }
                            if($value=='cwa'){ $OpriceNett = $product['pCwa']; $priceNett = $product['pCwaReal']; }
                            if($value=='cwb'){ $OpriceNett = $product['pCwb']; $priceNett = $product['pCwbReal']; }
                            if($value=='cnb'){ $OpriceNett = $product['pCnb']; $priceNett = $product['pCnbReal']; }

                            $data_details = [
                                'reservationID' => $sql,
                                'gCategory'     => $value,
                                'gFname'        => strtoupper($g_fname[$key]),
                                'gLname'        => strtoupper($g_lname[$key]),
                                'gPassport'     => strtoupper($g_passport[$key]),
                                'gExpired'      => strtoupper($g_expired[$key]),
                                'gDob'          => strtoupper($g_dob[$key]),
                                'OpriceNett'    => $OpriceNett,
                                'priceNett'     => $priceNett,
                                'OagentComm'    => $product['agentcomm'],
                                'agentComm'     => $product['agentcommReal'],
                                'OadminComm'    => $product['admincomm'],
                                'adminComm'     => $product['admincommReal'],
                                'exchangeRate'  => $product['kurs'],
                            ];

                            $sql_details = $this->trsmodul->add_reservation_details($data_details);
                        }

                        $this->response([
                            'code' => 200,
                            'status' => 'Success',
                            //'data' => $data,
                        ], 200);
                    }
                }
            }
        }
        
    }

}