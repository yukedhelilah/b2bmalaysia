<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('seller/authentication_model', 'mainmodul');
    }

    private function response($data = array(), $code) {
        echo json_encode($data);
    }

    public function index_get()
    {
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function signup_post()
    {
        $required = [];
        if(empty($this->input->post('companyName'))){
            $required[] = 'Company Name';
        }

        if(empty($this->input->post('companyAddress'))){
            $required[] = 'Company Address';
        }

        if(empty($this->input->post('companyCountry'))){
            $required[] = 'Company Country';
        }

        if(empty($this->input->post('companyCity'))){
            $required[] = 'Company City';
        }

        if(empty($this->input->post('companyEmail'))){
            $required[] = 'Company Email';
        }

        if(empty($this->input->post('companyLicence'))){
            $required[] = 'Company Licence No';
        }

        if(empty($this->input->post('contactName'))){
            $required[] = 'Contact person - Name';
        }

        if(empty($this->input->post('contactPosition'))){
            $required[] = 'Contact person - Position';
        }

        if(empty($this->input->post('contactMobile'))){
            $required[] = 'Contact person - Mobile';
        }

        if(empty($this->input->post('contactWhatsapp'))){
            $required[] = 'Contact person - Whatsapp';
        }

        if(empty($this->input->post('contactEmail'))){
            $required[] = 'Contact person - Email';
        }

        if(empty($this->input->post('emailAddress'))){
            $required[] = 'Email';
        }

        if(count($required)>0){
            $error = [
                'reason'    => 'required',
                'message'   => join(", ",$required).' is required',
            ];
            $this->response([
                'code'      => 401,
                'message'   => 'Unauthorized',
                'errors'    => $error,
            ], 200);
        }

        $data = $this->mainmodul->cek_email(strtolower($this->input->post('emailAddress')));
        if (isset($data['error'])) {
            $error = [
               'message'   => $data['error']
            ];
            $this->response([
                'code'      => 500,
                'message'   => 'Internal Server Error',
                'errors'    => $error,
            ], 200);
        }else{
            if($data['total']==0){
                //Action -> Save
                $input_data = [
                    'sellerEmail'       => strtolower($this->input->post('emailAddress')),
                    'companyName'       => strtoupper($this->input->post('companyName')),
                    'companyAddress'    => strtoupper($this->input->post('companyAddress')),
                    'companyCity'       => strtoupper($this->input->post('companyCity')),
                    'companyCountry'    => strtoupper($this->input->post('companyCountry')),
                    'companyZipcode'    => strtoupper($this->input->post('companyZipcode')),
                    'companyPhone'      => strtoupper($this->input->post('companyPhone')),
                    'companyFax'        => strtoupper($this->input->post('companyFax')),
                    'companyEmail'      => strtolower($this->input->post('companyEmail')),
                    'companyLicence'    => strtolower($this->input->post('companyLicence')),
                    'contactName'       => strtoupper($this->input->post('contactName')),
                    'contactPosition'   => strtoupper($this->input->post('contactPosition')),
                    'contactEmail'      => strtolower($this->input->post('contactEmail')),
                    'contactMobile'     => strtoupper($this->input->post('contactMobile')),
                    'contactWhatsapp'   => strtoupper($this->input->post('contactWhatsapp')),
                    // 'sellerUsername'    => strtolower('superid'),
                    // 'userPassword'      => md5('12345678'),
                    'isApproved'        => 0,
                    'flag'              => 0,
                    'createdDate'       => date('Y-m-d H:i:s'),
                    'approvedDate'      => date('Y-m-d H:i:s'),
                    'approvedBy'        => 0,
                ];
                $sql_insert = $this->mainmodul->add_seller($input_data);
                $this->response([
                    'code' => 200,
                    'status' => 'Success',
                    'data' => 'Thank you for registration B2B Malaysia Holiday. We will inform you soon when your account has been approved.',
                    //'data' => 'Hallo, '.strtoupper($this->input->post('companyName')).'. Your account is successfully created. Go to Sign-in page with your email dan password.',
                ], 200); 
            }else{
                $error = [
                    'message'   => 'This email already registered'
                ];
                $this->response([
                    'code'      => 500,
                    'message'   => 'Internal Server Error',
                    'errors'    => $error,
                ], 200);
            }
        }
    }

    public function signin_post()
    {
        $required = [];
        if(empty($this->input->post('companySellerID'))){
            $required[] = 'Email';
        }

        if(empty($this->input->post('companyUsername'))){
            $required[] = 'Username';
        }

        if(empty($this->input->post('companyPassword'))){
            $required[] = 'Password';
        }

        if(count($required)>0){
            $error = [
                'reason'    => 'required',
                'message'   => join(", ",$required).' is required',
            ];
            $this->response([
                'code'      => 401,
                'message'   => 'Unauthorized',
                'errors'    => $error,
            ], 200);
        }

        $data = $this->mainmodul->cek_akun(strtolower($this->input->post('companySellerID')),strtolower($this->input->post('companyUsername')),md5($this->input->post('companyPassword')));
        if (isset($data['error'])) {
            $error = [
               'message'   => $data['error']
            ];
            $this->response([
                'code'      => 500,
                'message'   => 'Internal Server Error',
                'errors'    => $error,
            ], 200);
        }else{
            if($data['total']==1){
                //Action -> Login
                $this->response([
                    'code' => 200,
                    'status' => 'Success',
                    'data' => $data,
                ], 200); 
            }else{
                $error = [
                   'message'   => 'Invalid combination Email and Password.'
                ];
                $this->response([
                    'code'      => 500,
                    'message'   => 'Internal Server Error',
                    'errors'    => $error,
                ], 200);
            }
        }

        // $username = strtolower($this->input->post('companyUsername'));
        // $email = strtolower($this->input->post('emailAddress'));
        // $cek = $this->mainmodul->cek_username($username, $email);

        // echo $cek;

        // if ($cek->username != "superid") {
        //         $this->response([
        //             'code'      => 500,
        //             'message'   => 'Internal Server Error',
        //             'errors'    => $error,
        //         ], 200);
        // }
        // else {
        //     $this->response([
        //         'code' => 200,
        //         'status' => 'Success',
        //         'data' => $data,
        //     ], 200);
        // }
    }
}