<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('seller/product_model', 'main');
    }

    public function get_master_location(){
        $data = $this->main->view_data_location();
        echo json_encode($data);
    }

    public function get_master_tour(){
        $data = $this->main->view_data_tour();
        echo json_encode($data);
    }

    public function get_category(){
        $result = $this->main->get_category();
        echo json_encode($result);
    }

    public function get_category_id(){
        $result = $this->main->get_category_id($this->input->post('id'));
        echo json_encode($result);
    }

    public function get_category_product(){
        $result = $this->main->get_category_id($this->input->post('id-product'));
        echo json_encode($result);
    }

    public function get_product(){
        $result = $this->main->get_product($this->input->post('id'));
        echo json_encode($result);
    }

    public function get_product_id(){
        $result = $this->main->get_product_id($this->input->post('id-product'));
        echo json_encode($result);
    }

    public function save_category(){
        if($this->input->post('action')=='add'){
            $result = $this->main->insert_category($this->input->post());
            echo json_encode($result);
        }
        else if ($this->input->post('action')=='edit') {
            $result = $this->main->update_category($this->input->post());
            echo json_encode($result);
        }  
    }

    public function uploadImage(){
        $status = "";
        $msg    = "";
        $file_element_name = 'userfile2';

        if ($status != "error")
        {
            $config['upload_path']      = './assets/upload/';
            $config['allowed_types']    = 'gif|jpg|png';
            $config['max_size']         = 1024 * 8;

            $new_name = 'thumbnail_'.time();
            $config['file_name'] = $new_name;
      
            $this->load->library('upload', $config);
      
            if (!$this->upload->do_upload('userfile2')){
                $status = 'error';
                $msg    = $this->upload->display_errors('', '');
            }else{
                $img    = $this->upload->data();
                $status = '';
                $msg    = $img['file_name'];
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo $status.'*'.$msg;
    }

    public function save_product(){
        if($this->input->post('action-product')=='add-product'){
            $result = $this->main->insert_product($this->input->post());
            echo json_encode($result);
        }
        else if ($this->input->post('action-product')=='edit-product') {
            $result = $this->main->update_product($this->input->post());
            echo json_encode($result);
        }
    }

    public function delete(){
        if($this->input->post('tipe')=='category'){
            $result = $this->main->delete_category($this->input->post('id-product'));
            echo json_encode($result);
        }
        else if ($this->input->post('tipe')=='product') {
            $result = $this->main->delete_product($this->input->post('id-product'));
            echo json_encode($result);
        }
    }

    public function save_room(){
        $result = $this->main->save_room();
        echo json_encode($result);
    }

    public function delete_room(){
        $result = $this->main->delete_room();
        echo json_encode($result);
    }

    public function save_bod(){
        $result = $this->main->save_bod();
        echo json_encode($result);
    }

    public function delete_bod(){
        $result = $this->main->delete_bod();
        echo json_encode($result);
    }
}