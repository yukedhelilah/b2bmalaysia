<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Detils extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('buyer/product_model', 'mainmodul');
        $this->load->model('buyer/utils_model', 'utilsmodul');
    }

    public function index_get()
    {
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function fne_post()
    {
        if(!empty($this->input->post('id'))){
            $data = $this->mainmodul->fne_detil($this->input->post('id'));
            $this->response([
                'code' => 200,
                'status' => 'Success',
                'data' => $data,
            ], 200);
        }
    }

    public function fne_package_post()
    {
        if(!empty($this->input->post('id'))){
            $data = $this->mainmodul->fne_package_detil($this->input->post('id'));
            $this->response([
                'code' => 200,
                'status' => 'Success',
                'data' => $data,
            ], 200);
        }
    }

}