<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reservation extends CI_Controller {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('cronjob/reservation_model', 'main');
    }

    public function get_reservation(){
        $data = $this->main->get_reservation();

        $cancel = array();
        $waiting = array();
        foreach ($data['data'] as $key => $value) {
            $dateNow = date("Y-m-d");
            $dateBook = $value->bookingDate;
            $limitDate = date('Y-m-d', strtotime($dateBook. ' + 48 hours'));

            if ($value->isStatus == 0 && $dateNow <= $limitDate) {
                $waiting[] = "<table>
                                <tr>
                                    <td>Id</td>
                                    <td> : ".$value->reservationID."</td>
                                </tr>
                                <tr>
                                    <td>Booking Code</td>
                                    <td> : ".$value->bookingCode."</td>
                                </tr>
                                <tr>
                                    <td>Contact Name</td>
                                    <td> : ".$value->contactName."</td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td> : <span style='color: orange'>Waiting List</span></td>
                                </tr>
                            </table><br>";
            }else if ($value->isStatus == 0 && $dateNow > $limitDate) {
                $cancel[] = "<table>
                                <tr>
                                    <td>Id</td>
                                    <td> : ".$value->reservationID."</td>
                                </tr>
                                <tr>
                                    <td>Booking Code</td>
                                    <td> : ".$value->bookingCode."</td>
                                </tr>
                                <tr>
                                    <td>Contact Name</td>
                                    <td> : ".$value->contactName."</td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td> : <span style='color: red'>Cancel</span></td>
                                </tr>
                            </table><br>";
                $this->main->set_status($value->reservationID,3);
            }
        }   

        $txt = '';
        if (!empty($waiting) || !empty($cancel)) {
            foreach ($waiting as $key => $value) {
                $txt .= $value;
            }
            foreach ($cancel as $key => $value) {
                $txt .= $value;
            }
        }
        //echo $txt;

        $to = "yukedhelilah@gmail.com";
        $subject = "Cron Job - B2B MALAYSIA HOLIDAYS ".date('d M Y');

        $message = "
        <html>
        <head>
            <title>Cron Job - B2B MALAYSIA HOLIDAYS ".date('d M Y')."</title>
        </head>
        <body>
            ".$txt."
        </body>
        </html>
        ";

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        $headers .= 'From: <info@thegreatholiday.com>' . "\r\n";
        $headers .= 'Cc: itcreatrix@gmail.com' . "\r\n";

        echo $message;

        // if(!empty($arr)){
        //     $send = mail($to,$subject,$message,$headers);
        //     if (!$send) {
        //         $errorMessage = error_get_last()['message'];
        //         print_r(error_get_last());
        //     }
        // }

    }
}