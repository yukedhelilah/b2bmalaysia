<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kurs extends App_Public {

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('cronjob/utils_model', 'utilsmodul');
    }

    public function index_get()
    {
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function kurs_post()
    {
        $url     = 'https://www.bca.co.id/individu/sarana/kurs-dan-suku-bunga/kurs-dan-kalkulator';
        $ch     = curl_init(); 
        curl_setopt($ch, CURLOPT_HEADER, false); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
        $data = "";//"pnr=".strtoupper($pnr)."&lname=".strtoupper($lastname)."&dep=".strtoupper($departure);
               
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $returned = curl_exec($ch); 
        curl_close($ch);

        preg_match_all('/<td align="center" style="background-color: white">.*?<\/td>/sim', $returned, $currency, PREG_PATTERN_ORDER);
        preg_match_all('/<td style="background-color: white">.*?<\/td>/sim', $returned, $rate_jual, PREG_PATTERN_ORDER);

        $arr = array();
        if(count($currency[0])>0){
            $x = 0;
            for ($i=0; $i < count($currency[0]); $i++) { 
                for ($j=0; $j < 6; $j++) { 
                    $real = str_replace(".", "", $rate_jual[0][$x]);
                    $real = str_replace(",", ".", $real);
                    $arr[$currency[0][$i]][] = $real;
                    $x++;
                }
            }
        }

        preg_match_all('/<h4 style="text-align:center"><small>.*?<\/small><\/h4>/sim', $returned, $date, PREG_PATTERN_ORDER);
        $text = preg_replace("/<([^<>]*)>/", "", $date[0][0]);
        $explode = explode(" ", $text);
        $source_month = array(
            '01' => 'JANUARI',
            '02' => 'FEBRUARI',
            '03' => 'MARET',
            '04' => 'APRIL',
            '05' => 'MEI',
            '06' => 'JUNI',
            '07' => 'JULI',
            '08' => 'AGUSTUS',
            '09' => 'SEPTEMBER',
            '10' => 'OKTOBER',
            '11' => 'NOVEMBER',
            '12' => 'DESEMBER',
        );

        $tgl = $explode[2].'-'.array_search(strtoupper($explode[1]), $source_month).'-'.$explode[0];
        $jam = $explode[3];

        foreach ($arr as $key => $value) {
            $data = array(
                'kursDate'      => $tgl,
                'kursTime'      => $jam.':00',
                'currency'      => preg_replace("/<([^<>]*)>/", "", $key),
                'rate_jual'     => preg_replace("/<([^<>]*)>/", "", $value[0]),
                'rate_beli'     => preg_replace("/<([^<>]*)>/", "", $value[1]),
                'tt_jual'       => preg_replace("/<([^<>]*)>/", "", $value[2]),
                'tt_beli'       => preg_replace("/<([^<>]*)>/", "", $value[3]),
                'notes_jual'    => preg_replace("/<([^<>]*)>/", "", $value[4]),
                'notes_beli'    => preg_replace("/<([^<>]*)>/", "", $value[5]),
                'inputDate'     => date('Y-m-d H:i:s'),
            );
            
            $sql = $this->utilsmodul->add_kurs_all($data);

            if(preg_replace("/<([^<>]*)>/", "", $key)=='MYR')
            {
                $data2 = array(
                    'kursDate'      => $tgl,
                    'currency'      => 'IDR',
                    'amount'        => preg_replace("/<([^<>]*)>/", "", $value[0]),
                    'inputDate'     => date('Y-m-d H:i:s'),
                );

                $sql2 = $this->utilsmodul->add_kurs($data2);
            }
        }

        $this->response([
            'code' => 200,
            'status' => 'Success',
        ], 200);
    }

}