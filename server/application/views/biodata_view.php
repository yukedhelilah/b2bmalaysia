<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
    <head>
        <title>Coba 2</title>
        <!-- Core CSS -->
        <link href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/additional.css" rel="stylesheet" type="text/css" />

        <!-- Core Javascript -->
        <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/js/additional.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center text-underline">
                    <h4>Form Biodata</h4>
                </div>
            </div>
            <div class="row" style="margin-top: 10px;">
                <div class="col-sm-6">
                    <form id="myForm">
                        <div class="form-group row">
                            <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" id="alamat" name="alamat" placeholder="Alamat"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                            <div class="col-sm-9">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" id="jkl" name="jenis_kelamin" value="L" />
                                    <label class="form-check-label" for="jkl">
                                        Laki-laki
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" id="jkp" name="jenis_kelamin" value="P" />
                                    <label class="form-check-label" for="jkp">
                                        Perempuan
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="no_telp" class="col-sm-3 col-form-label">No. Telp</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="no_telp" name="no_telp" placeholder="No. Telp" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="agama" class="col-sm-3 col-form-label">Agama</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="agama" name="agama">
                                    <option value="">--- Pilih Data ---</option>
                                    <?php
                                        if ($agama['status'] != 'failed' && count($agama['data']) > 0) {
                                            foreach($agama['data'] as $value) {
                                                echo '<option value="' . strtolower($value['nama']) . '">' . $value['nama'] . '</option>';
                                            }
                                        }
                                    ?>
                                    <option value="lainnya">Lainnya</option>
                                </select>
                                <input class="form-control" id="agama_lain" name="agama_lain" placeholder="Agama Lainnya" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="email" name="email" placeholder="Email" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                <input type="button" id="btn-simpan" class="btn btn-sm btn-primary" value="Simpan" />
                                <input type="reset" id="btn-reset" class="btn btn-sm btn-danger" value="Reset" />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-6">
                    <table id="table-result" class="table table-bordered table-condensed table-striped">
                        <thead>
                            <th width="10" class="text-center">No.</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th width="10" class="text-center">Jenis Kelamin</th>
                            <th>No. Telp</th>
                            <th>Agama</th>
                            <th>Email</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
<script>
$(function(){
    loadData();

    $('#btn-reset').click(function() {
        resetForm();
    });

    $('#btn-simpan').click(function() {
        let valid = validation();
        if (valid > 0) {
            alert('Form Belum Lengkap.');
            return false;
        }
        process();
    });

    $('#agama').change(function() {
        $('#agama_lain').val('');
        $display = ($(this).val() == 'lainnya' ? 'block' : 'none');
        $('#agama_lain').css('display', $display);
    });
});

function resetForm() {
    $('#agama_lain').css('display', 'none');
    $('#myForm').trigger('reset');
}

function loadData() {
    $.get('<?php echo base_url(); ?>biodata/get_biodata', function(response) {
        if (response.status !== 'failed') {
            let rows = [];
            response.data.map((v, i) => {
                rows.push(
                    '<tr>' +
                        '<td class="text-center">' + (i + 1) + '</td>' +
                        '<td>' + v.nama + '</td>' +
                        '<td>' + nl2br(v.alamat) + '</td>' +
                        '<td class="text-center">' + v.jk + '</td>' +
                        '<td>' + v.no_telp + '</td>' +
                        '<td>' + v.agama + '</td>' +
                        '<td>' + v.email + '</td>' +
                    '</tr>'
                );
            });
            $('#table-result > tbody').html(rows.join(''));
        }
    }, 'JSON');
}

function checkDataKosong(value) {
    return $.trim(value) == '' ? 1 : 0;
}

function validation() {
    let status = [],
        count = 0;

    status.push(checkDataKosong($('#nama').val()));
    status.push(checkDataKosong($('#alamat').val()));
    status.push(checkDataKosong($('input[name="jenis_kelamin"]:checked').val()));
    status.push(checkDataKosong($('#no_telp').val()));
    status.push(checkDataKosong($('#agama').val()));
    if ($.trim($('#agama').val()) == 'lainnya') {
        status.push(checkDataKosong($('#agama_lain').val()));
    }
    status.push(checkDataKosong($('#email').val()));

    status.map(v => { if (v == 1) { count += 1; } });
    
    return count;
}

function process() {
    $.ajax({
        method: 'POST',
        url: '<?php echo base_url(); ?>biodata/insert',
        data: $('#myForm').serialize(),
        dataType: 'JSON',
        success: function(response) {
            if (response.status === 'failed') {
                alert('Error: ' + response.error.message);
            } else {
                loadData();
                resetForm();
            }
        }
    });

    // $.post('<?php echo base_url(); ?>biodata/insert', $('#myForm').serialize(), function(response) {
    //     if (response.status === 'failed') {
    //         alert('Error: ' + response.error.message);
    //     } else {
    //         loadData();
    //         resetForm();
    //     }
    // }, 'JSON');
}
</script>