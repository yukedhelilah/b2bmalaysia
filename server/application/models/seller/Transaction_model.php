<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_model extends CI_Model
{   
    function add_reservation($data){
        $this->db->insert('reservation', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function add_reservation_details($data){
        $this->db->insert('reservation_details', $data);
        if($this->db->affected_rows()){
            return true;
        }else{
            return false;
        }
    }
}