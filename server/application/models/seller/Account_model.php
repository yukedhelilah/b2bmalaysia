<?php 
defined('BASEPATH') or exit('No direct script access allowed'); 

class Account_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    public function get_company_profile($agentID){
        $this->db->select('*')
                ->from('seller')
                ->where('id', $agentID);
        
        $query = $this->db->get();

        if (!$query) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query->num_rows() > 0) {
                $this->data = $query->row_array();
            }
        }

        return [
            'status' => $this->status, 
            'data' => $this->data,
            'error' => $this->error
        ];
    }

    public function get_staff_profile($staffID){
        $this->db->select('*')
                ->from('seller_staff')
                ->where('id', $staffID);
        
        $query = $this->db->get();

        if (!$query) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query->num_rows() > 0) {
                $this->data = $query->row_array();
            }
        }

        return [
            'status' => $this->status, 
            'data' => $this->data,
            'error' => $this->error
        ];
    }

    public function update_company_profile($agent){
        $object = array(
            'sellerEmail'       => strtolower($this->input->post('emailAddress')),
            'companyName'       => strtoupper($this->input->post('companyName')),
            'companyAddress'    => strtoupper($this->input->post('companyAddress')),
            'companyCity'       => strtoupper($this->input->post('companyCity')),
            'companyCountry'    => strtoupper($this->input->post('companyCountry')),
            'companyZipcode'    => strtoupper($this->input->post('companyZipcode')),
            'companyPhone'      => strtoupper($this->input->post('companyPhone')),
            'companyFax'        => strtoupper($this->input->post('companyFax')),
            'companyEmail'      => strtolower($this->input->post('companyEmail')),
            'companyLicence'    => strtolower($this->input->post('companyLicence')),
            'contactName'       => strtoupper($this->input->post('contactName')),
            'contactPosition'   => strtoupper($this->input->post('contactPosition')),
            'contactEmail'      => strtolower($this->input->post('contactEmail')),
            'contactMobile'     => strtoupper($this->input->post('contactMobile')),
            'contactWhatsapp'   => strtoupper($this->input->post('contactWhatsapp')),
        );
        // print_r($object);
        // exit();
        return $this->db->where('id', $agent)->update('seller', $object);
    }

    public function update_staff_profile($staff){
        if($this->input->post('password') == ""){
            $object = array(
                'staffName'     => strtolower($this->input->post('staffName')),
                'staffUsername' => strtoupper($this->input->post('staffUsername')),
            );
        } 
        else {
            $object = array(
                'staffName'     => strtolower($this->input->post('staffName')),
                'staffUsername' => strtoupper($this->input->post('staffUsername')),
                'staffPassword' => md5($this->input->post('password')),
            );
        }
        // print_r($object);
        // exit();
        return $this->db->where('id', $staff)->update('seller_staff', $object);
    }

    public function get_staff($agentID){
        $this->db->select('*')
                ->from('seller_staff')
                ->where('sellerID', $agentID);
        
        $query = $this->db->get();

        if (!$query) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query->num_rows() > 0) {
                $this->data = $query->result_array();
            }
        }

        return [
            'status' => $this->status, 
            'data' => $this->data,
            'error' => $this->error
        ];
    }

    public function get_staff_id($id){
        $this->db->select('*')
            ->from('seller_staff')
            ->where('id', $id);
            
        $query = $this->db->get();

        if(!$query){
            $this->status = '500';
            $this->error = $this->db->error();
        }
        else{
            if($query->num_rows() > 0){
                $this->data = $query->row_array();
            }
        }

        return[
            'status'    => $this->status,
            'data'      => $this->data,
            'error'     => $this->error
        ];
    }

    public function save_staff($agentID){
        if ($this->input->post('staffUsername') != 'superid') {
            $object = array(
                'sellerID'      => $agentID,
                'staffName'     => strtolower($this->input->post('staffName')),
                'staffUsername' => strtoupper($this->input->post('staffUsername')),
                'staffPassword' => md5(123456),
            );
        }
        else{
            return[
                'status'    => 500,
                'data'      => $this->data,
                'error'     => $this->error
            ];
        }

        return $this->db->insert('seller_staff', $object);
    }

    public function reset_staff($agentID){
        $object = array(
            'sellerID'      => $agentID,
            'staffName'     => strtolower($this->input->post('Name')),
            'staffUsername' => strtoupper($this->input->post('Username')),
            'staffPassword' => md5(123456),
        );
        return $this->db->where('id', $this->input->post('idStaff'))->update('seller_staff', $object);
    }

    public function delete_staff($id){
        $this->db->from('seller_staff')->where('seller_staff.id', $id)->delete('seller_staff');

        return [
            'status' => $this->status, 
            'data' => $this->data,
            'error' => $this->error
        ];
    }
}