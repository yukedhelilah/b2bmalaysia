<?php 
defined('BASEPATH') or exit('No direct script access allowed'); 

class Reservation_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];
    private $sub = '';

    public function get_reservation(){
        $select = array(
                        'reservation.id',
                        'reservation.bookingCode',
                        'reservation.bookingDate',
                        'reservation.bookingTime',
                        'fne_product.productName',
                        'reservation.contactName',
                        'reservation.noofpax',
                        'reservation.isStatus'
        );

        $this->db->select($select)
                        ->from('reservation')
                        ->join('fne_product','fne_product.id=reservation.productID','left')
                        ->group_by('reservation.id');
        
        $query = $this->db->get();

        if (!$query) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query->num_rows() > 0) {
                $this->data = $query->result_array();
            }
        }

        return [
            'status' => $this->status, 
            'data' => $this->data,
            'error' => $this->error
        ];
    }

    public function set_status($data){
        $object = array(
            'isStatus'      => $this->input->post('status'),
        );
        $this->db->where('id', $this->input->post('idStatus'))->update('reservation', $object);
        
        return [
            'status' => $this->status, 
            'data' => $this->data,
            'error' => $this->error
        ];
    }

    public function get_details($id){
        $select = array(
                        'a.id',
                        'a.bookingCode',
                        'a.bookingDate',
                        'c.categoryName',
                        'b.productName',
                        'b.noofdays',
                        'b.noofnights',
                        'a.inDate',
                        'e.settingName',
                        'a.contactName',
                        'a.contactMobile',
                        'a.contactEmail',

        );
        $this->db->select($select)
                        ->from('reservation a')
                        ->join('fne_product b','a.productID=b.id', 'left')
                        ->join('fne_category c','b.categoryID=c.id','left')
                        ->join('fne_room d','d.productID=a.productID','left')
                        ->join('ms_room_setting e','d.roomID=e.id','left')
                        ->where('a.id',$id);
        
        $query = $this->db->get();

        if (!$query) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query->num_rows() > 0) {
                $this->data = $query->row_array();
            }
        }

        $select2 = array(
                        'gFname',
                        'gPassport',
                        'gExpired',
                        'gDob',
                        'gCategory',
                        'OpriceNett'
                    );
        $this->db->select($select2)
                        ->from('reservation_details')
                        ->where('reservationID',$id);
        
        $query2 = $this->db->get();

        if (!$query2) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query2->num_rows() > 0) {
                $this->data['details'] = $query2->result();
            }
        }

        return [
            'status' => $this->status, 
            'data' => $this->data,
            'details' => $this->sub_data,
            'error' => $this->error
        ];
    }

    public function get_departure($id){
        $select = array(
                        'a.reservationID',
                        'a.id as departureID',
                        'b.id',
                        'a.arrivalDate',
                        'a.arrivalDetail',
                        'a.arrivalLocation',
                        'b.name',
                        'b.contact',
                        'b.pickupDetails'
                    );

        $this->db->select($select)
                ->from('reservation_departure_details a')
                ->join('pickup b', 'b.departureID=a.id', 'left')
                ->join('reservation c', 'c.id=a.reservationID','left')
            	->where('a.reservationID', $id);
                        // ->join('fne_product','fne_product.id=reservation.productID','left')
                        // ->group_by('reservation.id');
        
        $query = $this->db->get();

        if (!$query) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query->num_rows() > 0) {
                $this->data = $query->result();
            }
        }

        return [
            'status'    => $this->status, 
            'data'      => $this->data,
            'error'     => $this->error
        ];
    }

    public function get_pickup($id){
        $this->db->select('pickup.id,name,contact,pickupDetails')
                ->from('pickup')
                ->where('pickup.id', $id);
                        // ->join('fne_product','fne_product.id=reservation.productID','left')
                        // ->group_by('reservation.id');
        
        $query = $this->db->get();

        if (!$query) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query->num_rows() > 0) {
                $this->data = $query->row_array();
            }
        }

        return [
            'status' => $this->status, 
            'data' => $this->data,
            'error' => $this->error
        ];
    }

    public function set_pickup($data){
        $object = array(
            'departureID'   => $this->input->post('depID'),
            'name'          => $this->input->post('pickup_name'),
            'contact'       => $this->input->post('pickup_number'),
            'pickupDetails' => $this->input->post('pickup_details'),
        );
        $this->db->insert('pickup', $object);

        $id = $this->input->post('depID');
        $this->db->select('reservationID')
                ->from('pickup')
                ->join('reservation_departure_details','reservation_departure_details.id=pickup.departureID')
                ->where('departureID', $id);

        $query = $this->db->get();

        if (!$query) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query->num_rows() > 0) {
                $this->sub = $query->row_array();
            }
        }

        return [
            'status'    => $this->status, 
            'sub' => $this->sub,
            'error' => $this->error
        ]; 
    }

    public function update_pickup($data){
        $object = array(
            'departureID'   => $this->input->post('depID'),
            'name'          => $this->input->post('pickup_name'),
            'contact'       => $this->input->post('pickup_number'),
            'pickupDetails' => $this->input->post('pickup_details'),
        );
        $this->db->where('id', $this->input->post('pickID'))->update('pickup', $object);

        $id = $this->input->post('depID');
        $this->db->select('reservationID')
                ->from('pickup')
                ->join('reservation_departure_details','reservation_departure_details.id=pickup.departureID')
                ->where('departureID', $id);

        $query = $this->db->get();

        if (!$query) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query->num_rows() > 0) {
                $this->sub = $query->row_array();
            }
        }

        return [
            'status' => $this->status, 
            'sub' => $this->sub,
            'error' => $this->error
        ]; 
    }

}