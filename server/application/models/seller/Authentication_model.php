<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Authentication_model extends CI_Model
{   
    function cek_email($email){
        $this->db->select('*')
            ->from('seller a')
            ->where('a.sellerID', $email);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }

        $arr = array();
        if ($query->num_rows()) {
            $row = $query->row_array();
            $arr = [
                'total'  => $query->num_rows(),
                'id'     => $row['id']
            ];
        }else{
            $arr = [
                'total'  => $query->num_rows(),
                'id' => 0
            ];
        }

        return $arr;
    }

    function cek_akun($email, $username, $password){
        $this->db->select('a.id as sellerID, b.id as staffID, a.companyName, b.staffName, b.staffUsername, b.staffPassword')
            //->select('IF(MD5(b.staffPassword)="e10adc3949ba59abbe56e057f20f883e","0","1") as staffPassword', false)
            ->from('seller a')
            ->join('seller_staff b', 'b.sellerID=a.id')
            ->where('a.sellerID', $email)
            ->where('b.staffUsername', $username)
            ->where('b.staffPassword', $password)
            ->where('a.isApproved',1)
            ->where('a.flag', 0)
            ->where('b.flag', 0);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }

        $arr = array();
        if ($query->num_rows()) {
            $row = $query->row_array();
            $tokenKey = $row['sellerID'] . '|' . $row['companyName'] . '|' . date('Y-m-d H:i:s');
            $arr = [
                'total'         => $query->num_rows(),
                'userCode'      => $row['sellerID'],
                'staffCode'     => $row['staffID'],
                'companyName'   => strtoupper($row['companyName']),
                'staffName'     => strtoupper($row['staffName']),
                'staffUsername' => strtoupper($row['staffUsername']),
                'staffPassword' => $row['staffPassword'] == 'e10adc3949ba59abbe56e057f20f883e' ? 0 : 1,
                'token'         => md5($tokenKey),
                'expired'       => date("Y-m-d", strtotime("+1 day")),
            ];
        }else{
            $arr = [
                'total'     => $query->num_rows(),
                'userCode'  => ''
            ];
        }

        return $arr;
    }

    // function cek_username(){
    //     $uname = $this->db->select('*')
    //         ->from('seller a')
    //         ->where('a.sellerUsername')

    //     $query = $this->db->get();
    //     return $query;
    // }

    function cek_username($username, $email){
        return $this->db->where('sellerUsername','superid')->where('sellerEmail','yukedhelilah@gmail.com')->get('seller')->row();
    }

    function add_seller($data){
        $this->db->insert('seller', $data);
        if($this->db->affected_rows()){
            return true;
        }else{
            return false;
        }
    }    
}