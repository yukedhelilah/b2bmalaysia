<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Utils_model extends CI_Model
{   
    public function token($token)
    {
        $this->db->select('*')
            ->from('buyer')
            ->where('MD5(userID)', $token)
            ->where('flag', 0);
        
        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        $arr = array();
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            $arr['status']  = 'true';
            $arr['id']      = $row['id'];
        }else{
            $arr['status']  = 'false';
        }
        
        return $arr;
    }

    public function kurs()
    {
        $this->db->select('kursDate, currency, amount')
            ->from('kurs')
            ->order_by('kursDate','desc')
            ->limit(5);
        
        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        $arr = array();
        if ($query->num_rows()>0) {
            $row = $query->result();
            foreach ($row as $key => $value) {
                $dy = $value->kursDate == date('Y-m-d') ? 'Today' : date('d M Y', strtotime($value->kursDate));
                array_push($arr, [
                    'kursDate'  => $dy,
                    'currency'  => $value->currency,
                    'amountReal'=> $value->amount,
                    'amount'    => number_format($value->amount,2),
                ]);
            }
        }

        return $arr;
    }

    public function reservation($id)
    {
        $this->db->select('a.*, b.productName, b.noofdays, b.noofnights, c.categoryName')
            ->from('reservation a')
            ->join('fne_product b','a.productID=b.id')
            ->join('fne_category c','b.categoryID=c.id')
            ->where('a.agentID', $id)
            ->order_by('a.bookingDate, a.bookingTime','desc')
            ->limit(5);
        
        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        $arr = array();
        if ($query->num_rows()>0) {
            $row = $query->result();
            foreach ($row as $key => $value) {
                if($value->isStatus == 0){
                    $status = '<span class="tx-11 d-block"><span class="square-8 bg-warning mg-r-5 rounded-circle"></span> Waiting list</span>';
                }else if($value->isStatus == 1){
                    $status = '<span class="tx-11 d-block"><span class="square-8 bg-primary mg-r-5 rounded-circle"></span> Confirm</span>';
                }else if($value->isStatus == 2){
                    $status = '<span class="tx-11 d-block"><span class="square-8 bg-success mg-r-5 rounded-circle"></span> Guarantee</span>';
                }else if($value->isStatus == 3){
                    $status = '<span class="tx-11 d-block"><span class="square-8 bg-info mg-r-5 rounded-circle"></span> Complete</span>';
                }else if($value->isStatus == 4){
                    $status = '<span class="tx-11 d-block"><span class="square-8 bg-danger mg-r-5 rounded-circle"></span> Waiting list</span>';
                }

                array_push($arr, [
                    'bookingCode'  => $value->bookingCode,
                    'bookingDate'  => date('d M Y', strtotime($value->bookingDate)),
                    'bookingTime'  => $value->bookingTime,
                    'productName'  => $value->productName,
                    'categoryName' => $value->categoryName,
                    'inDate'       => date('d M Y', strtotime($value->inDate)),
                    'noofpax'      => $value->noofpax,
                    'noofdays'     => $value->noofdays,
                    'noofnights'   => $value->noofnights,
                    'contactName'  => $value->contactName,
                    'contactMobile'=> $value->contactMobile,
                    'status'       => $status,
                ]);
            }
        }

        return $arr;
    }
}