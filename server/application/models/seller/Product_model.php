<?php 
defined('BASEPATH') or exit('No direct script access allowed'); 

class Product_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];
    private $sub = '';

    public function view_data_location(){
        $data['location'] = $this->get_location();
        return $data;
    }

    public function get_location(){
        $this->db->select('id, location')
            ->from('ms_location');
    
        $query = $this->db->get();

        if (!$query) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query->num_rows() > 0) {
                $this->data = $query->result_array();
            }
        }

        return [
            'status' => $this->status, 
            'data' => $this->data,
            'error' => $this->error
        ];
    }

    public function view_data_tour(){
        $data['tour'] = $this->get_tour();
        return $data;
    }

    public function get_tour(){
        $this->db->select('id, product_category_name')
            ->from('ms_product_category');
    
        $query = $this->db->get();

        if (!$query) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query->num_rows() > 0) {
                $this->data = $query->result_array();
            }
        }

        return [
            'status' => $this->status, 
            'data' => $this->data,
            'error' => $this->error
        ];
    }

    public function get_category(){
        $select = array(
                        'fne_category.id',
                        'fne_category.categoryName',
                        'fne_category.image',
                        'count(fne_product.id) as total'
        );

        $this->db->select($select)
                        ->from('fne_category')
                        ->join('fne_product','fne_product.categoryID=fne_category.id','left')
                        ->group_by('fne_category.id');
        
        $query = $this->db->get();

        if (!$query) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query->num_rows() > 0) {
                $this->data = $query->result_array();
            }
        }

        return [
            'status' => $this->status, 
            'data' => $this->data,
            'error' => $this->error
        ];
    }

    public function get_category_id($id){
        $select = array(
                        'a.id',
                        'a.categoryName',
                        'a.locationID',
                        'a.productCategoryID',
                        'a.image',
                        'a.include',
                        'a.exclude',
                        'a.categoryPublish'
        );

        $this->db->select($select)
            ->from('fne_category a')
            ->where('id', $id);
            
        $query = $this->db->get();

        if(!$query){
            $this->status = '500';
            $this->error = $this->db->error();
        }
        else{
            if($query->num_rows() > 0){
                $this->data = $query->row_array();
            }
        }

        return[
            'status'    => $this->status,
            'data'      => $this->data,
            'error'     => $this->error
        ];
    }

    public function get_product($id){
        $select = array(
                        'a.id',
                        'a.productName',
                        'a.noofdays',
                        'a.noofnights',
                        'a.highlight',
                        'a.description'
        );

        $this->db->select($select)
            ->from('fne_product a')
            ->where('categoryID', $id);
        
        $query = $this->db->get();

        if (!$query) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query->num_rows() > 0) {
                $this->data = $query->result_array();
            }
        }

        $select = array(
                        'a.id',
                        'a.categoryName',
                        'b.location',
                        'c.product_category_name',
                        'a.include',
                        'a.exclude',
                        'a.image'
        );

        $this->db->select($select)
            ->from('fne_category a')
            ->join('ms_location b','b.id=a.locationID', 'left')
            ->join('ms_product_category c','c.id=a.productCategoryID','left')
            ->where('a.id', $id);
        
        $query2 = $this->db->get();

        if (!$query2) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query->num_rows() >= 0) {
                $this->sub = $query2->row_array();
            }
        }

        return [
            'status' => $this->status, 
            'sub' => $this->sub,
            'data' => $this->data,
            'error' => $this->error,
        ];
    }

    public function get_product_id($id){
        $select = array(
                        'a.id',
                        'a.categoryID',
                        'b.categoryName',
                        'a.productName',
                        'a.highlight',
                        'a.description',
                        'a.noofdays',
                        'a.noofnights',
                        'a.validDays',
                        'c.validFrom',
                        'c.validTo',
                        'c.cutDays',
                        'c.allotment',
                        'a.timeLimitGuarantee',
                        'a.productPublish',
                        'a.isConfirm',
                        'a.timeLimitConfirm',
                        'c.pSingle',
                        'c.pAdultTwin',
                        'c.pAdultTriple',
                        'c.pCwa',
                        'c.pCwb',
                        'c.pCnb'
        );

        $this->db->select($select);
        $this->db->from('fne_product a')->where('a.id', $id);
        $this->db->join('fne_category b','b.id=a.categoryID', 'left');
        $this->db->join('fne_product_pricing c', 'c.productID=a.id', 'left');

        $query = $this->db->get();

        if (!$query) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query->num_rows() > 0) {
                $this->data = $query->row_array();
            }
        }

        $this->db->select('boDate')->from('fne_bod')->where('productID', $id);
        $query2 = $this->db->get();
        if ($query2->num_rows() > 0) {
            $this->data['bod'] = $query2->result();
        }

        $select2 = array(
                        'a.roomID',
                        'b.settingName'
        );

        $this->db->select($select2)
                ->from('fne_room a')->where('productID', $id)
                ->join('ms_room_setting b','b.id=a.roomID','left');
        $query3 = $this->db->get();
        if ($query3->num_rows() > 0) {
            $this->data['room'] = $query3->result();
        }

        return [
            'bod'       => $this->sub_data,
            'room'      => $this->sub_data,
            'status'    => $this->status, 
            'data'      => $this->data,
            'error'     => $this->error
        ];
    }

    public function insert_category($data){
        $object = array(
            'categoryName'      => $this->input->post('categoryName'),
            'locationID'        => $this->input->post('locations'),
            'productCategoryID' => $this->input->post('tours'),
            'image'             => $this->input->post('imageName'),
            'include'           => $this->input->post('include'),
            'exclude'           => $this->input->post('exclude'),
            'categoryPublish'   => $this->input->post('publish'),
            'agentID'           => $this->input->post('agentID'),
            'createdDate'       => date('Y-m-d H:i:s'),
            'createdBy'         => $this->input->post('agentID')
        );
        $this->db->insert('fne_category', $object);
        $id = $this->db->select('id')->order_by('id','desc')->limit(1)->get('fne_category')->row('id');
        return $id;
    }

    public function update_category($data){
        $object = array(
            'categoryName'      => $this->input->post('categoryName'),
            'locationID'        => $this->input->post('locations'),
            'productCategoryID' => $this->input->post('tours'),
            'image'             => $this->input->post('imageName'),
            'include'           => $this->input->post('include'),
            'exclude'           => $this->input->post('exclude'),
            'categoryPublish'   => $this->input->post('publish'),
            'agentID'           => $this->input->post('agentID'),
            'lastDate'          => date('Y-m-d H:i:s'),
            'lastBy'            => $this->input->post('agentID')
        );
        $this->db->where('id', $this->input->post('id'))->update('fne_category', $object);
        return $this->input->post('id');
    }

    public function delete_category($id){
        $this->db->from('fne_category')->where('fne_category.id', $id)->delete('fne_category');
        $this->db->from('fne_product')->where('fne_product.categoryID', $id)->delete('fne_product');

        return [
            'status' => $this->status, 
            'data' => $this->data,
            'error' => $this->error
        ];
    }

    public function insert_product($data){
        $id = $this->db->select('id')->order_by('id','desc')->limit(1)->get('fne_product')->row('id');
        $check = $this->input->post('validdays');
        $days  = implode(",", $check);
        if ($this->input->post('autoconfirm') == 0) {
            $object = array(
                'id'                => $id+1,
                'categoryID'        => $this->input->post('idCat'),
                'productName'       => $this->input->post('productName'),
                'highlight'         => $this->input->post('highlight'),
                'description'       => $this->input->post('description'),
                'noofdays'          => $this->input->post('noofdays'),
                'noofnights'        => $this->input->post('noofnights'),
                'validDays'         => $days,
                'timeLimitGuarantee'=> $this->input->post('limitguarantee'),
                'productPublish'    => $this->input->post('productpublish'),
                'isConfirm'         => $this->input->post('autoconfirm'),
                'timeLimitConfirm'  => 0,
                'agentID'           => $this->input->post('agentID'),
                'createdDate'       => date('Y-m-d H:i:s'),
                'createdBy'         => $this->input->post('agentID')
            );
            $this->db->insert('fne_product', $object);
        } else {
            $object = array(
                'id'                => $id+1,
                'categoryID'        => $this->input->post('idCat'),
                'productName'       => $this->input->post('productName'),
                'highlight'         => $this->input->post('highlight'),
                'description'       => $this->input->post('description'),
                'noofdays'          => $this->input->post('noofdays'),
                'noofnights'        => $this->input->post('noofnights'),
                'validDays'         => $days,
                'timeLimitGuarantee'=> $this->input->post('limitguarantee'),
                'productPublish'    => $this->input->post('productpublish'),
                'isConfirm'         => $this->input->post('autoconfirm'),
                'timeLimitConfirm'  => $this->input->post('limitconfirm'),
                'agentID'           => $this->input->post('agentID'),
                'createdDate'       => date('Y-m-d H:i:s'),
                'createdBy'         => $this->input->post('agentID')
            );
            $this->db->insert('fne_product', $object);
        }

        $cek = $this->input->post('blackoutdate')[0];
        $date  = explode(",", $cek);
        for($i = 0; $i < count($date); $i++){
            if($date[$i]!=''){
                if($date[$i] >= $this->input->post('validfrom') && $date[$i] <= $this->input->post('validuntil')){
                    $item = array(
                        'productID'     => $id+1,
                        'boDate'        => $date[$i]
                    );
                    $this->db->insert('fne_bod', $item);  
                }              
            }
        }

        if($this->input->post('single') >= 0 && $this->input->post('adulttwin') >= 0 && $this->input->post('adulttriple') >= 0 && $this->input->post('childwithadult') >= 0 && $this->input->post('childwithbed') >= 0 && $this->input->post('childnobed') >= 0){
            $obj = array(
                'id'            => $id+1,
                'productID'     => $id+1,
                'validFrom'     => $this->input->post('validfrom'),
                'validTo'       => $this->input->post('validuntil'),
                'cutDays'       => $this->input->post('cutofdays'),
                'allotment'     => $this->input->post('allotment'),
                'pSingle'       => $this->input->post('single'),
                'pAdultTwin'    => $this->input->post('adulttwin'),
                'pAdultTriple'  => $this->input->post('adulttriple'),
                'pCwa'          => $this->input->post('childwithadult'),
                'pCwb'          => $this->input->post('childwithbed'),
                'pCnb'          => $this->input->post('childnobed'),
            );
        }
        $this->db->insert('fne_product_pricing', $obj);
        
        // $rset = $this->input->post('fneroom');
        // for ($i=0; $i < count($rset) ; $i++) {
        //     $room = array(
        //         'productID'     => $id,
        //         'roomID'        => $rset[$i],
        //     );
        //     $this->db->insert('fne_room', $room);
        // }

        return $this->input->post('idCat');
    }

    public function update_product($data){
        $check = $this->input->post('validdays');
        $days  = implode(",", $check);
        if ($this->input->post('autoconfirm') == 0) {
            $object = array(
                'categoryID'        => $this->input->post('idCat'),
                'productName'       => $this->input->post('productName'),
                'highlight'         => $this->input->post('highlight'),
                'description'       => $this->input->post('description'),
                'noofdays'          => $this->input->post('noofdays'),
                'noofnights'        => $this->input->post('noofnights'),
                'validDays'         => $days,
                'timeLimitGuarantee'=> $this->input->post('limitguarantee'),
                'productPublish'    => $this->input->post('productpublish'),
                'isConfirm'         => $this->input->post('autoconfirm'),
                'timeLimitConfirm'  => 0,
                'agentID'           => $this->input->post('agentID'),
                'createdDate'       => date('Y-m-d H:i:s'),
                'createdBy'         => $this->input->post('agentID')
            );
            $this->db->where('id', $this->input->post('id-product'))->update('fne_product', $object);
        } else {
            $object = array(
                'categoryID'        => $this->input->post('idCat'),
                'productName'       => $this->input->post('productName'),
                'highlight'         => $this->input->post('highlight'),
                'description'       => $this->input->post('description'),
                'noofdays'          => $this->input->post('noofdays'),
                'noofnights'        => $this->input->post('noofnights'),
                'validDays'         => $days,
                'timeLimitGuarantee'=> $this->input->post('limitguarantee'),
                'productPublish'    => $this->input->post('productpublish'),
                'isConfirm'         => $this->input->post('autoconfirm'),
                'timeLimitConfirm'  => $this->input->post('limitconfirm'),
                'agentID'           => $this->input->post('agentID'),
                'createdDate'       => date('Y-m-d H:i:s'),
                'createdBy'         => $this->input->post('agentID')
            );
            $this->db->where('id', $this->input->post('id-product'))->update('fne_product', $object);
        }

        $cek = $this->input->post('blackoutdate')[0];
        $date  = explode(",", $cek);
        $bod = $this->db->select('id')->where('productID', $this->input->post('id-product'))->get('fne_bod')->result();
            for($i = 0; $i < count($date); $i++){
                if($date[$i]!=''){
                    if($date[$i] >= $this->input->post('validfrom') && $date[$i] <= $this->input->post('validuntil')){
                        if(count($bod) == 0){
                            $item = array(
                                'productID'     => $this->input->post('id-product'),
                                'boDate'        => $date[$i]
                            );
                            $this->db->insert('fne_bod', $item);  
                        }
                        elseif (count($bod) == count($date)){
                            $item = array(
                                'id'            => $bod[$i]->id,
                                'productID'     => $this->input->post('id-product'),
                                'boDate'        => $date[$i]
                            );
                            $this->db->where('id', $bod[$i]->id)->update('fne_bod', $item); 
                        }
                    }              
                }
            }
        
        if($this->input->post('single') >= 0 && $this->input->post('adulttwin') >= 0 && $this->input->post('adulttriple') >= 0 && $this->input->post('childwithadult') >= 0 && $this->input->post('childwithbed') >= 0 && $this->input->post('childnobed') >= 0){
            $obj = array(
                'productID'     => $this->input->post('id-product'),
                'validFrom'     => $this->input->post('validfrom'),
                'validTo'       => $this->input->post('validuntil'),
                'cutDays'       => $this->input->post('cutofdays'),
                'allotment'     => $this->input->post('allotment'),
                'pSingle'       => $this->input->post('single'),
                'pAdultTwin'    => $this->input->post('adulttwin'),
                'pAdultTriple'  => $this->input->post('adulttriple'),
                'pCwa'          => $this->input->post('childwithadult'),
                'pCwb'          => $this->input->post('childwithbed'),
                'pCnb'          => $this->input->post('childnobed'),
            );
        }
        $this->db->where('id', $this->input->post('id-product'))->update('fne_product_pricing', $obj);

        // $rset = $this->input->post('fneroom');
        // for ($i=0; $i < count($rset) ; $i++) {
        //     $room = array(
        //         'productID'     => $this->input->post('id-product'),
        //         'roomID'        => $rset[$i],
        //     );
        //     $this->db->where('id', $this->input->post('id-product'))->update('fne_room', $room);
        // }

        return $this->input->post('idCat');
    }

    public function delete_product($id){
        $this->db->from('fne_product')->where('fne_product.id', $id)->delete('fne_product');
        $this->db->from('fne_product_pricing')->where('fne_product_pricing.productID', $id)->delete('fne_product_pricing');
        $this->db->from('fne_bod')->where('fne_bod.productID', $id)->delete('fne_bod');
        $this->db->from('fne_room')->where('fne_room.productID', $id)->delete('fne_room');

        return [
            'status' => $this->status, 
            'data' => $this->data,
            'error' => $this->error
        ];
    }

    public function save_room(){
        if ($this->input->post('action-product') == "add-product") {
            $id = $this->db->select('id')->order_by('id','desc')->limit(1)->get('fne_product')->row('id');
            $save = array(
              'productID'   => $id+1,
              'roomID'      => $this->input->post('labCheckded')
            );
            $this->db->insert('fne_room', $save);
        } else if($this->input->post('action-product') == "edit-product"){  
            $save = array(
              'productID'   => $this->input->post('id-product'),
              'roomID'      => $this->input->post('labCheckded')
            );
            $this->db->insert('fne_room', $save);
        }
    }

    public function delete_room(){
        // $delete = array(
        //   'roomID'  => $this->input->post('labCheckded')
        // );
        // print_r($delete);
        // exit();
        $this->db->where('roomID', $this->input->post('labCheckded')); 
        $this->db->delete('fne_room');
    }

    public function save_bod(){
        $save = array(
          'productID'   => $this->input->post('id-product'),
          'boDate'      => date('Y-m-d', $this->input->post('labCheckded'))
        );
        print_r($save);
        $this->db->insert('fne_room', $save);
    }

    public function delete_bod(){
        $delete = array(
          'boDate'  => date('Y-m-d', $this->input->post('labCheckded'))
        );
        print_r($delete);
        exit();
        $this->db->where('boDate', $this->input->post('labCheckded')); 
        $this->db->delete('fne_room');
    }
}