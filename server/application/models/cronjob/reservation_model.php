<?php 
defined('BASEPATH') or exit('No direct script access allowed'); 

class Reservation_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];
    private $sub = '';

    public function get_reservation(){
        $select = array(
                        'reservation.id as reservationID',
                        'reservation.bookingCode',
                        'reservation.bookingDate',
                        'reservation.bookingTime',
                        'reservation.contactName',
                        'reservation.isStatus'
        );

        $this->db->select($select)
                        ->from('reservation')
                        ->join('fne_product','fne_product.id=reservation.productID','left')
                        ->group_by('reservation.id');
        
        $query = $this->db->get();
        if (!$query) {
            $this->status = '500';
            $this->error = $this->db->error();
        } else {
            if ($query->num_rows() > 0) {
                $this->data = $query->result();
            }
        }

        return [
            'status' => $this->status, 
            'data' => $this->data,
            'error' => $this->error
        ];
    }

    public function set_status($id,$status){
        $object = array(
            'isStatus'      => $status,
        );
        $this->db->where('id', $id)->update('reservation', $object);
        
        return [
            'status' => $this->status, 
            'data' => $this->data,
            'error' => $this->error
        ];
    }
}