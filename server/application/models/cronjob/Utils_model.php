<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Utils_model extends CI_Model
{   
    public function add_kurs_all($data)
    {
        $this->db->insert('kurs_all', $data);
        if($this->db->affected_rows()){
            return true;
        }else{
            return false;
        }
    }

    public function add_kurs($data)
    {
        $this->db->insert('kurs', $data);
        if($this->db->affected_rows()){
            return true;
        }else{
            return false;
        }
    }
}