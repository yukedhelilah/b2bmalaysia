<?php include('../template/header.php'); ?>

<style type="text/css">
<!--
	.wrapper{
		width:70%;
	}
	@media(max-width:992px){
		.wrapper{
			width:100%;
		} 
	}
	.panel-heading {
		padding: 0;
		border:0;
	}
	.panel-title>a, .panel-title>a:active{
		display:block;
		padding:15px;
		color:#555;
		font-size:16px;
		font-weight:bold;
		text-transform:uppercase;
		letter-spacing:1px;
		word-spacing:3px;
		text-decoration:none;
	}
	.panel-heading  a:before {
		 font-family: 'Glyphicons Halflings';
		 content: "\e114";
		 float: right;
		 transition: all 0.5s;
	}
	.panel-heading.active a:before {
		-webkit-transform: rotate(180deg);
		-moz-transform: rotate(180deg);
		transform: rotate(180deg);
	}
	.custom_panel_title > a {
		padding: 10px 15px;
		font-size: 14px !important;
		text-transform: none !important;
	}
	.version-container {
		width: auto;
		height: 350px;
		overflow: auto;
	}
-->
</style>

<div class="row">
	<div class="col-xs-12">
		<h2>Home</h2>
	</div>
</div>

<div class="row" style="margin-bottom:10px;">
	<div class="col-xs-12">
		Welcome and Enjoy.
	</div>
</div>

<div class="row" style="margin-top:20px;">
	<div class="col-xs-12">
		
		<fieldset>
			<legend>Version History</legend>
			<div class="version-container">
				<div class="wrapper">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading active" role="tab" id="headingTwo">
								<h4 class="panel-title custom_panel_title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
										v.1.0.1
									</a>
								</h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body">
									<ul>
										<li>Add Version History</li>
										<li>Add Menu Google Drive Explorer</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title custom_panel_title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										v.1.0
									</a>
								</h4>
							</div>
							<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<ul>
										<li>Initial Release</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
			
<?php include('../template/footer.php'); ?>
<script type="text/javascript">
$(function(){
	$('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
  });

  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
  });
});
</script>