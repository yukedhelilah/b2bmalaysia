<?php include('../template/header.php'); ?>

<style type="text/css">
<!--
	.auth-box{
		max-width: 300px;
		border-radius: 5px;
		border:solid 1px #ccc;
		padding: 10px;
		background: #f0f9d7;
	}
	#drive-box > div {
		margin-bottom: 3px;
	}
-->
</style>

<div class="row">
	<div class="col-xs-12">
		<h2>Google Drive Explorer</h2>
		<code>Show files from shared folder</code> <br />
		<code>Note: this grab file only, any folder inside them will be skipped</code>
	</div>
</div>

<div class="row" style="margin-top:10px; margin-bottom:10px;">
	<div class="col-xs-12">
		<div id="login-box" class="auth-box hide">
			<p>Please login on your google account.</p>
			<button id="btnLogin" onClick="handleAuthClick()" class="btn btn-sm btn-primary">Login</button>
		</div>
		<div id="drive-box" class="auth-box hide">
			<div>Welcome <b><span id="span-name"></span></b></div>
			<div>Total Quota : <span id="span-totalQuota"></span></div>
			<div>Used Quota : <span id="span-usedQuota"></span></div>
			<div><button onClick="handleSignoutClick()" class="btn btn-sm btn-danger">Logout</button></div>
		</div>
	</div>
</div>

<div class="row" style="margin-bottom:10px;">
	<div class="col-xs-12">
		<form id="gdriveForm" class="hide">
			<div class="form-group" style="margin-bottom:10px;">
				<input type="text" class="form-control" id="sharedUrl" aria-describedby="sharedUrlExample" placeholder="Enter Shared URL Here" />
				<small id="sharedUrlExample" class="form-text text-muted">Example: https://drive.google.com/drive/folders/c2hhcmVkIGtleSBoZXJl</small>
			</div>
			<button id="generate" type="button" class="btn btn-sm btn-primary">Generate</button>
		</form>
	</div>
</div>

<div class="row" style="margin-bottom:10px;">
	<div id="result" class="col-xs-12"></div>
</div>

<?php include('../template/footer.php'); ?>
<script type="text/javascript" src="../assets/js/gdrive.min.js"></script>
<script async defer src="https://apis.google.com/js/api.js" 
	onload="this.onload=function(){};handleClientLoad()" 
  onreadystatechange="if (this.readyState === 'complete') this.onload()">
</script>