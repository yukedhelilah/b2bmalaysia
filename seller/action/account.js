var staffUsername = localStorage.getItem("staffUsername");
    if(staffUsername == "SUPERID"){
        $('#company_Profile').css('display','block');
        $('#staff_Profile').css('display','block');
        $('#staff').css('display','none');
    }
    else{
        $('#company_Profile').css('display','none');
        $('#staff_Profile').css('display','block');
        $('#superid').css('display','none');
    }

$(document).ready(function() {
    loadStaff();
    $('#tb_staff').dataTable(); 

    $('#btn-save-staff').click(function() {
        let valid = validation();
        if (valid > 0) {
            alert('Form Belum Lengkap.');
            return false;
        }
        save_staff();
    }); 

    $('#btn-reset-staff').click(function() {
        reset_staff();
    }); 

    $('#btn-delete').click(function(){
        delete_staff(id);
    });

    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/account/get_company_profile',
        data: $('#companyProfile').serialize()+"&agentID="+localStorage.getItem("userCode"),
        dataType: 'JSON',
        success: function(response) {
            if (response.status == '500') {
                alert("can't load");
            } else {
                $('#companyName').val(response.data.companyName);
                $('#companyAddress').val(response.data.companyAddress);
                $('#companyCountry').val(response.data.companyCountry);
                $('#companyCity').val(response.data.companyCity);
                $('#companyZipcode').val(response.data.companyZipcode);
                $('#companyPhone').val(response.data.companyPhone);
                $('#companyFax').val(response.data.companyFax);
                $('#companyEmail').val(response.data.companyEmail);
                $('#companyLicence').val(response.data.companyLicence);
                $('#contactName').val(response.data.contactName);
                $('#contactPosition').val(response.data.contactPosition);
                $('#contactMobile').val(response.data.contactMobile);
                $('#contactWhatsapp').val(response.data.contactWhatsapp);
                $('#contactEmail').val(response.data.contactEmail);
                $('#emailAddress').val(response.data.sellerEmail);
                $('#sellerID').val(response.data.sellerID);
            }
        }
    });

    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/account/get_staff_profile',
        data: $('#staffProfile').serialize()+"&staffID="+localStorage.getItem("staffCode"),
        dataType: 'JSON',
        success: function(response) {
            if (response.status == '500') {
                alert("can't load");
            } else {
                $('#staffName').val(response.data.staffName);
                $('#staffUsername').val(response.data.staffUsername);
            }
        }
    });

    $('#update_company_profile').click(function(e) {
        e.preventDefault();
        $.ajax({
            method: 'POST',
            url: 'http://localhost/b2bmalaysia/server/seller/account/update_company_profile',
            data: $('#companyProfile').serialize()+"&agent="+localStorage.getItem("userCode"),
            dataType: 'JSON',
            success: function(response) {
                if (response.status == '500') {
                    alert("failed");
                } 
                else {
                    alert("success");
                }
            }
        });
    });     

    $('#update_staff_profile').click(function(e) {
        e.preventDefault();
        if($('#password').val() == '123456') {
            alert('Password tidak boleh 123456');
        }else if($('#password').val().length > 0 && $('#password').val().length < 6) {
            alert('Password harus 6 digit');
        }else{
            $.ajax({
                method: 'POST',
                url: 'http://localhost/b2bmalaysia/server/seller/account/update_staff_profile',
                data: $('#staffProfile').serialize()+"&staff="+localStorage.getItem("staffCode"),
                dataType: 'JSON',
                success: function(response) {
                    if (response.status == '500') {
                        alert("failed");
                    }else {
                        window.location.href = "login";
                    }
                }
            });
        }
    });        
})

function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))

    return false;
    return true;
}

function loadStaff() {
    var aaData = [];
    $.ajax({
        type: 'post',
        url: 'http://localhost/b2bmalaysia/server/seller/account/get_staff',
        data: $('#tb_staff').serialize()+"&agentID="+localStorage.getItem("userCode"),
        dataType: 'json',
        cache: false,
        success: function (responses) {
            $.each(responses.data, function( index, value ) {
                var stringData = value._;
                if(value.staffUsername == 'SUPERID' || value.staffUsername == 'superid'){
                }else{
                    aaData.push([
                        (index+1),
                        value.staffName,  
                        value.staffUsername, 
                        '******', 
                        `<button class="btn btn-info btn-xs" onclick="action_edit_staff(`+ value.id +`)"><i class="fa fa-edit"></i></button>
                        <button class="btn btn-danger btn-xs" onclick="action_delete_staff(`+value.id+`)"><i class="fa fa-trash"></i></button>`,
                    ]);
                }
            });
            $('#tb_staff').DataTable().destroy();
            $("#tb_staff").dataTable({
                "responsive": true,
                "language": {
                    "search"            : '_INPUT_',
                    "searchPlaceholder" : 'Search...',
                    "sSearch"           : 'Search : ',
                    "lengthMenu"        : 'Items/page : _MENU_',
                    "sProcessing"       : '<img src="../assets/loading.gif"><span>&nbsp;&nbsp;Loading...</span>',
                },
                "aaData": aaData,
                "bJQueryUI": true,
                "aoColumns": [
                    { "sWidth": "5%" },  
                    { "sWidth": "25%" }, 
                    { "sWidth": "25%" }, 
                    { "sWidth": "35%" },
                    { "sWidth": "10%" }, 
                ],
                "aaSorting": [[0, 'desc']],
                "aLengthMenu": [
                    [10, 20, 50, -1],
                    [10, 20, 50, "All"]
                ],
            });
            $('.dataTables_length select').addClass('form-control input-sm');
            $('.dataTables_filter input').addClass('form-control input-sm');
        }
    });
}

function resetForm() {
    $('#myForm').trigger('reset');
}

function action_add_staff(){
    resetForm();
    $('#modal_add').modal('show');
    $('#staffPassword').val(123456);
}

function action_edit_staff(id){
    resetForm();
    $('#idStaff').val(id);
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/account/get_staff_id',
        data: {
            'id' : id
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                $('#Name').val(response.data.staffName);
                $('#Username').val(response.data.staffUsername);
                $('#Password').val(123456);
                $('#modal_edit').modal('show');
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function checkDataKosong(value) {
    return $.trim(value) == '' ? 1 : 0;
}

function validation() {
    let status = [],
        count = 0;

    status.push(checkDataKosong($('#staffName').val()));
    status.push(checkDataKosong($('#staffUsername').val()));
    status.push(checkDataKosong($('#staffPassword').val()));

    status.map(v => { if (v == 1) { count += 1; } });
        
    return count;
}

function save_staff() {
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/account/save_staff',
        data: $('#myForm').serialize()+"&agentID="+localStorage.getItem("userCode"),
        dataType: 'JSON',
        success: function(response) {
            if (response.status == '500') {
                alert("can't use 'superid' for username staff");
            } else {
                resetForm();
                loadStaff();
                $('#modal_add').modal('hide');
            }
        }
    });
}

function reset_staff() {
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/account/reset_staff',
        data: $('#form_staff').serialize()+"&agentID="+localStorage.getItem("userCode"),
        dataType: 'JSON',
        success: function(response) {
            if (response.status == '500') {
                alert("failed");
            } else {
                resetForm();
                loadStaff();
                $('#modal_edit').modal('hide');
            }
        }
    });
}

function action_delete_staff(id){
    $('#delete').val(id);
    $('#modal_delete').modal('show');
}
    
function delete_staff(){
    resetForm();
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/account/delete_staff',
        data: {
            'id' : $('#delete').val()
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                $('#modal_delete').modal('hide');
                loadStaff();
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}