localStorage.removeItem('token');
localStorage.removeItem('expired');
localStorage.removeItem('userCode');
localStorage.removeItem('staffCode');

$('#form_signin').trigger('reset');
$('input[name="companyEmail"]').focus();

$("#form_signin").submit(function(e){  
    e.preventDefault()       
    $.ajax({
        type: 'POST',
        //url: 'http://b2bmalaysiaholidays.com/server/seller/authentication/signin_post',
        url: 'http://localhost/b2bmalaysia/server/seller/authentication/signin_post',
        data: $("#form_signin").serialize(),
        dataType: 'JSON',
        // beforeSend: function() {
        //     var text = `<i class="fa fa-spinner fa-spin"></i> Loading...`;
        //     $('#btn_signin').html(text);
        // },
        success: function(response) {
            if (response.code == '200') {
                window.location.href = 'home';
                localStorage.setItem('userCode', response.data.userCode);
                localStorage.setItem('staffCode', response.data.staffCode);
                localStorage.setItem('companyName', response.data.companyName);
                localStorage.setItem('staffName', response.data.staffName);
                localStorage.setItem('staffUsername', response.data.staffUsername);
                localStorage.setItem('staffPassword', response.data.staffPassword);
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('expired', response.data.expired);
            }else{
                $('#txt_error_header').html(response.message);
                $('#txt_error_description').html(response.errors);
                $('#modal_error').modal('show');
            }
        },
    });
});