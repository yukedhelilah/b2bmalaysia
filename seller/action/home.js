var staffPassword = localStorage.getItem("staffPassword");
    if (staffPassword == 0) {
        $('#modal_setting_password').modal({backdrop: 'static', keyboard: false});
    }
    else{
        $('#modal_setting_password').modal('hide');
    }

$(document).ready(function() {
    loadCategory();
    loadReservation();
    masterLocation();
    masterTour();

    $('#tb_category').dataTable();   
    $('#tb_reservation').dataTable(); 
    
    $('#btn-save-pickup').click(function() {
        let valid = validation_pickup();
        if (valid > 0) {
            alert('Form Belum Lengkap.');
            return false;
        }
        save();
    }); 
    
    $('#btn-status-confirm').click(function(e) {
        e.preventDefault()
        var id = $('#idStatus').val();
        var stat = '1';
        save_status(id,stat);
    }); 
    
    $('#btn-status-complete').click(function(e) {
        e.preventDefault()
        var id = $('#idStatus').val();
        var stat = '2';
        save_status(id,stat);
    });  
    
    // $('#btn-status-cancel').click(function(e) {
    //  e.preventDefault()
    //  var stat = '3';
       //  save_status(stat);
    // });  

    $('#btn-reset').click(function() {
        reset_form();
    });

    $('#btn-save-category').click(function() {
        let valid = validation_category();
        if (valid > 0) {
            alert('Form Belum Lengkap.');
            return false;
        }
        process_category();
    });

    $('.date').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });

    $('.dates').datepicker({
        multidate: true,
        format: 'yyyy-mm-dd'
    });

    $("#userfile2").change(function () {
        uploadFile();
    });

    $('#btn-yes').click(function(){
        action_add_product(id);
    });

    $('#btn-no').click(function(){
        close();
    }); 

    $('#btn-delete').click(function(){
        action_delete(id);
    });

    $('#btn-cancel').click(function(){
        close();
    }); 

    $('#btn-save-product').click(function() {
        let valid = validation_product();
        if (valid > 0) {
            alert('Form Belum Lengkap.');
            return false;
        }
        process_product();
    });

    $('input:radio[name="autoconfirm"]').change(
        function(){
            if ($(this).is(':checked') && $(this).val() == '0') {
                $('.limitconfirm').css('display','none');
            }else{
                $('.limitconfirm').css('display','block');
            }
    });

    $('input[name="fneroom"]').change(
        function() {
            var labCheckded = $(this).val();
            if ($(this).is(':checked')) {
                var deleteLab = 1;
                $.ajax({
                    type:'POST',
                    url:'http://localhost/b2bmalaysia/server/seller/product/save_room',
                    data: {
                        labCheckded:labCheckded,
                        deleteLab:deleteLab,
                        'id-product' : $('#id-product').val(),
                        'action-product' : $('#action-product').val(),
                    },
                    success:function(data) {
                    },
                });
            } else {
                var deleteLab = 0;
                $.ajax({
                    type:'POST',
                    url:'http://localhost/b2bmalaysia/server/seller/product/delete_room',
                    data: {
                        labCheckded:labCheckded,
                        deleteLab:deleteLab,
                        'id-product' : $('#id-product').val(),
                    },
                    success:function(data) {
                    },
                });
            }
    });

    $( '.myr' ).mask('000.000.000', {reverse: true});

    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/account/get_staff_profile',
        data: $('#staffProfile').serialize()+"&staffID="+localStorage.getItem("staffCode"),
        dataType: 'JSON',
        success: function(response) {
            if (response.status == '500') {
                alert("can't load");
            } else {
                $('#staffName').val(response.data.staffName);
                $('#staffUsername').val(response.data.staffUsername);
            }
        }
    });
    
    $('#update_staff_password').click(function(e) {
        e.preventDefault();
        if($('#password').val() == '') {
            alert('Password harus diisi');
        }else if($('#password').val().length > 0 && $('#password').val().length < 6) {
            alert('Password harus 6 digit');
        }else if($('#password').val() == '123456') {
            alert('Password tidak boleh 123456');
        }else{
            $.ajax({
                method: 'POST',
                url: 'http://localhost/b2bmalaysia/server/seller/account/update_staff_profile',
                data: $('#staffProfile').serialize()+"&staff="+localStorage.getItem("staffCode"),
                dataType: 'JSON',
                success: function(response) {
                    if (response.status == '500') {
                        alert("failed");
                    }else {
                        window.location.href = "login";
                    }
                }
            });
        }
    });     
});

function checkDataKosong(value) {
    return $.trim(value) == '' ? 1 : 0;
}

function close(){
    loadCategory();
    $('#modal_add_edit').modal('hide');
    $('#modal_success').modal('hide');
    $('#modal_delete').modal('hide');
}

function reset_form() {
    $('#form_category').trigger('reset');
    $('#newcat').css('display','none');
    $('#editcat').css('display','none');
    $('input:radio[name="publish"]').attr('checked', false);
}

function reset_product() {
    $('input:checkbox').removeAttr('checked');
    $('#form_product').trigger('reset');
    $('#boDates').empty();
    $('.room').remove();
    $('.lc').remove();
    $('#newprod').css('display','none');
    $('#editprod').css('display','none');
    $('input:radio[name="autoconfirm"]').attr('checked', false);
    $('input:radio[name="productpublish"]').attr('checked', false);
}

function uploadFile(){
    if (($("#userfile2"))[0].files.length > 0) {
        var file = $("#userfile2")[0].files[0];
        var formdata = new FormData();
        formdata.append("userfile2", file);
        var ajax = new XMLHttpRequest();
        ajax.addEventListener("load", completeHandler, false);
        ajax.open("POST", "http://localhost/b2bmalaysia/server/seller/product/uploadImage");
        ajax.send(formdata);
    } else {
        alert("No file chosen!");
    }
}

function completeHandler(event){
    var data = event.target.responseText.split('*');
    if(data[0]!=''){
        $('#userfile2').val('');
        alert("Error!"+ data[1]);
    }else{
        $('#imageName').val(data[1]);
        $('#userfile2').val('');
    }   
}

function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))

    return false;
    return true;
}

var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];

function dateFormat1(d) {
  var t = new Date(d);
  return t.getDate() + ' ' + monthNames[t.getMonth()] + ' ' + t.getFullYear();
}

function loadCategory() {
    var aaData = [];
    $.ajax({
        type: 'post',
        url: 'http://localhost/b2bmalaysia/server/seller/product/get_category',
        data: {},
        dataType: 'json',
        cache: false,
        success: function (responses) {
            $.each(responses.data, function( index, value ) {
              var stringData = value._;

              aaData.push([
                value.id,
                value.categoryName, 
                value.total, 
                `<button class="btn btn-primary btn-xs" onclick="action_view_category(`+value.id+`)"><i class="fa fa-search"></i></button>
                <button class="btn btn-info btn-xs" onclick="action_edit_category(`+ value.id +`)"><i class="fa fa-edit"></i></button>
                <button class="btn btn-danger btn-xs" onclick="action_delete_category(`+value.id+`)"><i class="fa fa-trash"></i></button>`,
              ]);
            });
            $('#tb_category').DataTable().destroy();
            $("#tb_category").dataTable({
              "responsive": true,
              "language": {
                "search"            : '_INPUT_',
                "searchPlaceholder" : 'Search...',
                "sSearch"           : 'Search : ',
                "lengthMenu"        : 'Items/page : _MENU_',
                "sProcessing"       : '<img src="../assets/loading.gif"><span>&nbsp;&nbsp;Loading...</span>',
              },
              "aaData": aaData,
              "bJQueryUI": true,
              "aoColumns": [
                  { "sWidth": "10%" },  
                  { "sWidth": "53%" }, 
                  { "sWidth": "17%" }, 
                  { "sWidth": "20%" }, 
              ],
              "aaSorting": [[0, 'desc']],
              "aLengthMenu": [
                [5, 10, 20, -1],
                [5, 10, 20, "All"]
              ],
            });
            $('.dataTables_length select').addClass('form-control input-sm');
            $('.dataTables_filter input').addClass('form-control input-sm');
        }
    });
}

function action_add_category(){
    reset_form();
    $('#action').val('add');
    $('#id').val('');
    $('input:radio[name="publish"]').filter('[value=0]').attr('checked', true);
    $('#modal_add_edit').modal('show');
    $('#editcat').css('display','none');
    $('#newcat').css('display','block');
}

function action_view_category(id){
    reset_form();
    reset_product();
    var aaData = [];
    $.ajax({
        type: 'post',
        url: 'http://localhost/b2bmalaysia/server/seller/product/get_product',
        data: {
            'id' : id
        },
        dataType: 'json',
        cache: false,
        success: function (response) {
            $("#btn-add-product").click(function(){
                action_add_product(id);
            });
            $('#loc').html(response.sub.location);
            $('#ctour').html(response.sub.product_category_name);
            $('#img').attr("src","http://localhost/b2bmalaysia/server/assets/upload/"+response.sub.image);
            $('#inc').html(response.sub.include);
            $('#exc').html(response.sub.exclude);
            $('#cname').html(response.sub.categoryName);
            $('#view_category').modal('show');
            $.each(response.data, function( index, value ) {
              var stringData = value._;

              aaData.push([
                (index+1),
                value.productName, 
                value.noofdays+`D/`+value.noofnights+'N',
                value.highlight, 
                value.description, 
                `<button class="btn btn-primary btn-xs" onclick="action_view_product(`+value.id+`)"><i class="fa fa-search"></i></button>
                <button class="btn btn-info btn-xs" onclick="action_edit_product(`+ value.id +`)"><i class="fa fa-edit"></i></button>
                <button class="btn btn-danger btn-xs" onclick="action_delete_product(`+value.id+`)"><i class="fa fa-trash"></i></button>
                `,
              ]);
            });
            $('#tb_product').DataTable().destroy();
            $("#tb_product").dataTable({
              "responsive": true,
              "language": {
                "search"            : '_INPUT_',
                "searchPlaceholder" : 'Search...',
                "sSearch"           : 'Search : ',
                "lengthMenu"        : 'Items/page : _MENU_',
                "sProcessing"       : '<img src="../assets/loading.gif"><span>&nbsp;&nbsp;Loading...</span>',
              },
              "aaData": aaData,
              "bJQueryUI": true,
              "aoColumns": [
                  { "sWidth": "5%" },  
                  { "sWidth": "20%" }, 
                  { "sWidth": "20%" }, 
                  { "sWidth": "20%" },
                  { "sWidth": "20%" }, 
                  { "sWidth": "15%" }, 
              ],
              "aaSorting": [[0, 'desc']],
              "aLengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "All"]
              ],
            });
            $('.dataTables_length select').addClass('form-control input-sm');
            $('.dataTables_filter input').addClass('form-control input-sm');
        }
    });
}

function action_edit_category(id){
    reset_form();
    $('#action').val('edit');
    $('#id').val(id);
    $('#newcat').css('display','none');
    $('#editcat').css('display','block');
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/get_category_id',
        data: {
            'id' : id
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                $('#categoryName').val(response.data.categoryName);
                $('#location').val(response.data.locationID).attr('selected', true);
                $('#tour').val(response.data.productCategoryID).attr('selected', true);
                $('#imageName').val(response.data.image);
                $('#include').val(response.data.include);
                $('#exclude').val(response.data.exclude);
                $('input:radio[name="publish"]').filter('[value='+ response.data.categoryPublish +']').attr('checked', true);
                $('#modal_add_edit').modal('show');
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function action_delete_category(id){
    $('#tipe').val("category");
    $('#delete').val(id);
    $('#modal_delete').modal('show');
}

function validation_category() {
    let status = [],
        count = 0;

    status.push(checkDataKosong($('#categoryName').val()));
    status.push(checkDataKosong($('#location').val()));
    status.push(checkDataKosong($('#tour').val()));
    status.push(checkDataKosong($('#imageName').val()));
    status.push(checkDataKosong($('#include').val()));
    status.push(checkDataKosong($('#exclude').val()));
    status.push(checkDataKosong($('input[name="publish"]:checked').val()));

    status.map(v => { if (v == 1) { count += 1; } });
    
    return count;
}

function process_category() {
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/save_category',
        data: $('#form_category').serialize()+"&agentID="+localStorage.getItem("userCode"),
        dataType: 'JSON',
        success: function(response) {
            if (response.status == '500') {
                alert('Error: ' + response.error.message);
            } else {
                $('#modal_success').modal('show');
                id = response;
                reset_form();
                loadCategory();
            }
        }
    });
}

function action_add_product(id){
    reset_form();
    reset_product();
    $('#action-product').val('add-product');
    $('#id-product').val(id);
    $('#editprod').css('display','none');
    $('#newprod').css('display','block');
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/get_category_product',
        data: {
            'id-product' : id
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                $('#category').val(response.data.categoryName);
                $('#idCat').val(response.data.id);
                $('input:radio[name="productpublish"]').filter('[value=0]').attr('checked', true);
                $('input:radio[name="autoconfirm"]').filter('[value=0]').attr('checked', true);
                $('#modal_add_edit').modal('hide');
                $('#modal_success').modal('hide');
                $('#modal_add_product').modal('show');
                loadCategory();
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function action_view_product(id){
    reset_product();
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/get_product_id',
        data: {
            'id-product' : id
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                $('#pName').html(response.data.productName);
                $('#days').html(response.data.noofdays);
                $('#nights').html(response.data.noofnights);
                $('#hl').html(response.data.highlight);
                $('#desc').html(response.data.description);
                $('#vPeriod').html(dateFormat1(response.data.validFrom) +" - "+ dateFormat1(response.data.validTo));
                $('#vDays').html(response.data.validDays);
                $('#coDays').html(response.data.cutDays);
                var txt = "";
                $.each(response.data.bod, function (i) {
                    $('#boDates').html(txt += dateFormat1(response.data.bod[i].boDate) +', ');
                    // $('#blackoutdate').datepicker('setDates', response.data.bod[i].boDate);
                });
                $('#allot').html(response.data.allotment);
                $('#lg').html(response.data.timeLimitGuarantee);
                if(response.data.productPublish == 0){
                    $('#pub').html("Yes");
                } else{
                    $('#pub').html("No");
                }
                if(response.data.isConfirm == 0){
                    $('#con').html("Yes");
                } else{
                    $('#con').html("No");
                    $('#inf')
                        .append($(`<tr class="lc"></tr>`)
                            .append($(`<th></th>`).html("Limit Confirm"))
                            .append($(`<td></td>`).html(response.data.timeLimitConfirm + " Days"))
                        );
                }
                $('#psingle').html(response.data.pSingle);
                $('#padulttwin').html(response.data.pAdultTwin);
                $('#padulttriple').html(response.data.pAdultTriple);
                $('#pcnb').html(response.data.pCnb);
                $('#pcwa').html(response.data.pCwa);
                $('#pcwb').html(response.data.pCwb);
                $.each(response.data.room, function (i) {
                    $('#room')
                        .append($(`<tr class="room"></tr>`)
                            .append($(`<th style="width:10%"></th>`).html(i+1).css('text-align','center'))
                            .append($(`<td style="width:90%"></td>`).html(response.data.room[i].settingName))
                        );
                });
                $('#view_product').modal('show');
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function action_edit_product(id){
    reset_form();
    reset_product();
    $('#action-product').val('edit-product');
    $('#id-product').val(id);
    $('#newprod').css('display','none');
    $('#editprod').css('display','block');
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/get_product_id',
        data: {
            'id-product' : id
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                $('#idCat').val(response.data.categoryID);
                $('#category').val(response.data.categoryName);
                $('#productName').val(response.data.productName);
                $('#highlight').val(response.data.highlight);
                $('#description').val(response.data.description);
                $('#noofdays').val(response.data.noofdays);
                $('#noofnights').val(response.data.noofnights);
                $.each(response.data.validDays.split(","), function (key, val) {
                    $('input:checkbox[name="validdays[]"]').filter('[value='+ val +']').attr('checked', true);
                });
                $('#validfrom').val(response.data.validFrom);
                $('#validuntil').val(response.data.validTo);
                // $('#blackoutdate').datepicker('setDates', response.data.bod[0].boDate, response.data.bod[1].boDate, response.data.bod[2].boDate);
                var txt = "";
                $.each(response.data.bod, function (i) {
                    $('#blackoutdate').val(txt += response.data.bod[i].boDate +',');
                    // $('#blackoutdate').datepicker('setDates', response.data.bod[i].boDate);
                });
                $('#cutofdays').val(response.data.cutDays);
                $('#allotment').val(response.data.allotment);
                $('#limitguarantee').val(response.data.timeLimitGuarantee)
                $('input:radio[name="autoconfirm"]').filter('[value='+ response.data.isConfirm +']').attr('checked', true);
                $('input:radio[name="productpublish"]').filter('[value='+ response.data.productPublish +']').attr('checked', true);
                $('#limitconfirm').val(response.data.timeLimitConfirm);
                $('#single').val(response.data.pSingle);
                $('#adulttwin').val(response.data.pAdultTwin);
                $('#adulttriple').val(response.data.pAdultTriple);
                $('#childwithadult').val(response.data.pCwa);
                $('#childwithbed').val(response.data.pCwb);
                $('#childnobed').val(response.data.pCnb);
                $.each(response.data.room, function (i) {
                    $('input:checkbox[name="fneroom"]').filter('[value='+ response.data.room[i].roomID +']').attr('checked', true);
                });
                $('#modal_add_product').modal('show');
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function action_delete_product(id){
    $('#tipe').val("product");
    $('#delete').val(id);
    $('#modal_delete').modal('show');
}

function action_delete(){
    reset_form();
    reset_product();
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/delete',
        data: {
            'id-product' : $('#delete').val(),
            'tipe'       : $('#tipe').val(),
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                loadCategory();
                $('#view_category').modal('hide');
                $('#modal_delete').modal('hide');
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function validation_product() {
    let status = [],
        count = 0;

    status.push(checkDataKosong($('#productName').val()));
    status.push(checkDataKosong($('#highlight').val()));
    status.push(checkDataKosong($('#description').val()));
    status.push(checkDataKosong($('#noofdays').val()));
    status.push(checkDataKosong($('#noofnights').val()));
    // status.push(checkDataKosong($('input[name="validdays"]:checked').val()));
    status.push(checkDataKosong($('#validfrom').val()));
    status.push(checkDataKosong($('#validuntil').val()));
    // status.push(checkDataKosong($('#blackoutdate').val()));
    status.push(checkDataKosong($('#cutofdays').val()));
    status.push(checkDataKosong($('#allotment').val()));
    status.push(checkDataKosong($('#limitguarantee').val()));
    status.push(checkDataKosong($('input[name="productpublish"]:checked').val()));
    status.push(checkDataKosong($('input[name="autoconfirm"]:checked').val()));
    if($('input:radio[name="autoconfirm"]:checked').val() == 1){
        status.push(checkDataKosong($('#limitconfirm').val()));
    }
    status.push(checkDataKosong($('#single').val()));
    status.push(checkDataKosong($('#adulttwin').val()));
    status.push(checkDataKosong($('#adulttriple').val()));
    status.push(checkDataKosong($('#childwithadult').val()));
    status.push(checkDataKosong($('#childwithbed').val()));
    status.push(checkDataKosong($('#childnobed').val()));
    status.push(checkDataKosong($('input[name="fneroom"]:checked').val()));

    status.map(v => { if (v == 1) { count += 1; } });
    
    return count;
}

function process_product() {
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/save_product',
        data: $('#form_product').serialize()+"&agentID="+localStorage.getItem("userCode"),
        dataType: 'JSON',
        success: function(response) {
            if (response.status == '500') {
                alert('Error: ' + response.error.message);
            } else {
                reset_product();
                reset_form();
                $('#modal_add_edit').modal('hide');
                $('#modal_success').modal('hide');
                $('#modal_add_product').modal('hide');
                //reset_product();
                id = response;
                loadCategory();
                action_view_category(id);
            }
        }
    });
}

function masterLocation() {
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/get_master_location',
        data: {},
        dataType: 'JSON',
        success: function(response) {
            if(response.location.status=='200'){
                $.each(response.location.data, function( index, value ) {
                    $('#location').append($(`<option></option>`)
                        .attr("value",value.id.toUpperCase()).text(value.location)); 
                });
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function masterTour() {
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/get_master_tour',
        data: {},
        dataType: 'JSON',
        success: function(response) {
            if(response.tour.status=='200'){
                $.each(response.tour.data, function( index, value ) {
                    $('#tour').append($(`<option></option>`)
                        .attr("value",value.id.toUpperCase()).text(value.product_category_name)); 
                });
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function loadReservation() {
    var aaData = [];
    $.ajax({
        type: 'post',
        url: 'http://localhost/b2bmalaysia/server/seller/reservation/get_reservation',
        data: {},
        dataType: 'json',
        cache: false,
        success: function (responses) {
            $.each(responses.data, function( index, value ) {
              var dateNow = new Date();
              var dateBook = value.bookingDate+` `+value.bookingTime;
              var limitDate = new Date(new Date(dateBook).getTime()+(48*60*60*1000));
              var status = "";
              var stringData = value._;

              if (dateNow <= limitDate && value.isStatus == 0) {
                isStatus = `<h6 style="color:grey;cursor:pointer;" onclick="status(`+value.id+`,`+value.isStatus+`)"><i style="color:orange;" class="fa fa-circle"></i> Waiting List</h6>`;
              } else if (value.isStatus == 1){
                isStatus = `<h6 style="color:grey;cursor:pointer;" onclick="status(`+value.id+`,`+value.isStatus+`)"><i style="color:#337ab7;" class="fa fa-circle"></i> Confirm</h6>`;
              }else if (value.isStatus == 2){
                isStatus =`<h6 style="color:grey;cursor:default;"><i style="color:#5cb85c;" class="fa fa-circle"></i> Complete</h6>`;
              }else if (dateNow > limitDate && value.isStatus == 0){
                save_status(value.id,3);
                isStatus = `<h6 style="color:grey;cursor:default;"><i style="color:red;" class="glyphicon glyphicon-remove"></i> Cancel</h6>`;
              }else if (value.isStatus == 3){
                isStatus = `<h6 style="color:grey;cursor:default;"><i style="color:red;" class="glyphicon glyphicon-remove"></i> Cancel</h6>`;
              }

              aaData.push([
                value.id,
                value.bookingCode +`<br><h6 style="color:grey;">`+ dateFormat1(value.bookingDate) + `<br>`+ value.bookingTime + `</h6>`, 
                value.contactName, 
                isStatus,
                `<button class="btn btn-primary btn-xs" onclick="details(`+value.id+`)"><i class="fa fa-search"></i></button>
                <button class="btn btn-info btn-xs" onclick="confirm_departure(`+ value.id +`)"><i class="fa fa-pencil"></i></button>`,
              ]);
            });
            $('#tb_reservation').DataTable().destroy();
            $("#tb_reservation").dataTable({
              "responsive": true,
              "language": {
                "search"            : '_INPUT_',
                "searchPlaceholder" : 'Search...',
                "sSearch"           : 'Search : ',
                "lengthMenu"        : 'Items/page : _MENU_',
                "sProcessing"       : '<img src="../assets/loading.gif"><span>&nbsp;&nbsp;Loading...</span>',
              },
              "aaData": aaData,
              "bJQueryUI": true,
              "aoColumns": [
                  { "sWidth": "10%" },  
                  { "sWidth": "20%" }, 
                  { "sWidth": "30%" }, 
                  { "sWidth": "20%" },
                  { "sWidth": "15%" }, 
              ],
              "aaSorting": [[0, 'desc']],
              "aLengthMenu": [
                [5, 10, 20, -1],
                [5, 10, 20, "All"]
              ],
            });
            $('.dataTables_length select').addClass('form-control input-sm');
            $('.dataTables_filter input').addClass('form-control input-sm');
        }
    });
}

function status(id,isStatus){
    reset();
    if (isStatus == 0) {
        $('#btn-status-complete').css('display','none');
    }
    else if (isStatus == 1) {
        $('#btn-status-confirm').css('display','none');
    }
    $('#idStatus').val(id);
    $('#modal_status').modal('show');
}

function save_status(id,stat){
    // alert(stat); 
    
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/reservation/set_status',
        data: {
            'status' : stat,
            'idStatus' : id,
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                loadReservation();
                $('#modal_status').modal('hide');
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function details(id){
    reset();
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/reservation/get_details',
        data: {
            'id' : id
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                $('#bCode').html(response.data.bookingCode);
                $('#bDate').html(dateFormat1(response.data.bookingDate));
                $('#bCategory').html(response.data.categoryName);
                $('#bProduct').html(response.data.productName);
                $('#bDays').html(response.data.noofdays+`D `+response.data.noofnights+`N`);
                $('#arrival').html(dateFormat1(response.data.inDate));
                $('#bRoom').html(response.data.settingName);
                $('#cName').html(response.data.contactName);
                $('#cMobile').html(response.data.contactMobile);
                $('#cEmail').html(response.data.contactEmail);
                $.each(response.data.details, function (i) {
                    $('#guest')
                        .append($(`<tr class="guest"></tr>`)
                            .append($(`<td></td>`).html(i+1))
                            .append($(`<td></td>`).html(response.data.details[i].gFname))
                            .append($(`<td></td>`).html(response.data.details[i].gPassport))
                            .append($(`<td></td>`).html(response.data.details[i].gExpired))
                            .append($(`<td></td>`).html(response.data.details[i].gDob))
                        );
                });
                var total = 0;
                $.each(response.data.details, function (i) {
                    $('#summary')
                        .append($(`<tr class="summary"></tr>`)
                            .append($(`<td></td>`).html(i+1))
                            .append($(`<td></td>`).html(response.data.details[i].gFname))
                            .append($(`<td></td>`).html(response.data.details[i].gCategory))
                            .append($(`<td></td>`).html(response.data.details[i].OpriceNett+` MYR`))
                        );
                        total += Number(response.data.details[i].OpriceNett)
                });
                $('#summary').append($(`<tr class="total"></tr>`)
                            .append($(`<td colspan="3"></td>`).html("Total"))
                            .append($(`<td></td>`).html(total+` MYR`))
                        );
                $('#modal_details').modal('show');
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function confirm_departure(id){
    reset();
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/reservation/get_departure',
        data: {
            'id' : id
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                if (response.data == "") {
                    $('.exist').css('display','none');
                    $('#modal_departure').modal('show');
                }else{
                    $('.doesnt_exist').css('display','none');
                    $.each(response.data, function (i) {
                        var int = i+1;
                        
                        if (int == 1){
                            $('.tabDeparture').append(`
                                <li class="listDeparture active"><a href="#`+ int +`" data-toggle="tab">`+ int +`</a>
                                </li>
                            `);
                            if (response.data[i].id == null){    
                                $('.contentDeparture').append(`
                                    <div class="fieldDeparture tab-pane fade in active" id="`+ int +`">
                                        <input type="hidden" name="reservationID" value="`+response.data[i].reservationID+`">
                                        <input type="hidden" name="departureID" value="`+response.data[i].departureID+`">
                                        <input type="hidden" name="pickupID" value="`+response.data[i].id+`">
                                        <div class="panel panel-default">
                                            <div class="panel-body" style="color: grey">
                                            <table class="table table-bordered" style="width:100%;margin-bottom: 0px">
                                                <tr>
                                                    <th colspan="2" style="font-size:18px;">Arrival</th>
                                                </tr>
                                                <tr>
                                                    <th>Arrival Date</th>
                                                    <td>`+response.data[i].arrivalDate+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Location</th>
                                                    <td>`+response.data[i].arrivalLocation+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Detail</th>
                                                    <td>`+response.data[i].arrivalDetail+`</td>
                                                </tr>
                                            </table>
                                            </div>
                                        </div>
                                        <div style="text-align: right;">
                                            <button type="button" onclick="set_pickup(`+response.data[i].reservationID+`,`+response.data[i].departureID+`)" class="btn btn-sm btn-primary">Set Pickup</button>
                                        </div>
                                    </div>
                                `);
                            } else if (response.data[i].id != null){  
                                $('.contentDeparture').append(`
                                    <div class="fieldDeparture tab-pane fade in active" id="`+ int +`">
                                        <input type="hidden" name="reservationID" value="`+response.data[i].reservationID+`">
                                        <input type="hidden" name="departureID" value="`+response.data[i].departureID+`">
                                        <input type="hidden" name="pickupID" value="`+response.data[i].id+`">
                                        <div class="panel panel-default">
                                            <div class="panel-body" style="color: grey">
                                            <table class="table table-bordered" style="width:100%;margin-bottom: 0px">
                                                <tr>
                                                    <th colspan="2" style="font-size:18px;">Arrival</th>
                                                </tr>
                                                <tr>
                                                    <th>Arrival Date</th>
                                                    <td>`+response.data[i].arrivalDate+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Location</th>
                                                    <td>`+response.data[i].arrivalLocation+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Detail</th>
                                                    <td>`+response.data[i].arrivalDetail+`</td>
                                                </tr>
                                            </table>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-body" style="color: grey">
                                            <table class="table table-bordered" style="width:100%;margin-bottom: 0px">
                                                <tr>
                                                    <th colspan="2" style="font-size:18px;">Pickup</th>
                                                </tr>
                                                <tr>
                                                    <th>Name</th>
                                                    <td id="contact_name">`+response.data[i].name+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Contact</th>
                                                    <td id="contact_number">`+response.data[i].contact+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Details</th>
                                                    <td id="contact_details">`+response.data[i].pickupDetails+`</td>
                                                </tr>
                                            </table>
                                            </div>
                                        </div>
                                        <div style="text-align: right;">
                                            <button type="button" onclick="update_pickup(`+response.data[i].reservationID+`,`+response.data[i].departureID+`,`+response.data[i].id+`)" class="btn btn-sm btn-primary">Update Pickup</button>
                                        </div>
                                    </div>
                                `);
                            }
                        }else{
                            $('.tabDeparture').append(`
                                <li class="listDeparture"><a href="#`+ int +`" data-toggle="tab">`+ int +`</a>
                                </li>
                            `);
                            if (response.data[i].id == null){    
                                $('.contentDeparture').append(`
                                    <div class="fieldDeparture tab-pane fade" id="`+ int +`">
                                        <input type="hidden" name="reservationID" value="`+response.data[i].reservationID+`">
                                        <input type="hidden" name="departureID" value="`+response.data[i].departureID+`">
                                        <input type="hidden" name="pickupID" value="`+response.data[i].id+`">
                                        <div class="panel panel-default">
                                            <div class="panel-body" style="color: grey">
                                            <table class="table table-bordered" style="width:100%;margin-bottom: 0px">
                                                <tr>
                                                    <th colspan="2" style="font-size:18px;">Arrival</th>
                                                </tr>
                                                <tr>
                                                    <th>Arrival Date</th>
                                                    <td>`+response.data[i].arrivalDate+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Location</th>
                                                    <td>`+response.data[i].arrivalLocation+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Detail</th>
                                                    <td>`+response.data[i].arrivalDetail+`</td>
                                                </tr>
                                            </table>
                                            </div>
                                        </div>
                                        <div style="text-align: right;">
                                            <button type="button" onclick="set_pickup(`+response.data[i].reservationID+`,`+response.data[i].departureID+`)" class="btn btn-sm btn-primary">Set Pickup</button>
                                        </div>
                                    </div>
                                `);
                            } else if (response.data[i].id != null){  
                                $('.contentDeparture').append(`
                                    <div class="fieldDeparture tab-pane fade" id="`+ int +`">
                                        <input type="hidden" name="reservationID" value="`+response.data[i].reservationID+`">
                                        <input type="hidden" name="departureID" value="`+response.data[i].departureID+`">
                                        <input type="hidden" name="pickupID" value="`+response.data[i].id+`">
                                        <div class="panel panel-default">
                                            <div class="panel-body" style="color: grey">
                                            <table class="table table-bordered" style="width:100%;margin-bottom: 0px">
                                                <tr>
                                                    <th colspan="2" style="font-size:18px;">Arrival</th>
                                                </tr>
                                                <tr>
                                                    <th>Arrival Date</th>
                                                    <td>`+response.data[i].arrivalDate+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Location</th>
                                                    <td>`+response.data[i].arrivalLocation+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Detail</th>
                                                    <td>`+response.data[i].arrivalDetail+`</td>
                                                </tr>
                                            </table>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-body" style="color: grey">
                                            <table class="table table-bordered" style="width:100%;margin-bottom: 0px">
                                                <tr>
                                                    <th colspan="2" style="font-size:18px;">Pickup</th>
                                                </tr>
                                                <tr>
                                                    <th>Name</th>
                                                    <td id="contact_name">`+response.data[i].name+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Contact</th>
                                                    <td id="contact_number">`+response.data[i].contact+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Details</th>
                                                    <td id="contact_details">`+response.data[i].pickupDetails+`</td>
                                                </tr>
                                            </table>
                                            </div>
                                        </div>
                                        <div style="text-align: right;">
                                            <button type="button" onclick="update_pickup(`+response.data[i].reservationID+`,`+response.data[i].departureID+`,`+response.data[i].id+`)" class="btn btn-sm btn-primary">Update Pickup</button>
                                        </div>
                                    </div>
                                `);
                            }
                        }
                    });

                    $('#modal_departure').modal('show');
                }
            }
            else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function set_pickup(resID,depID) {
    $('#form_pickup').trigger('reset');
    $('#actionPickup').val("add");
    $('#resID').val(resID);
    $('#depID').val(depID);
    $('#modal_pickup').modal('show');
}

function update_pickup(resID,depID,pickID) {
    $('#form_pickup').trigger('reset');
    $('#actionPickup').val("edit");
    $('#resID').val(resID);
    $('#depID').val(depID);
    $('#pickID').val(pickID);
    $('#modal_pickup').modal('show');
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/reservation/get_pickup',
        data: {
            'ID' : $('#pickID').val()
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                $('#pickup_name').val(response.data.name);
                $('#pickup_number').val(response.data.contact);
                $('#pickup_details').val(response.data.pickupDetails);
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function validation_pickup() {
    let status = [],
        count = 0;

    status.push(checkDataKosong($('#pickup_name').val()));
    status.push(checkDataKosong($('#pickup_number').val()));
    status.push(checkDataKosong($('#pickup_details').val()));

    status.map(v => { if (v == 1) { count += 1; } });
    
    return count;
}

function save(){
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/reservation/set_pickup',
        data: $('#form_pickup').serialize(),
        dataType: 'JSON',
        success: function(response) {
            $('#modal_pickup').modal('hide');
            confirm_departure(response.sub.reservationID);           
            reset(); 
        }
    });
}

function reset(){
    $('.listDeparture').remove();
    $('.fieldDeparture').remove();
    $('.exist').css('display','block');
    $('.doesnt_exist').css('display','block');
    $('#form_pickup').trigger('reset');
    $('.guest').remove();
    $('.summary').remove();
    $('.total').remove();
    $('#btn-status-confirm').css('display','inline-block');
    $('#btn-status-complete').css('display','inline-block');
}