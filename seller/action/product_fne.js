var id;
$(document).ready(function() {
    loadCategory();
    masterLocation();
    masterTour();
    $('#tb_category').dataTable();  
    $('#tb_product').dataTable();    

    $('#btn-reset').click(function() {
        reset_form();
    });

    $('#btn-save-category').click(function() {
        let valid = validation_category();
        if (valid > 0) {
            alert('Form Belum Lengkap.');
            return false;
        }
        process_category();
    });

    $('.date').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });

    $('.dates').datepicker({
        multidate: true,
        format: 'yyyy-mm-dd',
    // })
    // .change(function() {
    //     var date = $('.dates').datepicker("getDate");
    //     console.log(date);
    //     var labCheckded = $(this).val();
    //     if ($(this).on(':select')) {
    //         var deleteLab = 1;
    //         $.ajax({
    //             type:'POST',
    //             url:'http://localhost/b2bmalaysia/server/seller/product/save_bod',
    //             data: {
    //                 labCheckded:labCheckded,
    //                 deleteLab:deleteLab,
    //                 'id-product' : $('#id-product').val(),
    //             },
    //             success:function(data) {
    //             },
    //         });
    //     } else {
    //         var deleteLab = 0;
    //         $.ajax({
    //             type:'POST',
    //             url:'http://localhost/b2bmalaysia/server/seller/product/delete_bod',
    //             data: {
    //                 labCheckded:labCheckded,
    //                 deleteLab:deleteLab,
    //                 'id-product' : $('#id-product').val(),
    //             },
    //             success:function(data) {
    //             },
    //         });
    //     }
    });

    // $(document).on('change', '.dates', function() { // Change event for datepicker
    //     var date = $('.dates').datepicker("getDate");
    //     console.log(date);
    // });


    // $(".dates").bind('change keyup', function() {
    //     alert("s");
    // });

    $("#userfile2").change(function () {
        uploadFile();
    });

    $('#btn-yes').click(function(){
        action_add_product(id);
    });

    $('#btn-no').click(function(){
        close();
    }); 

    $('#btn-delete').click(function(){
        action_delete(id);
    });

    $('#btn-cancel').click(function(){
        close();
    }); 
    
    $('#btn-save-product').click(function() {
        let valid = validation_product();
        if (valid > 0) {
            alert('Form Belum Lengkap.');
            return false;
        }
        process_product();
    });

    $('input:radio[name="autoconfirm"]').change(
        function(){
            if ($(this).is(':checked') && $(this).val() == '0') {
                $('.limitconfirm').css('display','none');
            }else{
                $('.limitconfirm').css('display','block');
            }
    });

    $('input[name="fneroom"]').change(
        function() {
            var labCheckded = $(this).val();
            if ($(this).is(':checked')) {
                var deleteLab = 1;
                $.ajax({
                    type:'POST',
                    url:'http://localhost/b2bmalaysia/server/seller/product/save_room',
                    data: {
                        labCheckded:labCheckded,
                        deleteLab:deleteLab,
                        'id-product' : $('#id-product').val(),
                        'action-product' : $('#action-product').val(),
                    },
                    success:function(data) {
                    },
                });
            } else {
                var deleteLab = 0;
                $.ajax({
                    type:'POST',
                    url:'http://localhost/b2bmalaysia/server/seller/product/delete_room',
                    data: {
                        labCheckded:labCheckded,
                        deleteLab:deleteLab,
                        'id-product' : $('#id-product').val(),
                    },
                    success:function(data) {
                    },
                });
            }
    });
    
    $( '.myr' ).mask('000.000.000', {reverse: true});
});

function loadCategory() {
    var aaData = [];
    $.ajax({
        type: 'post',
        url: 'http://localhost/b2bmalaysia/server/seller/product/get_category',
        data: {},
        dataType: 'json',
        cache: false,
        success: function (responses) {
            $.each(responses.data, function( index, value ) {
              var stringData = value._;

              aaData.push([
                value.id,
                value.categoryName, 
                `<img src="http://localhost/b2bmalaysia/server/assets/upload/`+value.image+`" id="img_gallery_4" width="150px" height="150px">`, 
                value.total, 
                `<button class="btn btn-primary btn-xs" onclick="action_view_category(`+value.id+`)"><i class="fa fa-search"></i></button>
                <button class="btn btn-info btn-xs" onclick="action_edit_category(`+ value.id +`)"><i class="fa fa-edit"></i></button>
                <button class="btn btn-danger btn-xs" onclick="action_delete_category(`+value.id+`)"><i class="fa fa-trash"></i></button>`,
              ]);
            });
            $('#tb_category').DataTable().destroy();
            $("#tb_category").dataTable({
              "responsive": true,
              "language": {
                "search"            : '_INPUT_',
                "searchPlaceholder" : 'Search...',
                "sSearch"           : 'Search : ',
                "lengthMenu"        : 'Items/page : _MENU_',
                "sProcessing"       : '<img src="../assets/loading.gif"><span>&nbsp;&nbsp;Loading...</span>',
              },
              "aaData": aaData,
              "bJQueryUI": true,
              "aoColumns": [
                  { "sWidth": "5%" },  
                  { "sWidth": "30%" }, 
                  { "sWidth": "25%" }, 
                  { "sWidth": "20%" },
                  { "sWidth": "20%" }, 
              ],
              "aaSorting": [[0, 'desc']],
              "aLengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "All"]
              ],
            });
            $('.dataTables_length select').addClass('form-control input-sm');
            $('.dataTables_filter input').addClass('form-control input-sm');
        }
    });
}

function uploadFile(){
    if (($("#userfile2"))[0].files.length > 0) {
        var file = $("#userfile2")[0].files[0];
        var formdata = new FormData();
        formdata.append("userfile2", file);
        var ajax = new XMLHttpRequest();
        ajax.addEventListener("load", completeHandler, false);
        ajax.open("POST", "http://localhost/b2bmalaysia/server/seller/product/uploadImage");
        ajax.send(formdata);
    } else {
        alert("No file chosen!");
    }
}

function completeHandler(event){
    var data = event.target.responseText.split('*');
    if(data[0]!=''){
        $('#userfile2').val('');
        alert("Error!"+ data[1]);
    }else{
        $('#imageName').val(data[1]);
        $('#userfile2').val('');
    }   
}

function action_add_category(){
    reset_form();
    $('#action').val('add');
    $('#id').val('');
    $('input:radio[name="publish"]').filter('[value=0]').attr('checked', true);
    $('#modal_add_edit').modal('show');
    $('#editcat').css('display','none');
    $('#newcat').css('display','block');
}

function action_view_category(id){
    reset_form();
    reset_product();
    var aaData = [];
    $.ajax({
        type: 'post',
        url: 'http://localhost/b2bmalaysia/server/seller/product/get_product',
        data: {
            'id' : id
        },
        dataType: 'json',
        cache: false,
        success: function (response) {
            $("#btn-add-product").click(function(){
                action_add_product(id);
            });
            $('#loc').html(response.sub.location);
            $('#ctour').html(response.sub.product_category_name);
            $('#img').attr("src","http://localhost/b2bmalaysia/server/assets/upload/"+response.sub.image);
            $('#inc').html(response.sub.include);
            $('#exc').html(response.sub.exclude);
            $('#cname').html(response.sub.categoryName);
            $('#view_category').modal('show');
            $.each(response.data, function( index, value ) {
              var stringData = value._;

              aaData.push([
                (index+1),
                value.productName, 
                value.noofdays+`D/`+value.noofnights+'N',
                value.highlight, 
                value.description, 
                `<button class="btn btn-primary btn-xs" onclick="action_view_product(`+value.id+`)"><i class="fa fa-search"></i></button>
                <button class="btn btn-info btn-xs" onclick="action_edit_product(`+ value.id +`)"><i class="fa fa-edit"></i></button>
                <button class="btn btn-danger btn-xs" onclick="action_delete_product(`+value.id+`)"><i class="fa fa-trash"></i></button>
                `,
              ]);
            });
            $('#tb_product').DataTable().destroy();
            $("#tb_product").dataTable({
              "responsive": true,
              "language": {
                "search"            : '_INPUT_',
                "searchPlaceholder" : 'Search...',
                "sSearch"           : 'Search : ',
                "lengthMenu"        : 'Items/page : _MENU_',
                "sProcessing"       : '<img src="../assets/loading.gif"><span>&nbsp;&nbsp;Loading...</span>',
              },
              "aaData": aaData,
              "bJQueryUI": true,
              "aoColumns": [
                  { "sWidth": "5%" },  
                  { "sWidth": "20%" }, 
                  { "sWidth": "20%" }, 
                  { "sWidth": "20%" },
                  { "sWidth": "20%" }, 
                  { "sWidth": "15%" }, 
              ],
              "aaSorting": [[0, 'desc']],
              "aLengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "All"]
              ],
            });
            $('.dataTables_length select').addClass('form-control input-sm');
            $('.dataTables_filter input').addClass('form-control input-sm');
        }
    });
}

function action_edit_category(id){
    reset_form();
    $('#action').val('edit');
    $('#id').val(id);
    $('#newcat').css('display','none');
    $('#editcat').css('display','block');
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/get_category_id',
        data: {
            'id' : id
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                $('#categoryName').val(response.data.categoryName);
                $('#location').val(response.data.locationID).attr('selected', true);
                $('#tour').val(response.data.productCategoryID).attr('selected', true);
                $('#imageName').val(response.data.image);
                $('#include').val(response.data.include);
                $('#exclude').val(response.data.exclude);
                $('input:radio[name="publish"]').filter('[value='+ response.data.categoryPublish +']').attr('checked', true);
                $('#modal_add_edit').modal('show');
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function action_delete_category(id){
    $('#tipe').val("category");
    $('#delete').val(id);
    $('#modal_delete').modal('show');
}

function close(){
    loadCategory();
    $('#modal_add_edit').modal('hide');
    $('#modal_success').modal('hide');
    $('#modal_delete').modal('hide');
}

function reset_form() {
    $('#form_category').trigger('reset');
    $('#newcat').css('display','none');
    $('#editcat').css('display','none');
    $('input:radio[name="publish"]').attr('checked', false);
}

function checkDataKosong(value) {
    return $.trim(value) == '' ? 1 : 0;
}

function validation_category() {
    let status = [],
        count = 0;

    status.push(checkDataKosong($('#categoryName').val()));
    status.push(checkDataKosong($('#location').val()));
    status.push(checkDataKosong($('#tour').val()));
    status.push(checkDataKosong($('#imageName').val()));
    status.push(checkDataKosong($('#include').val()));
    status.push(checkDataKosong($('#exclude').val()));
    status.push(checkDataKosong($('input[name="publish"]:checked').val()));

    status.map(v => { if (v == 1) { count += 1; } });
    
    return count;
}

function process_category() {
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/save_category',
        data: $('#form_category').serialize()+"&agentID="+localStorage.getItem("userCode"),
        dataType: 'JSON',
        success: function(response) {
            if (response.status == '500') {
                alert('Error: ' + response.error.message);
            } else {
                $('#modal_success').modal('show');
                id = response;
                reset_form();
                loadCategory();
            }
        }
    });
}

var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];

function dateFormat1(d) {
  var t = new Date(d);
  return t.getDate() + ' ' + monthNames[t.getMonth()] + ' ' + t.getFullYear();
}

function action_add_product(id){
    reset_form();
    reset_product();
    $('#action-product').val('add-product');
    $('#id-product').val(id);
    $('#editprod').css('display','none');
    $('#newprod').css('display','block');
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/get_category_product',
        data: {
            'id-product' : id
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                $('#category').val(response.data.categoryName);
                $('#idCat').val(response.data.id);
                $('input:radio[name="productpublish"]').filter('[value=0]').attr('checked', true);
                $('input:radio[name="autoconfirm"]').filter('[value=0]').attr('checked', true);
                $('#modal_add_edit').modal('hide');
                $('#modal_success').modal('hide');
                $('#modal_add_product').modal('show');
                loadCategory();
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function action_view_product(id){
    reset_product();
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/get_product_id',
        data: {
            'id-product' : id
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                $('#pName').html(response.data.productName);
                $('#days').html(response.data.noofdays);
                $('#nights').html(response.data.noofnights);
                $('#hl').html(response.data.highlight);
                $('#desc').html(response.data.description);
                $('#vPeriod').html(dateFormat1(response.data.validFrom) +" - "+ dateFormat1(response.data.validTo));
                $('#vDays').html(response.data.validDays);
                $('#coDays').html(response.data.cutDays);
                var txt = "";
                $.each(response.data.bod, function (i) {
                    $('#boDates').html(txt += dateFormat1(response.data.bod[i].boDate) +', ');
                    // $('#blackoutdate').datepicker('setDates', response.data.bod[i].boDate);
                });
                $('#allot').html(response.data.allotment);
                $('#lg').html(response.data.timeLimitGuarantee);
                if(response.data.productPublish == 0){
                    $('#pub').html("Yes");
                } else{
                    $('#pub').html("No");
                }
                if(response.data.isConfirm == 0){
                    $('#con').html("Yes");
                } else{
                    $('#con').html("No");
                    $('#inf')
                        .append($(`<tr class="lc"></tr>`)
                            .append($(`<th></th>`).html("Limit Confirm"))
                            .append($(`<td></td>`).html(response.data.timeLimitConfirm + " Days"))
                        );
                }
                $('#psingle').html(response.data.pSingle);
                $('#padulttwin').html(response.data.pAdultTwin);
                $('#padulttriple').html(response.data.pAdultTriple);
                $('#pcnb').html(response.data.pCnb);
                $('#pcwa').html(response.data.pCwa);
                $('#pcwb').html(response.data.pCwb);
                $.each(response.data.room, function (i) {
                    $('#room')
                        .append($(`<tr class="room"></tr>`)
                            .append($(`<th style="width:10%"></th>`).html(i+1).css('text-align','center'))
                            .append($(`<td style="width:90%"></td>`).html(response.data.room[i].settingName))
                        );
                });
                $('#view_product').modal('show');
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function action_edit_product(id){
    reset_form();
    reset_product();
    $('#action-product').val('edit-product');
    $('#id-product').val(id);
    $('#newprod').css('display','none');
    $('#editprod').css('display','block');
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/get_product_id',
        data: {
            'id-product' : id
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                $('#idCat').val(response.data.categoryID);
                $('#category').val(response.data.categoryName);
                $('#productName').val(response.data.productName);
                $('#highlight').val(response.data.highlight);
                $('#description').val(response.data.description);
                $('#noofdays').val(response.data.noofdays);
                $('#noofnights').val(response.data.noofnights);
                $.each(response.data.validDays.split(","), function (key, val) {
                    $('input:checkbox[name="validdays[]"]').filter('[value='+ val +']').attr('checked', true);
                });
                $('#validfrom').val(response.data.validFrom);
                $('#validuntil').val(response.data.validTo);
                // $('#blackoutdate').datepicker('setDates', response.data.bod[0].boDate, response.data.bod[1].boDate, response.data.bod[2].boDate);
                var txt = "";
                $.each(response.data.bod, function (i) {
                    $('#blackoutdate').val(txt += response.data.bod[i].boDate +',');
                    // $('#blackoutdate').datepicker('setDates', response.data.bod[i].boDate);
                });
                $('#cutofdays').val(response.data.cutDays);
                $('#allotment').val(response.data.allotment);
                $('#limitguarantee').val(response.data.timeLimitGuarantee)
                $('input:radio[name="autoconfirm"]').filter('[value='+ response.data.isConfirm +']').attr('checked', true);
                $('input:radio[name="productpublish"]').filter('[value='+ response.data.productPublish +']').attr('checked', true);
                $('#limitconfirm').val(response.data.timeLimitConfirm);
                $('#single').val(response.data.pSingle);
                $('#adulttwin').val(response.data.pAdultTwin);
                $('#adulttriple').val(response.data.pAdultTriple);
                $('#childwithadult').val(response.data.pCwa);
                $('#childwithbed').val(response.data.pCwb);
                $('#childnobed').val(response.data.pCnb);
                $.each(response.data.room, function (i) {
                    $('input:checkbox[name="fneroom"]').filter('[value='+ response.data.room[i].roomID +']').attr('checked', true);
                });
                $('#modal_add_product').modal('show');
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function action_delete_product(id){
    $('#tipe').val("product");
    $('#delete').val(id);
    $('#modal_delete').modal('show');
}

function action_delete(){
    reset_form();
    reset_product();
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/delete',
        data: {
            'id-product' : $('#delete').val(),
            'tipe'       : $('#tipe').val(),
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                loadCategory();
                $('#view_category').modal('hide');
                $('#modal_delete').modal('hide');
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function reset_product() {
    $('input:checkbox').removeAttr('checked');
    $('#form_product').trigger('reset');
    $('#boDates').empty();
    $('.room').remove();
    $('.lc').remove();
    $('#newprod').css('display','none');
    $('#editprod').css('display','none');
    $('input:radio[name="autoconfirm"]').attr('checked', false);
    $('input:radio[name="productpublish"]').attr('checked', false);
}

function validation_product() {
    let status = [],
        count = 0;

    status.push(checkDataKosong($('#productName').val()));
    status.push(checkDataKosong($('#highlight').val()));
    status.push(checkDataKosong($('#description').val()));
    status.push(checkDataKosong($('#noofdays').val()));
    status.push(checkDataKosong($('#noofnights').val()));
    // status.push(checkDataKosong($('input[name="validdays"]:checked').val()));
    status.push(checkDataKosong($('#validfrom').val()));
    status.push(checkDataKosong($('#validuntil').val()));
    // status.push(checkDataKosong($('#blackoutdate').val()));
    status.push(checkDataKosong($('#cutofdays').val()));
    status.push(checkDataKosong($('#allotment').val()));
    status.push(checkDataKosong($('#limitguarantee').val()));
    status.push(checkDataKosong($('input[name="productpublish"]:checked').val()));
    status.push(checkDataKosong($('input[name="autoconfirm"]:checked').val()));
    if($('input:radio[name="autoconfirm"]:checked').val() == 1){
        status.push(checkDataKosong($('#limitconfirm').val()));
    }
    status.push(checkDataKosong($('#single').val()));
    status.push(checkDataKosong($('#adulttwin').val()));
    status.push(checkDataKosong($('#adulttriple').val()));
    status.push(checkDataKosong($('#childwithadult').val()));
    status.push(checkDataKosong($('#childwithbed').val()));
    status.push(checkDataKosong($('#childnobed').val()));
    status.push(checkDataKosong($('input[name="fneroom"]:checked').val()));

    status.map(v => { if (v == 1) { count += 1; } });
    
    return count;
}

function process_product() {
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/save_product',
        data: $('#form_product').serialize()+"&agentID="+localStorage.getItem("userCode"),
        dataType: 'JSON',
        success: function(response) {
            if (response.status == '500') {
                alert('Error: ' + response.error.message);
            } else {
                reset_product();
                reset_form();
                $('#modal_add_edit').modal('hide');
                $('#modal_success').modal('hide');
                $('#modal_add_product').modal('hide');
                //reset_product();
                id = response;
                loadCategory();
                action_view_category(id);
            }
        }
    });
}

function masterLocation() {
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/get_master_location',
        data: {},
        dataType: 'JSON',
        success: function(response) {
            if(response.location.status=='200'){
                $.each(response.location.data, function( index, value ) {
                    $('#location').append($(`<option></option>`)
                        .attr("value",value.id.toUpperCase()).text(value.location)); 
                });
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function masterTour() {
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/product/get_master_tour',
        data: {},
        dataType: 'JSON',
        success: function(response) {
            if(response.tour.status=='200'){
                $.each(response.tour.data, function( index, value ) {
                    $('#tour').append($(`<option></option>`)
                        .attr("value",value.id.toUpperCase()).text(value.product_category_name)); 
                });
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}