var res;
$(document).ready(function() {
    loadReservation();

    $('#tb_reservation').dataTable(); 
    
    $('#btn-save-pickup').click(function() {
        let valid = validation_pickup();
        if (valid > 0) {
            alert('Form Belum Lengkap.');
            return false;
        }
	    save_pickup();
    }); 
    
    $('#btn-status-confirm').click(function(e) {
    	e.preventDefault()
        var id = $('#idStatus').val();
    	var stat = '1';
	    save_status(id,stat);
    }); 
    
    $('#btn-status-complete').click(function(e) {
    	e.preventDefault()
        var id = $('#idStatus').val();
    	var stat = '2';
	    save_status(id,stat);
    });  
});

var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];

function dateFormat1(d) {
  var t = new Date(d);
  return t.getDate() + ' ' + monthNames[t.getMonth()] + ' ' + t.getFullYear();
}

function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))

    return false;
    return true;
}

function checkDataKosong(value) {
    return $.trim(value) == '' ? 1 : 0;
}

function loadReservation() {
    var aaData = [];
    $.ajax({
        type: 'post',
        url: 'http://localhost/b2bmalaysia/server/seller/reservation/get_reservation',
        data: {},
        dataType: 'json',
        cache: false,
        success: function (responses) {
            $.each(responses.data, function( index, value ) {
              var dateNow = new Date();
              var dateBook = value.bookingDate+` `+value.bookingTime;
              var limitDate = new Date(new Date(dateBook).getTime()+(48*60*60*1000));
              var isStatus = "";
              var stringData = value._;

              if (dateNow <= limitDate && value.isStatus == 0) {
                isStatus = `<h6 style="color:grey;cursor:pointer;" onclick="status(`+value.id+`,`+value.isStatus+`)"><i style="color:orange;" class="fa fa-circle"></i> Waiting List</h6>`;
              } else if (value.isStatus == 1){
                isStatus = `<h6 style="color:grey;cursor:pointer;" onclick="status(`+value.id+`,`+value.isStatus+`)"><i style="color:#337ab7;" class="fa fa-circle"></i> Confirm</h6>`;
              }else if (value.isStatus == 2){
                isStatus =`<h6 style="color:grey;cursor:default;"><i style="color:#5cb85c;" class="fa fa-circle"></i> Complete</h6>`;
              }else if (dateNow > limitDate && value.isStatus == 0){
                save_status(value.id,3);
                isStatus = `<h6 style="color:grey;cursor:default;"><i style="color:red;" class="glyphicon glyphicon-remove"></i> Cancel</h6>`;
              }else if (value.isStatus == 3){
                isStatus = `<h6 style="color:grey;cursor:default;"><i style="color:red;" class="glyphicon glyphicon-remove"></i> Cancel</h6>`;
              }

              aaData.push([
                value.id,
                value.bookingCode +`<br><h6 style="color:grey;">`+ dateFormat1(value.bookingDate) + `<br>`+ value.bookingTime + `</h6>`, 
                value.productName,
                value.contactName, 
                value.noofpax,
                isStatus,
                `<button class="btn btn-primary btn-xs" onclick="detail_reservation(`+value.id+`)"><i class="fa fa-search"></i></button>
                <button class="btn btn-info btn-xs" onclick="confirm_departure(`+ value.id +`)"><i class="fa fa-pencil"></i></button>`,
              ]);
            });
            $('#tb_reservation').DataTable().destroy();
            $("#tb_reservation").dataTable({
              "responsive": true,
              "language": {
                "search"            : '_INPUT_',
                "searchPlaceholder" : 'Search...',
                "sSearch"           : 'Search : ',
                "lengthMenu"        : 'Items/page : _MENU_',
                "sProcessing"       : '<img src="../assets/loading.gif"><span>&nbsp;&nbsp;Loading...</span>',
              },
              "aaData": aaData,
              "bJQueryUI": true,
              "aoColumns": [
                  { "sWidth": "5%" },  
                  { "sWidth": "12%" }, 
                  { "sWidth": "40%" }, 
                  { "sWidth": "17%" },
                  { "sWidth": "5%" }, 
                  { "sWidth": "13%" },
                  { "sWidth": "8%" }, 
              ],
              "aaSorting": [[0, 'desc']],
              "aLengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "All"]
              ],
            });
            $('.dataTables_length select').addClass('form-control input-sm');
            $('.dataTables_filter input').addClass('form-control input-sm');
        }
    });
}

function status(id,isStatus){
    reset();
    if (isStatus == 0) {
        $('#btn-status-complete').css('display','none');
    }
    else if (isStatus == 1) {
        $('#btn-status-confirm').css('display','none');
    }
	$('#idStatus').val(id);
	$('#modal_status').modal('show');
}

function save_status(id,stat){
	// alert(stat);	
	
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/reservation/set_status',
        data: {
        	'status' : stat,
            'idStatus' : id,
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
	    		loadReservation();
                $('#modal_status').modal('hide');
	    	}else{
                alert('Error: ' + response.error.message);
            }
    	}
    });
}

function detail_reservation(id){
    reset();
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/reservation/get_details',
        data: {
            'id' : id
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
            	$('#bCode').html(response.data.bookingCode);
            	$('#bDate').html(dateFormat1(response.data.bookingDate));
            	$('#category').html(response.data.categoryName);
            	$('#product').html(response.data.productName);
            	$('#days').html(response.data.noofdays+`D `+response.data.noofnights+`N`);
                $('#arrival').html(dateFormat1(response.data.inDate));
                $('#bRoom').html(response.data.settingName);
                $('#cName').html(response.data.contactName);
                $('#cMobile').html(response.data.contactMobile);
                $('#cEmail').html(response.data.contactEmail);
                $.each(response.data.details, function (i) {
                    $('#guest')
                        .append($(`<tr class="guest"></tr>`)
                        	.append($(`<td></td>`).html(i+1))
                        	.append($(`<td></td>`).html(response.data.details[i].gFname))
                        	.append($(`<td></td>`).html(response.data.details[i].gPassport))
                        	.append($(`<td></td>`).html(response.data.details[i].gExpired))
                        	.append($(`<td></td>`).html(response.data.details[i].gDob))
                        );
                });
                var total = 0;
                $.each(response.data.details, function (i) {
                    $('#summary')
                        .append($(`<tr class="summary"></tr>`)
                            .append($(`<td></td>`).html(i+1))
                            .append($(`<td></td>`).html(response.data.details[i].gFname))
                            .append($(`<td></td>`).html(response.data.details[i].gCategory))
                            .append($(`<td></td>`).html(response.data.details[i].OpriceNett+` MYR`))
                        );
                        total += Number(response.data.details[i].OpriceNett)
                });
                $('#summary').append($(`<tr class="total"></tr>`)
                            .append($(`<td colspan="3"></td>`).html("Total"))
                            .append($(`<td></td>`).html(total+` MYR`))
                        );
                $('#modal_details').modal('show');
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function confirm_departure(id){
	reset();
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/reservation/get_departure',
        data: {
            'id' : id
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                if (response.data == "") {
                    $('.exist').css('display','none');
                    $('#modal_departure').modal('show');
                }else{
                    $('.doesnt_exist').css('display','none');
                    $.each(response.data, function (i) {
                        var int = i+1;
                    	
                        if (int == 1){
                            $('.tabDeparture').append(`
                                <li class="listDeparture active"><a href="#`+ int +`" data-toggle="tab">`+ int +`</a>
                                </li>
                            `);
                            if (response.data[i].id == null){    
                                $('.contentDeparture').append(`
                                    <div class="fieldDeparture tab-pane fade in active" id="`+ int +`">
                                        <input type="hidden" name="reservationID" value="`+response.data[i].reservationID+`">
                                        <input type="hidden" name="departureID" value="`+response.data[i].departureID+`">
                                        <input type="hidden" name="pickupID" value="`+response.data[i].id+`">
                                        <div class="panel panel-default">
                                            <div class="panel-body" style="color: grey">
                                            <table class="table table-bordered" style="width:100%;margin-bottom: 0px">
                                                <tr>
                                                    <th colspan="2" style="font-size:18px;">Arrival</th>
                                                </tr>
                                                <tr>
                                                    <th>Arrival Date</th>
                                                    <td>`+response.data[i].arrivalDate+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Location</th>
                                                    <td>`+response.data[i].arrivalLocation+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Detail</th>
                                                    <td>`+response.data[i].arrivalDetail+`</td>
                                                </tr>
                                            </table>
                                            </div>
                                        </div>
                                        <div style="text-align: right;">
                                            <button type="button" onclick="set_pickup(`+response.data[i].reservationID+`,`+response.data[i].departureID+`)" class="btn btn-sm btn-primary">Set Pickup</button>
                                        </div>
                                    </div>
                                `);
                            } else if (response.data[i].id != null){  
                                $('.contentDeparture').append(`
                                    <div class="fieldDeparture tab-pane fade in active" id="`+ int +`">
                                        <input type="hidden" name="reservationID" value="`+response.data[i].reservationID+`">
                                        <input type="hidden" name="departureID" value="`+response.data[i].departureID+`">
                                        <input type="hidden" name="pickupID" value="`+response.data[i].id+`">
                                        <div class="panel panel-default">
                                            <div class="panel-body" style="color: grey">
                                            <table class="table table-bordered" style="width:100%;margin-bottom: 0px">
                                                <tr>
                                                    <th colspan="2" style="font-size:18px;">Arrival</th>
                                                </tr>
                                                <tr>
                                                    <th>Arrival Date</th>
                                                    <td>`+response.data[i].arrivalDate+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Location</th>
                                                    <td>`+response.data[i].arrivalLocation+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Detail</th>
                                                    <td>`+response.data[i].arrivalDetail+`</td>
                                                </tr>
                                            </table>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-body" style="color: grey">
                                            <table class="table table-bordered" style="width:100%;margin-bottom: 0px">
                                                <tr>
                                                    <th colspan="2" style="font-size:18px;">Pickup</th>
                                                </tr>
                                                <tr>
                                                    <th>Name</th>
                                                    <td id="contact_name">`+response.data[i].name+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Contact</th>
                                                    <td id="contact_number">`+response.data[i].contact+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Details</th>
                                                    <td id="contact_details">`+response.data[i].pickupDetails+`</td>
                                                </tr>
                                            </table>
                                            </div>
                                        </div>
                                        <div style="text-align: right;">
                                            <button type="button" onclick="update_pickup(`+response.data[i].reservationID+`,`+response.data[i].departureID+`,`+response.data[i].id+`)" class="btn btn-sm btn-primary">Update Pickup</button>
                                        </div>
                                    </div>
                                `);
                            }
                        }else{
                            $('.tabDeparture').append(`
                                <li class="listDeparture"><a href="#`+ int +`" data-toggle="tab">`+ int +`</a>
                                </li>
                            `);
                            if (response.data[i].id == null){    
                                $('.contentDeparture').append(`
                                    <div class="fieldDeparture tab-pane fade" id="`+ int +`">
                                        <input type="hidden" name="reservationID" value="`+response.data[i].reservationID+`">
                                        <input type="hidden" name="departureID" value="`+response.data[i].departureID+`">
                                        <input type="hidden" name="pickupID" value="`+response.data[i].id+`">
                                        <div class="panel panel-default">
                                            <div class="panel-body" style="color: grey">
                                            <table class="table table-bordered" style="width:100%;margin-bottom: 0px">
                                                <tr>
                                                    <th colspan="2" style="font-size:18px;">Arrival</th>
                                                </tr>
                                                <tr>
                                                    <th>Arrival Date</th>
                                                    <td>`+response.data[i].arrivalDate+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Location</th>
                                                    <td>`+response.data[i].arrivalLocation+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Detail</th>
                                                    <td>`+response.data[i].arrivalDetail+`</td>
                                                </tr>
                                            </table>
                                            </div>
                                        </div>
                                        <div style="text-align: right;">
                                            <button type="button" onclick="set_pickup(`+response.data[i].reservationID+`,`+response.data[i].departureID+`)" class="btn btn-sm btn-primary">Set Pickup</button>
                                        </div>
                                    </div>
                                `);
                            } else if (response.data[i].id != null){  
                                $('.contentDeparture').append(`
                                    <div class="fieldDeparture tab-pane fade" id="`+ int +`">
                                        <input type="hidden" name="reservationID" value="`+response.data[i].reservationID+`">
                                        <input type="hidden" name="departureID" value="`+response.data[i].departureID+`">
                                        <input type="hidden" name="pickupID" value="`+response.data[i].id+`">
                                        <div class="panel panel-default">
                                            <div class="panel-body" style="color: grey">
                                            <table class="table table-bordered" style="width:100%;margin-bottom: 0px">
                                                <tr>
                                                    <th colspan="2" style="font-size:18px;">Arrival</th>
                                                </tr>
                                                <tr>
                                                    <th>Arrival Date</th>
                                                    <td>`+response.data[i].arrivalDate+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Location</th>
                                                    <td>`+response.data[i].arrivalLocation+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Detail</th>
                                                    <td>`+response.data[i].arrivalDetail+`</td>
                                                </tr>
                                            </table>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-body" style="color: grey">
                                            <table class="table table-bordered" style="width:100%;margin-bottom: 0px">
                                                <tr>
                                                    <th colspan="2" style="font-size:18px;">Pickup</th>
                                                </tr>
                                                <tr>
                                                    <th>Name</th>
                                                    <td id="contact_name">`+response.data[i].name+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Contact</th>
                                                    <td id="contact_number">`+response.data[i].contact+`</td>
                                                </tr>
                                                <tr>
                                                    <th>Details</th>
                                                    <td id="contact_details">`+response.data[i].pickupDetails+`</td>
                                                </tr>
                                            </table>
                                            </div>
                                        </div>
                                        <div style="text-align: right;">
                                            <button type="button" onclick="update_pickup(`+response.data[i].reservationID+`,`+response.data[i].departureID+`,`+response.data[i].id+`)" class="btn btn-sm btn-primary">Update Pickup</button>
                                        </div>
                                    </div>
                                `);
                            }
                        }
                    });

                    $('#modal_departure').modal('show');
            	}
            }
            else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function set_pickup(resID,depID) {
    $('#form_pickup').trigger('reset');
    $('#actionPickup').val("add");
    $('#resID').val(resID);
    $('#depID').val(depID);
    $('#modal_pickup').modal('show');
}

function update_pickup(resID,depID,pickID) {
    $('#form_pickup').trigger('reset');
    $('#actionPickup').val("edit");
    $('#resID').val(resID);
    $('#depID').val(depID);
    $('#pickID').val(pickID);
    $('#modal_pickup').modal('show');
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/reservation/get_pickup',
        data: {
            'ID' : $('#pickID').val()
        },
        dataType: 'JSON',
        success: function(response) {
            if(response.status=='200'){
                $('#pickup_name').val(response.data.name);
                $('#pickup_number').val(response.data.contact);
                $('#pickup_details').val(response.data.pickupDetails);
            }else{
                alert('Error: ' + response.error.message);
            }
        }
    });
}

function validation_pickup() {
    let status = [],
        count = 0;

    status.push(checkDataKosong($('#pickup_name').val()));
    status.push(checkDataKosong($('#pickup_number').val()));
    status.push(checkDataKosong($('#pickup_details').val()));

    status.map(v => { if (v == 1) { count += 1; } });
    
    return count;
}

function save_pickup(){
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/reservation/set_pickup',
        data: $('#form_pickup').serialize(),
        dataType: 'JSON',
        success: function(response) {
	    	$('#modal_pickup').modal('hide');
	   		confirm_departure(response.sub.reservationID);           
	 		reset(); 
    	}
    });
}

function reset(){
    $('.listDeparture').remove();
    $('.fieldDeparture').remove();
	$('.exist').css('display','block');
	$('.doesnt_exist').css('display','block');
    $('#form_pickup').trigger('reset');
    $('.guest').remove();
    $('.summary').remove();
    $('.total').remove();
    $('#btn-status-confirm').css('display','inline-block');
    $('#btn-status-complete').css('display','inline-block');
}