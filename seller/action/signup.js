// $("#form_signup").submit(function(e){     
//     e.preventDefault();
//     $.ajax({
//         url: 'http://localhost/b2bmalaysia/server/seller/authentication/signup',
//         type: 'post',
//         dataType: 'json',
//         data: $("#form_signup").serialize(),
//         /*beforeSend: function() {
//             var text = `<i class="fa fa-spinner fa-spin"></i> Loading...`;
//             $('#btn_signup').html(text);
//         },*/
//         success: function(responses) {
//           if(responses.code==200){
//             //window.location.href = 'login';
//             $('#txt_success_header').html('Success.');
//             $('#txt_success_description').html(responses.data);
//             $('#modal_success').modal('show');
//           }else{
//             alert(responses.message+' '+responses.errors);
//             window.location.href = 'signup';
//             /*$('#txt_error_header').html(responses.message);
//             $('#txt_error_description').html(responses.errors);
//             $('#modal_error').modal('show');*/
//           }
//         },
//     });
// });

$("#form_signup").submit(function(e){ 
    e.preventDefault()
    $.ajax({
        method: 'POST',
        url: 'http://localhost/b2bmalaysia/server/seller/authentication/signup_post',
        data: $("#form_signup").serialize(),
        dataType: 'JSON',
        success: function(response) {
            if (response.code == '200') {
                //window.location.href = 'login';
                $('#txt_success_header').html('Success.');
                $('#txt_success_description').html(response.data);
                $('#modal_success').modal('show');
            } else {
                // alert('Error: ' + response.error.message);
                //window.location.href = 'signup';
                $('#txt_error_header').html(response.message);
                $('#txt_error_description').html(response.errors);
                $('#modal_error').modal('show');
            }
        }
    });
});