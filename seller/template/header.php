<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SELLER PAGE</title>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/img/favicon.ico"/>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/DataTables/dataTables.bootstrap.css" rel="stylesheet">


    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../assets/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="../assets/css/additional.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Core JavaScript -->
    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../vendor/DataTables/jquery.dataTables.js"></script>
    <script src="../vendor/DataTables/dataTables.bootstrap.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../vendor/raphael/raphael.min.js"></script>
    <script src="../vendor/morrisjs/morris.min.js"></script>

    <!-- Flot Charts JavaScript -->
    <script src="../vendor/flot/excanvas.min.js"></script>
    <script src="../vendor/flot/jquery.flot.js"></script>
    <script src="../vendor/flot/jquery.flot.pie.js"></script>
    <script src="../vendor/flot/jquery.flot.resize.js"></script>
    <script src="../vendor/flot/jquery.flot.time.js"></script>
    <script src="../vendor/flot-tooltip/jquery.flot.tooltip.min.js"></script>

    <!-- Datepicker JavaScript -->
    <!-- <script src="../assets/js/jquery-ui.js"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="../assets/js/sb-admin-2.min.js"></script>
    <script src="../assets/js/jquery.mask.min.js"></script>
    
    <!-- Additional JavaScript -->
    <script src="../assets/js/additional.js"></script>

    <script type="text/javascript">
      $(document).ready(function () {
        $('.panel-collapse').on('show.bs.collapse', function () {
          $(this).siblings('.panel-heading').addClass('active');
        });

        $('.panel-collapse').on('hide.bs.collapse', function () {
          $(this).siblings('.panel-heading').removeClass('active');
        });
      });
    </script>
  </head>
  <body>
    <div id="wrapper">

      <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0; background-color: #0e5077">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home">B2B MALAYSIA HOLIDAYS | SELLER</a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
          <span id="txt_name" style="color: white">You're login as </span>
          <span id="user_name" style="color: white">...</span>
          <span style="margin-left: 10px; border-left: solid 1px #ddd;"></span>
          <li class="dropdown custom_dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: white;padding: 15px;border:none;">
              Account&nbsp&nbsp<i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
              <li>
                <a href="account"><i class="fa fa-user fa-fw"></i> Account Setting</a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="login"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
              </li>
            </ul>
          </li>
        </ul>
        
      </nav>
        <?php include('menu.php'); ?>
        <div class="wrapper-konten">
        