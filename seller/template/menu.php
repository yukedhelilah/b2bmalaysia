
        <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
          <ul class="nav" id="side-menu">
            <li>
              <a href="home"><i class="fa fa-home"></i> Dashboard </a>
            </li>
            <li>
            <a href="#"><i class="fa fa-folder"></i> Product <span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                <li>
                  <a href="product_fne"><i class="fa fa-briefcase"></i> Free & Easy</a>
                  <a onclick="action_add_category()"><i class="fa fa-plus"></i> New Category</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="reservation"><i class="fa fa-shopping-cart "></i> Reservation </a>
            </li>
            <li>
            <a href="#"><i class="fa fa-gear"></i> Setting <span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                <li>
                  <a href="account"><i class="fa fa-user fa-fw"></i> Account Setting</a>
                </li>
                <li>
                  <a href="login"><i class="fa fa-sign-out"></i>Logout</a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>