<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>LOGIN | B2B Malaysia</title>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/img/favicon.ico"/>

      <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

      <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

      <!-- Datatable CSS -->
    <link href="../vendor/DataTables/datatables.min.css" rel="stylesheet">

      <!-- Custom CSS -->
    <link href="../assets/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="../assets/css/additional.css" rel="stylesheet">

      <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        
      <!-- Core JavaScript -->
      <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

      <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

      <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

      <!-- Datatable JavaScript -->
    <script src="../vendor/DataTables/datatables.min.js"></script>

      <!-- Custom Theme JavaScript -->
    <script src="../assets/js/sb-admin-2.min.js"></script>
        
      <!-- Additional JavaScript -->
    <script src="../assets/js/additional.js"></script>
  </head>
  <body>
    <div class="container">
      <div class="row" style="margin-top:10%">
        <div class="col-12">
          <center>
            <h3>B2B Malaysia Holidays</h3>
            Selamat datang di B2B Malaysia Holidays
          </center>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <div class="login-panel panel panel-primary active">
            <div class="panel-heading">
              <h3 class="panel-title">Login your account</h3>
            </div>
            <div class="panel-body">
              <form role="form" autocomplete="off" id="form_signin">
              <fieldset>
                <div class="form-group">
                  <input class="form-control" placeholder="Seller ID" name="companySellerID" id="sellerid" type="text" autofocus>
                </div>
                <div class="form-group">
                  <input class="form-control" placeholder="Username" name="companyUsername" id="username" type="text" autofocus>
                </div>
                <div class="form-group">
                  <input class="form-control" placeholder="Password" name="companyPassword" id="password" type="password" value="">
                </div>
                  <button class="btn btn-primary btn-block btn-signin" id="btn_signin">Sign In</button>
                <p class="mg-b-0">Don't have an account? <a href="signup">Sign Up</a></p>
              </fieldset>
              </form>
              
              <div id="modal_error" class="modal fade" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog" role="document">
                  <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                      <i class="icon icon ion-ios-close-outline tx-100 tx-danger lh-1 mg-t-20 d-inline-block"></i>
                      <h4 class="tx-danger mg-b-20" id="txt_error_header">Error: Cannot process your entry!</h4>
                      <p class="mg-b-20 mg-x-20" id="txt_error_description">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration.</p>
                      <a href="login"><button type="button" class="btn btn-danger pd-x-25">Continue</button></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="../action/login.js"></script>
  </body>
</html>
