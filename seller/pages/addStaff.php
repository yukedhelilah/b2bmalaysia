<?php include('../template/header.php'); ?>

	<div class="container">
		<div class="row">
	        <div class="col-sm-6">
	            <h3 class="main-tittle">Account</h3>
	        </div>
	        <div class="col-sm-6">
	            <nav aria-label="breadcrumb"  class="menutop">
	                <ol class="breadcrumb">
	                    <li class="breadcrumb-item"><a href="home">Home</a></li>
	                    <li class="breadcrumb-item"><a href="account">Account Setting</a></li>
	                    <li class="breadcrumb-item active" aria-current="page">Staff Setting</li>
	                </ol>
	            </nav>
	        </div>
    	</div>
		<div class="row">
	        <div class="col-lg-12">
	            <div class="panel panel-default">
	                <div class="panel-heading">
						<span id="staffusername">...</span>
	                    <div class="pull-right">
	                        <button class="btn btn-primary btn-xs" onclick="action_add_staff()"><i class="fa fa-plus"></i> New Staff</button>
	                        <button class="btn btn-warning btn-xs" onclick="loadStaff()"><i class="fa fa-refresh"></i> Refresh</button>
	                    </div>	
					</div>
	                <div class="panel-body">
	                    <div class="table-responsive">
	                    <table id="tb_staff" class="table table-striped table-bordered" style="width:100%">
	                        <thead>
	                            <tr>
	                                <th>No</th>
	                                <th>Staff Name</th>
	                                <th>Staff Username</th>
	                                <th>Staff Password</th>
	                                <th>Action</th>
	                            </tr>
	                        </thead>
	                        <tbody></tbody>
	                    </table>
	                    </div>
	                </div>
				</div>
			</div>
		</div>

	    <div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                    <h4 class="modal-title" id="myModalLabel">Add Staff</h4>
	                </div>
	                <div class="modal-body">
	                    <form id="myForm">
	                        <div class="form-group row">
	                            <label for="staffName" class="col-sm-3 col-form-label">Staff Name</label>
	                            <div class="col-sm-9">
	                                <input type="text" class="form-control" id="staffName" name="staffName" placeholder="Staff Name" required="required">
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label for="noProduct" class="col-sm-3 col-form-label">Staff Username</label>
	                            <div class="col-sm-9">
	                                <input type="text" class="form-control" id="staffUsername" name="staffUsername" placeholder="Staff Username" required="required">
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label for="noProduct" class="col-sm-3 col-form-label">Password</label>
	                            <div class="col-sm-9">
	                                <input type="Password" class="form-control" id="staffPassword" name="staffPassword" readonly="readonly" minlength="6" maxlength="6"  onkeypress="return hanyaAngka(event)">
	                            </div>
	                        </div>
	                    </form>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" id="btn-save-staff" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Save</button>
	                    <input type="reset" id="btn-reset" class="btn btn-sm btn-default" data-dismiss="modal" value="Cancel" />
	                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                    <button type="button" class="btn btn-primary">Save changes</button> -->
	                </div>
	            </div>
	        </div>
	    </div>

	    <div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                    <h4 class="modal-title" id="myModalLabel">Add Staff</h4>
	                </div>
	                <div class="modal-body">
	                    <form id="form_staff">
                        	<input type="hidden" name="idStaff" id="idStaff" value="">
	                        <div class="form-group row">
	                            <label for="Name" class="col-sm-3 col-form-label">Staff Name</label>
	                            <div class="col-sm-9">
	                                <input type="text" class="form-control" id="Name" name="Name" required="required" >
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label for="noProduct" class="col-sm-3 col-form-label">Staff Username</label>
	                            <div class="col-sm-9">
	                                <input type="text" class="form-control" id="Username" name="Username" required="required" >
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label for="noProduct" class="col-sm-3 col-form-label">Password</label>
	                            <div class="col-sm-9">
	                                <input type="Password" class="form-control" id="Password" name="Password" readonly="readonly"minlength="6" maxlength="6"  onkeypress="return hanyaAngka(event)">
	                            </div>
	                        </div>
	                    </form>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" id="btn-reset-staff" class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i> Reset</button>
	                    <input type="reset" id="btn-reset" class="btn btn-sm btn-default" data-dismiss="modal" value="Cancel" />
	                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                    <button type="button" class="btn btn-primary">Save changes</button> -->
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

<?php include('new_product.php'); ?>
<?php include('../template/footer.php'); ?>
<script src="../action/account.js"></script>
<script src="../action/product_fne.js"></script>