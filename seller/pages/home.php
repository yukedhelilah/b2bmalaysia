<?php include('../template/header.php'); ?>

<div class="slim-mainpanel">
	<div class="container">
		<div class="row">
		    <div class="col-sm-6">
		        <h3 class="main-tittle">Dashboard</h3>
		    </div>
		    <div class="col-sm-6">
		        <nav aria-label="breadcrumb" style="padding-top:20px;">
		            <ol class="breadcrumb">
		                <li class="breadcrumb-item"><a href="home">Home</a></li>
		                <li class="breadcrumb-item active" aria-current="page">Home</li>
		            </ol>
		        </nav>
		    </div>
		</div>

		<div class="row">
			<div class="col-lg-3 col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-comments fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">26</div>
								<div>New Comments!</div>
							</div>
						</div>
					</div>
					<a href="#">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-6">
				<div class="panel panel-green">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-tasks fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">12</div>
								<div>New Tasks!</div>
							</div>
						</div>
					</div>
					<a href="#">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-6">
				<div class="panel panel-yellow">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-shopping-cart fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">124</div>
								<div>New Orders!</div>
							</div>
						</div>
					</div>
					<a href="#">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-6">
				<div class="panel panel-red">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-support fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">13</div>
								<div>Support Tickets!</div>
							</div>
						</div>
					</div>
					<a href="#">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
		</div>

	    <div class="row">
	        <div class="col-lg-6">
	            <div class="panel panel-default">
	                <div class="panel-heading">
	                    Last Product
	                    <div class="pull-right">
	                        <button class="btn btn-warning btn-xs" onclick="loadCategory()"><i class="fa fa-refresh"></i> Refresh</button>
	                    </div>
	                </div>
	                <div class="panel-body">
	                    <div class="table-responsive">
	                    <table id="tb_category" class="table table-striped table-bordered" style="width:100%">
	                        <thead>
	                            <tr>
	                                <th>No</th>
	                                <th>Category Name</th>
	                                <!-- <th>Image</th> -->
	                                <th>Product</th>
	                                <th>Action</th>
	                            </tr>
	                        </thead>
	                        <tbody></tbody>
	                    </table>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="col-lg-6">
	            <div class="panel panel-default">
	                <div class="panel-heading">
	                    Last Reservation
	                    <div class="pull-right">
	                        <button class="btn btn-warning btn-xs" onclick="loadReservation()"><i class="fa fa-refresh"></i> Refresh</button>
	                    </div>
	                </div>
	                <div class="panel-body">
	                    <div class="table-responsive">
	                    <table id="tb_reservation" class="table table-striped table-bordered" style="width:100%">
	                        <thead>
	                            <tr>
	                                <th>No</th>
	                                <th>Reservation</th>
	                                <th>Contact Person</th>
	                                <th>Status</th>
	                                <th>Action</th>
	                            </tr>
	                        </thead>
	                        <tbody></tbody>
	                    </table>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

    <div class="modal fade" id="modal_add_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="newcat">NEW CATEGORY</h4>
                    <h4 class="modal-title" id="editcat">EDIT CATEGORY</h4>
                </div>
                <div class="modal-body">
                    <form id="form_category">
                        <input type="hidden" name="action" id="action" value="add">
                        <input type="hidden" name="id" id="id" value="">
                        <div class="form-group row">
                            <label for="categoryName" class="col-sm-3 col-form-label">Category Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="categoryName" name="categoryName" placeholder="Category Name" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="location" class="col-sm-3 col-form-label">Location</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="location" name="locations">
                                    <option value="">--- LOCATION ---</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="location" class="col-sm-3 col-form-label">Category Tour</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="tour" name="tours">
                                    <option value="">--- CATEGORY TOURS ---</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="image" class="col-sm-3 col-form-label">Image</label>
                            <div class="col-sm-9">
                                <input type="file" class="form-control" id="userfile2" name="userfile2" placeholder="Image" />
                                <input type="text" name="imageName" id="imageName" style="display: none;">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="noProduct" class="col-sm-3 col-form-label">Include</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" id="include" name="include" placeholder="Include"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="noProduct" class="col-sm-3 col-form-label">Exclude</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" id="exclude" name="exclude" placeholder="Exclude"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Is Publish?</label>
                            <div class="col-sm-9">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" id="yes" name="publish" value="0" />
                                    <label class="form-check-label" for="">
                                        Yes
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" id="no" name="publish" value="1" />
                                    <label class="form-check-label" for="">
                                        No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-save-category" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Save</button>
                    <input type="reset" id="btn-reset" class="btn btn-sm btn-default" data-dismiss="modal" value="Cancel" />
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="view_category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog view">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">PRODUCT</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="action" id="action" value="add">
                    <input type="hidden" name="id" id="id" value="">
                    <div class="row" style="margin-bottom: 5px;">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="col-sm-3">
                                        <img id="img" src="" class="img-responsive">
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="col-sm-12">
                                            <h4 id="cname" style="font-weight: bold;"></h4>
                                        </div>
                                        <div class="col-sm-12">
                                            <h4>Location</h4>
                                            <h5 id="loc" class="subtxt"></h5>
                                        </div>
                                        <div class="col-sm-12">
                                            <h4>Category Tour</h4>
                                            <h5 id="ctour" class="subtxt"></h5>
                                        </div>
                                        <div class="col-sm-12">
                                            <h4>Include</h4>
                                            <h5 id="inc" class="subtxt"></h5>
                                        </div>
                                        <div class="col-sm-12">
                                            <h4>Exclude</h4>
                                            <h5 id="exc" class="subtxt"></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Product
                                    <div class="pull-right">
                                        <button class="btn btn-primary btn-xs" id="btn-add-product"><i class="fa fa-plus"></i> New Product</button>
                                        <!-- <button class="btn btn-warning btn-xs" onclick="loadData()"><i class="fa fa-refresh"></i> Refresh</button> -->
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                    <table id="tb_product" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Product Name</th>
                                                <th>DN</th>
                                                <th>Highlight</th>
                                                <th>Description</th>
                                                <th>Detail</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_success" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header" style="text-align: center;">
                    <h4>Input Product?</h4>
                    <button type="button" id="btn-yes" class="btn btn-sm btn-primary">Yes</button>
                    <button type="button" id="btn-no" class="btn btn-sm btn-primary">No</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_add_product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-x: hidden;overflow-y: auto;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="newprod">NEW PRODUCT</h4>
                    <h4 class="modal-title" id="editprod">EDIT PRODUCT</h4>
                </div>
                <div class="modal-body">
                    <form id="form_product">
                        <input type="hidden" name="action-product" id="action-product" value="">
                        <input type="hidden" name="id-product" id="id-product" value="">
                        <div>
                            <h3 style="margin-top: 0px;margin-bottom: 20px;border-bottom: 1px solid #e5e5e5;">Product Information</h3>
                        </div>
                        <input type="hidden" name="idCat" id="idCat" readonly="readonly">
                        <div class="form-group row">
                            <label for="categoryName" class="col-sm-4 col-form-label">Category Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="category" name="categoryName" placeholder="Category Name" readonly="readonly" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="productName" class="col-sm-4 col-form-label">Product Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="productName" name="productName" placeholder="Product Name" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="highlight" class="col-sm-4 col-form-label">Highlight</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="highlight" name="highlight" placeholder="highlight"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-sm-4 col-form-label">Description</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="description" name="description" placeholder="description"></textarea>
                            </div>
                        </div>

                        <div>
                            <h3 style="margin-top: 40px;margin-bottom: 20px;border-bottom: 1px solid #e5e5e5;">Product Setting</h3>
                        </div>                      
                        <div class="form-group row">
                            <label for="noofdays" class="col-sm-4 col-form-label">No Of Days</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="noofdays" name="noofdays">
                                    <option value="">--- NO OF DAYS ---</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                    <option>8</option>
                                    <option>9</option>
                                    <option>10</option>
                                    <option>11</option>
                                    <option>12</option>
                                    <option>13</option>
                                    <option>14</option>
                                    <option>15</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="noofnights" class="col-sm-4 col-form-label">No Of Nights</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="noofnights" name="noofnights">
                                    <option value="">--- NO OF NIGHTS ---</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                    <option>8</option>
                                    <option>9</option>
                                    <option>10</option>
                                    <option>11</option>
                                    <option>12</option>
                                    <option>13</option>
                                    <option>14</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="validdays" class="col-sm-4 col-form-label">Valid Days</label>
                            <div class="col-sm-8">
                                <input type="checkbox" class="col-sm-1 col-xs-1" value="mon" name="validdays[]">
                                <div class="col-sm-3 col-xs-11">Monday</div>
                                <input type="checkbox" class="col-sm-1 col-xs-1" value="tue" name="validdays[]">
                                <div class="col-sm-3 col-xs-11">Tuesday</div>
                                <input type="checkbox" class="col-sm-1 col-xs-1" value="wed" name="validdays[]">
                                <div class="col-sm-3 col-xs-11">Wednesday</div>
                                <input type="checkbox" class="col-sm-1 col-xs-1" value="thu" name="validdays[]">
                                <div class="col-sm-3 col-xs-11">Thursday</div>
                                <input type="checkbox" class="col-sm-1 col-xs-1" value="fri" name="validdays[]">
                                <div class="col-sm-3 col-xs-11">Friday</div>
                                <input type="checkbox" class="col-sm-1 col-xs-1" value="sat" name="validdays[]">
                                <div class="col-sm-3 col-xs-11">Saturday</div>
                                <input type="checkbox" class="col-sm-1 col-xs-1" value="sun" name="validdays[]">
                                <div class="col-sm-3 col-xs-11">Sunday</div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="validfrom" class="col-sm-4 col-form-label">Valid From</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control date" id="validfrom" name="validfrom" placeholder="dd-mm-yyyy">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="validuntil" class="col-sm-4 col-form-label">Valid Until</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control date" id="validuntil" name="validuntil" placeholder="dd-mm-yyyy">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="blackoutdate" class="col-sm-4 col-form-label">Black Out Date</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control dates" id="blackoutdate" name="blackoutdate[]" placeholder="dd-mm-yyyy">
                                <!-- <input type="text" class="form-control date" placeholder="Pick the multiple dates"> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cutofdays" class="col-sm-4 col-form-label">Cut Of Days</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="cutofdays" name="cutofdays">
                                    <option value="">--- CUT OF DAYS ---</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                    <option>8</option>
                                    <option>9</option>
                                    <option>10</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="allotment" class="col-sm-4 col-form-label">Allotment</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="allotment" name="allotment">
                                    <option value="">--- ALLOTMENT ---</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                    <option>8</option>
                                    <option>9</option>
                                    <option>10</option>
                                    <option>11</option>
                                    <option>12</option>
                                    <option>13</option>
                                    <option>14</option>
                                    <option>15</option>
                                    <option>16</option>
                                    <option>17</option>
                                    <option>18</option>
                                    <option>19</option>
                                    <option>20</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Limit Guarantee</label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" id="limitguarantee" name="limitguarantee" placeholder="0 Days" min="1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Is Publish?</label>
                            <div class="col-sm-8" id="autoconfirm">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="productpublish" value="0" />
                                        Yes
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="productpublish" value="1" />
                                        No
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Is Product Auto Confirm?</label>
                            <div class="col-sm-8" id="autoconfirm">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="autoconfirm" value="0" />
                                        Yes
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="autoconfirm" value="1" />
                                        No
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label limitconfirm" style="display: none">Limit Confirm</label>
                            <div class="col-sm-8 limitconfirm" style="display: none">
                                <input type="number" class="form-control" id="limitconfirm" name="limitconfirm" placeholder="0 Days" min="1">
                            </div>
                        </div>

                        <div>
                            <h3 style="margin-top: 40px;margin-bottom: 20px;border-bottom: 1px solid #e5e5e5;">Product Pricing</h3>
                        </div>  
                        <div class="form-group row">
                            <label for="single" class="col-sm-4 col-form-label">Single (MYR)</label>
                            <div class="col-sm-8">
                                <input type="text" min="0" class="form-control myr" id="single" name="single"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="adulttwin" class="col-sm-4 col-form-label">Adult Twin (MYR)</label>
                            <div class="col-sm-8">
                                <input type="text" min="0" class="form-control myr" id="adulttwin" name="adulttwin"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="adulttriple" class="col-sm-4 col-form-label">Adult Triple (MYR)</label>
                            <div class="col-sm-8">
                                <input type="text" min="0" class="form-control myr" id="adulttriple" name="adulttriple"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="childwithadult" class="col-sm-4 col-form-label">Child With Adult (MYR)</label>
                            <div class="col-sm-8">
                                <input type="text" min="0" class="form-control myr" id="childwithadult" name="childwithadult"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="childwithbed" class="col-sm-4 col-form-label">Child With Bed (MYR)</label>
                            <div class="col-sm-8">
                                <input type="text" min="0" class="form-control myr" id="childwithbed" name="childwithbed"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="childnobed" class="col-sm-4 col-form-label">Child No Bed (MYR)</label>
                            <div class="col-sm-8">
                                <input type="text" min="0" class="form-control myr" id="childnobed" name="childnobed"/>
                            </div>
                        </div>
                        <div>
                            <h3 style="margin-top: 0px;margin-bottom: 20px;border-bottom: 1px solid #e5e5e5;">Room Setting</h3>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="checkbox" class="col-xs-1" value="1" name="fneroom">
                                <label class="col-xs-11 col-form-label">Adult Single</label>
                                <input type="checkbox" class="col-xs-1" value="2" name="fneroom">
                                <label class="col-xs-11 col-form-label">Adult + Adult (TwinSharing)</label>
                                <input type="checkbox" class="col-xs-1" value="3" name="fneroom">
                                <label class="col-xs-11 col-form-label">Adult + Adult + Adult (Triple)</label>
                                <input type="checkbox" class="col-xs-1" value="4" name="fneroom">
                                <label class="col-xs-11 col-form-label">Adult + Adult + Child No Bed</label>
                                <input type="checkbox" class="col-xs-1" value="5" name="fneroom">
                                <label class="col-xs-11 col-form-label">Adult + Adult + Child With Bed</label>
                                <input type="checkbox" class="col-xs-1" value="6" name="fneroom">
                                <label class="col-xs-11 col-form-label">Adult + Adult + Child No Bed + Child No Bed</label>
                                <input type="checkbox" class="col-xs-1" value="7" name="fneroom">
                                <label class="col-xs-11 col-form-label">Adult + Adult + Child with Bed + Child No Bed</label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-save-product" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Save</button>
                    <input type="reset" id="btn-reset" class="btn btn-sm btn-default" data-dismiss="modal" value="Cancel" />
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="view_product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="padding-left: 0px !important;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><span id="pName"></span> / <span id="days"></span>D<span id="nights"></span>N</h4>
                </div>
                <div class="modal-body">
                    <!-- <div class="col-sm-12" style="margin-bottom: 40px;">
                        <div class="col-sm-8">
                            <div class="col-sm-12">
                                <h4>Description</h4>
                                <h5 id="desc" class="subtxt"></h5>
                            </div>
                            <div class="col-sm-12">
                                <h4>Highlight</h4>
                                <h5 id="hl" class="subtxt"></h5>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-sm-12">
                        <table id="inf" class="table table-bordered" style="width:100%">
                            <tr>
                                <th colspan="2">Information</th>
                            </tr>
                            <tr>
                                <th>Highlights</th>
                                <td id="hl"></td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td id="desc"></td>
                            </tr>
                            <tr>
                                <th>Validity Period</th>
                                <td id="vPeriod"></td>
                            </tr>
                            <tr>
                                <th>Valid Days</th>
                                <td id="vDays"></td>
                            </tr>
                            <tr>
                                <th>Cut Off Days</th>
                                <td id="coDays"></td>
                            </tr>
                            <tr>
                                <th>Black Out Dates</th>
                                <td id="boDates"></td>
                            </tr>
                            <tr>
                                <th>Allotment</th>
                                <td id="allot"></td>
                            </tr>
                            <tr>
                                <th>Limit Guarantee</th>
                                <td><span id="lg"></span> Days</td>
                            </tr>
                            <tr>
                                <th>Publish</th>
                                <td id="pub"></td>
                            </tr>
                            <tr>
                                <th>Auto Confirm</th>
                                <td id="con"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-12">
                        <table class="table table-bordered" style="width:100%">
                            <tr>
                                <th colspan="6">Price (MYR)</th>
                            </tr>
                            <tr>
                                <th style="width: 35%;">Single</th>
                                <td style="width: 65%;"><span id="psingle"></span> MYR</td>
                            </tr>
                            <tr>
                                <th>Adult Twin</th>
                                <td><span id="padulttwin"></span> MYR</td>
                            </tr>
                            <tr>
                                <th>Adult Triple</th>
                                <td><span id="padulttriple"></span> MYR</td>
                            </tr>
                            <tr>
                                <th>Cwa</th>
                                <td><span id="pcwa"></span> MYR</td>
                            </tr>
                            <tr>
                                <th>Cwb</th>
                                <td><span id="pcwb"></span> MYR</td>
                            </tr>
                            <tr>
                                <th>Cnb</th>
                                <td><span id="pcnb"></span> MYR</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-12">
                        <table class="table table-bordered" style="width:100%" id="room">
                            <tr>
                                <th colspan="2">Room</th>
                            </tr>
                        </table>
                    </div>

                    <!-- <table class="table table-striped table-bordered" style="width:100%">
                        <tr>
                            <th>Product Name</th>
                            <td id="pName"></td>
                        </tr>
                        <tr>                            
                            <th>Highlight</th>
                            <td id="hl"></td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td id="desc"></td>
                        </tr>
                    </table> -->
                </div>
                <div class="modal-footer" style="border: none;">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header" style="text-align: center;">
                    <input type="hidden" name="tipe" id="tipe">
                    <input type="hidden" name="delete" id="delete">
                    <h4>Are you sure?</h4>
                    <button type="button" id="btn-delete" class="btn btn-sm btn-primary">Yes</button>
                    <button type="button" id="btn-cancel" class="btn btn-sm btn-primary">No</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_departure" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="panel panel-default" style="margin-bottom: 0px">
                    <div class="panel-heading">
                        Departure
                    </div>
                    <div class="panel-body doesnt_exist" style="color: grey;text-align: center;">
                        data is doesnt exist
                    </div>
                    <div class="panel-body exist">
                        <ul class="nav nav-tabs tabDeparture">
                        </ul>

                        <div class="tab-content contentDeparture">
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_pickup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Contact</h4>
                </div>
                <div class="modal-body" style="color: grey">
                            <form id="form_pickup">
                                <input type="hidden" name="actionPickup" id="actionPickup">
                                <input type="hidden" name="resID" id="resID">
                                <input type="hidden" name="depID" id="depID">
                                <input type="hidden" name="pickID" id="pickID">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="pickup_name" name="pickup_name" placeholder="Name" required="required" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Contact</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="pickup_number" name="pickup_number" placeholder="Contact" required="required" onkeypress="return hanyaAngka(event)"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Details</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="pickup_details" name="pickup_details" placeholder="Details" required="required"></textarea>
                                    </div>
                                </div>
                            </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-save-pickup" class="btn btn-sm btn-primary">Save</button>
                    <input type="reset" id="btn-reset" class="btn btn-sm btn-default" data-dismiss="modal" value="Cancel" />
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top:50%;transform: translateY(-50%);">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Status</h4>
                </div>
                <div class="modal-body" style="color: grey">
                    <form style="text-align: center;">
                        <input type="hidden" name="idStatus" id="idStatus">
                        <button class="btn btn-primary btn-md" id="btn-status-confirm">Confirm</button>
                        <button class="btn btn-success btn-md" id="btn-status-complete">Complete</button>
                        <!-- <button class="btn btn-danger btn-md" id="btn-status-cancel">Cancel</button> -->
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog view">
            <div class="modal-content">
                    <div class="panel panel-default" style="margin-bottom: 0px">
                        <div class="panel-heading">
                            Basic Tabs
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#home" data-toggle="tab">Information</a>
                                </li>
                                <li><a href="#profile" data-toggle="tab">Guest</a>
                                </li>
                                <li><a href="#messages" data-toggle="tab">Pricing</a>
                                </li>
                                <!-- <li><a href="#settings" data-toggle="tab">Settings</a>
                                </li> -->
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="home">
                                    <div class="panel panel-default">
                                        <div class="panel-body" style="color: grey">
                                            <div class="col-lg-6">
                                                <table class="table table-bordered" style="width:100%;">
                                                    <tr>
                                                        <th colspan="2">GENERAL</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Booking Code</td>
                                                        <td id="bCode"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Booking Date</td>
                                                        <td id="bDate"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-lg-6">
                                                <table class="table table-bordered" style="width:100%;margin-bottom: 0px">
                                                    <tr>
                                                        <th colspan="2">PRODUCT</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Category</td>
                                                        <td id="bCategory"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Product</td>
                                                        <td id="bProduct"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Days</td>
                                                        <td id="bDays"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Arrival</td>
                                                        <td id="arrival"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Room</td>
                                                        <td id="bRoom"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profile">
                                    <div class="panel panel-default">
                                        <div class="panel-body" style="color: grey">
                                            <div class="col-lg-12">
                                                <table class="table table-bordered" style="width:100%;" id="guest">
                                                    <tr>
                                                        <th colspan="5">GUEST</th>
                                                    </tr>
                                                    <tr>
                                                        <td>No</td>
                                                        <td>Name</td>
                                                        <td>Passport No</td>
                                                        <td>Passport Exp</td>
                                                        <td>DOB</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-lg-12">
                                                <table class="table table-bordered" style="width:100%;margin-bottom: 0px">
                                                    <tr>
                                                        <th colspan="2">CONTACT PERSON</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Name</td>
                                                        <td id="cName"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mobile</td>
                                                        <td id="cMobile"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Email</td>
                                                        <td id="cEmail"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="messages">
                                    <div class="panel panel-default">
                                        <div class="panel-body" style="color: grey">
                                            <div class="col-lg-12">
                                                <table class="table table-bordered" style="width:100%;margin-bottom: 0px" id="summary">
                                                    <tr>
                                                        <th colspan="4">SUMMARY</th>
                                                    </tr>
                                                    <tr>
                                                        <td>No</td>
                                                        <td>Guest</td>
                                                        <td>Category</td>
                                                        <td>Nett Price</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="settings">
                                    <h4>Settings Tab</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="modal_setting_password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-x: hidden;overflow-y: auto;">
        <div class="modal-dialog">
            <div class="modal-content">	
    		    <div class="row" id="staff_Profile">
    		      <div class="col-lg-12">
    		        <div class="panel panel-default">
    		          <div class="panel-heading">
    		            Staff Profile
    		          </div>
    		          <div class="panel-body">
    		            <form class="form-horizontal" id="staffProfile" autocomplete="off" data-parsley-trigger="keyup" data-parsley-validate>
    					  <div class="form-group row">
    		                <label for="categoryName" class="control-label col-sm-2">Staff Name</label>
    		                <div class="col-sm-10">
    		                  <input type="text" class="form-control" name="staffName" id="staffName" required="required">
    		                </div>
    		              </div>
    					  <div class="form-group row">
    		                <label for="categoryName" class="control-label col-sm-2">Staff Username</label>
    		                <div class="col-sm-10">
    		                  <input type="text" class="form-control" name="staffUsername" id="staffUsername" required="required" readonly="readonly">
    		                </div>
    		              </div>
    					  <div class="form-group row">
    		                <label for="categoryName" class="control-label col-sm-2">Password</label>
    		                <div class="col-sm-10">
    		                  <input type="password" class="form-control" name="password" id="password" placeholder="password" minlength="6" maxlength="6"  onkeypress="return hanyaAngka(event)">
    		                </div>
    		              </div>
    		              <button class="btn btn-primary btn-block btn-signin" type="submit" id="update_staff_password">Update</button>
    		            </form>
    		          </div>
    		        </div>
    		      </div>
    		    </div>
    		</div>
    	</div>
    </div>
</div>

<?php include('../template/footer.php'); ?>
<script src="../action/home.js"></script>
<!-- <script src="../assets/data/morris-data.js"></script> -->