<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>AGENT | SIGN UP</title>

        <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Datatable CSS -->
    <link href="../vendor/DataTables/datatables.min.css" rel="stylesheet">

        <!-- Custom CSS -->
    <link href="../assets/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="../assets/css/additional.css" rel="stylesheet">

        <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        
        <!-- Core JavaScript -->
        <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

        <!-- Datatable JavaScript -->
    <script src="../vendor/DataTables/datatables.min.js"></script>

        <!-- Custom Theme JavaScript -->
    <script src="../assets/js/sb-admin-2.min.js"></script>
        
        <!-- Additional JavaScript -->
    <script src="../assets/js/additional.js"></script>
        
    <!-- </head> -->
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="login-panel panel panel-primary active">
            <div class="panel-heading">
              <h3 class="panel-title">AGENT SIGN UP</h3>
            </div>
            <div class="panel-body">
              <form id="form_signup" autocomplete="off" data-parsley-trigger="keyup" data-parsley-validate>
              <fieldset>
                <legend>
                    <h5 class="signin-title-primary" style="text-transform: uppercase; font-weight: bold;">Travel Agent Details</h5>
                </legend>
                <div class="form-group">
                  <input type="text" class="form-control" name="companyName" id="companyName" placeholder="Company Name (required)" required="required">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="companyAddress" id="companyAddress" placeholder="Address (required)" required="required">
                </div>
                <div class="form-group">
                  <div class="col-md-4 col-xs-4 nopad">
                    <input type="text" class="form-control noradright" name="companyCountry" id="companyCountry" placeholder="Country (required)" value="MALAYSIA" readonly="readonly">
                  </div>
                  <div class="col-md-8 col-xs-8 nopad">
                    <input type="text" class="form-control noradleft" name="companyCity" id="companyCity" placeholder="City (required)" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control martop" name="companyZipcode" id="companyZipcode" placeholder="Zipcode">
                </div>
                <div class="form-group">
                  <div class="col-md-6 noleft">
                    <div class="col-md-2 col-xs-2 nopad">
                      <input type="text" class="form-control noradright" value="+62" readonly="readonly">
                    </div>
                    <div class="col-md-10 col-xs-10 nopad">
                      <input type="text" class="form-control noradleft" name="companyPhone" id="companyPhone" placeholder="Phone" data-parsley-validation-threshold="1" data-parsley-trigger="keyup" data-parsley-type="digits">
                    </div>
                  </div>
                  <div class="col-md-6 noright">
                    <div class="col-md-2 col-xs-2 nopad">
                      <input type="text" class="form-control noradright" value="+62" readonly="readonly">
                    </div>
                    <div class="col-md-10 col-xs-10 nopad">
                      <input type="text" class="form-control noradleft" name="companyFax" id="companyFax" placeholder="Fax" data-parsley-validation-threshold="1" data-parsley-trigger="keyup" data-parsley-type="digits">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control martop em" name="companyEmail" id="companyEmail" placeholder="Email (required)" required="required">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="companyLicence" id="companyLicence" placeholder="Company licence no (required)" required="required">
                </div>
              </fieldset>

              <fieldset>
                  <legend>
                    <h5 class="signin-title-primary" style="text-transform: uppercase; font-weight: bold;">Contact Person</h5>
                  </legend>
                <div class="form-group">
                  <input type="text" class="form-control" name="contactName" id="contactName" placeholder="Name (required)" required="required">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="contactPosition" id="contactPosition" placeholder="Position (required)" required="required">
                </div>
                <div class="form-group">
                  <div class="col-md-6 noleft">
                    <div class="col-md-2 col-xs-2 nopad">
                      <input type="text" class="form-control noradright" value="+62" readonly="readonly">
                    </div>
                    <div class="col-md-10 col-xs-10 nopad">
                      <input type="text" class="form-control noradleft" name="contactMobile" id="contactMobile" placeholder="Mobile number (Required)" data-parsley-validation-threshold="1" data-parsley-trigger="keyup" data-parsley-type="digits" required="required">
                    </div>
                  </div>
                  <div class="col-md-6 noright">
                    <div class="col-md-2 col-xs-2 nopad">
                      <input type="text" class="form-control noradright" value="+62" readonly="readonly">
                    </div>
                    <div class="col-md-10 col-xs-10 nopad">
                      <input type="text" class="form-control noradleft" name="contactWhatsapp" id="contactWhatsapp" placeholder="Whatsapp number (Required)" data-parsley-validation-threshold="1" data-parsley-trigger="keyup" data-parsley-type="digits" required="required">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control martop em" name="contactEmail" id="contactEmail" placeholder="Email (required)" required="required">
                </div>
              </fieldset>

              <fieldset>
                  <legend>
                    <h5 class="signin-title-primary" style="text-transform: uppercase; font-weight: bold;">Account</h5>
                  </legend>
                <div class="form-group">
                  <input type="email" class="form-control" name="emailAddress" id="emailAddress" placeholder="Email Address" required="required">
                </div>
              </fieldset>
                <button class="btn btn-primary btn-block btn-signin" type="submit" id="btn_signup">Sign Up</button>
                <p>Already have an account? <a href="login">Sign In</a></p>
              </form>
              <div id="modal_success" class="modal fade" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog" role="document">
                  <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                      <i class="icon ion-ios-checkmark-outline tx-100 tx-success lh-1 mg-t-20 d-inline-block"></i>
                      <h4 class="tx-success tx-semibold mg-b-20" id="txt_success_header">Congratulations!</h4>
                      <div id="txt_success_header"></div>
                      <div id="txt_success_description"></div><br>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button> -->
                      <!-- <p class="mg-b-20 mg-x-20" id="txt_success_description">default username : superid<br>default password : 12345678</p> -->
                      <a href="login"><button type="button" class="btn btn-success pd-x-25">Continue</button></a>
                    </div><!-- modal-body -->
                  </div><!-- modal-content -->
                </div><!-- modal-dialog -->
              </div>

              <div id="modal_error" class="modal fade" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog" role="document">
                  <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button> -->
                      <i class="icon icon ion-ios-close-outline tx-100 tx-danger lh-1 mg-t-20 d-inline-block"></i>
                      <h4 class="tx-danger mg-b-20" id="txt_error_header">Error: Cannot process your entry!</h4>
                      <div id="txt_error_header"></div>
                      <div id="txt_error_description"></div><br>
                      <a href="signup"><button type="button" class="btn btn-danger pd-x-25">Continue</button></a>
                    </div><!-- modal-body -->
                  </div><!-- modal-content -->
                </div><!-- modal-dialog -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="../action/signup.js"></script>
  </body>
</html>
