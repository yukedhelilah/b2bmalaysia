<?php include('../template/header.php'); ?>
	<div class="container">
		<div class="row">
      <div class="col-sm-6">
        <h3 class="main-tittle">Account</h3>
      </div>
      <div class="col-sm-6" id="superid">
        <nav aria-label="breadcrumb" class="menutop">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="home">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Account Setting</li>
            <li class="breadcrumb-item" ><a href="addStaff">Staff Setting</a></li>
          </ol>
        </nav>
      </div>
      <div class="col-md-6" id="staff">
        <nav aria-label="breadcrumb" class="menutop">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="home">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Account Setting</li>
          </ol>
        </nav>
      </div>
    </div>
    <div class="row" id="company_Profile">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            Company Profile
					</div>
          <div class="panel-body">
            <form class="form-horizontal" id="companyProfile" autocomplete="off" data-parsley-trigger="keyup" data-parsley-validate>
              <div class="col-md-6">
              <fieldset>
						    <div class="form-group row">
                  <label for="categoryName" class="control-label col-md-3">Company Name</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" name="companyName" id="companyName" placeholder="Company Name (required)" required="required">
                    </div>
                </div>
						    <div class="form-group row">
                  <label for="categoryName" class="control-label col-md-3">Address</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" name="companyAddress" id="companyAddress" placeholder="Address (required)" required="required">
                  </div>
                </div>
						    <div class="form-group row">
                  <label for="categoryName" class="control-label col-md-3">Country / City</label>
                  <div class="col-md-9">
	                  <div class="col-md-6 col-xs-6 nopad" style="padding-right: 10px;">
	                    <input type="text" class="form-control" name="companyCountry" id="companyCountry" placeholder="Country (required)" value="MALAYSIA">
	                  </div>
	                  <div class="col-md-6 col-xs-6 nopad" style="padding-left: 10px;" >
	                    <input type="text" class="form-control" name="companyCity" id="companyCity" placeholder="City (required)" required="required">
	                  </div>
                  </div>
                </div>
						    <div class="form-group row">
                  <label for="categoryName" class="control-label col-md-3">Zipcode</label>
                  <div class="col-md-9">
                    <div class="col-md-6 col-xs-6 nopad" style="padding-right: 10px;">
                      <input type="text" class="form-control" name="companyZipcode" id="companyZipcode" placeholder="Zipcode">
                    </div>
                  </div>
                </div>
						    <div class="form-group row">
                  <label for="categoryName" class="control-label col-md-3">Phone</label>
                  <div class="col-md-9">
	                  <div class="col-md-2 col-xs-2 nopad">
	                    <input type="text" class="form-control noradright" value="+60" readonly="readonly">
	                  </div>
	                  <div class="col-md-10 col-xs-10 nopad">
	                    <input type="text" class="form-control noradleft" name="companyPhone" id="companyPhone" placeholder="Phone" data-parsley-validation-threshold="1" data-parsley-trigger="keyup" data-parsley-type="digits">
	                  </div>
                  </div>
                </div>
						    <div class="form-group row">
                  <label for="categoryName" class="control-label col-md-3">Fax</label>
                  <div class="col-md-9">
	                  <div class="col-md-2 col-xs-2 nopad">
	                    <input type="text" class="form-control noradright" value="+60" readonly="readonly">
	                  </div>
	                  <div class="col-md-10 col-xs-10 nopad">
	                    <input type="text" class="form-control noradleft" name="companyFax" id="companyFax" placeholder="Fax" data-parsley-validation-threshold="1" data-parsley-trigger="keyup" data-parsley-type="digits">
	                  </div>
                  </div>
                </div>
						    <div class="form-group row">
                  <label for="categoryName" class="control-label col-md-3">Email</label>
                  <div class="col-md-9">
                    <input type="email" class="form-control" name="companyEmail" id="companyEmail" placeholder="Email (required)" required="required">
                  </div>
                </div>
						    <div class="form-group row">
                  <label for="categoryName" class="control-label col-md-3">License Number</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" name="companyLicence" id="companyLicence" placeholder="Company licence no (required)" required="required">
                  </div>
                </div>
              </fieldset>
              </div>
              <div class="col-md-6">
              <fieldset>
						    <div class="form-group row">
                  <label for="categoryName" class="control-label col-md-3">Contact Name</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" name="contactName" id="contactName" placeholder="Name (required)" required="required">
                  </div>
                </div>
						    <div class="form-group row">
                  <label for="categoryName" class="control-label col-md-3">Contact Position</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" name="contactPosition" id="contactPosition" placeholder="Position (required)" required="required">
                  </div>
                </div>
						    <div class="form-group row">
                  <label for="categoryName" class="control-label col-md-3">Contact Mobile</label>
                  <div class="col-md-9">
	                  <div class="col-md-2 col-xs-2 nopad">
	                      <input type="text" class="form-control noradright" value="+60" readonly="readonly">
	                  </div>
	                  <div class="col-md-10 col-xs-10 nopad">
	                    <input type="text" class="form-control noradleft" name="contactMobile" id="contactMobile" placeholder="Mobile number (Required)" data-parsley-validation-threshold="1" data-parsley-trigger="keyup" data-parsley-type="digits" required="required">
	                  </div>
                  </div>
                </div>
						    <div class="form-group row">
                  <label for="categoryName" class="control-label col-md-3">Contact Whatsapp</label>
                  <div class="col-md-9">
	                  <div class="col-md-2 col-xs-2 nopad">
	                    <input type="text" class="form-control noradright" value="+60" readonly="readonly">
	                  </div>
	                  <div class="col-md-10 col-xs-10 nopad">
	                    <input type="text" class="form-control noradleft" name="contactWhatsapp" id="contactWhatsapp" placeholder="Whatsapp number (Required)" data-parsley-validation-threshold="1" data-parsley-trigger="keyup" data-parsley-type="digits" required="required">
	                  </div>
                  </div>
                </div>
						    <div class="form-group row">
                  <label for="categoryName" class="control-label col-md-3">Contact Email</label>
                  <div class="col-md-9">
                    <input type="email" class="form-control" name="contactEmail" id="contactEmail" placeholder="Email (required)" required="required">
                  </div>
                </div>
              </fieldset>
              <fieldset>
						    <div class="form-group row">
                  <label for="categoryName" class="control-label col-md-3">Email Address</label>
                  <div class="col-md-9">
                    <input type="email" class="form-control" name="emailAddress" id="emailAddress" placeholder="Email Address" required="required">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="categoryName" class="control-label col-md-3">Seller ID</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" name="sellerID" id="sellerID" readonly="readonly">
                    </div>
                </div>
              </fieldset>
              <div class="form-group row">
                <label for="categoryName" class="control-label col-md-3"></label>
                <div class="col-md-3">
                  <button class="btn btn-primary btn-block btn-signin" type="submit" id="update_company_profile">Update</button>
                </div>
              </div>
              </div>
            </form>
          </div>
				</div>
			</div>
		</div>
    <div class="row" id="staff_Profile">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            Staff Profile
          </div>
          <div class="panel-body">
            <form class="form-horizontal col-md-6" id="staffProfile" autocomplete="off" data-parsley-trigger="keyup" data-parsley-validate>
						  <div class="form-group row">
                <label for="categoryName" class="control-label col-md-3">Staff Name</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="staffName" id="staffName" required="required">
                </div>
              </div>
						  <div class="form-group row">
                <label for="categoryName" class="control-label col-md-3">Staff Username</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="staffUsername" id="staffUsername" required="required" readonly="readonly">
                </div>
              </div>
						  <div class="form-group row">
                <label for="categoryName" class="control-label col-md-3">Password</label>
                <div class="col-md-9">
                  <input type="password" class="form-control" name="password" id="password" placeholder="password" required="required" minlength="6" maxlength="6" onkeypress="return hanyaAngka(event)">
                  <h6 style="color: grey">*abaikan jika tidak ingin mengganti password</h6>
                </div>
              </div>
              <div class="form-group row">
                <label for="categoryName" class="control-label col-md-3"></label>
                <div class="col-md-3">
                  <button class="btn btn-primary btn-block btn-signin" type="submit" id="update_staff_profile">Update</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
	</div>
<?php include('new_product.php'); ?>
<?php include('../template/footer.php'); ?>
<script src="../action/account.js"></script>
<script src="../action/product_fne.js"></script>