<?php include('../template/header.php'); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <h3 class="main-tittle">Reservation</h3>
        </div>
        <div class="col-sm-6">
            <nav aria-label="breadcrumb" class="menutop">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="home">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Reservation</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reservation
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                    <table id="tb_reservation" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Reservation</th>
                                <th>Products</th>
                                <th>Contact Person</th>
                                <th>Pax</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_departure" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
	        <div class="panel panel-default" style="margin-bottom: 0px">
	            <div class="panel-heading">
	                Departure
	            </div>
                <div class="panel-body doesnt_exist" style="color: grey;text-align: center;">
                    data is doesnt exist
                </div>
	            <div class="panel-body exist">
	                <ul class="nav nav-tabs tabDeparture">
	                </ul>

	                <div class="tab-content contentDeparture">
	                </div>
		            <!-- /.panel-body -->
		        </div>
		        <!-- /.panel -->
		    </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_pickup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Contact</h4>
            </div>
            <div class="modal-body" style="color: grey">
                    	<form id="form_pickup">
                			<input type="hidden" name="actionPickup" id="actionPickup">
                			<input type="hidden" name="resID" id="resID">
                			<input type="hidden" name="depID" id="depID">
                			<input type="hidden" name="pickID" id="pickID">
	                        <div class="form-group row">
	                            <label class="col-sm-3 col-form-label">Name</label>
	                            <div class="col-sm-9">
	                                <input type="text" class="form-control" id="pickup_name" name="pickup_name" placeholder="Name" required="required" />
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label class="col-sm-3 col-form-label">Contact</label>
	                            <div class="col-sm-9">
	                                <input type="text" class="form-control" id="pickup_number" name="pickup_number" placeholder="Contact" required="required" onkeypress="return hanyaAngka(event)"/>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label class="col-sm-3 col-form-label">Details</label>
	                            <div class="col-sm-9">
	                                <textarea class="form-control" id="pickup_details" name="pickup_details" placeholder="Details" required="required"></textarea>
	                            </div>
	                        </div>
                    	</form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-save-pickup" class="btn btn-sm btn-primary">Save</button>
                <input type="reset" id="btn-reset" class="btn btn-sm btn-default" data-dismiss="modal" value="Cancel" />
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top:50%;transform: translateY(-50%);">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Status</h4>
            </div>
            <div class="modal-body" style="color: grey">
            	<form style="text-align: center;">
            		<input type="hidden" name="idStatus" id="idStatus">
            		<button class="btn btn-primary btn-md" id="btn-status-confirm">Confirm</button>
            		<button class="btn btn-success btn-md" id="btn-status-complete">Complete</button>
            		<!-- <button class="btn btn-danger btn-md" id="btn-status-cancel">Cancel</button> -->
            	</form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog view">
        <div class="modal-content">
	        <div class="panel panel-default" style="margin-bottom: 0px">
	            <div class="panel-heading">
	                Reservation Details
	            </div>
	            <!-- /.panel-heading -->
	            <div class="panel-body">
	                <!-- Nav tabs -->
	                <ul class="nav nav-tabs">
	                    <li class="active"><a href="#home" data-toggle="tab">Information</a>
	                    </li>
	                    <li><a href="#profile" data-toggle="tab">Guest</a>
	                    </li>
	                    <li><a href="#messages" data-toggle="tab">Pricing</a>
	                    </li>
	                    <!-- <li><a href="#settings" data-toggle="tab">Settings</a>
	                    </li> -->
	                </ul>

	                <!-- Tab panes -->
	                <div class="tab-content">
	                    <div class="tab-pane fade in active" id="home">
		                    <div class="panel panel-default">
			                	<div class="panel-body" style="color: grey">
			                		<div class="col-lg-6">
				                        <table class="table table-bordered" style="width:100%;">
					                		<tr>
					                			<th colspan="2">GENERAL</th>
					                		</tr>
					                		<tr>
					                			<td>Booking Code</td>
					                			<td id="bCode"></td>
					                		</tr>
					                		<tr>
					                			<td>Booking Date</td>
					                			<td id="bDate"></td>
					                		</tr>
					                	</table>
			                		</div>
			                		<div class="col-lg-6">
				                        <table class="table table-bordered" style="width:100%;margin-bottom: 0px">
					                		<tr>
					                			<th colspan="2">PRODUCT</th>
					                		</tr>
					                		<tr>
					                			<td>Category</td>
					                			<td id="category"></td>
					                		</tr>
					                		<tr>
					                			<td>Product</td>
					                			<td id="product"></td>
					                		</tr>
					                		<tr>
					                			<td>Days</td>
					                			<td id="days"></td>
					                		</tr>
					                		<tr>
					                			<td>Arrival Date</td>
					                			<td id="arrival"></td>
					                		</tr>
					                		<tr>
					                			<td>Room</td>
					                			<td id="bRoom"></td>
					                		</tr>
					                	</table>
			                		</div>
			                	</div>
		                	</div>
	                    </div>
	                    <div class="tab-pane fade" id="profile">
		                    <div class="panel panel-default">
			                	<div class="panel-body" style="color: grey">
			                		<div class="col-lg-12">
				                        <table class="table table-bordered" style="width:100%;" id="guest">
					                		<tr>
					                			<th colspan="5">GUEST</th>
					                		</tr>
					                		<tr>
					                			<td>No</td>
					                			<td>Name</td>
					                			<td>Passport No</td>
					                			<td>Passport Exp</td>
					                			<td>DOB</td>
					                		</tr>
					                	</table>
			                		</div>
			                		<div class="col-lg-12">
				                        <table class="table table-bordered" style="width:100%;margin-bottom: 0px">
					                		<tr>
					                			<th colspan="2">CONTACT PERSON</th>
					                		</tr>
					                		<tr>
					                			<td>Name</td>
					                			<td id="cName"></td>
					                		</tr>
					                		<tr>
					                			<td>Mobile</td>
					                			<td id="cMobile"></td>
					                		</tr>
					                		<tr>
					                			<td>Email</td>
					                			<td id="cEmail"></td>
					                		</tr>
					                	</table>
			                		</div>
			                	</div>
		                	</div>
	                    </div>
	                    <div class="tab-pane fade" id="messages">
		                    <div class="panel panel-default">
			                	<div class="panel-body" style="color: grey">
			                		<div class="col-lg-12">
				                        <table class="table table-bordered" style="width:100%;margin-bottom: 0px" id="summary">
					                		<tr>
					                			<th colspan="4">SUMMARY</th>
					                		</tr>
					                		<tr>
					                			<td>No</td>
					                			<td>Guest</td>
					                			<td>Category</td>
					                			<td>Nett Price</td>
					                		</tr>
					                	</table>
			                		</div>
			                	</div>
		                	</div>
	                    </div>
	                    <div class="tab-pane fade" id="settings">
	                        <h4>Settings Tab</h4>
	                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	                    </div>
	                </div>
		            <!-- /.panel-body -->
		        </div>
		        <!-- /.panel -->
		    </div>
        </div>
    </div>
</div>

<?php include('new_product.php'); ?>
<?php include('../template/footer.php'); ?>
<script src="../action/reservation.js"></script>
<script src="../action/product_fne.js"></script>