-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 10 Jul 2019 pada 12.42
-- Versi server: 10.1.32-MariaDB
-- Versi PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `malaysia`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `userID` varchar(100) DEFAULT NULL,
  `userPassword` varchar(100) DEFAULT NULL,
  `userName` varchar(100) DEFAULT NULL,
  `flag` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `userID`, `userPassword`, `userName`, `flag`) VALUES
(1, 'riaabdul', '25d55ad283aa400af464c76d713c07ad', 'Ria Abdul', 0),
(2, 'freddy', '25d55ad283aa400af464c76d713c07ad', 'Freddy', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `buyer`
--

CREATE TABLE `buyer` (
  `id` int(11) NOT NULL,
  `buyerID` varchar(100) DEFAULT NULL,
  `buyerEmail` varchar(100) DEFAULT NULL,
  `userPassword` varchar(100) DEFAULT NULL,
  `companyName` varchar(100) DEFAULT NULL,
  `companyAddress` varchar(100) DEFAULT NULL,
  `companyCity` varchar(100) DEFAULT NULL,
  `companyCountry` varchar(100) DEFAULT NULL,
  `companyZipcode` varchar(100) DEFAULT NULL,
  `companyPhone` varchar(100) DEFAULT NULL,
  `companyFax` varchar(100) DEFAULT NULL,
  `companyEmail` varchar(100) DEFAULT NULL,
  `companyLicence` varchar(200) DEFAULT NULL,
  `contactName` varchar(100) DEFAULT NULL,
  `contactPosition` varchar(100) DEFAULT NULL,
  `contactMobile` varchar(100) DEFAULT NULL,
  `contactWhatsapp` varchar(100) DEFAULT NULL,
  `contactEmail` varchar(100) DEFAULT NULL,
  `isApproved` char(1) DEFAULT '0' COMMENT '0 Belum approve 1 Sudah',
  `flag` char(1) DEFAULT '0',
  `createdDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `approvedDate` datetime DEFAULT NULL,
  `approvedBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buyer`
--

INSERT INTO `buyer` (`id`, `buyerID`, `buyerEmail`, `userPassword`, `companyName`, `companyAddress`, `companyCity`, `companyCountry`, `companyZipcode`, `companyPhone`, `companyFax`, `companyEmail`, `companyLicence`, `contactName`, `contactPosition`, `contactMobile`, `contactWhatsapp`, `contactEmail`, `isApproved`, `flag`, `createdDate`, `approvedDate`, `approvedBy`) VALUES
(1, NULL, 'freddywuisan@gmail.com', 'd0970714757783e6cf17b26fb8e2298f', 'ASIA HOLIDAY TOUR AND TRAVEL', 'RUKO LANDMARK. JALAN INDRAGIRI 12-18 #A9', 'SURABAYA', 'INDONESIA', NULL, '315454022', NULL, 'tour@abcvacation.com', NULL, 'FREDDY WUISAN', 'MANAGING DIRECTOR', NULL, NULL, NULL, '1', '0', '2019-04-19 16:07:36', NULL, NULL),
(3, NULL, 'oktavianatobing@yahoo.com', 'e43c1e0cd16e870cd66fbaea512e2ef7', 'YUK JALAN-JALAN', 'JL KARANG ANTAR BLOK A JAKARTA PUSAT', 'JAKARTA', 'INDONESIA', '10740', '17052331', '', 'oktavianatobing@yahoo.com', NULL, 'OKTAVIANA TOBING', 'TICKETING', '85697205531', '85697205531', 'oktavianatobing@yahoo.com', '1', '1', '2019-04-22 20:21:43', NULL, NULL),
(5, NULL, 'exoplanet@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'EXO PLANET COMPANY', 'EXO PLANET', 'BALI', 'INDONESIA', '', '', '', 'exoplanet@gmail.com', NULL, 'DO KYUNGSOO', 'MANAGING DIRECTOR', '85607719219', '085607719219', 'exoplanet@gmail.com', '0', '1', '2019-04-24 08:23:11', NULL, NULL),
(6, NULL, 'test@test.com', '5e8667a439c68f5145dd2fcbecf02209', 'TESTING AGENT', 'TBA', 'SURABAYA', 'INDONESIA', '', '315454022', '315454022', 'test@test.com', NULL, 'TBA', 'TBA', '85607719219', '85607719219', 'test@test.com', '1', '0', '2019-04-24 10:16:09', NULL, NULL),
(7, NULL, 'rafieza.roslan@gmail.com', '4970b76fcca7c70b5d0047c39b49b164', 'ADIBA', 'NO 47, JALAN BANDAR DARULAMAN', 'KUNINGAN', 'INDONESIA', '12950', '', '', 'rafieza.roslan@gmail.com', NULL, 'RAFIEZA', 'TRAVEL', '87750600099', '87750600099', 'rafieza.roslan@gmail.com', '1', '0', '2019-04-24 11:59:46', NULL, NULL),
(8, NULL, 'galaxytourdep03@gmail.com', 'b7a2c3b25c9441b0a38d0a874ace268b', 'GALAXY TOUR', 'JLN CENGKEH NO 14K', 'JAKARTA', 'INDONESIA', '11110', '6917870', '6917871', 'galaxytourdep03@gmail.com', NULL, 'SISKA', 'STAFF TOUR', '85261568774', '85261568774', 'galaxytourdep03@gmail.com', '0', '0', '2019-05-07 15:15:53', NULL, NULL),
(9, NULL, 'talitha.harahap15@gmail.com', '2cd102bcd7895ac3d43c6fff1634ad12', 'ALALO WISATA', 'GRAHA MAS BLOK A10', 'JAKARTA', 'INDONESIA', '11530', '53650882', '', 'sales@alalo.id', NULL, 'TALITHA', 'TRAVEL CONSULTANT', '81287719003', '81287719003', 'talitha.harahap15@gmail.com', '1', '0', '2019-05-07 15:16:42', NULL, NULL),
(10, NULL, 'paulus@rivenatour.com', '39d40e53b2e8bef0203de7b5f00a013e', 'RIVENA TOUR', 'PERUM PURI METLAND BLOK C 1 NO 10 JL KH AHMAD DAHLAN', 'KOTA TANGERANG - CIPONDOH', 'INDONESIA', '15147', '81932448229', '2155700995', 'paulus@rivenatour.com', NULL, 'PAULUS', 'MARKETING', '81932448229', '81932448229', 'paulus@rivenatour.com', '0', '0', '2019-05-08 11:16:33', NULL, NULL),
(11, NULL, 'counter07@obajatour.com', 'f5375b95ffb666ef4948e77aa0cd997a', 'OBAJA TOUR', ' JL. BANDENGAN SEL. NO.54, RT.2/RW.15, PEJAGALAN, PENJARINGAN, JAKARTA, DAERAH KHUSUS IBUKOTA JAKART', 'JAKARTA UTARA', 'INDONESIA', '14450', '2180629898', '', 'counter07@obajatour.com', NULL, 'LISSA', 'TOUR COUNTER', '2180629898', '85204196543', 'counter07@obajatour.com', '0', '0', '2019-05-08 14:28:30', NULL, NULL),
(12, NULL, 'exo@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'EXO PLANET', 'EXO PLANET', 'SURABAYA', 'INDONESIA', '60174', '315454022', '315454022', 'exo@gmail.com', NULL, 'SUHO', 'LEADER', '85607719219', '85607719219', 'suho.exo@gmail.com', '1', '0', '2019-05-09 15:21:08', NULL, NULL),
(13, NULL, 'sativa_tourinterntional@yahoo.com', 'd54d1702ad0f8326224b817c796763c9', 'SATIVA WISATA DUNIA', 'RUKO ESTE SQUARE C 2', 'SURABAYA', 'INDONESIA', '60115', '3199011103', '3199011103', 'sativa_tourinternational@yahoo.com', NULL, 'MELISSA', 'TOUR INTERNATIONAL DEPARTMENT', '81216070703', '81216070703', 'sativa_tourinternational@yahoo.com', '0', '1', '2019-05-09 15:27:42', NULL, NULL),
(14, NULL, 'vivotoursurabaya@gmail.com', '923be201692b40208e703093a15b110c', 'VIVOTOUR', 'SURABAYA', 'SURABAYA', 'INDONESIA', '60228', '81231477677', '', 'vivotoursurabaya@gmail.com', NULL, 'YULIS ERDA ', 'DIREKTUR', '81231477677', '81231477677', 'infoerda@yahoo.com', '1', '0', '2019-05-09 22:38:44', NULL, NULL),
(15, NULL, 'sales@mentariholidays.com', '25d55ad283aa400af464c76d713c07ad', 'MENTARI HOLIDAYS', 'RUKO METROPOLIS APARTMENT JL. RAYA TENGGILIS NO.127 SURABAYA', 'SURABAYA', 'INDONESIA', '60292', '318490717', '318490617', 'info@mentariholidays.com', NULL, 'AISYAH', 'STAFF', '81333563820', '81333563820', 'sales@mentariholidays.com', '0', '0', '2019-05-10 08:31:08', NULL, NULL),
(16, NULL, 'kayaholidaytravel@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'KAYA HOLIDAY', 'DIAN ISTANA PARK AVENUE C5/33', 'SURABAYA', 'INDONESIA', '60228', '83830303747', '', 'kayaholidaytravel@gmail.com', NULL, 'GAMALIEL GANATTA', 'MANAGING DIRECTOR', '83830303747', '83830303747', 'kayaholidaytravel@gmail.com', '0', '0', '2019-05-10 11:51:18', NULL, NULL),
(17, NULL, '3ttoursindonesia@gmail.com', 'c9b99e82c3e42ce4b0f36472d1ee0f98', '3T TOURS INDONESIA', 'JL DANAU SUNTER UTARA F20 NO. 23 SUNTER AGUNG JAKARTA UTARA', 'DKI JAKARTA ', 'INDONESIA', '14350', '2122655618', '', '3ttoursindonesia@gmail.com', NULL, 'NOVIRITA GUNAWAN', 'OPERATION MANAGER', '85105500770', '87787243411', '3ttoursindonesia@gmail.com', '0', '0', '2019-05-12 18:23:20', NULL, NULL),
(19, '123', 'dudinabcde@gmail.com', NULL, 'TES COMPANY', 'TES ADDRESS', 'SURABAYA', 'INDONESIA', '123123', '1', '1', 'dudinabcde@gmail.com', NULL, 'TES CONTACT NAME', 'TES POSITION', '1', '1', 'dudinabcde@gmail.com', '1', '0', '2019-07-01 10:42:34', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `buyer_log`
--

CREATE TABLE `buyer_log` (
  `id` int(11) NOT NULL,
  `buyerID` int(11) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `createdDate` date DEFAULT NULL,
  `createdTime` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `buyer_staff`
--

CREATE TABLE `buyer_staff` (
  `id` int(11) NOT NULL,
  `buyerID` int(11) DEFAULT NULL,
  `staffName` varchar(100) DEFAULT NULL,
  `staffUsername` varchar(100) DEFAULT NULL,
  `staffPassword` varchar(100) DEFAULT NULL,
  `flag` char(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buyer_staff`
--

INSERT INTO `buyer_staff` (`id`, `buyerID`, `staffName`, `staffUsername`, `staffPassword`, `flag`) VALUES
(1, 19, 'SUPER ID', 'superid', 'd0970714757783e6cf17b26fb8e2298f', '0'),
(2, 19, 'tes1', 'tes1', 'e10adc3949ba59abbe56e057f20f883e', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fne_bod`
--

CREATE TABLE `fne_bod` (
  `id` int(11) NOT NULL,
  `productID` int(11) DEFAULT NULL,
  `boDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fne_bod`
--

INSERT INTO `fne_bod` (`id`, `productID`, `boDate`) VALUES
(1, 1, '2019-04-12'),
(2, 1, '2019-04-19'),
(3, 1, '2019-04-26'),
(4, 2, '2019-04-14'),
(5, 2, '2019-04-22'),
(6, 2, '2019-04-30'),
(7, 20, '2019-07-16'),
(8, 20, '2019-07-18'),
(9, 20, '2019-07-17'),
(10, 24, '2019-07-23'),
(11, 24, '2019-07-24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fne_category`
--

CREATE TABLE `fne_category` (
  `id` int(11) NOT NULL,
  `categoryName` varchar(50) DEFAULT NULL,
  `productCategoryID` int(11) DEFAULT NULL COMMENT 'ms_product_category.id',
  `image` varchar(100) DEFAULT NULL,
  `include` text,
  `exclude` text,
  `agentID` int(11) DEFAULT NULL,
  `categoryPublish` int(11) DEFAULT '0' COMMENT '0 Publish 1 Draft',
  `createdDate` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `lastDate` datetime DEFAULT NULL,
  `lastBy` int(11) DEFAULT NULL,
  `locationID` int(11) DEFAULT NULL COMMENT 'ms_location'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fne_category`
--

INSERT INTO `fne_category` (`id`, `categoryName`, `productCategoryID`, `image`, `include`, `exclude`, `agentID`, `categoryPublish`, `createdDate`, `createdBy`, `lastDate`, `lastBy`, `locationID`) VALUES
(1, 'HARD ROCK DESARU COAST GOLF PACKAGE', 2, 'thumbnail_1562656867.png', '<ul>\r\n<li>Accomodation at Hard Rock Hotel Desaru Coast incl. Breakfast</li>\r\n<li>The Els Club Desaru Coast - Ocean Course 18 holes, include buggy</li>\r\n<li>The Els Club Desaru Coast - Valley Course 18 holes, include buggy</li>\r\n<li>Choice of Return / Round Trip Transfer Senai Airport - Desaru Coast vv / Changi Ferry Terminal - Desaru Coast vv / Changi Airport - Desaru Coast vv / Any Singapore Hotel - Desaru Coast vv</li>\r\n</ul>', 'exclude', 1, 0, '2019-05-06 11:30:34', 1, '2019-07-09 09:21:11', 1, 1),
(2, 'DESARU COAST SUMMER PACKAGE', 2, 'thumbnail_1562656859.png', '<ul>\r\n<li>2 Nights Hard Rock Hotel Desaru Coast incl. Breakfast</li>\r\n<li>Entrance to Desaru Coast Adventure Water Park</li>\r\n<li>Fullday tour Johor City and Johor Premium Outlet</li>\r\n<li>Malaysia signature dinner at Hard Rock Desaru Coast</li>\r\n</ul>', 'exclude', 1, 0, NULL, NULL, '2019-07-09 09:21:00', 1, 1),
(3, 'DESARU COAST SUMMER PACKAGE WITH KIDS', 2, 'thumbnail_1562656839.png', '<ul>\r\n<li>2 Nights Hard Rock Hotel Desaru Coast incl. Breakfast</li>\r\n<li>Entrance to Desaru Coast Adventure Water Park</li>\r\n<li>Fullday tour Johor City and Johor Premium Outlet</li>\r\n<li>Malaysia signature dinner at Hard Rock Desaru Coast</li>\r\n<li>Seafood dinner at Tanamaya Rest Desaru Coast</li>\r\n</ul>', 'exclude', 1, 0, NULL, NULL, '2019-07-09 09:20:49', 1, 1),
(4, 'KUALA LUMPUR', 2, 'thumbnail_1562656832.png', 'INCLUDE : TESTING CATEGORY KUL\r\n', 'EXCLUDE : TESTING CATEGORY KUL', 1, 0, '2019-07-08 04:21:59', 1, '2019-07-09 12:42:37', 1, 1),
(5, 'JOHOR', 1, 'thumbnail_1562745738.png', 'inc', 'exc', 1, 0, '2019-07-10 10:02:26', 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `fne_product`
--

CREATE TABLE `fne_product` (
  `id` int(11) NOT NULL,
  `categoryID` int(11) DEFAULT NULL COMMENT 'fne_category',
  `productName` varchar(100) DEFAULT NULL,
  `highlight` text,
  `description` text,
  `noofdays` int(11) DEFAULT NULL,
  `noofnights` int(11) DEFAULT NULL,
  `validDays` varchar(50) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `isConfirm` int(11) DEFAULT '0' COMMENT '0 Auto 1 No',
  `productPublish` tinyint(1) DEFAULT '0',
  `agentID` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `timeLimitConfirm` int(11) DEFAULT NULL COMMENT 'time limit to isConfirm for not auto confirm pax > allotment',
  `timeLimitGuarantee` int(11) DEFAULT NULL COMMENT 'time limit to pay',
  `lastDate` datetime DEFAULT NULL,
  `lastBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fne_product`
--

INSERT INTO `fne_product` (`id`, `categoryID`, `productName`, `highlight`, `description`, `noofdays`, `noofnights`, `validDays`, `image`, `isConfirm`, `productPublish`, `agentID`, `createdDate`, `createdBy`, `timeLimitConfirm`, `timeLimitGuarantee`, `lastDate`, `lastBy`) VALUES
(1, 1, 'RETURN SENAI AIRPORT - DESARU COAST', 'This is highlight', 'Description', 3, 2, 'mon,tue,wed,thu,fri,sat,sun', '01.jpg', 0, NULL, 1, '2019-05-06 11:33:22', 1, NULL, NULL, '2019-05-06 11:33:29', 1),
(2, 1, 'RETURN SENAI AIRPORT - DESARU COAST', 'This is highlight', NULL, 4, 3, 'mon,tue,wed,thu,fri,sat,sun', '01.jpg', 0, NULL, 1, '2019-05-06 11:33:22', 1, NULL, NULL, '2019-05-06 11:33:29', 1),
(3, 1, 'RETURN CHANGI FERRY TERMINAL - DESARU COAST', 'This is highlight', NULL, 3, 2, 'mon,tue,wed,thu,fri,sat,sun', '02.jpg', 0, NULL, 1, '2019-05-06 11:33:22', 1, NULL, NULL, '2019-05-06 11:33:29', 1),
(4, 1, 'RETURN CHANGI FERRY TERMINAL - DESARU COAST', 'This is highlight', NULL, 4, 3, 'mon,tue,wed,thu,fri,sat,sun', '02.jpg', 0, NULL, 1, '2019-05-06 11:33:22', 1, NULL, NULL, '2019-05-06 11:33:29', 1),
(5, 1, 'RETURN CHANGI AIRPORT OR ANY SINGAPORE HOTEL - DESARU COAST', 'This is highlight', NULL, 3, 2, 'mon,tue,wed,thu,fri,sat,sun', NULL, 0, NULL, 1, '2019-05-06 11:33:22', 1, NULL, NULL, '2019-05-06 11:33:29', 1),
(6, 1, 'RETURN CHANGI AIRPORT OR ANY SINGAPORE HOTEL - DESARU COAST', 'This is highlight', '', 4, 3, 'mon,tue,wed,thu,fri,sat,sun', NULL, 0, NULL, 1, '2019-07-09 10:47:01', 1, 0, 0, '2019-05-06 11:33:29', 1),
(7, 2, 'RETURN SENAI AIRPORT - DESARU COAST', 'This is highlight', NULL, 3, 2, 'mon,tue,wed,thu,fri,sat,sun', NULL, 0, NULL, 1, '2019-05-06 11:33:22', 1, NULL, NULL, '2019-05-06 11:33:29', 1),
(8, 2, 'RETURN SENAI AIRPORT - DESARU COAST', 'This is highlight', NULL, 4, 3, 'mon,tue,wed,thu,fri,sat,sun', NULL, 0, NULL, 1, '2019-05-06 11:33:22', 1, NULL, NULL, '2019-05-06 11:33:29', 1),
(9, 2, 'RETURN CHANGI FERRY TERMINAL - DESARU COAST', 'This is highlight', NULL, 3, 2, 'mon,tue,wed,thu,fri,sat,sun', NULL, 0, NULL, 1, '2019-05-06 11:33:22', 1, NULL, NULL, '2019-05-06 11:33:29', 1),
(10, 2, 'RETURN CHANGI FERRY TERMINAL - DESARU COAST', 'This is highlight', NULL, 4, 3, 'mon,tue,wed,thu,fri,sat,sun', NULL, 0, NULL, 1, '2019-05-06 11:33:22', 1, NULL, NULL, '2019-05-06 11:33:29', 1),
(11, 2, 'RETURN CHANGI AIRPORT OR ANY SINGAPORE HOTEL - DESARU COAST', 'This is highlight', NULL, 3, 2, 'mon,tue,wed,thu,fri,sat,sun', NULL, 0, NULL, 1, '2019-05-06 11:33:22', 1, NULL, NULL, '2019-05-06 11:33:29', 1),
(12, 2, 'RETURN CHANGI AIRPORT OR ANY SINGAPORE HOTEL - DESARU COAST', 'This is highlight', '', 4, 3, 'mon,tue,wed,thu,fri,sat,sun', NULL, 0, NULL, 1, '2019-07-09 10:46:16', 1, 0, 0, '2019-05-06 11:33:29', 1),
(13, 3, 'RETURN SENAI AIRPORT - DESARU COAST', 'This is highlight', 'this is description', 3, 2, 'mon,tue,wed,thu,fri,sat,sun', NULL, 0, NULL, 1, '2019-05-06 11:33:22', 1, NULL, NULL, '2019-07-08 04:46:26', 1),
(14, 3, 'RETURN SENAI AIRPORT - DESARU COAST', 'This is highlight', 'this is description', 4, 3, 'mon,tue,wed,thu,fri,sat,sun', NULL, 0, NULL, 1, '2019-05-06 11:33:22', 1, NULL, NULL, '2019-07-08 04:48:06', 1),
(15, 3, 'RETURN CHANGI FERRY TERMINAL - DESARU COAST', 'This is highlight', NULL, 3, 2, 'mon,tue,wed,thu,fri,sat,sun', NULL, 0, NULL, 1, '2019-05-06 11:33:22', 1, NULL, NULL, '2019-05-06 11:33:29', 1),
(16, 3, 'RETURN CHANGI FERRY TERMINAL - DESARU COAST', 'This is highlight', 'desc', 4, 3, 'mon,tue,wed,thu,fri,sat,sun', NULL, 0, NULL, 1, '2019-05-06 11:33:22', 1, NULL, NULL, '2019-07-01 04:34:58', 1),
(17, 3, 'RETURN CHANGI AIRPORT OR ANY SINGAPORE HOTEL - DESARU COAST', 'This is highlight', NULL, 3, 2, 'mon,tue,wed,thu,fri,sat,sun', NULL, 0, NULL, 1, '2019-05-06 11:33:22', 1, NULL, NULL, '2019-05-06 11:33:29', 1),
(18, 3, 'RETURN CHANGI AIRPORT OR ANY SINGAPORE HOTEL - DES...', 'This is highlight', 'this is description', 4, 3, 'mon,tue,wed,thu,fri,sat,sun', NULL, 0, NULL, 1, '2019-05-06 11:33:22', 1, NULL, NULL, '2019-07-05 03:30:33', 1),
(19, 3, 'DESARU GOLF PACKAGE', 'bla bla bla Desaru GOlf Package', 'Description Desaru Golf PAckage', 3, 2, 'mon,tue,wed,thu,fri', NULL, 0, 0, 1, '2019-07-09 09:44:46', 1, 0, 4, '2019-07-09 05:35:28', 1),
(20, 4, 'KL', 'KL', 'KL', 4, 3, 'mon', NULL, 0, 1, 1, '2019-07-10 12:29:58', 1, 0, 1, '2019-07-09 09:15:37', 1),
(24, 4, 'KL', 'KL', 'KL', 3, 2, NULL, NULL, 0, 0, 1, '2019-07-10 12:28:35', 1, 0, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `fne_product_pricing`
--

CREATE TABLE `fne_product_pricing` (
  `id` int(11) NOT NULL,
  `productID` int(11) DEFAULT NULL COMMENT 'fne_product',
  `pSingle` float DEFAULT NULL,
  `pAdultTwin` float DEFAULT NULL,
  `pAdultTriple` float DEFAULT NULL,
  `pCwa` float DEFAULT NULL,
  `pCwb` float DEFAULT NULL,
  `pCnb` float DEFAULT NULL,
  `agentcomm` float DEFAULT NULL,
  `admincomm` float DEFAULT NULL,
  `validFrom` date DEFAULT NULL,
  `validTo` date DEFAULT NULL,
  `cutDays` int(11) DEFAULT NULL COMMENT '-XX days',
  `allotment` int(11) DEFAULT NULL COMMENT 'XX Package'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fne_product_pricing`
--

INSERT INTO `fne_product_pricing` (`id`, `productID`, `pSingle`, `pAdultTwin`, `pAdultTriple`, `pCwa`, `pCwb`, `pCnb`, `agentcomm`, `admincomm`, `validFrom`, `validTo`, `cutDays`, `allotment`) VALUES
(1, 1, 2057.94, 1571.36, 1571.36, 1571.36, 1571.36, 1571.36, 115, 30, '2019-05-01', '2019-09-30', 5, 10),
(2, 2, 2573.14, 1857.59, 1857.59, 1857.59, 1857.59, 1857.59, 115, 30, '2019-05-01', '2019-09-30', 5, 10),
(3, 3, 2115.19, 1657.23, 1657.23, 1657.23, 1657.23, 1657.23, 115, 30, '2019-05-01', '2019-09-30', 5, 10),
(4, 4, 2687.63, 1943.45, 1943.45, 1943.45, 1943.45, 1943.45, 115, 30, '2019-05-01', '2019-09-30', 5, 10),
(5, 5, 2430.03, 1943.45, 1943.45, 1943.45, 1943.45, 1943.45, 115, 30, '2019-05-01', '2019-09-30', 5, 10),
(6, 6, 2973.86, 2258.3, 2258.3, 2258.3, 2258.3, 2258.3, 115, 30, '2019-05-01', '2019-09-30', 5, 10),
(7, 7, 1486.38, 998.57, 998.57, 998.57, 998.57, 998.57, 115, 30, '2019-05-01', '2019-09-30', 5, 10),
(8, 8, 2347.22, 1629.86, 1629.86, 1629.86, 1629.86, 1629.86, 115, 30, '2019-05-01', '2019-09-30', 5, 10),
(9, 9, 1572.47, 1113.35, 1113.35, 1113.35, 1113.35, 1113.35, 115, 30, '2019-05-01', '2019-09-30', 5, 10),
(10, 10, 2433.31, 1715.94, 1715.94, 1715.94, 1715.94, 1715.94, 115, 30, '2019-05-01', '2019-09-30', 5, 10),
(11, 11, 1572.47, 1084.66, 1084.66, 1084.66, 1084.66, 1084.66, 115, 30, '2019-05-01', '2019-09-30', 5, 10),
(12, 12, 2748.95, 2002.89, 2002.89, 2002.89, 2002.89, 2002.89, 115, 30, '2019-05-01', '2019-09-30', 5, 10),
(13, 13, 0, 1084.66, 1084.66, 424.68, 424.68, 0, 115, 30, '2019-05-01', '2019-11-14', 5, 10),
(14, 14, 0, 1773.33, 1773.33, 510.76, 510.76, 0, 115, 30, '2019-05-01', '2020-06-24', 5, 10),
(15, 15, 0, 1199.44, 1199.44, 432.07, 432.07, 0, 115, 30, '2019-05-01', '2019-09-30', 5, 10),
(16, 16, 0, 1859.41, 1859.41, 596.85, 596.85, 0, 115, 30, '2019-05-01', '2019-09-30', 5, 10),
(17, 17, 0, 1471.46, 1471.46, 510.76, 510.76, 0, 115, 30, '2019-05-01', '2019-09-30', 5, 10),
(18, 18, 0, 2203.75, 2203.75, 625.54, 625.54, 0, 115, 30, '2019-05-01', '2019-09-30', 5, 10),
(19, 19, 1000, 900, 800, 700, 700, 700, NULL, NULL, '2019-07-25', '2019-08-29', 3, 4),
(20, 20, 1, 1, 1, 1, 1, 1, NULL, NULL, '2019-07-01', '2019-07-31', 3, 10),
(24, 24, 1, 1, 1, 1, 1, 1, NULL, NULL, '2019-07-01', '2019-07-30', 1, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `fne_room`
--

CREATE TABLE `fne_room` (
  `id` int(11) NOT NULL,
  `productID` int(11) DEFAULT NULL,
  `roomID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fne_room`
--

INSERT INTO `fne_room` (`id`, `productID`, `roomID`) VALUES
(4, 1, 4),
(11, 2, 4),
(18, 3, 4),
(25, 4, 4),
(32, 5, 4),
(39, 6, 4),
(46, 7, 4),
(53, 8, 4),
(60, 9, 4),
(67, 10, 4),
(74, 11, 4),
(81, 12, 4),
(88, 13, 4),
(95, 14, 4),
(102, 15, 4),
(109, 16, 4),
(116, 17, 4),
(123, 18, 4),
(131, 24, 4),
(133, 24, 3),
(134, 24, 5),
(135, 24, 1),
(136, 1, 3),
(137, 20, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `itinerary_fne`
--

CREATE TABLE `itinerary_fne` (
  `id` int(11) NOT NULL,
  `productID` int(11) DEFAULT NULL,
  `dayNo` int(11) DEFAULT NULL,
  `highlight` varchar(200) DEFAULT NULL,
  `description` text,
  `meals` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `itinerary_fne`
--

INSERT INTO `itinerary_fne` (`id`, `productID`, `dayNo`, `highlight`, `description`, `meals`) VALUES
(1, 1, 1, 'Arrival Kuala Lumpur', 'Setibanya di Kuala Lumpur International Airport, Anda akan dijemput oleh perwakilan kami dan diantar menuju hotel untuk check-in. Setelah Anda tiba di hotel, Anda dapat menikmati waktu Anda dengan berkeliling di sekitar hotel menikmati keindahan kota serta mencicipi kuliner khas Negeri Jiran.', NULL),
(2, 1, 2, 'Kuala Lumpur Sightseeing', 'Setelah makan pagi di hotel, Anda akan diajak untuk mengelilingi kota Kuala Lumpur selama setengah hari. Anda akan mengunjungi beberapa tempat yang terkenal seperti King Palace yang merupakan tempat tinggal raja, Independent Square yang merupakan tempat pertama kalinya bendera Malaysia dikibarkan, National Mosque, National Monument  atau yang lebih dikenal dengan Tugu Negara yang merupakan sebuah monumen yang dibangun untuk menghormati para pahlawan yang menyerahkan diri mereka untuk perdamaian, kemudian menuju Twin Tower menara kembar yang menjadi icon kota Kuala Lumpur, dan terakhir mengunjungi Cocoa House yang merupakan pusat penjualan beraneka ragam coklat yang lezat. Setelah tour selesai Anda akan diantar kembali menuju hotel untuk beristirahat dan makan siang, Anda dapat mencicipi cita rasa masakan Malaysia dengan bersantap siang di restoran-restoran yang terdapat di sekitar hotel. Setelah itu Anda dapat mengeksplor lebih dalam kota Kuala Lumpur sesuai dengan keinginan Anda.', 'B'),
(3, 1, 3, 'Departure Kuala Lumpur', 'Setelah menikmati sarapan pagi di hotel, Anda dapat memanfaatkan waktu bebas Anda sebelum tiba waktunya check-out sebelum jam 12 siang. Anda akan di antar ke bandara 3 jam sebelum (KLIA) dan 4 jam sebelum (KLIA2).', 'B'),
(4, 2, 1, 'Arrival Langkawi', 'Selamat datang di pulau Langkawi! Pulau yang terletak 400 km dari ibukota Kuala Lumpur ini menjadi salah satu tempat favourite para wisatawan untuk berlibur, karena selain memiliki pantai-pantai yang indah, pemandangan alamnya pun masih sangat asri untuk dinikmati. Sejarah dan keindahan alam merupakan sajian utama yang bisa dinikmati selama berada di Langkawi', NULL),
(5, 2, 2, 'Langkawi', 'Selamat Pagi di Langkawi!! Perjalanan Anda kali ini akan dimulai pada pukul 09.00 AM. Sebelum mengikuti Half Day City Tour, Anda dapat mengisi tenaga Anda dengan menikmati santap pagi. Perjalanan pertama dengan mengunjungi Mahsuri Tomb, dilanjutkan menuju Batik Factory, kemudian melihat keindahan Black Sands Beach, dilanjutkan menuju Eagle Square dan Kuah Town. Selesai mengikuti Half Day City Tour, Anda akan diantar kembali ke Hotel dan Anda bebas untuk menikmati sisa waktu Anda di Langkawi.', 'B'),
(6, 2, 3, 'Departure Langkawi', 'Hari terakhir Anda di pulau indah Langkawi ini, Anda masih memiliki waktu untuk bersantai atau berkeliling di sekitar hotel tempat Anda menginap hingga tiba waktu check-out dan diantar menuju Airport untuk berpulang ke tanah air. Terimakasih telah memilih kami sebagai partner perjalanan Anda, sampai jumpa di perjalanan menyenangkan lainnya.', 'B');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kurs`
--

CREATE TABLE `kurs` (
  `id` int(11) NOT NULL,
  `kursDate` date DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `inputDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kurs`
--

INSERT INTO `kurs` (`id`, `kursDate`, `currency`, `amount`, `inputDate`) VALUES
(1, '2019-04-19', 'IDR', 3423.97, NULL),
(2, '2019-04-20', 'IDR', 3423.97, NULL),
(3, '2019-04-21', 'IDR', 3423.97, NULL),
(4, '2019-04-22', 'IDR', 3500.00, NULL),
(5, '2019-04-24', 'IDR', 3451.94, NULL),
(11, '2019-04-25', 'IDR', 3462.22, '2019-04-25 14:57:03'),
(13, '2019-04-26', 'IDR', 3471.17, '2019-04-26 00:00:05'),
(14, '2019-04-27', 'IDR', 3471.34, '2019-04-27 00:00:05'),
(15, '2019-04-28', 'IDR', 3471.34, '2019-04-28 00:00:06'),
(16, '2019-04-29', 'IDR', 3471.34, '2019-04-29 00:00:03'),
(17, '2019-04-30', 'IDR', 3475.04, '2019-04-30 00:00:04'),
(18, '2019-05-01', 'IDR', 3484.22, '2019-05-01 00:00:05'),
(19, '2019-05-02', 'IDR', 3484.22, '2019-05-02 00:00:05'),
(20, '2019-05-03', 'IDR', 3483.98, '2019-05-03 00:00:08'),
(21, '2019-05-04', 'IDR', 3481.40, '2019-05-04 00:00:04'),
(22, '2019-05-05', 'IDR', 3481.40, '2019-05-05 00:00:04'),
(23, '2019-05-06', 'IDR', 3481.40, '2019-05-06 00:00:07'),
(24, '2019-05-07', 'IDR', 3484.97, '2019-05-07 00:00:03'),
(25, '2019-05-08', 'IDR', 3481.73, '2019-05-08 00:00:05'),
(26, '2019-05-09', 'IDR', 3485.34, '2019-05-09 00:00:06'),
(27, '2019-05-10', 'IDR', 3493.71, '2019-05-10 00:00:07'),
(28, '2019-05-11', 'IDR', 3484.79, '2019-05-11 00:00:07'),
(29, '2019-05-12', 'IDR', 3484.79, '2019-05-12 00:00:05'),
(30, '2019-05-13', 'IDR', 3484.79, '2019-05-13 00:00:08'),
(31, '2019-05-14', 'IDR', 3500.88, '2019-05-14 00:00:05'),
(32, '2019-05-15', 'IDR', 3499.54, '2019-05-15 00:00:05'),
(33, '2019-05-16', 'IDR', 3503.41, '2019-05-16 00:00:05'),
(34, '2019-05-17', 'IDR', 3508.84, '2019-05-17 00:00:05'),
(35, '2019-05-18', 'IDR', 3499.29, '2019-05-18 00:00:05'),
(36, '2019-05-19', 'IDR', 3499.29, '2019-05-19 00:00:05'),
(37, '2019-05-20', 'IDR', 3499.29, '2019-05-20 00:00:07'),
(38, '2019-05-21', 'IDR', 3500.55, '2019-05-21 00:00:05'),
(39, '2019-05-22', 'IDR', 3498.67, '2019-05-22 00:00:06'),
(40, '2019-05-23', 'IDR', 3511.10, '2019-05-23 00:00:05'),
(41, '2019-05-24', 'IDR', 3487.00, '2019-05-24 00:00:06'),
(42, '2019-05-25', 'IDR', 3473.14, '2019-05-25 00:00:05'),
(43, '2019-05-26', 'IDR', 3473.14, '2019-05-26 00:00:05'),
(44, '2019-05-27', 'IDR', 3473.14, '2019-05-27 00:00:11'),
(45, '2019-05-28', 'IDR', 3474.37, '2019-05-28 00:00:05'),
(46, '2019-05-29', 'IDR', 3472.53, '2019-05-29 00:00:04'),
(47, '2019-05-30', 'IDR', 3468.57, '2019-05-30 00:00:07'),
(48, '2019-05-31', 'IDR', 3468.57, '2019-05-31 00:00:05'),
(49, '2019-06-01', 'IDR', 3446.52, '2019-06-01 00:00:08'),
(50, '2019-06-02', 'IDR', 3446.52, '2019-06-02 00:00:05'),
(51, '2019-06-03', 'IDR', 3446.52, '2019-06-03 00:00:40'),
(52, '2019-06-04', 'IDR', 3446.52, '2019-06-04 00:00:04'),
(53, '2019-06-05', 'IDR', 3446.52, '2019-06-05 00:00:05'),
(54, '2019-06-06', 'IDR', 3446.52, '2019-06-06 00:00:05'),
(55, '2019-06-07', 'IDR', 3446.52, '2019-06-07 00:00:05'),
(56, '2019-06-08', 'IDR', 3446.52, '2019-06-08 00:00:04'),
(57, '2019-06-09', 'IDR', 3446.52, '2019-06-09 00:00:05'),
(58, '2019-06-10', 'IDR', 3446.52, '2019-06-10 00:00:19'),
(59, '2019-06-11', 'IDR', 3462.12, '2019-06-11 00:00:06'),
(60, '2019-06-12', 'IDR', 3458.90, '2019-06-12 00:00:06'),
(61, '2019-06-13', 'IDR', 3461.40, '2019-06-13 00:00:04'),
(62, '2019-06-14', 'IDR', 3468.85, '2019-06-14 00:00:04'),
(63, '2019-06-15', 'IDR', 3475.94, '2019-06-15 00:00:04'),
(64, '2019-06-16', 'IDR', 3475.94, '2019-06-16 00:00:04'),
(65, '2019-06-17', 'IDR', 3475.94, '2019-06-17 00:00:05'),
(66, '2019-06-18', 'IDR', 3471.41, '2019-06-18 00:00:05'),
(67, '2019-06-19', 'IDR', 3465.33, '2019-06-19 00:00:05'),
(68, '2019-06-20', 'IDR', 3456.26, '2019-06-20 00:00:07'),
(69, '2019-06-21', 'IDR', 3455.87, '2019-06-21 00:00:04'),
(70, '2019-06-22', 'IDR', 3448.24, '2019-06-22 00:00:04'),
(71, '2019-06-23', 'IDR', 3448.24, '2019-06-23 00:00:04'),
(72, '2019-06-24', 'IDR', 3448.24, '2019-06-24 00:00:04'),
(73, '2019-06-25', 'IDR', 3451.27, '2019-06-25 00:00:03'),
(74, '2019-06-26', 'IDR', 3448.47, '2019-06-26 00:00:02'),
(75, '2019-06-27', 'IDR', 3455.59, '2019-06-27 00:00:04'),
(76, '2019-06-28', 'IDR', 3450.38, '2019-06-28 00:00:05'),
(77, '2019-06-29', 'IDR', 3457.44, '2019-06-29 00:00:04'),
(78, '2019-06-30', 'IDR', 3457.44, '2019-06-30 00:00:05'),
(79, '2019-07-01', 'IDR', 3457.44, '2019-07-01 00:00:04'),
(80, '2019-07-02', 'IDR', 3453.89, '2019-07-02 00:00:07'),
(81, '2019-07-03', 'IDR', 3456.80, '2019-07-03 00:00:04'),
(82, '2019-07-04', 'IDR', 3452.21, '2019-07-04 00:00:04'),
(83, '2019-07-05', 'IDR', 3458.21, '2019-07-05 00:00:03'),
(84, '2019-07-06', 'IDR', 3445.73, '2019-07-06 00:00:03'),
(85, '2019-07-07', 'IDR', 3445.73, '2019-07-07 00:00:04'),
(86, '2019-07-08', 'IDR', 3445.73, '2019-07-08 00:00:04'),
(87, '2019-07-09', 'IDR', 3447.32, '2019-07-09 00:00:04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kurs_all`
--

CREATE TABLE `kurs_all` (
  `id` int(11) NOT NULL,
  `kursDate` date DEFAULT NULL,
  `kursTime` time DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `rate_jual` decimal(10,2) DEFAULT NULL,
  `rate_beli` decimal(10,2) DEFAULT NULL,
  `tt_jual` decimal(10,2) DEFAULT NULL,
  `tt_beli` decimal(10,2) DEFAULT NULL,
  `notes_jual` decimal(10,2) DEFAULT NULL,
  `notes_beli` decimal(10,2) DEFAULT NULL,
  `inputDate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kurs_all`
--

INSERT INTO `kurs_all` (`id`, `kursDate`, `kursTime`, `currency`, `rate_jual`, `rate_beli`, `tt_jual`, `tt_beli`, `notes_jual`, `notes_beli`, `inputDate`) VALUES
(64, '2019-04-26', '00:00:00', 'THB', '446.06', '438.06', '447.10', '437.10', '449.00', '419.00', '2019-04-26 00:00:05'),
(63, '2019-04-26', '00:00:00', 'MYR', '3471.17', '3391.17', '3477.90', '3376.90', '0.00', '0.00', '2019-04-26 00:00:05'),
(62, '2019-04-26', '00:00:00', 'CNY', '2161.36', '2041.36', '2185.25', '2019.25', '2158.00', '2029.00', '2019-04-26 00:00:05'),
(61, '2019-04-26', '00:00:00', 'SAR', '3821.47', '3741.47', '3834.35', '3729.35', '3837.00', '3667.00', '2019-04-26 00:00:05'),
(60, '2019-04-26', '00:00:00', 'JPY', '128.46', '125.06', '129.17', '124.45', '129.03', '122.50', '2019-04-26 00:00:05'),
(59, '2019-04-26', '00:00:00', 'HKD', '1822.91', '1792.91', '1824.95', '1791.05', '1831.00', '1762.00', '2019-04-26 00:00:05'),
(58, '2019-04-26', '00:00:00', 'GBP', '18325.06', '18225.06', '18515.30', '18044.30', '18415.00', '17958.00', '2019-04-26 00:00:05'),
(57, '2019-04-26', '00:00:00', 'NZD', '9392.37', '9312.37', '9494.35', '9211.35', '9426.00', '9159.00', '2019-04-26 00:00:05'),
(56, '2019-04-26', '00:00:00', 'CHF', '13952.94', '13852.94', '14079.25', '13743.25', '13996.00', '13617.00', '2019-04-26 00:00:05'),
(55, '2019-04-26', '00:00:00', 'CAD', '10539.78', '10459.78', '10628.25', '10365.25', '10585.00', '10287.00', '2019-04-26 00:00:05'),
(54, '2019-04-26', '00:00:00', 'SEK', '1506.48', '1466.48', '1515.30', '1463.10', '1551.00', '1433.00', '2019-04-26 00:00:05'),
(53, '2019-04-26', '00:00:00', 'DKK', '2146.68', '2086.68', '2154.90', '2088.60', '2167.00', '2037.00', '2019-04-26 00:00:05'),
(52, '2019-04-26', '00:00:00', 'AUD', '9986.55', '9906.55', '10082.35', '9814.35', '10025.00', '9749.00', '2019-04-26 00:00:05'),
(51, '2019-04-26', '00:00:00', 'EUR', '15854.72', '15754.72', '16003.70', '15625.70', '15926.00', '15513.00', '2019-04-26 00:00:05'),
(50, '2019-04-26', '00:00:00', 'SGD', '10421.01', '10373.01', '10438.30', '10357.30', '10455.00', '10227.00', '2019-04-26 00:00:05'),
(49, '2019-04-26', '00:00:00', 'USD', '14189.00', '14173.00', '14331.00', '14031.00', '14243.00', '13943.00', '2019-04-26 00:00:05'),
(33, '2019-04-25', '14:58:00', 'USD', '14164.00', '14148.00', '14301.00', '14001.00', '14243.00', '13943.00', '2019-04-25 14:58:02'),
(34, '2019-04-25', '14:58:00', 'SGD', '10408.34', '10360.34', '10424.70', '10343.70', '10455.00', '10227.00', '2019-04-25 14:58:02'),
(35, '2019-04-25', '14:58:00', 'EUR', '15834.04', '15734.04', '15973.05', '15595.05', '15926.00', '15513.00', '2019-04-25 14:58:02'),
(36, '2019-04-25', '14:58:00', 'AUD', '9964.77', '9884.77', '10061.20', '9793.20', '10025.00', '9749.00', '2019-04-25 14:58:02'),
(37, '2019-04-25', '14:58:00', 'DKK', '2143.94', '2083.94', '2150.60', '2084.40', '2167.00', '2037.00', '2019-04-25 14:58:02'),
(38, '2019-04-25', '14:58:00', 'SEK', '1502.02', '1462.02', '1528.05', '1475.45', '1551.00', '1433.00', '2019-04-25 14:58:02'),
(39, '2019-04-25', '14:58:00', 'CAD', '10525.86', '10445.86', '10610.75', '10347.75', '10585.00', '10287.00', '2019-04-25 14:58:02'),
(40, '2019-04-25', '14:58:00', 'CHF', '13916.20', '13816.20', '14037.40', '13702.40', '13996.00', '13617.00', '2019-04-25 14:58:02'),
(41, '2019-04-25', '14:58:00', 'NZD', '9371.54', '9291.54', '9467.35', '9184.35', '9426.00', '9159.00', '2019-04-25 14:58:02'),
(42, '2019-04-25', '14:58:00', 'GBP', '18306.74', '18206.74', '18492.25', '18021.25', '18415.00', '17958.00', '2019-04-25 14:58:02'),
(43, '2019-04-25', '14:58:00', 'HKD', '1819.84', '1789.84', '1821.45', '1787.55', '1831.00', '1762.00', '2019-04-25 14:58:02'),
(44, '2019-04-25', '14:58:00', 'JPY', '128.31', '124.92', '128.65', '123.94', '129.03', '122.50', '2019-04-25 14:58:02'),
(45, '2019-04-25', '14:58:00', 'SAR', '3814.81', '3734.81', '3826.30', '3721.30', '3837.00', '3667.00', '2019-04-25 14:58:02'),
(46, '2019-04-25', '14:58:00', 'CNY', '2158.57', '2038.57', '2184.50', '2018.40', '2158.00', '2029.00', '2019-04-25 14:58:02'),
(47, '2019-04-25', '14:58:00', 'MYR', '3462.22', '3382.22', '3468.95', '3367.95', '0.00', '0.00', '2019-04-25 14:58:02'),
(48, '2019-04-25', '14:58:00', 'THB', '445.27', '437.27', '446.00', '437.00', '449.00', '419.00', '2019-04-25 14:58:02'),
(65, '2019-04-27', '00:00:00', 'USD', '14188.00', '14172.00', '14330.00', '14030.00', '14331.00', '14031.00', '2019-04-27 00:00:05'),
(66, '2019-04-27', '00:00:00', 'SGD', '10432.10', '10384.10', '10448.25', '10367.25', '10515.00', '10287.00', '2019-04-27 00:00:05'),
(67, '2019-04-27', '00:00:00', 'EUR', '15850.07', '15750.07', '15996.85', '15618.85', '15994.00', '15582.00', '2019-04-27 00:00:05'),
(68, '2019-04-27', '00:00:00', 'AUD', '10017.04', '9937.04', '10111.70', '9842.70', '10083.00', '9808.00', '2019-04-27 00:00:05'),
(69, '2019-04-27', '00:00:00', 'DKK', '2146.29', '2086.29', '2154.45', '2088.15', '2177.00', '2047.00', '2019-04-27 00:00:05'),
(70, '2019-04-27', '00:00:00', 'SEK', '1512.92', '1472.92', '1521.85', '1469.45', '1544.00', '1427.00', '2019-04-27 00:00:05'),
(71, '2019-04-27', '00:00:00', 'CAD', '10552.27', '10472.27', '10643.30', '10380.30', '10665.00', '10367.00', '2019-04-27 00:00:05'),
(72, '2019-04-27', '00:00:00', 'CHF', '13945.84', '13845.84', '14072.75', '13736.75', '14094.00', '13714.00', '2019-04-27 00:00:05'),
(73, '2019-04-27', '00:00:00', 'NZD', '9475.37', '9395.37', '9575.30', '9290.30', '9535.00', '9266.00', '2019-04-27 00:00:05'),
(74, '2019-04-27', '00:00:00', 'GBP', '18354.26', '18254.26', '18544.05', '18072.05', '18514.00', '18057.00', '2019-04-27 00:00:05'),
(75, '2019-04-27', '00:00:00', 'HKD', '1822.61', '1792.61', '1824.60', '1790.70', '1842.00', '1773.00', '2019-04-27 00:00:05'),
(76, '2019-04-27', '00:00:00', 'JPY', '128.70', '125.30', '129.42', '124.69', '130.56', '124.01', '2019-04-27 00:00:05'),
(77, '2019-04-27', '00:00:00', 'SAR', '3821.20', '3741.20', '3834.05', '3729.05', '3860.00', '3691.00', '2019-04-27 00:00:05'),
(78, '2019-04-27', '00:00:00', 'CNY', '2164.00', '2044.00', '2187.90', '2021.80', '2164.00', '2035.00', '2019-04-27 00:00:05'),
(79, '2019-04-27', '00:00:00', 'MYR', '3471.34', '3391.34', '3479.35', '3378.35', '0.00', '0.00', '2019-04-27 00:00:05'),
(80, '2019-04-27', '00:00:00', 'THB', '447.41', '439.41', '448.45', '438.45', '451.00', '422.00', '2019-04-27 00:00:05'),
(81, '2019-04-28', '00:00:00', 'USD', '14188.00', '14172.00', '14330.00', '14030.00', '14331.00', '14031.00', '2019-04-28 00:00:06'),
(82, '2019-04-28', '00:00:00', 'SGD', '10432.10', '10384.10', '10448.25', '10367.25', '10515.00', '10287.00', '2019-04-28 00:00:06'),
(83, '2019-04-28', '00:00:00', 'EUR', '15850.07', '15750.07', '15996.85', '15618.85', '15994.00', '15582.00', '2019-04-28 00:00:06'),
(84, '2019-04-28', '00:00:00', 'AUD', '10017.04', '9937.04', '10111.70', '9842.70', '10083.00', '9808.00', '2019-04-28 00:00:06'),
(85, '2019-04-28', '00:00:00', 'DKK', '2146.29', '2086.29', '2154.45', '2088.15', '2177.00', '2047.00', '2019-04-28 00:00:06'),
(86, '2019-04-28', '00:00:00', 'SEK', '1512.92', '1472.92', '1521.85', '1469.45', '1544.00', '1427.00', '2019-04-28 00:00:06'),
(87, '2019-04-28', '00:00:00', 'CAD', '10552.27', '10472.27', '10643.30', '10380.30', '10665.00', '10367.00', '2019-04-28 00:00:06'),
(88, '2019-04-28', '00:00:00', 'CHF', '13945.84', '13845.84', '14072.75', '13736.75', '14094.00', '13714.00', '2019-04-28 00:00:06'),
(89, '2019-04-28', '00:00:00', 'NZD', '9475.37', '9395.37', '9575.30', '9290.30', '9535.00', '9266.00', '2019-04-28 00:00:06'),
(90, '2019-04-28', '00:00:00', 'GBP', '18354.26', '18254.26', '18544.05', '18072.05', '18514.00', '18057.00', '2019-04-28 00:00:06'),
(91, '2019-04-28', '00:00:00', 'HKD', '1822.61', '1792.61', '1824.60', '1790.70', '1842.00', '1773.00', '2019-04-28 00:00:06'),
(92, '2019-04-28', '00:00:00', 'JPY', '128.70', '125.30', '129.42', '124.69', '130.56', '124.01', '2019-04-28 00:00:06'),
(93, '2019-04-28', '00:00:00', 'SAR', '3821.20', '3741.20', '3834.05', '3729.05', '3860.00', '3691.00', '2019-04-28 00:00:06'),
(94, '2019-04-28', '00:00:00', 'CNY', '2164.00', '2044.00', '2187.90', '2021.80', '2164.00', '2035.00', '2019-04-28 00:00:06'),
(95, '2019-04-28', '00:00:00', 'MYR', '3471.34', '3391.34', '3479.35', '3378.35', '0.00', '0.00', '2019-04-28 00:00:06'),
(96, '2019-04-28', '00:00:00', 'THB', '447.41', '439.41', '448.45', '438.45', '451.00', '422.00', '2019-04-28 00:00:06'),
(97, '2019-04-29', '00:00:00', 'USD', '14188.00', '14172.00', '14330.00', '14030.00', '14331.00', '14031.00', '2019-04-29 00:00:03'),
(98, '2019-04-29', '00:00:00', 'SGD', '10432.10', '10384.10', '10448.25', '10367.25', '10515.00', '10287.00', '2019-04-29 00:00:03'),
(99, '2019-04-29', '00:00:00', 'EUR', '15850.07', '15750.07', '15996.85', '15618.85', '15994.00', '15582.00', '2019-04-29 00:00:03'),
(100, '2019-04-29', '00:00:00', 'AUD', '10017.04', '9937.04', '10111.70', '9842.70', '10083.00', '9808.00', '2019-04-29 00:00:03'),
(101, '2019-04-29', '00:00:00', 'DKK', '2146.29', '2086.29', '2154.45', '2088.15', '2177.00', '2047.00', '2019-04-29 00:00:03'),
(102, '2019-04-29', '00:00:00', 'SEK', '1512.92', '1472.92', '1521.85', '1469.45', '1544.00', '1427.00', '2019-04-29 00:00:03'),
(103, '2019-04-29', '00:00:00', 'CAD', '10552.27', '10472.27', '10643.30', '10380.30', '10665.00', '10367.00', '2019-04-29 00:00:03'),
(104, '2019-04-29', '00:00:00', 'CHF', '13945.84', '13845.84', '14072.75', '13736.75', '14094.00', '13714.00', '2019-04-29 00:00:03'),
(105, '2019-04-29', '00:00:00', 'NZD', '9475.37', '9395.37', '9575.30', '9290.30', '9535.00', '9266.00', '2019-04-29 00:00:03'),
(106, '2019-04-29', '00:00:00', 'GBP', '18354.26', '18254.26', '18544.05', '18072.05', '18514.00', '18057.00', '2019-04-29 00:00:03'),
(107, '2019-04-29', '00:00:00', 'HKD', '1822.61', '1792.61', '1824.60', '1790.70', '1842.00', '1773.00', '2019-04-29 00:00:03'),
(108, '2019-04-29', '00:00:00', 'JPY', '128.70', '125.30', '129.42', '124.69', '130.56', '124.01', '2019-04-29 00:00:03'),
(109, '2019-04-29', '00:00:00', 'SAR', '3821.20', '3741.20', '3834.05', '3729.05', '3860.00', '3691.00', '2019-04-29 00:00:03'),
(110, '2019-04-29', '00:00:00', 'CNY', '2164.00', '2044.00', '2187.90', '2021.80', '2164.00', '2035.00', '2019-04-29 00:00:03'),
(111, '2019-04-29', '00:00:00', 'MYR', '3471.34', '3391.34', '3479.35', '3378.35', '0.00', '0.00', '2019-04-29 00:00:03'),
(112, '2019-04-29', '00:00:00', 'THB', '447.41', '439.41', '448.45', '438.45', '451.00', '422.00', '2019-04-29 00:00:03'),
(113, '2019-04-30', '00:00:00', 'USD', '14205.00', '14189.00', '14347.00', '14047.00', '14330.00', '14030.00', '2019-04-30 00:00:04'),
(114, '2019-04-30', '00:00:00', 'SGD', '10455.68', '10407.68', '10471.90', '10390.90', '10526.00', '10297.00', '2019-04-30 00:00:04'),
(115, '2019-04-30', '00:00:00', 'EUR', '15900.24', '15800.24', '16045.95', '15667.95', '16016.00', '15603.00', '2019-04-30 00:00:04'),
(116, '2019-04-30', '00:00:00', 'AUD', '10049.59', '9969.59', '10145.20', '9876.20', '10131.00', '9855.00', '2019-04-30 00:00:04'),
(117, '2019-04-30', '00:00:00', 'DKK', '2152.93', '2092.93', '2160.90', '2094.50', '2179.00', '2049.00', '2019-04-30 00:00:04'),
(118, '2019-04-30', '00:00:00', 'SEK', '1514.10', '1474.10', '1522.85', '1470.45', '1547.00', '1430.00', '2019-04-30 00:00:04'),
(119, '2019-04-30', '00:00:00', 'CAD', '10579.72', '10499.72', '10670.95', '10406.95', '10682.00', '10383.00', '2019-04-30 00:00:04'),
(120, '2019-04-30', '00:00:00', 'CHF', '13978.19', '13878.19', '14104.70', '13768.70', '14106.00', '13726.00', '2019-04-30 00:00:04'),
(121, '2019-04-30', '00:00:00', 'NZD', '9503.72', '9423.72', '9606.75', '9321.75', '9593.00', '9323.00', '2019-04-30 00:00:04'),
(122, '2019-04-30', '00:00:00', 'GBP', '18423.76', '18323.76', '18613.35', '18140.35', '18547.00', '18090.00', '2019-04-30 00:00:04'),
(123, '2019-04-30', '00:00:00', 'HKD', '1824.92', '1794.92', '1826.85', '1792.95', '1841.00', '1773.00', '2019-04-30 00:00:04'),
(124, '2019-04-30', '00:00:00', 'JPY', '128.75', '125.35', '129.48', '124.75', '130.45', '123.90', '2019-04-30 00:00:04'),
(125, '2019-04-30', '00:00:00', 'SAR', '3825.74', '3745.74', '3838.60', '3733.60', '3860.00', '3690.00', '2019-04-30 00:00:04'),
(126, '2019-04-30', '00:00:00', 'CNY', '2166.74', '2046.74', '2190.80', '2024.70', '2168.00', '2039.00', '2019-04-30 00:00:04'),
(127, '2019-04-30', '00:00:00', 'MYR', '3475.04', '3395.04', '3483.05', '3382.05', '0.00', '0.00', '2019-04-30 00:00:04'),
(128, '2019-04-30', '00:00:00', 'THB', '448.91', '440.91', '449.70', '439.70', '454.00', '424.00', '2019-04-30 00:00:04'),
(129, '2019-05-01', '00:00:00', 'USD', '14255.00', '14239.00', '14397.00', '14097.00', '14390.00', '14090.00', '2019-05-01 00:00:04'),
(130, '2019-05-01', '00:00:00', 'SGD', '10495.88', '10447.88', '10511.32', '10430.32', '10574.00', '10346.00', '2019-05-01 00:00:05'),
(131, '2019-05-01', '00:00:00', 'EUR', '16019.46', '15919.46', '16166.55', '15786.55', '16133.00', '15719.00', '2019-05-01 00:00:05'),
(132, '2019-05-01', '00:00:00', 'AUD', '10089.84', '10009.84', '10186.30', '9916.30', '10187.00', '9910.00', '2019-05-01 00:00:05'),
(133, '2019-05-01', '00:00:00', 'DKK', '2169.05', '2109.05', '2177.15', '2110.35', '2195.00', '2065.00', '2019-05-01 00:00:05'),
(134, '2019-05-01', '00:00:00', 'SEK', '1522.58', '1482.58', '1531.20', '1478.70', '1553.00', '1436.00', '2019-05-01 00:00:05'),
(135, '2019-05-01', '00:00:00', 'CAD', '10631.78', '10551.78', '10721.70', '10457.70', '10723.00', '10425.00', '2019-05-01 00:00:05'),
(136, '2019-05-01', '00:00:00', 'CHF', '14043.72', '13943.72', '14169.15', '13832.15', '14161.00', '13781.00', '2019-05-01 00:00:05'),
(137, '2019-05-01', '00:00:00', 'NZD', '9550.59', '9470.59', '9648.85', '9362.85', '9638.00', '9368.00', '2019-05-01 00:00:05'),
(138, '2019-05-01', '00:00:00', 'GBP', '18535.49', '18435.49', '18723.80', '18247.80', '18645.00', '18187.00', '2019-05-01 00:00:05'),
(139, '2019-05-01', '00:00:00', 'HKD', '1831.10', '1801.10', '1833.10', '1799.20', '1849.00', '1781.00', '2019-05-01 00:00:05'),
(140, '2019-05-01', '00:00:00', 'JPY', '129.60', '126.20', '130.34', '125.57', '130.94', '124.39', '2019-05-01 00:00:05'),
(141, '2019-05-01', '00:00:00', 'SAR', '3839.02', '3759.02', '3851.85', '3746.85', '3877.00', '3707.00', '2019-05-01 00:00:05'),
(142, '2019-05-01', '00:00:00', 'CNY', '2173.49', '2053.49', '2197.60', '2031.50', '2176.00', '2047.00', '2019-05-01 00:00:05'),
(143, '2019-05-01', '00:00:00', 'MYR', '3484.22', '3404.22', '3492.20', '3391.20', '0.00', '0.00', '2019-05-01 00:00:05'),
(144, '2019-05-01', '00:00:00', 'THB', '450.27', '442.27', '451.40', '441.40', '455.00', '426.00', '2019-05-01 00:00:05'),
(145, '2019-05-02', '00:00:00', 'USD', '14255.00', '14239.00', '14397.00', '14097.00', '14390.00', '14090.00', '2019-05-02 00:00:05'),
(146, '2019-05-02', '00:00:00', 'SGD', '10495.88', '10447.88', '10511.32', '10430.32', '10574.00', '10346.00', '2019-05-02 00:00:05'),
(147, '2019-05-02', '00:00:00', 'EUR', '16019.46', '15919.46', '16166.55', '15786.55', '16133.00', '15719.00', '2019-05-02 00:00:05'),
(148, '2019-05-02', '00:00:00', 'AUD', '10089.84', '10009.84', '10186.30', '9916.30', '10187.00', '9910.00', '2019-05-02 00:00:05'),
(149, '2019-05-02', '00:00:00', 'DKK', '2169.05', '2109.05', '2177.15', '2110.35', '2195.00', '2065.00', '2019-05-02 00:00:05'),
(150, '2019-05-02', '00:00:00', 'SEK', '1522.58', '1482.58', '1531.20', '1478.70', '1553.00', '1436.00', '2019-05-02 00:00:05'),
(151, '2019-05-02', '00:00:00', 'CAD', '10631.78', '10551.78', '10721.70', '10457.70', '10723.00', '10425.00', '2019-05-02 00:00:05'),
(152, '2019-05-02', '00:00:00', 'CHF', '14043.72', '13943.72', '14169.15', '13832.15', '14161.00', '13781.00', '2019-05-02 00:00:05'),
(153, '2019-05-02', '00:00:00', 'NZD', '9550.59', '9470.59', '9648.85', '9362.85', '9638.00', '9368.00', '2019-05-02 00:00:05'),
(154, '2019-05-02', '00:00:00', 'GBP', '18535.49', '18435.49', '18723.80', '18247.80', '18645.00', '18187.00', '2019-05-02 00:00:05'),
(155, '2019-05-02', '00:00:00', 'HKD', '1831.10', '1801.10', '1833.10', '1799.20', '1849.00', '1781.00', '2019-05-02 00:00:05'),
(156, '2019-05-02', '00:00:00', 'JPY', '129.60', '126.20', '130.34', '125.57', '130.94', '124.39', '2019-05-02 00:00:05'),
(157, '2019-05-02', '00:00:00', 'SAR', '3839.02', '3759.02', '3851.85', '3746.85', '3877.00', '3707.00', '2019-05-02 00:00:05'),
(158, '2019-05-02', '00:00:00', 'CNY', '2173.49', '2053.49', '2197.60', '2031.50', '2176.00', '2047.00', '2019-05-02 00:00:05'),
(159, '2019-05-02', '00:00:00', 'MYR', '3484.22', '3404.22', '3492.20', '3391.20', '0.00', '0.00', '2019-05-02 00:00:05'),
(160, '2019-05-02', '00:00:00', 'THB', '450.27', '442.27', '451.40', '441.40', '455.00', '426.00', '2019-05-02 00:00:05'),
(161, '2019-05-03', '00:00:00', 'USD', '14254.00', '14238.00', '14396.00', '14096.00', '14361.00', '14061.00', '2019-05-03 00:00:08'),
(162, '2019-05-03', '00:00:00', 'SGD', '10491.69', '10443.69', '10508.30', '10427.30', '10554.00', '10325.00', '2019-05-03 00:00:08'),
(163, '2019-05-03', '00:00:00', 'EUR', '16019.05', '15919.05', '16164.00', '15784.00', '16126.00', '15712.00', '2019-05-03 00:00:08'),
(164, '2019-05-03', '00:00:00', 'AUD', '10047.82', '9967.82', '10143.85', '9874.85', '10112.00', '9836.00', '2019-05-03 00:00:08'),
(165, '2019-05-03', '00:00:00', 'DKK', '2168.94', '2108.94', '2177.30', '2110.50', '2195.00', '2064.00', '2019-05-03 00:00:08'),
(166, '2019-05-03', '00:00:00', 'SEK', '1515.17', '1475.17', '1523.30', '1471.00', '1548.00', '1430.00', '2019-05-03 00:00:08'),
(167, '2019-05-03', '00:00:00', 'CAD', '10637.34', '10557.34', '10727.30', '10463.30', '10718.00', '10419.00', '2019-05-03 00:00:08'),
(168, '2019-05-03', '00:00:00', 'CHF', '14034.50', '13934.50', '14161.20', '13825.20', '14159.00', '13778.00', '2019-05-03 00:00:08'),
(169, '2019-05-03', '00:00:00', 'NZD', '9490.09', '9410.09', '9589.20', '9304.20', '9557.00', '9288.00', '2019-05-03 00:00:08'),
(170, '2019-05-03', '00:00:00', 'GBP', '18649.58', '18549.58', '18844.75', '18366.75', '18783.00', '18321.00', '2019-05-03 00:00:08'),
(171, '2019-05-03', '00:00:00', 'HKD', '1830.99', '1800.99', '1833.00', '1799.10', '1845.00', '1777.00', '2019-05-03 00:00:08'),
(172, '2019-05-03', '00:00:00', 'JPY', '129.44', '126.04', '130.17', '125.41', '130.85', '124.30', '2019-05-03 00:00:08'),
(173, '2019-05-03', '00:00:00', 'SAR', '3838.63', '3758.63', '3851.50', '3746.50', '3868.00', '3698.00', '2019-05-03 00:00:08'),
(174, '2019-05-03', '00:00:00', 'CNY', '2173.61', '2053.61', '2197.60', '2031.50', '2171.00', '2042.00', '2019-05-03 00:00:08'),
(175, '2019-05-03', '00:00:00', 'MYR', '3483.98', '3403.98', '3492.00', '3391.00', '0.00', '0.00', '2019-05-03 00:00:08'),
(176, '2019-05-03', '00:00:00', 'THB', '448.98', '440.98', '449.95', '439.95', '454.00', '424.00', '2019-05-03 00:00:08'),
(177, '2019-05-04', '00:00:00', 'USD', '14264.00', '14248.00', '14406.00', '14106.00', '14396.00', '14096.00', '2019-05-04 00:00:04'),
(178, '2019-05-04', '00:00:00', 'SGD', '10478.68', '10430.68', '10493.70', '10412.70', '10568.00', '10340.00', '2019-05-04 00:00:04'),
(179, '2019-05-04', '00:00:00', 'EUR', '15967.53', '15867.53', '16113.35', '15735.35', '16128.00', '15715.00', '2019-05-04 00:00:04'),
(180, '2019-05-04', '00:00:00', 'AUD', '10017.07', '9937.07', '10110.60', '9842.60', '10107.00', '9832.00', '2019-05-04 00:00:04'),
(181, '2019-05-04', '00:00:00', 'DKK', '2161.91', '2101.91', '2169.85', '2103.35', '2194.00', '2064.00', '2019-05-04 00:00:04'),
(182, '2019-05-04', '00:00:00', 'SEK', '1508.48', '1468.48', '1516.90', '1464.90', '1545.00', '1427.00', '2019-05-04 00:00:04'),
(183, '2019-05-04', '00:00:00', 'CAD', '10631.77', '10551.77', '10718.80', '10454.80', '10720.00', '10421.00', '2019-05-04 00:00:04'),
(184, '2019-05-04', '00:00:00', 'CHF', '14025.10', '13925.10', '14148.80', '13812.80', '14171.00', '13791.00', '2019-05-04 00:00:04'),
(185, '2019-05-04', '00:00:00', 'NZD', '9475.34', '9395.34', '9571.40', '9287.40', '9557.00', '9289.00', '2019-05-04 00:00:04'),
(186, '2019-05-04', '00:00:00', 'GBP', '18609.89', '18509.89', '18797.40', '18320.40', '18798.00', '18337.00', '2019-05-04 00:00:04'),
(187, '2019-05-04', '00:00:00', 'HKD', '1832.25', '1802.25', '1834.20', '1800.30', '1850.00', '1781.00', '2019-05-04 00:00:04'),
(188, '2019-05-04', '00:00:00', 'JPY', '129.56', '126.16', '130.27', '125.51', '131.23', '124.68', '2019-05-04 00:00:04'),
(189, '2019-05-04', '00:00:00', 'SAR', '3841.30', '3761.30', '3854.20', '3749.20', '3878.00', '3708.00', '2019-05-04 00:00:04'),
(190, '2019-05-04', '00:00:00', 'CNY', '2174.28', '2054.28', '2198.15', '2032.05', '2177.00', '2048.00', '2019-05-04 00:00:04'),
(191, '2019-05-04', '00:00:00', 'MYR', '3481.40', '3401.40', '3489.35', '3388.35', '0.00', '0.00', '2019-05-04 00:00:04'),
(192, '2019-05-04', '00:00:00', 'THB', '449.09', '441.09', '450.00', '440.00', '454.00', '425.00', '2019-05-04 00:00:04'),
(193, '2019-05-05', '00:00:00', 'USD', '14264.00', '14248.00', '14406.00', '14106.00', '14396.00', '14096.00', '2019-05-05 00:00:04'),
(194, '2019-05-05', '00:00:00', 'SGD', '10478.68', '10430.68', '10493.70', '10412.70', '10568.00', '10340.00', '2019-05-05 00:00:04'),
(195, '2019-05-05', '00:00:00', 'EUR', '15967.53', '15867.53', '16113.35', '15735.35', '16128.00', '15715.00', '2019-05-05 00:00:04'),
(196, '2019-05-05', '00:00:00', 'AUD', '10017.07', '9937.07', '10110.60', '9842.60', '10107.00', '9832.00', '2019-05-05 00:00:04'),
(197, '2019-05-05', '00:00:00', 'DKK', '2161.91', '2101.91', '2169.85', '2103.35', '2194.00', '2064.00', '2019-05-05 00:00:04'),
(198, '2019-05-05', '00:00:00', 'SEK', '1508.48', '1468.48', '1516.90', '1464.90', '1545.00', '1427.00', '2019-05-05 00:00:04'),
(199, '2019-05-05', '00:00:00', 'CAD', '10631.77', '10551.77', '10718.80', '10454.80', '10720.00', '10421.00', '2019-05-05 00:00:04'),
(200, '2019-05-05', '00:00:00', 'CHF', '14025.10', '13925.10', '14148.80', '13812.80', '14171.00', '13791.00', '2019-05-05 00:00:04'),
(201, '2019-05-05', '00:00:00', 'NZD', '9475.34', '9395.34', '9571.40', '9287.40', '9557.00', '9289.00', '2019-05-05 00:00:04'),
(202, '2019-05-05', '00:00:00', 'GBP', '18609.89', '18509.89', '18797.40', '18320.40', '18798.00', '18337.00', '2019-05-05 00:00:04'),
(203, '2019-05-05', '00:00:00', 'HKD', '1832.25', '1802.25', '1834.20', '1800.30', '1850.00', '1781.00', '2019-05-05 00:00:04'),
(204, '2019-05-05', '00:00:00', 'JPY', '129.56', '126.16', '130.27', '125.51', '131.23', '124.68', '2019-05-05 00:00:04'),
(205, '2019-05-05', '00:00:00', 'SAR', '3841.30', '3761.30', '3854.20', '3749.20', '3878.00', '3708.00', '2019-05-05 00:00:04'),
(206, '2019-05-05', '00:00:00', 'CNY', '2174.28', '2054.28', '2198.15', '2032.05', '2177.00', '2048.00', '2019-05-05 00:00:04'),
(207, '2019-05-05', '00:00:00', 'MYR', '3481.40', '3401.40', '3489.35', '3388.35', '0.00', '0.00', '2019-05-05 00:00:04'),
(208, '2019-05-05', '00:00:00', 'THB', '449.09', '441.09', '450.00', '440.00', '454.00', '425.00', '2019-05-05 00:00:04'),
(209, '2019-05-06', '00:00:00', 'USD', '14264.00', '14248.00', '14406.00', '14106.00', '14396.00', '14096.00', '2019-05-06 00:00:06'),
(210, '2019-05-06', '00:00:00', 'SGD', '10478.68', '10430.68', '10493.70', '10412.70', '10568.00', '10340.00', '2019-05-06 00:00:07'),
(211, '2019-05-06', '00:00:00', 'EUR', '15967.53', '15867.53', '16113.35', '15735.35', '16128.00', '15715.00', '2019-05-06 00:00:07'),
(212, '2019-05-06', '00:00:00', 'AUD', '10017.07', '9937.07', '10110.60', '9842.60', '10107.00', '9832.00', '2019-05-06 00:00:07'),
(213, '2019-05-06', '00:00:00', 'DKK', '2161.91', '2101.91', '2169.85', '2103.35', '2194.00', '2064.00', '2019-05-06 00:00:07'),
(214, '2019-05-06', '00:00:00', 'SEK', '1508.48', '1468.48', '1516.90', '1464.90', '1545.00', '1427.00', '2019-05-06 00:00:07'),
(215, '2019-05-06', '00:00:00', 'CAD', '10631.77', '10551.77', '10718.80', '10454.80', '10720.00', '10421.00', '2019-05-06 00:00:07'),
(216, '2019-05-06', '00:00:00', 'CHF', '14025.10', '13925.10', '14148.80', '13812.80', '14171.00', '13791.00', '2019-05-06 00:00:07'),
(217, '2019-05-06', '00:00:00', 'NZD', '9475.34', '9395.34', '9571.40', '9287.40', '9557.00', '9289.00', '2019-05-06 00:00:07'),
(218, '2019-05-06', '00:00:00', 'GBP', '18609.89', '18509.89', '18797.40', '18320.40', '18798.00', '18337.00', '2019-05-06 00:00:07'),
(219, '2019-05-06', '00:00:00', 'HKD', '1832.25', '1802.25', '1834.20', '1800.30', '1850.00', '1781.00', '2019-05-06 00:00:07'),
(220, '2019-05-06', '00:00:00', 'JPY', '129.56', '126.16', '130.27', '125.51', '131.23', '124.68', '2019-05-06 00:00:07'),
(221, '2019-05-06', '00:00:00', 'SAR', '3841.30', '3761.30', '3854.20', '3749.20', '3878.00', '3708.00', '2019-05-06 00:00:07'),
(222, '2019-05-06', '00:00:00', 'CNY', '2174.28', '2054.28', '2198.15', '2032.05', '2177.00', '2048.00', '2019-05-06 00:00:07'),
(223, '2019-05-06', '00:00:00', 'MYR', '3481.40', '3401.40', '3489.35', '3388.35', '0.00', '0.00', '2019-05-06 00:00:07'),
(224, '2019-05-06', '00:00:00', 'THB', '449.09', '441.09', '450.00', '440.00', '454.00', '425.00', '2019-05-06 00:00:07'),
(225, '2019-05-07', '00:00:00', 'USD', '14296.00', '14280.00', '14438.00', '14138.00', '14478.00', '14178.00', '2019-05-07 00:00:03'),
(226, '2019-05-07', '00:00:00', 'SGD', '10504.45', '10456.45', '10521.40', '10440.40', '10611.00', '10383.00', '2019-05-07 00:00:03'),
(227, '2019-05-07', '00:00:00', 'EUR', '16028.99', '15928.99', '16180.90', '15801.90', '16237.00', '15823.00', '2019-05-07 00:00:03'),
(228, '2019-05-07', '00:00:00', 'AUD', '10025.17', '9945.17', '10118.65', '9850.65', '10131.00', '9857.00', '2019-05-07 00:00:03'),
(229, '2019-05-07', '00:00:00', 'DKK', '2170.24', '2110.24', '2178.80', '2112.10', '2209.00', '2079.00', '2019-05-07 00:00:03'),
(230, '2019-05-07', '00:00:00', 'SEK', '1509.87', '1469.87', '1518.55', '1466.55', '1554.00', '1436.00', '2019-05-07 00:00:03'),
(231, '2019-05-07', '00:00:00', 'CAD', '10640.98', '10560.98', '10729.05', '10465.05', '10774.00', '10476.00', '2019-05-07 00:00:03'),
(232, '2019-05-07', '00:00:00', 'CHF', '14092.26', '13992.26', '14217.90', '13880.90', '14301.00', '13919.00', '2019-05-07 00:00:03'),
(233, '2019-05-07', '00:00:00', 'NZD', '9494.37', '9414.37', '9594.10', '9310.10', '9602.00', '9333.00', '2019-05-07 00:00:03'),
(234, '2019-05-07', '00:00:00', 'GBP', '18777.99', '18677.99', '18970.40', '18490.40', '19056.00', '18592.00', '2019-05-07 00:00:03'),
(235, '2019-05-07', '00:00:00', 'HKD', '1836.08', '1806.08', '1838.05', '1804.15', '1860.00', '1792.00', '2019-05-07 00:00:03'),
(236, '2019-05-07', '00:00:00', 'JPY', '130.75', '127.35', '131.48', '126.69', '133.04', '126.46', '2019-05-07 00:00:03'),
(237, '2019-05-07', '00:00:00', 'SAR', '3849.83', '3769.83', '3862.75', '3757.75', '3899.00', '3729.00', '2019-05-07 00:00:03'),
(238, '2019-05-07', '00:00:00', 'CNY', '2166.38', '2046.38', '2190.35', '2024.55', '2172.00', '2043.00', '2019-05-07 00:00:03'),
(239, '2019-05-07', '00:00:00', 'MYR', '3484.97', '3404.97', '3492.85', '3392.85', '0.00', '0.00', '2019-05-07 00:00:03'),
(240, '2019-05-07', '00:00:00', 'THB', '451.90', '443.90', '453.00', '443.00', '457.00', '428.00', '2019-05-07 00:00:03'),
(241, '2019-05-08', '00:00:00', 'USD', '14286.00', '14270.00', '14428.00', '14128.00', '14438.00', '14138.00', '2019-05-08 00:00:05'),
(242, '2019-05-08', '00:00:00', 'SGD', '10507.50', '10459.50', '10523.30', '10442.30', '10592.00', '10364.00', '2019-05-08 00:00:05'),
(243, '2019-05-08', '00:00:00', 'EUR', '16034.93', '15934.93', '16182.65', '15803.65', '16213.00', '15798.00', '2019-05-08 00:00:05'),
(244, '2019-05-08', '00:00:00', 'AUD', '10063.16', '9983.16', '10159.20', '9890.20', '10131.00', '9856.00', '2019-05-08 00:00:05'),
(245, '2019-05-08', '00:00:00', 'DKK', '2171.08', '2111.08', '2179.60', '2112.90', '2205.00', '2075.00', '2019-05-08 00:00:05'),
(246, '2019-05-08', '00:00:00', 'SEK', '1513.30', '1473.30', '1522.15', '1470.05', '1551.00', '1433.00', '2019-05-08 00:00:05'),
(247, '2019-05-08', '00:00:00', 'CAD', '10663.51', '10583.51', '10751.95', '10487.95', '10771.00', '10472.00', '2019-05-08 00:00:05'),
(248, '2019-05-08', '00:00:00', 'CHF', '14067.96', '13967.96', '14194.05', '13857.05', '14244.00', '13863.00', '2019-05-08 00:00:05'),
(249, '2019-05-08', '00:00:00', 'NZD', '9471.34', '9391.34', '9570.15', '9286.15', '9575.00', '9307.00', '2019-05-08 00:00:05'),
(250, '2019-05-08', '00:00:00', 'GBP', '18748.47', '18648.47', '18941.40', '18462.40', '18955.00', '18492.00', '2019-05-08 00:00:05'),
(251, '2019-05-08', '00:00:00', 'HKD', '1834.29', '1804.29', '1836.25', '1802.35', '1855.00', '1786.00', '2019-05-08 00:00:05'),
(252, '2019-05-08', '00:00:00', 'JPY', '130.85', '127.45', '131.58', '126.78', '132.45', '125.88', '2019-05-08 00:00:05'),
(253, '2019-05-08', '00:00:00', 'SAR', '3847.17', '3767.17', '3860.05', '3755.05', '3889.00', '3719.00', '2019-05-08 00:00:05'),
(254, '2019-05-08', '00:00:00', 'CNY', '2167.42', '2047.42', '2191.15', '2025.25', '2172.00', '2044.00', '2019-05-08 00:00:05'),
(255, '2019-05-08', '00:00:00', 'MYR', '3481.73', '3401.73', '3489.60', '3389.60', '0.00', '0.00', '2019-05-08 00:00:05'),
(256, '2019-05-08', '00:00:00', 'THB', '451.52', '443.52', '452.50', '442.50', '456.00', '427.00', '2019-05-08 00:00:05'),
(257, '2019-05-09', '00:00:00', 'USD', '14301.00', '14285.00', '14443.00', '14143.00', '14463.00', '14163.00', '2019-05-09 00:00:06'),
(258, '2019-05-09', '00:00:00', 'SGD', '10524.29', '10476.29', '10540.52', '10459.52', '10613.00', '10385.00', '2019-05-09 00:00:06'),
(259, '2019-05-09', '00:00:00', 'EUR', '16056.02', '15956.02', '16205.25', '15826.25', '16226.00', '15812.00', '2019-05-09 00:00:06'),
(260, '2019-05-09', '00:00:00', 'AUD', '10064.40', '9984.40', '10161.10', '9892.10', '10173.00', '9898.00', '2019-05-09 00:00:06'),
(261, '2019-05-09', '00:00:00', 'DKK', '2174.00', '2114.00', '2182.45', '2115.65', '2207.00', '2077.00', '2019-05-09 00:00:06'),
(262, '2019-05-09', '00:00:00', 'SEK', '1511.79', '1471.79', '1520.25', '1468.25', '1549.00', '1432.00', '2019-05-09 00:00:06'),
(263, '2019-05-09', '00:00:00', 'CAD', '10655.33', '10575.33', '10743.15', '10479.15', '10770.00', '10471.00', '2019-05-09 00:00:06'),
(264, '2019-05-09', '00:00:00', 'CHF', '14094.42', '13994.42', '14220.00', '13883.00', '14234.00', '13854.00', '2019-05-09 00:00:06'),
(265, '2019-05-09', '00:00:00', 'NZD', '9458.37', '9378.37', '9555.60', '9271.60', '9577.00', '9309.00', '2019-05-09 00:00:06'),
(266, '2019-05-09', '00:00:00', 'GBP', '18693.08', '18593.08', '18881.75', '18403.75', '18935.00', '18473.00', '2019-05-09 00:00:06'),
(267, '2019-05-09', '00:00:00', 'HKD', '1836.12', '1806.12', '1838.10', '1804.20', '1857.00', '1789.00', '2019-05-09 00:00:06'),
(268, '2019-05-09', '00:00:00', 'JPY', '131.46', '128.06', '132.24', '127.42', '133.29', '126.71', '2019-05-09 00:00:06'),
(269, '2019-05-09', '00:00:00', 'SAR', '3851.17', '3771.17', '3864.05', '3759.05', '3895.00', '3725.00', '2019-05-09 00:00:06'),
(270, '2019-05-09', '00:00:00', 'CNY', '2164.07', '2044.08', '2188.00', '2022.30', '2173.00', '2044.00', '2019-05-09 00:00:06'),
(271, '2019-05-09', '00:00:00', 'MYR', '3485.34', '3405.34', '3493.25', '3393.25', '0.00', '0.00', '2019-05-09 00:00:06'),
(272, '2019-05-09', '00:00:00', 'THB', '453.68', '445.68', '454.70', '444.70', '458.00', '428.00', '2019-05-09 00:00:06'),
(273, '2019-05-10', '00:00:00', 'USD', '14353.00', '14337.00', '14495.00', '14195.00', '14471.00', '14171.00', '2019-05-10 00:00:07'),
(274, '2019-05-10', '00:00:00', 'SGD', '10542.02', '10494.02', '10558.92', '10477.92', '10620.00', '10392.00', '2019-05-10 00:00:07'),
(275, '2019-05-10', '00:00:00', 'EUR', '16100.62', '16000.62', '16250.55', '15871.55', '16236.00', '15823.00', '2019-05-10 00:00:07'),
(276, '2019-05-10', '00:00:00', 'AUD', '10047.79', '9967.79', '10143.10', '9877.10', '10132.00', '9858.00', '2019-05-10 00:00:07'),
(277, '2019-05-10', '00:00:00', 'DKK', '2179.92', '2119.92', '2188.45', '2121.65', '2209.00', '2079.00', '2019-05-10 00:00:07'),
(278, '2019-05-10', '00:00:00', 'SEK', '1508.91', '1468.91', '1518.00', '1466.20', '1546.00', '1429.00', '2019-05-10 00:00:07'),
(279, '2019-05-10', '00:00:00', 'CAD', '10678.53', '10598.53', '10770.60', '10506.60', '10768.00', '10470.00', '2019-05-10 00:00:07'),
(280, '2019-05-10', '00:00:00', 'CHF', '14142.74', '14042.74', '14268.40', '13931.40', '14231.00', '13851.00', '2019-05-10 00:00:07'),
(281, '2019-05-10', '00:00:00', 'NZD', '9476.86', '9396.86', '9576.95', '9292.95', '9552.00', '9285.00', '2019-05-10 00:00:07'),
(282, '2019-05-10', '00:00:00', 'GBP', '18690.61', '18590.61', '18883.10', '18406.10', '18860.00', '18400.00', '2019-05-10 00:00:07'),
(283, '2019-05-10', '00:00:00', 'HKD', '1842.86', '1812.86', '1844.85', '1810.95', '1858.00', '1790.00', '2019-05-10 00:00:07'),
(284, '2019-05-10', '00:00:00', 'JPY', '132.46', '129.06', '133.22', '128.38', '133.73', '127.14', '2019-05-10 00:00:07'),
(285, '2019-05-10', '00:00:00', 'SAR', '3865.03', '3785.03', '3877.95', '3772.95', '3898.00', '3728.00', '2019-05-10 00:00:07'),
(286, '2019-05-10', '00:00:00', 'CNY', '2154.71', '2034.71', '2178.35', '2012.95', '2172.00', '2044.00', '2019-05-10 00:00:07'),
(287, '2019-05-10', '00:00:00', 'MYR', '3493.71', '3413.71', '3501.55', '3401.55', '0.00', '0.00', '2019-05-10 00:00:07'),
(288, '2019-05-10', '00:00:00', 'THB', '455.17', '447.17', '456.20', '446.20', '460.00', '430.00', '2019-05-10 00:00:07'),
(289, '2019-05-11', '00:00:00', 'USD', '14328.00', '14312.00', '14470.00', '14170.00', '14495.00', '14195.00', '2019-05-11 00:00:07'),
(290, '2019-05-11', '00:00:00', 'SGD', '10535.25', '10487.25', '10552.60', '10471.60', '10628.00', '10400.00', '2019-05-11 00:00:07'),
(291, '2019-05-11', '00:00:00', 'EUR', '16136.37', '16036.37', '16284.70', '15904.70', '16307.00', '15892.00', '2019-05-11 00:00:07'),
(292, '2019-05-11', '00:00:00', 'AUD', '10062.57', '9982.57', '10157.00', '9889.00', '10169.00', '9895.00', '2019-05-11 00:00:07'),
(293, '2019-05-11', '00:00:00', 'DKK', '2184.55', '2124.55', '2193.20', '2126.20', '2218.00', '2088.00', '2019-05-11 00:00:07'),
(294, '2019-05-11', '00:00:00', 'SEK', '1509.06', '1469.06', '1517.55', '1465.65', '1545.00', '1428.00', '2019-05-11 00:00:07'),
(295, '2019-05-11', '00:00:00', 'CAD', '10683.68', '10603.68', '10772.85', '10508.85', '10803.00', '10504.00', '2019-05-11 00:00:07'),
(296, '2019-05-11', '00:00:00', 'CHF', '14179.26', '14079.26', '14305.70', '13967.70', '14317.00', '13936.00', '2019-05-11 00:00:07'),
(297, '2019-05-11', '00:00:00', 'NZD', '9488.34', '9408.34', '9589.35', '9305.35', '9585.00', '9318.00', '2019-05-11 00:00:07'),
(298, '2019-05-11', '00:00:00', 'GBP', '18680.32', '18580.32', '18872.25', '18395.25', '18895.00', '18435.00', '2019-05-11 00:00:07'),
(299, '2019-05-11', '00:00:00', 'HKD', '1839.73', '1809.73', '1841.70', '1807.80', '1861.00', '1793.00', '2019-05-11 00:00:07'),
(300, '2019-05-11', '00:00:00', 'JPY', '132.09', '128.69', '132.85', '128.02', '133.96', '127.37', '2019-05-11 00:00:07'),
(301, '2019-05-11', '00:00:00', 'SAR', '3858.37', '3778.37', '3871.30', '3766.30', '3904.00', '3734.00', '2019-05-11 00:00:07'),
(302, '2019-05-11', '00:00:00', 'CNY', '2153.53', '2033.54', '2177.45', '2011.95', '2161.00', '2033.00', '2019-05-11 00:00:07'),
(303, '2019-05-11', '00:00:00', 'MYR', '3484.79', '3404.79', '3492.60', '3392.60', '0.00', '0.00', '2019-05-11 00:00:07'),
(304, '2019-05-11', '00:00:00', 'THB', '457.60', '449.60', '458.60', '448.60', '461.00', '432.00', '2019-05-11 00:00:07'),
(305, '2019-05-12', '00:00:00', 'USD', '14328.00', '14312.00', '14470.00', '14170.00', '14495.00', '14195.00', '2019-05-12 00:00:05'),
(306, '2019-05-12', '00:00:00', 'SGD', '10535.25', '10487.25', '10552.60', '10471.60', '10628.00', '10400.00', '2019-05-12 00:00:05'),
(307, '2019-05-12', '00:00:00', 'EUR', '16136.37', '16036.37', '16284.70', '15904.70', '16307.00', '15892.00', '2019-05-12 00:00:05'),
(308, '2019-05-12', '00:00:00', 'AUD', '10062.57', '9982.57', '10157.00', '9889.00', '10169.00', '9895.00', '2019-05-12 00:00:05'),
(309, '2019-05-12', '00:00:00', 'DKK', '2184.55', '2124.55', '2193.20', '2126.20', '2218.00', '2088.00', '2019-05-12 00:00:05'),
(310, '2019-05-12', '00:00:00', 'SEK', '1509.06', '1469.06', '1517.55', '1465.65', '1545.00', '1428.00', '2019-05-12 00:00:05'),
(311, '2019-05-12', '00:00:00', 'CAD', '10683.68', '10603.68', '10772.85', '10508.85', '10803.00', '10504.00', '2019-05-12 00:00:05'),
(312, '2019-05-12', '00:00:00', 'CHF', '14179.26', '14079.26', '14305.70', '13967.70', '14317.00', '13936.00', '2019-05-12 00:00:05'),
(313, '2019-05-12', '00:00:00', 'NZD', '9488.34', '9408.34', '9589.35', '9305.35', '9585.00', '9318.00', '2019-05-12 00:00:05'),
(314, '2019-05-12', '00:00:00', 'GBP', '18680.32', '18580.32', '18872.25', '18395.25', '18895.00', '18435.00', '2019-05-12 00:00:05'),
(315, '2019-05-12', '00:00:00', 'HKD', '1839.73', '1809.73', '1841.70', '1807.80', '1861.00', '1793.00', '2019-05-12 00:00:05'),
(316, '2019-05-12', '00:00:00', 'JPY', '132.09', '128.69', '132.85', '128.02', '133.96', '127.37', '2019-05-12 00:00:05'),
(317, '2019-05-12', '00:00:00', 'SAR', '3858.37', '3778.37', '3871.30', '3766.30', '3904.00', '3734.00', '2019-05-12 00:00:05'),
(318, '2019-05-12', '00:00:00', 'CNY', '2153.53', '2033.54', '2177.45', '2011.95', '2161.00', '2033.00', '2019-05-12 00:00:05'),
(319, '2019-05-12', '00:00:00', 'MYR', '3484.79', '3404.79', '3492.60', '3392.60', '0.00', '0.00', '2019-05-12 00:00:05'),
(320, '2019-05-12', '00:00:00', 'THB', '457.60', '449.60', '458.60', '448.60', '461.00', '432.00', '2019-05-12 00:00:05'),
(321, '2019-05-13', '00:00:00', 'USD', '14328.00', '14312.00', '14470.00', '14170.00', '14495.00', '14195.00', '2019-05-13 00:00:08'),
(322, '2019-05-13', '00:00:00', 'SGD', '10535.25', '10487.25', '10552.60', '10471.60', '10628.00', '10400.00', '2019-05-13 00:00:08'),
(323, '2019-05-13', '00:00:00', 'EUR', '16136.37', '16036.37', '16284.70', '15904.70', '16307.00', '15892.00', '2019-05-13 00:00:08'),
(324, '2019-05-13', '00:00:00', 'AUD', '10062.57', '9982.57', '10157.00', '9889.00', '10169.00', '9895.00', '2019-05-13 00:00:08'),
(325, '2019-05-13', '00:00:00', 'DKK', '2184.55', '2124.55', '2193.20', '2126.20', '2218.00', '2088.00', '2019-05-13 00:00:08'),
(326, '2019-05-13', '00:00:00', 'SEK', '1509.06', '1469.06', '1517.55', '1465.65', '1545.00', '1428.00', '2019-05-13 00:00:08'),
(327, '2019-05-13', '00:00:00', 'CAD', '10683.68', '10603.68', '10772.85', '10508.85', '10803.00', '10504.00', '2019-05-13 00:00:08'),
(328, '2019-05-13', '00:00:00', 'CHF', '14179.26', '14079.26', '14305.70', '13967.70', '14317.00', '13936.00', '2019-05-13 00:00:08'),
(329, '2019-05-13', '00:00:00', 'NZD', '9488.34', '9408.34', '9589.35', '9305.35', '9585.00', '9318.00', '2019-05-13 00:00:08'),
(330, '2019-05-13', '00:00:00', 'GBP', '18680.32', '18580.32', '18872.25', '18395.25', '18895.00', '18435.00', '2019-05-13 00:00:08'),
(331, '2019-05-13', '00:00:00', 'HKD', '1839.73', '1809.73', '1841.70', '1807.80', '1861.00', '1793.00', '2019-05-13 00:00:08'),
(332, '2019-05-13', '00:00:00', 'JPY', '132.09', '128.69', '132.85', '128.02', '133.96', '127.37', '2019-05-13 00:00:08'),
(333, '2019-05-13', '00:00:00', 'SAR', '3858.37', '3778.37', '3871.30', '3766.30', '3904.00', '3734.00', '2019-05-13 00:00:08'),
(334, '2019-05-13', '00:00:00', 'CNY', '2153.53', '2033.54', '2177.45', '2011.95', '2161.00', '2033.00', '2019-05-13 00:00:08'),
(335, '2019-05-13', '00:00:00', 'MYR', '3484.79', '3404.79', '3492.60', '3392.60', '0.00', '0.00', '2019-05-13 00:00:08'),
(336, '2019-05-13', '00:00:00', 'THB', '457.60', '449.60', '458.60', '448.60', '461.00', '432.00', '2019-05-13 00:00:08'),
(337, '2019-05-14', '00:00:00', 'USD', '14426.00', '14410.00', '14568.00', '14268.00', '14558.00', '14258.00', '2019-05-14 00:00:05'),
(338, '2019-05-14', '00:00:00', 'SGD', '10562.32', '10514.32', '10580.30', '10499.30', '10673.00', '10445.00', '2019-05-14 00:00:05'),
(339, '2019-05-14', '00:00:00', 'EUR', '16242.13', '16142.13', '16393.50', '16012.50', '16394.00', '15979.00', '2019-05-14 00:00:05'),
(340, '2019-05-14', '00:00:00', 'AUD', '10098.72', '10018.72', '10198.10', '9930.10', '10199.00', '9924.00', '2019-05-14 00:00:05'),
(341, '2019-05-14', '00:00:00', 'DKK', '2198.92', '2138.92', '2207.90', '2140.80', '2230.00', '2100.00', '2019-05-14 00:00:05'),
(342, '2019-05-14', '00:00:00', 'SEK', '1514.90', '1474.90', '1524.00', '1472.20', '1553.00', '1436.00', '2019-05-14 00:00:05'),
(343, '2019-05-14', '00:00:00', 'CAD', '10777.26', '10697.26', '10867.60', '10602.60', '10868.00', '10568.00', '2019-05-14 00:00:05'),
(344, '2019-05-14', '00:00:00', 'CHF', '14330.20', '14230.20', '14459.70', '14119.70', '14447.00', '14065.00', '2019-05-14 00:00:05'),
(345, '2019-05-14', '00:00:00', 'NZD', '9529.93', '9449.93', '9633.90', '9349.90', '9621.00', '9353.00', '2019-05-14 00:00:05'),
(346, '2019-05-14', '00:00:00', 'GBP', '18808.54', '18708.54', '19004.40', '18526.40', '18973.00', '18513.00', '2019-05-14 00:00:05'),
(347, '2019-05-14', '00:00:00', 'HKD', '1852.09', '1822.09', '1854.10', '1820.10', '1869.00', '1801.00', '2019-05-14 00:00:05'),
(348, '2019-05-14', '00:00:00', 'JPY', '133.16', '129.76', '133.92', '129.08', '134.75', '128.15', '2019-05-14 00:00:05'),
(349, '2019-05-14', '00:00:00', 'SAR', '3884.49', '3804.49', '3897.50', '3791.50', '3921.00', '3751.00', '2019-05-14 00:00:05'),
(350, '2019-05-14', '00:00:00', 'CNY', '2149.34', '2029.34', '2173.75', '2008.65', '2172.00', '2043.00', '2019-05-14 00:00:05'),
(351, '2019-05-14', '00:00:00', 'MYR', '3500.88', '3420.88', '3508.20', '3408.20', '0.00', '0.00', '2019-05-14 00:00:05'),
(352, '2019-05-14', '00:00:00', 'THB', '459.47', '451.47', '460.50', '450.50', '465.00', '436.00', '2019-05-14 00:00:05'),
(353, '2019-05-15', '00:00:00', 'USD', '14436.00', '14420.00', '14578.00', '14278.00', '14600.00', '14300.00', '2019-05-15 00:00:05'),
(354, '2019-05-15', '00:00:00', 'SGD', '10565.39', '10517.39', '10581.40', '10500.40', '10663.00', '10436.00', '2019-05-15 00:00:05'),
(355, '2019-05-15', '00:00:00', 'EUR', '16266.35', '16166.35', '16413.50', '16032.50', '16444.00', '16029.00', '2019-05-15 00:00:05'),
(356, '2019-05-15', '00:00:00', 'AUD', '10058.08', '9978.08', '10153.15', '9888.15', '10183.00', '9909.00', '2019-05-15 00:00:05'),
(357, '2019-05-15', '00:00:00', 'DKK', '2201.30', '2141.30', '2210.00', '2142.80', '2236.00', '2106.00', '2019-05-15 00:00:05'),
(358, '2019-05-15', '00:00:00', 'SEK', '1523.09', '1483.09', '1531.60', '1479.60', '1557.00', '1440.00', '2019-05-15 00:00:05'),
(359, '2019-05-15', '00:00:00', 'CAD', '10757.58', '10677.58', '10845.95', '10581.95', '10871.00', '10572.00', '2019-05-15 00:00:05'),
(360, '2019-05-15', '00:00:00', 'CHF', '14372.02', '14272.02', '14499.80', '14158.80', '14565.00', '14181.00', '2019-05-15 00:00:05'),
(361, '2019-05-15', '00:00:00', 'NZD', '9535.07', '9455.07', '9636.15', '9352.15', '9638.00', '9371.00', '2019-05-15 00:00:05'),
(362, '2019-05-15', '00:00:00', 'GBP', '18727.77', '18627.77', '18913.05', '18439.05', '18966.00', '18507.00', '2019-05-15 00:00:05'),
(363, '2019-05-15', '00:00:00', 'HKD', '1853.13', '1823.13', '1855.15', '1821.25', '1875.00', '1806.00', '2019-05-15 00:00:05'),
(364, '2019-05-15', '00:00:00', 'JPY', '133.27', '129.86', '134.03', '129.19', '135.75', '129.14', '2019-05-15 00:00:05'),
(365, '2019-05-15', '00:00:00', 'SAR', '3887.26', '3807.26', '3900.25', '3794.25', '3932.00', '3762.00', '2019-05-15 00:00:05'),
(366, '2019-05-15', '00:00:00', 'CNY', '2147.74', '2027.75', '2171.25', '2006.25', '2161.00', '2033.00', '2019-05-15 00:00:05'),
(367, '2019-05-15', '00:00:00', 'MYR', '3499.54', '3419.54', '3507.25', '3407.25', '0.00', '0.00', '2019-05-15 00:00:05'),
(368, '2019-05-15', '00:00:00', 'THB', '462.04', '454.04', '463.05', '453.05', '466.00', '436.00', '2019-05-15 00:00:05'),
(369, '2019-05-16', '00:00:00', 'USD', '14466.00', '14450.00', '14608.00', '14308.00', '14578.00', '14278.00', '2019-05-16 00:00:05'),
(370, '2019-05-16', '00:00:00', 'SGD', '10593.10', '10545.10', '10609.52', '10528.52', '10646.00', '10419.00', '2019-05-16 00:00:05'),
(371, '2019-05-16', '00:00:00', 'EUR', '16263.20', '16163.20', '16413.70', '16033.70', '16371.00', '15957.00', '2019-05-16 00:00:05'),
(372, '2019-05-16', '00:00:00', 'AUD', '10056.50', '9976.50', '10150.70', '9885.70', '10133.00', '9860.00', '2019-05-16 00:00:05'),
(373, '2019-05-16', '00:00:00', 'DKK', '2201.12', '2141.12', '2209.85', '2142.85', '2226.00', '2096.00', '2019-05-16 00:00:05'),
(374, '2019-05-16', '00:00:00', 'SEK', '1526.25', '1486.25', '1534.75', '1482.65', '1558.00', '1441.00', '2019-05-16 00:00:05'),
(375, '2019-05-16', '00:00:00', 'CAD', '10772.68', '10692.68', '10865.05', '10601.05', '10854.00', '10556.00', '2019-05-16 00:00:05'),
(376, '2019-05-16', '00:00:00', 'CHF', '14426.77', '14326.77', '14557.10', '14215.10', '14511.00', '14128.00', '2019-05-16 00:00:05'),
(377, '2019-05-16', '00:00:00', 'NZD', '9530.24', '9450.24', '9629.75', '9345.75', '9605.00', '9338.00', '2019-05-16 00:00:05'),
(378, '2019-05-16', '00:00:00', 'GBP', '18719.62', '18619.62', '18909.65', '18436.65', '18849.00', '18392.00', '2019-05-16 00:00:05'),
(379, '2019-05-16', '00:00:00', 'HKD', '1856.83', '1826.83', '1858.80', '1824.90', '1872.00', '1803.00', '2019-05-16 00:00:05'),
(380, '2019-05-16', '00:00:00', 'JPY', '133.84', '130.44', '134.61', '129.76', '135.12', '128.52', '2019-05-16 00:00:05'),
(381, '2019-05-16', '00:00:00', 'SAR', '3895.16', '3815.16', '3908.50', '3802.50', '3926.00', '3756.00', '2019-05-16 00:00:05'),
(382, '2019-05-16', '00:00:00', 'CNY', '2153.30', '2033.30', '2176.80', '2011.80', '2158.00', '2030.00', '2019-05-16 00:00:05'),
(383, '2019-05-16', '00:00:00', 'MYR', '3503.41', '3423.41', '3511.10', '3411.10', '0.00', '0.00', '2019-05-16 00:00:05'),
(384, '2019-05-16', '00:00:00', 'THB', '461.97', '453.97', '462.95', '452.95', '466.00', '437.00', '2019-05-16 00:00:05'),
(385, '2019-05-17', '00:00:00', 'USD', '14454.00', '14438.00', '14596.00', '14296.00', '14608.00', '14308.00', '2019-05-17 00:00:05'),
(386, '2019-05-17', '00:00:00', 'SGD', '10582.78', '10534.78', '10599.20', '10518.20', '10676.00', '10449.00', '2019-05-17 00:00:05'),
(387, '2019-05-17', '00:00:00', 'EUR', '16258.41', '16158.41', '16409.00', '16028.00', '16410.00', '15996.00', '2019-05-17 00:00:05'),
(388, '2019-05-17', '00:00:00', 'AUD', '10052.52', '9972.52', '10146.75', '9881.75', '10146.00', '9873.00', '2019-05-17 00:00:05'),
(389, '2019-05-17', '00:00:00', 'DKK', '2200.45', '2140.46', '2209.35', '2142.25', '2231.00', '2101.00', '2019-05-17 00:00:05'),
(390, '2019-05-17', '00:00:00', 'SEK', '1526.47', '1486.47', '1535.05', '1482.95', '1563.00', '1445.00', '2019-05-17 00:00:05'),
(391, '2019-05-17', '00:00:00', 'CAD', '10810.55', '10730.55', '10903.95', '10637.95', '10901.00', '10602.00', '2019-05-17 00:00:05'),
(392, '2019-05-17', '00:00:00', 'CHF', '14378.51', '14278.51', '14510.50', '14169.50', '14538.00', '14155.00', '2019-05-17 00:00:05'),
(393, '2019-05-17', '00:00:00', 'NZD', '9546.19', '9466.19', '9646.60', '9362.60', '9613.00', '9346.00', '2019-05-17 00:00:05'),
(394, '2019-05-17', '00:00:00', 'GBP', '18595.05', '18495.05', '18781.85', '18310.85', '18801.00', '18346.00', '2019-05-17 00:00:05'),
(395, '2019-05-17', '00:00:00', 'HKD', '1855.45', '1825.45', '1857.45', '1823.55', '1876.00', '1807.00', '2019-05-17 00:00:05'),
(396, '2019-05-17', '00:00:00', 'JPY', '133.55', '130.14', '134.34', '129.49', '135.53', '128.93', '2019-05-17 00:00:05'),
(397, '2019-05-17', '00:00:00', 'SAR', '3891.95', '3811.95', '3904.95', '3798.95', '3934.00', '3764.00', '2019-05-17 00:00:05'),
(398, '2019-05-17', '00:00:00', 'CNY', '2151.20', '2031.19', '2174.55', '2009.55', '2163.00', '2035.00', '2019-05-17 00:00:05'),
(399, '2019-05-17', '00:00:00', 'MYR', '3508.84', '3428.84', '3516.65', '3416.65', '0.00', '0.00', '2019-05-17 00:00:05'),
(400, '2019-05-17', '00:00:00', 'THB', '461.30', '453.30', '462.45', '452.45', '468.00', '438.00', '2019-05-17 00:00:05'),
(401, '2019-05-18', '00:00:00', 'USD', '14451.00', '14405.00', '14596.00', '14296.00', '14616.00', '14316.00', '2019-05-18 00:00:04'),
(402, '2019-05-18', '00:00:00', 'SGD', '10526.36', '10478.36', '10542.52', '10462.52', '10645.00', '10419.00', '2019-05-18 00:00:05'),
(403, '2019-05-18', '00:00:00', 'EUR', '16194.85', '16094.85', '16344.80', '15965.80', '16367.00', '15953.00', '2019-05-18 00:00:05'),
(404, '2019-05-18', '00:00:00', 'AUD', '9975.24', '9895.24', '10070.90', '9806.90', '10102.00', '9830.00', '2019-05-18 00:00:05'),
(405, '2019-05-18', '00:00:00', 'DKK', '2191.80', '2131.80', '2200.35', '2133.55', '2226.00', '2096.00', '2019-05-18 00:00:05'),
(406, '2019-05-18', '00:00:00', 'SEK', '1518.90', '1478.90', '1527.65', '1475.75', '1556.00', '1439.00', '2019-05-18 00:00:05'),
(407, '2019-05-18', '00:00:00', 'CAD', '10751.85', '10671.85', '10841.65', '10577.65', '10884.00', '10585.00', '2019-05-18 00:00:05'),
(408, '2019-05-18', '00:00:00', 'CHF', '14365.73', '14265.73', '14494.65', '14154.65', '14509.00', '14126.00', '2019-05-18 00:00:05'),
(409, '2019-05-18', '00:00:00', 'NZD', '9476.85', '9396.85', '9575.15', '9292.15', '9577.00', '9311.00', '2019-05-18 00:00:05'),
(410, '2019-05-18', '00:00:00', 'GBP', '18516.32', '18416.32', '18700.20', '18230.20', '18724.00', '18270.00', '2019-05-18 00:00:05'),
(411, '2019-05-18', '00:00:00', 'HKD', '1855.41', '1825.41', '1857.40', '1823.50', '1876.00', '1808.00', '2019-05-18 00:00:05'),
(412, '2019-05-18', '00:00:00', 'JPY', '133.46', '130.06', '134.24', '129.39', '135.03', '128.44', '2019-05-18 00:00:05'),
(413, '2019-05-18', '00:00:00', 'SAR', '3891.95', '3811.95', '3904.95', '3798.95', '3936.00', '3766.00', '2019-05-18 00:00:05'),
(414, '2019-05-18', '00:00:00', 'CNY', '2140.33', '2020.33', '2164.25', '1999.35', '2162.00', '2034.00', '2019-05-18 00:00:05'),
(415, '2019-05-18', '00:00:00', 'MYR', '3499.29', '3419.29', '3506.50', '3406.50', '0.00', '0.00', '2019-05-18 00:00:05'),
(416, '2019-05-18', '00:00:00', 'THB', '459.14', '451.14', '460.10', '450.10', '466.00', '436.00', '2019-05-18 00:00:05'),
(417, '2019-05-19', '00:00:00', 'USD', '14451.00', '14405.00', '14596.00', '14296.00', '14616.00', '14316.00', '2019-05-19 00:00:05'),
(418, '2019-05-19', '00:00:00', 'SGD', '10526.36', '10478.36', '10542.52', '10462.52', '10645.00', '10419.00', '2019-05-19 00:00:05'),
(419, '2019-05-19', '00:00:00', 'EUR', '16194.85', '16094.85', '16344.80', '15965.80', '16367.00', '15953.00', '2019-05-19 00:00:05'),
(420, '2019-05-19', '00:00:00', 'AUD', '9975.24', '9895.24', '10070.90', '9806.90', '10102.00', '9830.00', '2019-05-19 00:00:05');
INSERT INTO `kurs_all` (`id`, `kursDate`, `kursTime`, `currency`, `rate_jual`, `rate_beli`, `tt_jual`, `tt_beli`, `notes_jual`, `notes_beli`, `inputDate`) VALUES
(421, '2019-05-19', '00:00:00', 'DKK', '2191.80', '2131.80', '2200.35', '2133.55', '2226.00', '2096.00', '2019-05-19 00:00:05'),
(422, '2019-05-19', '00:00:00', 'SEK', '1518.90', '1478.90', '1527.65', '1475.75', '1556.00', '1439.00', '2019-05-19 00:00:05'),
(423, '2019-05-19', '00:00:00', 'CAD', '10751.85', '10671.85', '10841.65', '10577.65', '10884.00', '10585.00', '2019-05-19 00:00:05'),
(424, '2019-05-19', '00:00:00', 'CHF', '14365.73', '14265.73', '14494.65', '14154.65', '14509.00', '14126.00', '2019-05-19 00:00:05'),
(425, '2019-05-19', '00:00:00', 'NZD', '9476.85', '9396.85', '9575.15', '9292.15', '9577.00', '9311.00', '2019-05-19 00:00:05'),
(426, '2019-05-19', '00:00:00', 'GBP', '18516.32', '18416.32', '18700.20', '18230.20', '18724.00', '18270.00', '2019-05-19 00:00:05'),
(427, '2019-05-19', '00:00:00', 'HKD', '1855.41', '1825.41', '1857.40', '1823.50', '1876.00', '1808.00', '2019-05-19 00:00:05'),
(428, '2019-05-19', '00:00:00', 'JPY', '133.46', '130.06', '134.24', '129.39', '135.03', '128.44', '2019-05-19 00:00:05'),
(429, '2019-05-19', '00:00:00', 'SAR', '3891.95', '3811.95', '3904.95', '3798.95', '3936.00', '3766.00', '2019-05-19 00:00:05'),
(430, '2019-05-19', '00:00:00', 'CNY', '2140.33', '2020.33', '2164.25', '1999.35', '2162.00', '2034.00', '2019-05-19 00:00:05'),
(431, '2019-05-19', '00:00:00', 'MYR', '3499.29', '3419.29', '3506.50', '3406.50', '0.00', '0.00', '2019-05-19 00:00:05'),
(432, '2019-05-19', '00:00:00', 'THB', '459.14', '451.14', '460.10', '450.10', '466.00', '436.00', '2019-05-19 00:00:05'),
(433, '2019-05-20', '00:00:00', 'USD', '14451.00', '14405.00', '14596.00', '14296.00', '14616.00', '14316.00', '2019-05-20 00:00:06'),
(434, '2019-05-20', '00:00:00', 'SGD', '10526.36', '10478.36', '10542.52', '10462.52', '10645.00', '10419.00', '2019-05-20 00:00:07'),
(435, '2019-05-20', '00:00:00', 'EUR', '16194.85', '16094.85', '16344.80', '15965.80', '16367.00', '15953.00', '2019-05-20 00:00:07'),
(436, '2019-05-20', '00:00:00', 'AUD', '9975.24', '9895.24', '10070.90', '9806.90', '10102.00', '9830.00', '2019-05-20 00:00:07'),
(437, '2019-05-20', '00:00:00', 'DKK', '2191.80', '2131.80', '2200.35', '2133.55', '2226.00', '2096.00', '2019-05-20 00:00:07'),
(438, '2019-05-20', '00:00:00', 'SEK', '1518.90', '1478.90', '1527.65', '1475.75', '1556.00', '1439.00', '2019-05-20 00:00:07'),
(439, '2019-05-20', '00:00:00', 'CAD', '10751.85', '10671.85', '10841.65', '10577.65', '10884.00', '10585.00', '2019-05-20 00:00:07'),
(440, '2019-05-20', '00:00:00', 'CHF', '14365.73', '14265.73', '14494.65', '14154.65', '14509.00', '14126.00', '2019-05-20 00:00:07'),
(441, '2019-05-20', '00:00:00', 'NZD', '9476.85', '9396.85', '9575.15', '9292.15', '9577.00', '9311.00', '2019-05-20 00:00:07'),
(442, '2019-05-20', '00:00:00', 'GBP', '18516.32', '18416.32', '18700.20', '18230.20', '18724.00', '18270.00', '2019-05-20 00:00:07'),
(443, '2019-05-20', '00:00:00', 'HKD', '1855.41', '1825.41', '1857.40', '1823.50', '1876.00', '1808.00', '2019-05-20 00:00:07'),
(444, '2019-05-20', '00:00:00', 'JPY', '133.46', '130.06', '134.24', '129.39', '135.03', '128.44', '2019-05-20 00:00:07'),
(445, '2019-05-20', '00:00:00', 'SAR', '3891.95', '3811.95', '3904.95', '3798.95', '3936.00', '3766.00', '2019-05-20 00:00:07'),
(446, '2019-05-20', '00:00:00', 'CNY', '2140.33', '2020.33', '2164.25', '1999.35', '2162.00', '2034.00', '2019-05-20 00:00:07'),
(447, '2019-05-20', '00:00:00', 'MYR', '3499.29', '3419.29', '3506.50', '3406.50', '0.00', '0.00', '2019-05-20 00:00:07'),
(448, '2019-05-20', '00:00:00', 'THB', '459.14', '451.14', '460.10', '450.10', '466.00', '436.00', '2019-05-20 00:00:07'),
(449, '2019-05-21', '00:00:00', 'USD', '14458.00', '14412.00', '14603.00', '14303.00', '14635.00', '14335.00', '2019-05-21 00:00:05'),
(450, '2019-05-21', '00:00:00', 'SGD', '10530.30', '10482.30', '10546.10', '10466.10', '10639.00', '10413.00', '2019-05-21 00:00:05'),
(451, '2019-05-21', '00:00:00', 'EUR', '16173.77', '16073.77', '16323.50', '15944.50', '16382.00', '15969.00', '2019-05-21 00:00:05'),
(452, '2019-05-21', '00:00:00', 'AUD', '10041.48', '9961.48', '10137.00', '9872.00', '10153.00', '9881.00', '2019-05-21 00:00:05'),
(453, '2019-05-21', '00:00:00', 'DKK', '2188.91', '2128.91', '2197.50', '2130.90', '2228.00', '2098.00', '2019-05-21 00:00:05'),
(454, '2019-05-21', '00:00:00', 'SEK', '1518.18', '1478.18', '1527.10', '1475.30', '1561.00', '1444.00', '2019-05-21 00:00:05'),
(455, '2019-05-21', '00:00:00', 'CAD', '10790.52', '10710.52', '10879.95', '10614.95', '10927.00', '10628.00', '2019-05-21 00:00:05'),
(456, '2019-05-21', '00:00:00', 'CHF', '14369.83', '14269.83', '14498.75', '14158.75', '14519.00', '14137.00', '2019-05-21 00:00:05'),
(457, '2019-05-21', '00:00:00', 'NZD', '9487.93', '9407.93', '9588.50', '9305.50', '9600.00', '9334.00', '2019-05-21 00:00:05'),
(458, '2019-05-21', '00:00:00', 'GBP', '18468.18', '18368.18', '18659.55', '18190.55', '18677.00', '18224.00', '2019-05-21 00:00:05'),
(459, '2019-05-21', '00:00:00', 'HKD', '1856.33', '1826.33', '1858.30', '1824.40', '1879.00', '1811.00', '2019-05-21 00:00:05'),
(460, '2019-05-21', '00:00:00', 'JPY', '133.07', '129.67', '133.83', '129.00', '134.75', '128.17', '2019-05-21 00:00:05'),
(461, '2019-05-21', '00:00:00', 'SAR', '3893.83', '3813.83', '3906.85', '3800.85', '3942.00', '3772.00', '2019-05-21 00:00:05'),
(462, '2019-05-21', '00:00:00', 'CNY', '2141.67', '2021.66', '2165.00', '2000.20', '2154.00', '2026.00', '2019-05-21 00:00:05'),
(463, '2019-05-21', '00:00:00', 'MYR', '3500.55', '3420.55', '3508.20', '3408.20', '0.00', '0.00', '2019-05-21 00:00:05'),
(464, '2019-05-21', '00:00:00', 'THB', '457.64', '449.64', '458.85', '448.85', '464.00', '435.00', '2019-05-21 00:00:05'),
(465, '2019-05-22', '00:00:00', 'USD', '14488.00', '14468.00', '14628.00', '14328.00', '14603.00', '14303.00', '2019-05-22 00:00:05'),
(466, '2019-05-22', '00:00:00', 'SGD', '10523.68', '10475.68', '10539.80', '10459.80', '10615.00', '10389.00', '2019-05-22 00:00:06'),
(467, '2019-05-22', '00:00:00', 'EUR', '16189.35', '16089.35', '16338.30', '15959.30', '16346.00', '15933.00', '2019-05-22 00:00:06'),
(468, '2019-05-22', '00:00:00', 'AUD', '9993.62', '9913.62', '10087.15', '9823.15', '10140.00', '9867.00', '2019-05-22 00:00:06'),
(469, '2019-05-22', '00:00:00', 'DKK', '2191.06', '2131.06', '2199.45', '2132.85', '2223.00', '2093.00', '2019-05-22 00:00:06'),
(470, '2019-05-22', '00:00:00', 'SEK', '1516.59', '1476.59', '1525.15', '1473.35', '1552.00', '1435.00', '2019-05-22 00:00:06'),
(471, '2019-05-22', '00:00:00', 'CAD', '10821.95', '10741.95', '10911.55', '10646.55', '10908.00', '10608.00', '2019-05-22 00:00:06'),
(472, '2019-05-22', '00:00:00', 'CHF', '14383.24', '14283.24', '14513.50', '14173.50', '14516.00', '14133.00', '2019-05-22 00:00:06'),
(473, '2019-05-22', '00:00:00', 'NZD', '9460.84', '9380.84', '9559.60', '9276.60', '9583.00', '9317.00', '2019-05-22 00:00:06'),
(474, '2019-05-22', '00:00:00', 'GBP', '18431.99', '18331.99', '18614.05', '18146.05', '18618.00', '18166.00', '2019-05-22 00:00:06'),
(475, '2019-05-22', '00:00:00', 'HKD', '1859.49', '1829.49', '1861.45', '1827.55', '1875.00', '1807.00', '2019-05-22 00:00:06'),
(476, '2019-05-22', '00:00:00', 'JPY', '133.18', '129.78', '133.95', '129.12', '134.64', '128.05', '2019-05-22 00:00:06'),
(477, '2019-05-22', '00:00:00', 'SAR', '3900.49', '3820.49', '3913.55', '3807.55', '3933.00', '3763.00', '2019-05-22 00:00:06'),
(478, '2019-05-22', '00:00:00', 'CNY', '2145.48', '2025.48', '2168.90', '2004.10', '2151.00', '2023.00', '2019-05-22 00:00:06'),
(479, '2019-05-22', '00:00:00', 'MYR', '3498.67', '3418.67', '3506.20', '3406.20', '0.00', '0.00', '2019-05-22 00:00:06'),
(480, '2019-05-22', '00:00:00', 'THB', '457.50', '449.50', '458.50', '448.50', '463.00', '434.00', '2019-05-22 00:00:06'),
(481, '2019-05-23', '00:00:00', 'USD', '14540.00', '14495.00', '14680.00', '14380.00', '14628.00', '14328.00', '2019-05-23 00:00:05'),
(482, '2019-05-23', '00:00:00', 'SGD', '10553.75', '10505.75', '10567.90', '10487.90', '10623.00', '10397.00', '2019-05-23 00:00:05'),
(483, '2019-05-23', '00:00:00', 'EUR', '16255.31', '16155.31', '16403.70', '16024.70', '16367.00', '15954.00', '2019-05-23 00:00:05'),
(484, '2019-05-23', '00:00:00', 'AUD', '10041.00', '9961.00', '10131.80', '9867.80', '10099.00', '9827.00', '2019-05-23 00:00:05'),
(485, '2019-05-23', '00:00:00', 'DKK', '2199.82', '2139.82', '2208.25', '2141.55', '2226.00', '2096.00', '2019-05-23 00:00:05'),
(486, '2019-05-23', '00:00:00', 'SEK', '1525.00', '1485.00', '1533.50', '1481.70', '1557.00', '1440.00', '2019-05-23 00:00:05'),
(487, '2019-05-23', '00:00:00', 'CAD', '10882.07', '10802.07', '10969.95', '10703.95', '10945.00', '10645.00', '2019-05-23 00:00:05'),
(488, '2019-05-23', '00:00:00', 'CHF', '14424.76', '14324.76', '14552.10', '14212.10', '14519.00', '14137.00', '2019-05-23 00:00:05'),
(489, '2019-05-23', '00:00:00', 'NZD', '9484.50', '9404.50', '9583.35', '9300.35', '9544.00', '9279.00', '2019-05-23 00:00:05'),
(490, '2019-05-23', '00:00:00', 'GBP', '18460.97', '18360.97', '18639.15', '18172.15', '18631.00', '18180.00', '2019-05-23 00:00:05'),
(491, '2019-05-23', '00:00:00', 'HKD', '1866.10', '1836.10', '1868.05', '1834.15', '1878.00', '1810.00', '2019-05-23 00:00:05'),
(492, '2019-05-23', '00:00:00', 'JPY', '133.33', '129.93', '134.09', '129.27', '134.42', '127.84', '2019-05-23 00:00:05'),
(493, '2019-05-23', '00:00:00', 'SAR', '3914.36', '3834.36', '3927.45', '3821.45', '3940.00', '3770.00', '2019-05-23 00:00:05'),
(494, '2019-05-23', '00:00:00', 'CNY', '2154.80', '2034.80', '2177.60', '2012.70', '2158.00', '2030.00', '2019-05-23 00:00:05'),
(495, '2019-05-23', '00:00:00', 'MYR', '3511.10', '3431.10', '3518.65', '3418.65', '0.00', '0.00', '2019-05-23 00:00:05'),
(496, '2019-05-23', '00:00:00', 'THB', '458.28', '450.28', '459.25', '449.25', '462.00', '433.00', '2019-05-23 00:00:05'),
(497, '2019-05-24', '00:00:00', 'USD', '14465.00', '14445.00', '14605.00', '14305.00', '14670.00', '14370.00', '2019-05-24 00:00:05'),
(498, '2019-05-24', '00:00:00', 'SGD', '10488.78', '10440.78', '10503.30', '10423.30', '10635.00', '10409.00', '2019-05-24 00:00:05'),
(499, '2019-05-24', '00:00:00', 'EUR', '16151.43', '16051.43', '16298.00', '15920.00', '16399.00', '15986.00', '2019-05-24 00:00:05'),
(500, '2019-05-24', '00:00:00', 'AUD', '9974.92', '9894.92', '10066.90', '9803.90', '10117.00', '9846.00', '2019-05-24 00:00:05'),
(501, '2019-05-24', '00:00:00', 'DKK', '2186.28', '2126.28', '2194.45', '2127.85', '2230.00', '2100.00', '2019-05-24 00:00:05'),
(502, '2019-05-24', '00:00:00', 'SEK', '1519.03', '1479.03', '1527.55', '1475.65', '1564.00', '1447.00', '2019-05-24 00:00:05'),
(503, '2019-05-24', '00:00:00', 'CAD', '10778.43', '10698.43', '10867.65', '10603.65', '10946.00', '10647.00', '2019-05-24 00:00:05'),
(504, '2019-05-24', '00:00:00', 'CHF', '14373.22', '14273.22', '14502.15', '14162.15', '14584.00', '14201.00', '2019-05-24 00:00:05'),
(505, '2019-05-24', '00:00:00', 'NZD', '9422.74', '9342.74', '9521.25', '9239.25', '9548.00', '9283.00', '2019-05-24 00:00:05'),
(506, '2019-05-24', '00:00:00', 'GBP', '18297.27', '18197.27', '18484.10', '18018.10', '18599.00', '18150.00', '2019-05-24 00:00:05'),
(507, '2019-05-24', '00:00:00', 'HKD', '1856.61', '1826.61', '1858.60', '1824.60', '1883.00', '1815.00', '2019-05-24 00:00:05'),
(508, '2019-05-24', '00:00:00', 'JPY', '132.93', '129.53', '133.71', '128.88', '135.18', '128.60', '2019-05-24 00:00:05'),
(509, '2019-05-24', '00:00:00', 'SAR', '3894.36', '3814.36', '3907.40', '3801.40', '3951.00', '3781.00', '2019-05-24 00:00:06'),
(510, '2019-05-24', '00:00:00', 'CNY', '2143.17', '2023.16', '2166.40', '2001.60', '2163.00', '2035.00', '2019-05-24 00:00:06'),
(511, '2019-05-24', '00:00:00', 'MYR', '3487.00', '3407.00', '3494.45', '3394.45', '0.00', '0.00', '2019-05-24 00:00:06'),
(512, '2019-05-24', '00:00:00', 'THB', '455.93', '447.93', '456.80', '446.80', '464.00', '434.00', '2019-05-24 00:00:06'),
(513, '2019-05-25', '00:00:00', 'USD', '14400.00', '14380.00', '14540.00', '14240.00', '14605.00', '14305.00', '2019-05-25 00:00:05'),
(514, '2019-05-25', '00:00:00', 'SGD', '10471.98', '10423.98', '10489.22', '10409.22', '10592.00', '10367.00', '2019-05-25 00:00:05'),
(515, '2019-05-25', '00:00:00', 'EUR', '16145.93', '16045.93', '16295.20', '15916.20', '16369.00', '15955.00', '2019-05-25 00:00:05'),
(516, '2019-05-25', '00:00:00', 'AUD', '9966.22', '9886.22', '10061.30', '9797.30', '10112.00', '9840.00', '2019-05-25 00:00:05'),
(517, '2019-05-25', '00:00:00', 'DKK', '2185.32', '2125.32', '2193.80', '2127.00', '2226.00', '2096.00', '2019-05-25 00:00:05'),
(518, '2019-05-25', '00:00:00', 'SEK', '1521.11', '1481.11', '1530.00', '1477.90', '1560.00', '1443.00', '2019-05-25 00:00:05'),
(519, '2019-05-25', '00:00:00', 'CAD', '10734.91', '10654.91', '10825.75', '10561.75', '10870.00', '10572.00', '2019-05-25 00:00:05'),
(520, '2019-05-25', '00:00:00', 'CHF', '14399.10', '14299.10', '14531.30', '14189.30', '14602.00', '14217.00', '2019-05-25 00:00:05'),
(521, '2019-05-25', '00:00:00', 'NZD', '9435.23', '9355.23', '9535.50', '9252.50', '9557.00', '9291.00', '2019-05-25 00:00:05'),
(522, '2019-05-25', '00:00:00', 'GBP', '18312.35', '18212.35', '18499.20', '18032.20', '18523.00', '18073.00', '2019-05-25 00:00:05'),
(523, '2019-05-25', '00:00:00', 'HKD', '1848.58', '1818.58', '1850.50', '1816.60', '1875.00', '1807.00', '2019-05-25 00:00:05'),
(524, '2019-05-25', '00:00:00', 'JPY', '133.07', '129.67', '133.85', '129.00', '135.17', '128.58', '2019-05-25 00:00:05'),
(525, '2019-05-25', '00:00:00', 'SAR', '3877.13', '3797.13', '3890.10', '3785.10', '3934.00', '3764.00', '2019-05-25 00:00:05'),
(526, '2019-05-25', '00:00:00', 'CNY', '2138.74', '2018.75', '2162.05', '1997.05', '2152.00', '2024.00', '2019-05-25 00:00:05'),
(527, '2019-05-25', '00:00:00', 'MYR', '3473.14', '3393.14', '3480.55', '3381.55', '0.00', '0.00', '2019-05-25 00:00:05'),
(528, '2019-05-25', '00:00:00', 'THB', '455.74', '447.74', '456.60', '446.60', '463.00', '434.00', '2019-05-25 00:00:05'),
(529, '2019-05-26', '00:00:00', 'USD', '14400.00', '14380.00', '14540.00', '14240.00', '14605.00', '14305.00', '2019-05-26 00:00:05'),
(530, '2019-05-26', '00:00:00', 'SGD', '10471.98', '10423.98', '10489.22', '10409.22', '10592.00', '10367.00', '2019-05-26 00:00:05'),
(531, '2019-05-26', '00:00:00', 'EUR', '16145.93', '16045.93', '16295.20', '15916.20', '16369.00', '15955.00', '2019-05-26 00:00:05'),
(532, '2019-05-26', '00:00:00', 'AUD', '9966.22', '9886.22', '10061.30', '9797.30', '10112.00', '9840.00', '2019-05-26 00:00:05'),
(533, '2019-05-26', '00:00:00', 'DKK', '2185.32', '2125.32', '2193.80', '2127.00', '2226.00', '2096.00', '2019-05-26 00:00:05'),
(534, '2019-05-26', '00:00:00', 'SEK', '1521.11', '1481.11', '1530.00', '1477.90', '1560.00', '1443.00', '2019-05-26 00:00:05'),
(535, '2019-05-26', '00:00:00', 'CAD', '10734.91', '10654.91', '10825.75', '10561.75', '10870.00', '10572.00', '2019-05-26 00:00:05'),
(536, '2019-05-26', '00:00:00', 'CHF', '14399.10', '14299.10', '14531.30', '14189.30', '14602.00', '14217.00', '2019-05-26 00:00:05'),
(537, '2019-05-26', '00:00:00', 'NZD', '9435.23', '9355.23', '9535.50', '9252.50', '9557.00', '9291.00', '2019-05-26 00:00:05'),
(538, '2019-05-26', '00:00:00', 'GBP', '18312.35', '18212.35', '18499.20', '18032.20', '18523.00', '18073.00', '2019-05-26 00:00:05'),
(539, '2019-05-26', '00:00:00', 'HKD', '1848.58', '1818.58', '1850.50', '1816.60', '1875.00', '1807.00', '2019-05-26 00:00:05'),
(540, '2019-05-26', '00:00:00', 'JPY', '133.07', '129.67', '133.85', '129.00', '135.17', '128.58', '2019-05-26 00:00:05'),
(541, '2019-05-26', '00:00:00', 'SAR', '3877.13', '3797.13', '3890.10', '3785.10', '3934.00', '3764.00', '2019-05-26 00:00:05'),
(542, '2019-05-26', '00:00:00', 'CNY', '2138.74', '2018.75', '2162.05', '1997.05', '2152.00', '2024.00', '2019-05-26 00:00:05'),
(543, '2019-05-26', '00:00:00', 'MYR', '3473.14', '3393.14', '3480.55', '3381.55', '0.00', '0.00', '2019-05-26 00:00:05'),
(544, '2019-05-26', '00:00:00', 'THB', '455.74', '447.74', '456.60', '446.60', '463.00', '434.00', '2019-05-26 00:00:05'),
(545, '2019-05-27', '00:00:00', 'USD', '14400.00', '14380.00', '14540.00', '14240.00', '14605.00', '14305.00', '2019-05-27 00:00:11'),
(546, '2019-05-27', '00:00:00', 'SGD', '10471.98', '10423.98', '10489.22', '10409.22', '10592.00', '10367.00', '2019-05-27 00:00:11'),
(547, '2019-05-27', '00:00:00', 'EUR', '16145.93', '16045.93', '16295.20', '15916.20', '16369.00', '15955.00', '2019-05-27 00:00:11'),
(548, '2019-05-27', '00:00:00', 'AUD', '9966.22', '9886.22', '10061.30', '9797.30', '10112.00', '9840.00', '2019-05-27 00:00:11'),
(549, '2019-05-27', '00:00:00', 'DKK', '2185.32', '2125.32', '2193.80', '2127.00', '2226.00', '2096.00', '2019-05-27 00:00:11'),
(550, '2019-05-27', '00:00:00', 'SEK', '1521.11', '1481.11', '1530.00', '1477.90', '1560.00', '1443.00', '2019-05-27 00:00:11'),
(551, '2019-05-27', '00:00:00', 'CAD', '10734.91', '10654.91', '10825.75', '10561.75', '10870.00', '10572.00', '2019-05-27 00:00:11'),
(552, '2019-05-27', '00:00:00', 'CHF', '14399.10', '14299.10', '14531.30', '14189.30', '14602.00', '14217.00', '2019-05-27 00:00:11'),
(553, '2019-05-27', '00:00:00', 'NZD', '9435.23', '9355.23', '9535.50', '9252.50', '9557.00', '9291.00', '2019-05-27 00:00:11'),
(554, '2019-05-27', '00:00:00', 'GBP', '18312.35', '18212.35', '18499.20', '18032.20', '18523.00', '18073.00', '2019-05-27 00:00:11'),
(555, '2019-05-27', '00:00:00', 'HKD', '1848.58', '1818.58', '1850.50', '1816.60', '1875.00', '1807.00', '2019-05-27 00:00:11'),
(556, '2019-05-27', '00:00:00', 'JPY', '133.07', '129.67', '133.85', '129.00', '135.17', '128.58', '2019-05-27 00:00:11'),
(557, '2019-05-27', '00:00:00', 'SAR', '3877.13', '3797.13', '3890.10', '3785.10', '3934.00', '3764.00', '2019-05-27 00:00:11'),
(558, '2019-05-27', '00:00:00', 'CNY', '2138.74', '2018.75', '2162.05', '1997.05', '2152.00', '2024.00', '2019-05-27 00:00:11'),
(559, '2019-05-27', '00:00:00', 'MYR', '3473.14', '3393.14', '3480.55', '3381.55', '0.00', '0.00', '2019-05-27 00:00:11'),
(560, '2019-05-27', '00:00:00', 'THB', '455.74', '447.74', '456.60', '446.60', '463.00', '434.00', '2019-05-27 00:00:11'),
(561, '2019-05-28', '00:00:00', 'USD', '14388.00', '14368.00', '14528.00', '14228.00', '14540.00', '14240.00', '2019-05-28 00:00:05'),
(562, '2019-05-28', '00:00:00', 'SGD', '10485.29', '10437.29', '10501.12', '10421.12', '10580.00', '10354.00', '2019-05-28 00:00:05'),
(563, '2019-05-28', '00:00:00', 'EUR', '16141.85', '16041.85', '16290.45', '15910.45', '16331.00', '15917.00', '2019-05-28 00:00:05'),
(564, '2019-05-28', '00:00:00', 'AUD', '9993.17', '9913.17', '10087.85', '9823.85', '10109.00', '9836.00', '2019-05-28 00:00:05'),
(565, '2019-05-28', '00:00:00', 'DKK', '2184.75', '2124.75', '2193.30', '2126.50', '2221.00', '2091.00', '2019-05-28 00:00:05'),
(566, '2019-05-28', '00:00:00', 'SEK', '1524.09', '1484.09', '1532.95', '1480.75', '1566.00', '1448.00', '2019-05-28 00:00:05'),
(567, '2019-05-28', '00:00:00', 'CAD', '10731.95', '10651.95', '10820.85', '10556.85', '10852.00', '10553.00', '2019-05-28 00:00:05'),
(568, '2019-05-28', '00:00:00', 'CHF', '14345.09', '14245.09', '14477.40', '14136.40', '14554.00', '14169.00', '2019-05-28 00:00:05'),
(569, '2019-05-28', '00:00:00', 'NZD', '9451.84', '9371.84', '9552.30', '9269.30', '9557.00', '9290.00', '2019-05-28 00:00:05'),
(570, '2019-05-28', '00:00:00', 'GBP', '18319.40', '18219.40', '18505.70', '18038.70', '18531.00', '18079.00', '2019-05-28 00:00:05'),
(571, '2019-05-28', '00:00:00', 'HKD', '1846.72', '1816.72', '1848.70', '1814.80', '1867.00', '1799.00', '2019-05-28 00:00:05'),
(572, '2019-05-28', '00:00:00', 'JPY', '133.01', '129.61', '133.81', '128.96', '134.87', '128.27', '2019-05-28 00:00:05'),
(573, '2019-05-28', '00:00:00', 'SAR', '3873.83', '3793.83', '3887.10', '3782.10', '3916.00', '3746.00', '2019-05-28 00:00:05'),
(574, '2019-05-28', '00:00:00', 'CNY', '2140.40', '2020.39', '2163.80', '1998.80', '2146.00', '2018.00', '2019-05-28 00:00:05'),
(575, '2019-05-28', '00:00:00', 'MYR', '3474.37', '3394.37', '3481.85', '3381.85', '0.00', '0.00', '2019-05-28 00:00:05'),
(576, '2019-05-28', '00:00:00', 'THB', '456.00', '448.00', '456.80', '446.80', '462.00', '432.00', '2019-05-28 00:00:05'),
(577, '2019-05-29', '00:00:00', 'USD', '14382.00', '14362.00', '14522.00', '14222.00', '14528.00', '14228.00', '2019-05-29 00:00:04'),
(578, '2019-05-29', '00:00:00', 'SGD', '10457.40', '10409.40', '10473.50', '10393.50', '10565.00', '10339.00', '2019-05-29 00:00:04'),
(579, '2019-05-29', '00:00:00', 'EUR', '16138.74', '16038.74', '16283.70', '15904.70', '16299.00', '15885.00', '2019-05-29 00:00:04'),
(580, '2019-05-29', '00:00:00', 'AUD', '9983.27', '9903.27', '10083.70', '9819.70', '10092.00', '9820.00', '2019-05-29 00:00:04'),
(581, '2019-05-29', '00:00:00', 'DKK', '2184.80', '2124.80', '2192.95', '2126.15', '2217.00', '2087.00', '2019-05-29 00:00:04'),
(582, '2019-05-29', '00:00:00', 'SEK', '1523.26', '1483.26', '1531.90', '1479.70', '1558.00', '1441.00', '2019-05-29 00:00:04'),
(583, '2019-05-29', '00:00:00', 'CAD', '10716.37', '10636.37', '10811.55', '10547.55', '10842.00', '10543.00', '2019-05-29 00:00:04'),
(584, '2019-05-29', '00:00:00', 'CHF', '14371.16', '14271.16', '14494.50', '14152.50', '14516.00', '14132.00', '2019-05-29 00:00:04'),
(585, '2019-05-29', '00:00:00', 'NZD', '9454.37', '9374.37', '9552.70', '9269.70', '9549.00', '9282.00', '2019-05-29 00:00:04'),
(586, '2019-05-29', '00:00:00', 'GBP', '18261.48', '18161.48', '18444.35', '17978.35', '18456.00', '18006.00', '2019-05-29 00:00:04'),
(587, '2019-05-29', '00:00:00', 'HKD', '1846.12', '1816.12', '1848.15', '1814.25', '1865.00', '1797.00', '2019-05-29 00:00:04'),
(588, '2019-05-29', '00:00:00', 'JPY', '133.24', '129.84', '134.00', '129.14', '134.59', '128.00', '2019-05-29 00:00:04'),
(589, '2019-05-29', '00:00:00', 'SAR', '3872.33', '3792.33', '3885.30', '3780.30', '3913.00', '3743.00', '2019-05-29 00:00:04'),
(590, '2019-05-29', '00:00:00', 'CNY', '2134.55', '2014.56', '2157.90', '1993.00', '2145.00', '2017.00', '2019-05-29 00:00:04'),
(591, '2019-05-29', '00:00:00', 'MYR', '3472.53', '3392.53', '3480.85', '3380.85', '0.00', '0.00', '2019-05-29 00:00:04'),
(592, '2019-05-29', '00:00:00', 'THB', '456.38', '448.38', '457.35', '447.35', '461.00', '431.00', '2019-05-29 00:00:04'),
(593, '2019-05-30', '00:00:00', 'USD', '14398.00', '14378.00', '14538.00', '14238.00', '14568.00', '14268.00', '2019-05-30 00:00:07'),
(594, '2019-05-30', '00:00:00', 'SGD', '10427.47', '10379.47', '10442.62', '10362.62', '10565.00', '10339.00', '2019-05-30 00:00:07'),
(595, '2019-05-30', '00:00:00', 'EUR', '16099.82', '15999.82', '16245.00', '15867.00', '16310.00', '15897.00', '2019-05-30 00:00:07'),
(596, '2019-05-30', '00:00:00', 'AUD', '9991.46', '9911.46', '10083.15', '9819.15', '10124.00', '9852.00', '2019-05-30 00:00:07'),
(597, '2019-05-30', '00:00:00', 'DKK', '2179.57', '2119.57', '2187.65', '2121.05', '2219.00', '2089.00', '2019-05-30 00:00:07'),
(598, '2019-05-30', '00:00:00', 'SEK', '1517.14', '1477.14', '1525.35', '1473.35', '1562.00', '1444.00', '2019-05-30 00:00:07'),
(599, '2019-05-30', '00:00:00', 'CAD', '10691.47', '10611.47', '10780.95', '10517.95', '10831.00', '10532.00', '2019-05-30 00:00:07'),
(600, '2019-05-30', '00:00:00', 'CHF', '14357.88', '14257.88', '14487.35', '14146.35', '14517.00', '14133.00', '2019-05-30 00:00:07'),
(601, '2019-05-30', '00:00:00', 'NZD', '9418.82', '9338.82', '9516.80', '9234.80', '9572.00', '9306.00', '2019-05-30 00:00:07'),
(602, '2019-05-30', '00:00:00', 'GBP', '18240.75', '18140.75', '18422.55', '17956.55', '18479.00', '18029.00', '2019-05-30 00:00:07'),
(603, '2019-05-30', '00:00:00', 'HKD', '1848.00', '1818.00', '1849.95', '1816.05', '1870.00', '1802.00', '2019-05-30 00:00:07'),
(604, '2019-05-30', '00:00:00', 'JPY', '133.40', '130.00', '134.17', '129.31', '135.46', '128.85', '2019-05-30 00:00:07'),
(605, '2019-05-30', '00:00:00', 'SAR', '3876.65', '3796.65', '3889.55', '3784.55', '3924.00', '3754.00', '2019-05-30 00:00:07'),
(606, '2019-05-30', '00:00:00', 'CNY', '2135.98', '2015.98', '2159.30', '1994.40', '2146.00', '2019.00', '2019-05-30 00:00:07'),
(607, '2019-05-30', '00:00:00', 'MYR', '3468.57', '3388.57', '3475.95', '3376.95', '0.00', '0.00', '2019-05-30 00:00:07'),
(608, '2019-05-30', '00:00:00', 'THB', '456.03', '448.03', '456.95', '446.95', '461.00', '432.00', '2019-05-30 00:00:07'),
(609, '2019-05-31', '00:00:00', 'USD', '14398.00', '14378.00', '14538.00', '14238.00', '14568.00', '14268.00', '2019-05-31 00:00:05'),
(610, '2019-05-31', '00:00:00', 'SGD', '10427.47', '10379.47', '10442.62', '10362.62', '10565.00', '10339.00', '2019-05-31 00:00:05'),
(611, '2019-05-31', '00:00:00', 'EUR', '16099.82', '15999.82', '16245.00', '15867.00', '16310.00', '15897.00', '2019-05-31 00:00:05'),
(612, '2019-05-31', '00:00:00', 'AUD', '9991.46', '9911.46', '10083.15', '9819.15', '10124.00', '9852.00', '2019-05-31 00:00:05'),
(613, '2019-05-31', '00:00:00', 'DKK', '2179.57', '2119.57', '2187.65', '2121.05', '2219.00', '2089.00', '2019-05-31 00:00:05'),
(614, '2019-05-31', '00:00:00', 'SEK', '1517.14', '1477.14', '1525.35', '1473.35', '1562.00', '1444.00', '2019-05-31 00:00:05'),
(615, '2019-05-31', '00:00:00', 'CAD', '10691.47', '10611.47', '10780.95', '10517.95', '10831.00', '10532.00', '2019-05-31 00:00:05'),
(616, '2019-05-31', '00:00:00', 'CHF', '14357.88', '14257.88', '14487.35', '14146.35', '14517.00', '14133.00', '2019-05-31 00:00:05'),
(617, '2019-05-31', '00:00:00', 'NZD', '9418.82', '9338.82', '9516.80', '9234.80', '9572.00', '9306.00', '2019-05-31 00:00:05'),
(618, '2019-05-31', '00:00:00', 'GBP', '18240.75', '18140.75', '18422.55', '17956.55', '18479.00', '18029.00', '2019-05-31 00:00:05'),
(619, '2019-05-31', '00:00:00', 'HKD', '1848.00', '1818.00', '1849.95', '1816.05', '1870.00', '1802.00', '2019-05-31 00:00:05'),
(620, '2019-05-31', '00:00:00', 'JPY', '133.40', '130.00', '134.17', '129.31', '135.46', '128.85', '2019-05-31 00:00:05'),
(621, '2019-05-31', '00:00:00', 'SAR', '3876.65', '3796.65', '3889.55', '3784.55', '3924.00', '3754.00', '2019-05-31 00:00:05'),
(622, '2019-05-31', '00:00:00', 'CNY', '2135.98', '2015.98', '2159.30', '1994.40', '2146.00', '2019.00', '2019-05-31 00:00:05'),
(623, '2019-05-31', '00:00:00', 'MYR', '3468.57', '3388.57', '3475.95', '3376.95', '0.00', '0.00', '2019-05-31 00:00:05'),
(624, '2019-05-31', '00:00:00', 'THB', '456.03', '448.03', '456.95', '446.95', '461.00', '432.00', '2019-05-31 00:00:05'),
(625, '2019-06-01', '00:00:00', 'USD', '14300.00', '14260.00', '14425.00', '14125.00', '14429.00', '14129.00', '2019-06-01 00:00:08'),
(626, '2019-06-01', '00:00:00', 'SGD', '10388.85', '10340.85', '10404.60', '10324.60', '10472.00', '10247.00', '2019-06-01 00:00:08'),
(627, '2019-06-01', '00:00:00', 'EUR', '15968.77', '15868.77', '16114.45', '15736.45', '16112.00', '15699.00', '2019-06-01 00:00:08'),
(628, '2019-06-01', '00:00:00', 'AUD', '9918.30', '9838.30', '10013.45', '9749.45', '10012.00', '9740.00', '2019-06-01 00:00:08'),
(629, '2019-06-01', '00:00:00', 'DKK', '2161.71', '2101.71', '2169.55', '2103.15', '2192.00', '2062.00', '2019-06-01 00:00:08'),
(630, '2019-06-01', '00:00:00', 'SEK', '1514.95', '1474.95', '1523.40', '1471.20', '1553.00', '1436.00', '2019-06-01 00:00:08'),
(631, '2019-06-01', '00:00:00', 'CAD', '10585.17', '10505.17', '10675.85', '10413.85', '10684.00', '10386.00', '2019-06-01 00:00:08'),
(632, '2019-06-01', '00:00:00', 'CHF', '14258.22', '14158.22', '14390.50', '14049.50', '14396.00', '14012.00', '2019-06-01 00:00:08'),
(633, '2019-06-01', '00:00:00', 'NZD', '9345.87', '9265.87', '9444.25', '9162.25', '9431.00', '9165.00', '2019-06-01 00:00:08'),
(634, '2019-06-01', '00:00:00', 'GBP', '18050.78', '17950.78', '18234.70', '17770.70', '18237.00', '17788.00', '2019-06-01 00:00:08'),
(635, '2019-06-01', '00:00:00', 'HKD', '1834.95', '1804.95', '1836.95', '1803.05', '1854.00', '1785.00', '2019-06-01 00:00:08'),
(636, '2019-06-01', '00:00:00', 'JPY', '132.89', '129.49', '133.69', '128.82', '134.62', '128.01', '2019-06-01 00:00:08'),
(637, '2019-06-01', '00:00:00', 'SAR', '3846.57', '3766.57', '3859.45', '3754.45', '3887.00', '3717.00', '2019-06-01 00:00:08'),
(638, '2019-06-01', '00:00:00', 'CNY', '2120.62', '2000.61', '2144.10', '1979.20', '2128.00', '2001.00', '2019-06-01 00:00:08'),
(639, '2019-06-01', '00:00:00', 'MYR', '3446.52', '3366.52', '3453.90', '3354.90', '0.00', '0.00', '2019-06-01 00:00:08'),
(640, '2019-06-01', '00:00:00', 'THB', '455.10', '447.10', '456.00', '446.00', '459.00', '430.00', '2019-06-01 00:00:08'),
(641, '2019-06-02', '00:00:00', 'USD', '14300.00', '14260.00', '14425.00', '14125.00', '14429.00', '14129.00', '2019-06-02 00:00:05'),
(642, '2019-06-02', '00:00:00', 'SGD', '10388.85', '10340.85', '10404.60', '10324.60', '10472.00', '10247.00', '2019-06-02 00:00:05'),
(643, '2019-06-02', '00:00:00', 'EUR', '15968.77', '15868.77', '16114.45', '15736.45', '16112.00', '15699.00', '2019-06-02 00:00:05'),
(644, '2019-06-02', '00:00:00', 'AUD', '9918.30', '9838.30', '10013.45', '9749.45', '10012.00', '9740.00', '2019-06-02 00:00:05'),
(645, '2019-06-02', '00:00:00', 'DKK', '2161.71', '2101.71', '2169.55', '2103.15', '2192.00', '2062.00', '2019-06-02 00:00:05'),
(646, '2019-06-02', '00:00:00', 'SEK', '1514.95', '1474.95', '1523.40', '1471.20', '1553.00', '1436.00', '2019-06-02 00:00:05'),
(647, '2019-06-02', '00:00:00', 'CAD', '10585.17', '10505.17', '10675.85', '10413.85', '10684.00', '10386.00', '2019-06-02 00:00:05'),
(648, '2019-06-02', '00:00:00', 'CHF', '14258.22', '14158.22', '14390.50', '14049.50', '14396.00', '14012.00', '2019-06-02 00:00:05'),
(649, '2019-06-02', '00:00:00', 'NZD', '9345.87', '9265.87', '9444.25', '9162.25', '9431.00', '9165.00', '2019-06-02 00:00:05'),
(650, '2019-06-02', '00:00:00', 'GBP', '18050.78', '17950.78', '18234.70', '17770.70', '18237.00', '17788.00', '2019-06-02 00:00:05'),
(651, '2019-06-02', '00:00:00', 'HKD', '1834.95', '1804.95', '1836.95', '1803.05', '1854.00', '1785.00', '2019-06-02 00:00:05'),
(652, '2019-06-02', '00:00:00', 'JPY', '132.89', '129.49', '133.69', '128.82', '134.62', '128.01', '2019-06-02 00:00:05'),
(653, '2019-06-02', '00:00:00', 'SAR', '3846.57', '3766.57', '3859.45', '3754.45', '3887.00', '3717.00', '2019-06-02 00:00:05'),
(654, '2019-06-02', '00:00:00', 'CNY', '2120.62', '2000.61', '2144.10', '1979.20', '2128.00', '2001.00', '2019-06-02 00:00:05'),
(655, '2019-06-02', '00:00:00', 'MYR', '3446.52', '3366.52', '3453.90', '3354.90', '0.00', '0.00', '2019-06-02 00:00:05'),
(656, '2019-06-02', '00:00:00', 'THB', '455.10', '447.10', '456.00', '446.00', '459.00', '430.00', '2019-06-02 00:00:05'),
(657, '2019-06-03', '00:00:00', 'USD', '14300.00', '14260.00', '14425.00', '14125.00', '14429.00', '14129.00', '2019-06-03 00:00:40'),
(658, '2019-06-03', '00:00:00', 'SGD', '10388.85', '10340.85', '10404.60', '10324.60', '10472.00', '10247.00', '2019-06-03 00:00:40'),
(659, '2019-06-03', '00:00:00', 'EUR', '15968.77', '15868.77', '16114.45', '15736.45', '16112.00', '15699.00', '2019-06-03 00:00:40'),
(660, '2019-06-03', '00:00:00', 'AUD', '9918.30', '9838.30', '10013.45', '9749.45', '10012.00', '9740.00', '2019-06-03 00:00:40'),
(661, '2019-06-03', '00:00:00', 'DKK', '2161.71', '2101.71', '2169.55', '2103.15', '2192.00', '2062.00', '2019-06-03 00:00:40'),
(662, '2019-06-03', '00:00:00', 'SEK', '1514.95', '1474.95', '1523.40', '1471.20', '1553.00', '1436.00', '2019-06-03 00:00:40'),
(663, '2019-06-03', '00:00:00', 'CAD', '10585.17', '10505.17', '10675.85', '10413.85', '10684.00', '10386.00', '2019-06-03 00:00:40'),
(664, '2019-06-03', '00:00:00', 'CHF', '14258.22', '14158.22', '14390.50', '14049.50', '14396.00', '14012.00', '2019-06-03 00:00:40'),
(665, '2019-06-03', '00:00:00', 'NZD', '9345.87', '9265.87', '9444.25', '9162.25', '9431.00', '9165.00', '2019-06-03 00:00:40'),
(666, '2019-06-03', '00:00:00', 'GBP', '18050.78', '17950.78', '18234.70', '17770.70', '18237.00', '17788.00', '2019-06-03 00:00:40'),
(667, '2019-06-03', '00:00:00', 'HKD', '1834.95', '1804.95', '1836.95', '1803.05', '1854.00', '1785.00', '2019-06-03 00:00:40'),
(668, '2019-06-03', '00:00:00', 'JPY', '132.89', '129.49', '133.69', '128.82', '134.62', '128.01', '2019-06-03 00:00:40'),
(669, '2019-06-03', '00:00:00', 'SAR', '3846.57', '3766.57', '3859.45', '3754.45', '3887.00', '3717.00', '2019-06-03 00:00:40'),
(670, '2019-06-03', '00:00:00', 'CNY', '2120.62', '2000.61', '2144.10', '1979.20', '2128.00', '2001.00', '2019-06-03 00:00:40'),
(671, '2019-06-03', '00:00:00', 'MYR', '3446.52', '3366.52', '3453.90', '3354.90', '0.00', '0.00', '2019-06-03 00:00:40'),
(672, '2019-06-03', '00:00:00', 'THB', '455.10', '447.10', '456.00', '446.00', '459.00', '430.00', '2019-06-03 00:00:40'),
(673, '2019-06-04', '00:00:00', 'USD', '14300.00', '14260.00', '14425.00', '14125.00', '14429.00', '14129.00', '2019-06-04 00:00:04'),
(674, '2019-06-04', '00:00:00', 'SGD', '10388.85', '10340.85', '10404.60', '10324.60', '10472.00', '10247.00', '2019-06-04 00:00:04'),
(675, '2019-06-04', '00:00:00', 'EUR', '15968.77', '15868.77', '16114.45', '15736.45', '16112.00', '15699.00', '2019-06-04 00:00:04'),
(676, '2019-06-04', '00:00:00', 'AUD', '9918.30', '9838.30', '10013.45', '9749.45', '10012.00', '9740.00', '2019-06-04 00:00:04'),
(677, '2019-06-04', '00:00:00', 'DKK', '2161.71', '2101.71', '2169.55', '2103.15', '2192.00', '2062.00', '2019-06-04 00:00:04'),
(678, '2019-06-04', '00:00:00', 'SEK', '1514.95', '1474.95', '1523.40', '1471.20', '1553.00', '1436.00', '2019-06-04 00:00:04'),
(679, '2019-06-04', '00:00:00', 'CAD', '10585.17', '10505.17', '10675.85', '10413.85', '10684.00', '10386.00', '2019-06-04 00:00:04'),
(680, '2019-06-04', '00:00:00', 'CHF', '14258.22', '14158.22', '14390.50', '14049.50', '14396.00', '14012.00', '2019-06-04 00:00:04'),
(681, '2019-06-04', '00:00:00', 'NZD', '9345.87', '9265.87', '9444.25', '9162.25', '9431.00', '9165.00', '2019-06-04 00:00:04'),
(682, '2019-06-04', '00:00:00', 'GBP', '18050.78', '17950.78', '18234.70', '17770.70', '18237.00', '17788.00', '2019-06-04 00:00:04'),
(683, '2019-06-04', '00:00:00', 'HKD', '1834.95', '1804.95', '1836.95', '1803.05', '1854.00', '1785.00', '2019-06-04 00:00:04'),
(684, '2019-06-04', '00:00:00', 'JPY', '132.89', '129.49', '133.69', '128.82', '134.62', '128.01', '2019-06-04 00:00:04'),
(685, '2019-06-04', '00:00:00', 'SAR', '3846.57', '3766.57', '3859.45', '3754.45', '3887.00', '3717.00', '2019-06-04 00:00:04'),
(686, '2019-06-04', '00:00:00', 'CNY', '2120.62', '2000.61', '2144.10', '1979.20', '2128.00', '2001.00', '2019-06-04 00:00:04'),
(687, '2019-06-04', '00:00:00', 'MYR', '3446.52', '3366.52', '3453.90', '3354.90', '0.00', '0.00', '2019-06-04 00:00:04'),
(688, '2019-06-04', '00:00:00', 'THB', '455.10', '447.10', '456.00', '446.00', '459.00', '430.00', '2019-06-04 00:00:04'),
(689, '2019-06-05', '00:00:00', 'USD', '14300.00', '14260.00', '14425.00', '14125.00', '14429.00', '14129.00', '2019-06-05 00:00:05'),
(690, '2019-06-05', '00:00:00', 'SGD', '10388.85', '10340.85', '10404.60', '10324.60', '10472.00', '10247.00', '2019-06-05 00:00:05'),
(691, '2019-06-05', '00:00:00', 'EUR', '15968.77', '15868.77', '16114.45', '15736.45', '16112.00', '15699.00', '2019-06-05 00:00:05'),
(692, '2019-06-05', '00:00:00', 'AUD', '9918.30', '9838.30', '10013.45', '9749.45', '10012.00', '9740.00', '2019-06-05 00:00:05'),
(693, '2019-06-05', '00:00:00', 'DKK', '2161.71', '2101.71', '2169.55', '2103.15', '2192.00', '2062.00', '2019-06-05 00:00:05'),
(694, '2019-06-05', '00:00:00', 'SEK', '1514.95', '1474.95', '1523.40', '1471.20', '1553.00', '1436.00', '2019-06-05 00:00:05'),
(695, '2019-06-05', '00:00:00', 'CAD', '10585.17', '10505.17', '10675.85', '10413.85', '10684.00', '10386.00', '2019-06-05 00:00:05'),
(696, '2019-06-05', '00:00:00', 'CHF', '14258.22', '14158.22', '14390.50', '14049.50', '14396.00', '14012.00', '2019-06-05 00:00:05'),
(697, '2019-06-05', '00:00:00', 'NZD', '9345.87', '9265.87', '9444.25', '9162.25', '9431.00', '9165.00', '2019-06-05 00:00:05'),
(698, '2019-06-05', '00:00:00', 'GBP', '18050.78', '17950.78', '18234.70', '17770.70', '18237.00', '17788.00', '2019-06-05 00:00:05'),
(699, '2019-06-05', '00:00:00', 'HKD', '1834.95', '1804.95', '1836.95', '1803.05', '1854.00', '1785.00', '2019-06-05 00:00:05'),
(700, '2019-06-05', '00:00:00', 'JPY', '132.89', '129.49', '133.69', '128.82', '134.62', '128.01', '2019-06-05 00:00:05'),
(701, '2019-06-05', '00:00:00', 'SAR', '3846.57', '3766.57', '3859.45', '3754.45', '3887.00', '3717.00', '2019-06-05 00:00:05'),
(702, '2019-06-05', '00:00:00', 'CNY', '2120.62', '2000.61', '2144.10', '1979.20', '2128.00', '2001.00', '2019-06-05 00:00:05'),
(703, '2019-06-05', '00:00:00', 'MYR', '3446.52', '3366.52', '3453.90', '3354.90', '0.00', '0.00', '2019-06-05 00:00:05'),
(704, '2019-06-05', '00:00:00', 'THB', '455.10', '447.10', '456.00', '446.00', '459.00', '430.00', '2019-06-05 00:00:05'),
(705, '2019-06-06', '00:00:00', 'USD', '14300.00', '14260.00', '14425.00', '14125.00', '14429.00', '14129.00', '2019-06-06 00:00:05'),
(706, '2019-06-06', '00:00:00', 'SGD', '10388.85', '10340.85', '10404.60', '10324.60', '10472.00', '10247.00', '2019-06-06 00:00:05'),
(707, '2019-06-06', '00:00:00', 'EUR', '15968.77', '15868.77', '16114.45', '15736.45', '16112.00', '15699.00', '2019-06-06 00:00:05'),
(708, '2019-06-06', '00:00:00', 'AUD', '9918.30', '9838.30', '10013.45', '9749.45', '10012.00', '9740.00', '2019-06-06 00:00:05'),
(709, '2019-06-06', '00:00:00', 'DKK', '2161.71', '2101.71', '2169.55', '2103.15', '2192.00', '2062.00', '2019-06-06 00:00:05'),
(710, '2019-06-06', '00:00:00', 'SEK', '1514.95', '1474.95', '1523.40', '1471.20', '1553.00', '1436.00', '2019-06-06 00:00:05'),
(711, '2019-06-06', '00:00:00', 'CAD', '10585.17', '10505.17', '10675.85', '10413.85', '10684.00', '10386.00', '2019-06-06 00:00:05'),
(712, '2019-06-06', '00:00:00', 'CHF', '14258.22', '14158.22', '14390.50', '14049.50', '14396.00', '14012.00', '2019-06-06 00:00:05'),
(713, '2019-06-06', '00:00:00', 'NZD', '9345.87', '9265.87', '9444.25', '9162.25', '9431.00', '9165.00', '2019-06-06 00:00:05'),
(714, '2019-06-06', '00:00:00', 'GBP', '18050.78', '17950.78', '18234.70', '17770.70', '18237.00', '17788.00', '2019-06-06 00:00:05'),
(715, '2019-06-06', '00:00:00', 'HKD', '1834.95', '1804.95', '1836.95', '1803.05', '1854.00', '1785.00', '2019-06-06 00:00:05'),
(716, '2019-06-06', '00:00:00', 'JPY', '132.89', '129.49', '133.69', '128.82', '134.62', '128.01', '2019-06-06 00:00:05'),
(717, '2019-06-06', '00:00:00', 'SAR', '3846.57', '3766.57', '3859.45', '3754.45', '3887.00', '3717.00', '2019-06-06 00:00:05'),
(718, '2019-06-06', '00:00:00', 'CNY', '2120.62', '2000.61', '2144.10', '1979.20', '2128.00', '2001.00', '2019-06-06 00:00:05'),
(719, '2019-06-06', '00:00:00', 'MYR', '3446.52', '3366.52', '3453.90', '3354.90', '0.00', '0.00', '2019-06-06 00:00:05'),
(720, '2019-06-06', '00:00:00', 'THB', '455.10', '447.10', '456.00', '446.00', '459.00', '430.00', '2019-06-06 00:00:05'),
(721, '2019-06-07', '00:00:00', 'USD', '14300.00', '14260.00', '14425.00', '14125.00', '14429.00', '14129.00', '2019-06-07 00:00:05'),
(722, '2019-06-07', '00:00:00', 'SGD', '10388.85', '10340.85', '10404.60', '10324.60', '10472.00', '10247.00', '2019-06-07 00:00:05'),
(723, '2019-06-07', '00:00:00', 'EUR', '15968.77', '15868.77', '16114.45', '15736.45', '16112.00', '15699.00', '2019-06-07 00:00:05'),
(724, '2019-06-07', '00:00:00', 'AUD', '9918.30', '9838.30', '10013.45', '9749.45', '10012.00', '9740.00', '2019-06-07 00:00:05'),
(725, '2019-06-07', '00:00:00', 'DKK', '2161.71', '2101.71', '2169.55', '2103.15', '2192.00', '2062.00', '2019-06-07 00:00:05'),
(726, '2019-06-07', '00:00:00', 'SEK', '1514.95', '1474.95', '1523.40', '1471.20', '1553.00', '1436.00', '2019-06-07 00:00:05'),
(727, '2019-06-07', '00:00:00', 'CAD', '10585.17', '10505.17', '10675.85', '10413.85', '10684.00', '10386.00', '2019-06-07 00:00:05'),
(728, '2019-06-07', '00:00:00', 'CHF', '14258.22', '14158.22', '14390.50', '14049.50', '14396.00', '14012.00', '2019-06-07 00:00:05'),
(729, '2019-06-07', '00:00:00', 'NZD', '9345.87', '9265.87', '9444.25', '9162.25', '9431.00', '9165.00', '2019-06-07 00:00:05'),
(730, '2019-06-07', '00:00:00', 'GBP', '18050.78', '17950.78', '18234.70', '17770.70', '18237.00', '17788.00', '2019-06-07 00:00:05'),
(731, '2019-06-07', '00:00:00', 'HKD', '1834.95', '1804.95', '1836.95', '1803.05', '1854.00', '1785.00', '2019-06-07 00:00:05'),
(732, '2019-06-07', '00:00:00', 'JPY', '132.89', '129.49', '133.69', '128.82', '134.62', '128.01', '2019-06-07 00:00:05'),
(733, '2019-06-07', '00:00:00', 'SAR', '3846.57', '3766.57', '3859.45', '3754.45', '3887.00', '3717.00', '2019-06-07 00:00:05'),
(734, '2019-06-07', '00:00:00', 'CNY', '2120.62', '2000.61', '2144.10', '1979.20', '2128.00', '2001.00', '2019-06-07 00:00:05'),
(735, '2019-06-07', '00:00:00', 'MYR', '3446.52', '3366.52', '3453.90', '3354.90', '0.00', '0.00', '2019-06-07 00:00:05'),
(736, '2019-06-07', '00:00:00', 'THB', '455.10', '447.10', '456.00', '446.00', '459.00', '430.00', '2019-06-07 00:00:05'),
(737, '2019-06-08', '00:00:00', 'USD', '14300.00', '14260.00', '14425.00', '14125.00', '14429.00', '14129.00', '2019-06-08 00:00:04'),
(738, '2019-06-08', '00:00:00', 'SGD', '10388.85', '10340.85', '10404.60', '10324.60', '10472.00', '10247.00', '2019-06-08 00:00:04'),
(739, '2019-06-08', '00:00:00', 'EUR', '15968.77', '15868.77', '16114.45', '15736.45', '16112.00', '15699.00', '2019-06-08 00:00:04'),
(740, '2019-06-08', '00:00:00', 'AUD', '9918.30', '9838.30', '10013.45', '9749.45', '10012.00', '9740.00', '2019-06-08 00:00:04'),
(741, '2019-06-08', '00:00:00', 'DKK', '2161.71', '2101.71', '2169.55', '2103.15', '2192.00', '2062.00', '2019-06-08 00:00:04'),
(742, '2019-06-08', '00:00:00', 'SEK', '1514.95', '1474.95', '1523.40', '1471.20', '1553.00', '1436.00', '2019-06-08 00:00:04'),
(743, '2019-06-08', '00:00:00', 'CAD', '10585.17', '10505.17', '10675.85', '10413.85', '10684.00', '10386.00', '2019-06-08 00:00:04'),
(744, '2019-06-08', '00:00:00', 'CHF', '14258.22', '14158.22', '14390.50', '14049.50', '14396.00', '14012.00', '2019-06-08 00:00:04'),
(745, '2019-06-08', '00:00:00', 'NZD', '9345.87', '9265.87', '9444.25', '9162.25', '9431.00', '9165.00', '2019-06-08 00:00:04'),
(746, '2019-06-08', '00:00:00', 'GBP', '18050.78', '17950.78', '18234.70', '17770.70', '18237.00', '17788.00', '2019-06-08 00:00:04'),
(747, '2019-06-08', '00:00:00', 'HKD', '1834.95', '1804.95', '1836.95', '1803.05', '1854.00', '1785.00', '2019-06-08 00:00:04'),
(748, '2019-06-08', '00:00:00', 'JPY', '132.89', '129.49', '133.69', '128.82', '134.62', '128.01', '2019-06-08 00:00:04'),
(749, '2019-06-08', '00:00:00', 'SAR', '3846.57', '3766.57', '3859.45', '3754.45', '3887.00', '3717.00', '2019-06-08 00:00:04'),
(750, '2019-06-08', '00:00:00', 'CNY', '2120.62', '2000.61', '2144.10', '1979.20', '2128.00', '2001.00', '2019-06-08 00:00:04'),
(751, '2019-06-08', '00:00:00', 'MYR', '3446.52', '3366.52', '3453.90', '3354.90', '0.00', '0.00', '2019-06-08 00:00:04'),
(752, '2019-06-08', '00:00:00', 'THB', '455.10', '447.10', '456.00', '446.00', '459.00', '430.00', '2019-06-08 00:00:04'),
(753, '2019-06-09', '00:00:00', 'USD', '14300.00', '14260.00', '14425.00', '14125.00', '14429.00', '14129.00', '2019-06-09 00:00:05'),
(754, '2019-06-09', '00:00:00', 'SGD', '10388.85', '10340.85', '10404.60', '10324.60', '10472.00', '10247.00', '2019-06-09 00:00:05'),
(755, '2019-06-09', '00:00:00', 'EUR', '15968.77', '15868.77', '16114.45', '15736.45', '16112.00', '15699.00', '2019-06-09 00:00:05'),
(756, '2019-06-09', '00:00:00', 'AUD', '9918.30', '9838.30', '10013.45', '9749.45', '10012.00', '9740.00', '2019-06-09 00:00:05'),
(757, '2019-06-09', '00:00:00', 'DKK', '2161.71', '2101.71', '2169.55', '2103.15', '2192.00', '2062.00', '2019-06-09 00:00:05'),
(758, '2019-06-09', '00:00:00', 'SEK', '1514.95', '1474.95', '1523.40', '1471.20', '1553.00', '1436.00', '2019-06-09 00:00:05'),
(759, '2019-06-09', '00:00:00', 'CAD', '10585.17', '10505.17', '10675.85', '10413.85', '10684.00', '10386.00', '2019-06-09 00:00:05'),
(760, '2019-06-09', '00:00:00', 'CHF', '14258.22', '14158.22', '14390.50', '14049.50', '14396.00', '14012.00', '2019-06-09 00:00:05'),
(761, '2019-06-09', '00:00:00', 'NZD', '9345.87', '9265.87', '9444.25', '9162.25', '9431.00', '9165.00', '2019-06-09 00:00:05'),
(762, '2019-06-09', '00:00:00', 'GBP', '18050.78', '17950.78', '18234.70', '17770.70', '18237.00', '17788.00', '2019-06-09 00:00:05'),
(763, '2019-06-09', '00:00:00', 'HKD', '1834.95', '1804.95', '1836.95', '1803.05', '1854.00', '1785.00', '2019-06-09 00:00:05'),
(764, '2019-06-09', '00:00:00', 'JPY', '132.89', '129.49', '133.69', '128.82', '134.62', '128.01', '2019-06-09 00:00:05'),
(765, '2019-06-09', '00:00:00', 'SAR', '3846.57', '3766.57', '3859.45', '3754.45', '3887.00', '3717.00', '2019-06-09 00:00:05'),
(766, '2019-06-09', '00:00:00', 'CNY', '2120.62', '2000.61', '2144.10', '1979.20', '2128.00', '2001.00', '2019-06-09 00:00:05'),
(767, '2019-06-09', '00:00:00', 'MYR', '3446.52', '3366.52', '3453.90', '3354.90', '0.00', '0.00', '2019-06-09 00:00:05'),
(768, '2019-06-09', '00:00:00', 'THB', '455.10', '447.10', '456.00', '446.00', '459.00', '430.00', '2019-06-09 00:00:05'),
(769, '2019-06-10', '00:00:00', 'USD', '14300.00', '14260.00', '14425.00', '14125.00', '14429.00', '14129.00', '2019-06-10 00:00:19'),
(770, '2019-06-10', '00:00:00', 'SGD', '10388.85', '10340.85', '10404.60', '10324.60', '10472.00', '10247.00', '2019-06-10 00:00:19'),
(771, '2019-06-10', '00:00:00', 'EUR', '15968.77', '15868.77', '16114.45', '15736.45', '16112.00', '15699.00', '2019-06-10 00:00:19'),
(772, '2019-06-10', '00:00:00', 'AUD', '9918.30', '9838.30', '10013.45', '9749.45', '10012.00', '9740.00', '2019-06-10 00:00:19'),
(773, '2019-06-10', '00:00:00', 'DKK', '2161.71', '2101.71', '2169.55', '2103.15', '2192.00', '2062.00', '2019-06-10 00:00:19'),
(774, '2019-06-10', '00:00:00', 'SEK', '1514.95', '1474.95', '1523.40', '1471.20', '1553.00', '1436.00', '2019-06-10 00:00:19'),
(775, '2019-06-10', '00:00:00', 'CAD', '10585.17', '10505.17', '10675.85', '10413.85', '10684.00', '10386.00', '2019-06-10 00:00:19'),
(776, '2019-06-10', '00:00:00', 'CHF', '14258.22', '14158.22', '14390.50', '14049.50', '14396.00', '14012.00', '2019-06-10 00:00:19'),
(777, '2019-06-10', '00:00:00', 'NZD', '9345.87', '9265.87', '9444.25', '9162.25', '9431.00', '9165.00', '2019-06-10 00:00:19'),
(778, '2019-06-10', '00:00:00', 'GBP', '18050.78', '17950.78', '18234.70', '17770.70', '18237.00', '17788.00', '2019-06-10 00:00:19'),
(779, '2019-06-10', '00:00:00', 'HKD', '1834.95', '1804.95', '1836.95', '1803.05', '1854.00', '1785.00', '2019-06-10 00:00:19'),
(780, '2019-06-10', '00:00:00', 'JPY', '132.89', '129.49', '133.69', '128.82', '134.62', '128.01', '2019-06-10 00:00:19'),
(781, '2019-06-10', '00:00:00', 'SAR', '3846.57', '3766.57', '3859.45', '3754.45', '3887.00', '3717.00', '2019-06-10 00:00:19'),
(782, '2019-06-10', '00:00:00', 'CNY', '2120.62', '2000.61', '2144.10', '1979.20', '2128.00', '2001.00', '2019-06-10 00:00:19'),
(783, '2019-06-10', '00:00:00', 'MYR', '3446.52', '3366.52', '3453.90', '3354.90', '0.00', '0.00', '2019-06-10 00:00:19'),
(784, '2019-06-10', '00:00:00', 'THB', '455.10', '447.10', '456.00', '446.00', '459.00', '430.00', '2019-06-10 00:00:19'),
(785, '2019-06-11', '00:00:00', 'USD', '14258.00', '14238.00', '14398.00', '14098.00', '14403.00', '14103.00', '2019-06-11 00:00:06'),
(786, '2019-06-11', '00:00:00', 'SGD', '10447.20', '10399.20', '10464.80', '10383.80', '10566.00', '10338.00', '2019-06-11 00:00:06'),
(787, '2019-06-11', '00:00:00', 'EUR', '16148.10', '16048.10', '16300.00', '15918.00', '16348.00', '15930.00', '2019-06-11 00:00:06'),
(788, '2019-06-11', '00:00:00', 'AUD', '9968.01', '9888.01', '10062.30', '9797.30', '10105.00', '9830.00', '2019-06-11 00:00:06'),
(789, '2019-06-11', '00:00:00', 'DKK', '2185.59', '2125.59', '2194.85', '2127.45', '2223.00', '2093.00', '2019-06-11 00:00:06'),
(790, '2019-06-11', '00:00:00', 'SEK', '1530.07', '1490.07', '1539.60', '1486.90', '1574.00', '1456.00', '2019-06-11 00:00:06'),
(791, '2019-06-11', '00:00:00', 'CAD', '10775.79', '10695.79', '10867.95', '10599.95', '10886.00', '10584.00', '2019-06-11 00:00:06'),
(792, '2019-06-11', '00:00:00', 'CHF', '14425.94', '14325.94', '14558.00', '14212.00', '14600.00', '14211.00', '2019-06-11 00:00:06'),
(793, '2019-06-11', '00:00:00', 'NZD', '9475.03', '9395.03', '9574.70', '9290.70', '9624.00', '9354.00', '2019-06-11 00:00:06'),
(794, '2019-06-11', '00:00:00', 'GBP', '18133.56', '18033.56', '18315.65', '17849.65', '18361.00', '17909.00', '2019-06-11 00:00:06'),
(795, '2019-06-11', '00:00:00', 'HKD', '1831.68', '1801.68', '1833.65', '1799.75', '1851.00', '1783.00', '2019-06-11 00:00:06'),
(796, '2019-06-11', '00:00:00', 'JPY', '132.93', '129.53', '133.73', '128.85', '134.89', '128.27', '2019-06-11 00:00:06'),
(797, '2019-06-11', '00:00:00', 'SAR', '3839.26', '3759.26', '3852.15', '3747.15', '3880.00', '3710.00', '2019-06-11 00:00:06'),
(798, '2019-06-11', '00:00:00', 'CNY', '2108.93', '1988.92', '2132.25', '1967.55', '2122.00', '1994.00', '2019-06-11 00:00:06'),
(799, '2019-06-11', '00:00:00', 'MYR', '3462.12', '3382.12', '3469.80', '3369.80', '0.00', '0.00', '2019-06-11 00:00:06'),
(800, '2019-06-11', '00:00:00', 'THB', '458.27', '450.27', '459.20', '449.20', '465.00', '435.00', '2019-06-11 00:00:06'),
(801, '2019-06-12', '00:00:00', 'USD', '14248.00', '14228.00', '14388.00', '14088.00', '14398.00', '14098.00', '2019-06-12 00:00:06'),
(802, '2019-06-12', '00:00:00', 'SGD', '10454.39', '10406.39', '10471.22', '10390.22', '10538.00', '10310.00', '2019-06-12 00:00:06'),
(803, '2019-06-12', '00:00:00', 'EUR', '16158.16', '16058.16', '16311.70', '15929.70', '16322.00', '15905.00', '2019-06-12 00:00:06'),
(804, '2019-06-12', '00:00:00', 'AUD', '9943.24', '9863.24', '10036.60', '9771.60', '10045.00', '9772.00', '2019-06-12 00:00:06'),
(805, '2019-06-12', '00:00:00', 'DKK', '2187.08', '2127.08', '2196.30', '2128.80', '2220.00', '2089.00', '2019-06-12 00:00:06'),
(806, '2019-06-12', '00:00:00', 'SEK', '1527.89', '1487.89', '1537.10', '1484.40', '1570.00', '1452.00', '2019-06-12 00:00:06'),
(807, '2019-06-12', '00:00:00', 'CAD', '10776.75', '10696.75', '10868.60', '10600.60', '10882.00', '10580.00', '2019-06-12 00:00:06'),
(808, '2019-06-12', '00:00:00', 'CHF', '14416.58', '14316.58', '14549.35', '14203.35', '14592.00', '14203.00', '2019-06-12 00:00:06');
INSERT INTO `kurs_all` (`id`, `kursDate`, `kursTime`, `currency`, `rate_jual`, `rate_beli`, `tt_jual`, `tt_beli`, `notes_jual`, `notes_beli`, `inputDate`) VALUES
(809, '2019-06-12', '00:00:00', 'NZD', '9422.13', '9342.13', '9522.05', '9239.05', '9546.00', '9278.00', '2019-06-12 00:00:06'),
(810, '2019-06-12', '00:00:00', 'GBP', '18150.77', '18050.77', '18341.70', '17874.70', '18297.00', '17846.00', '2019-06-12 00:00:06'),
(811, '2019-06-12', '00:00:00', 'HKD', '1832.07', '1802.07', '1834.05', '1800.15', '1850.00', '1782.00', '2019-06-12 00:00:06'),
(812, '2019-06-12', '00:00:00', 'JPY', '132.74', '129.34', '133.54', '128.67', '134.84', '128.21', '2019-06-12 00:00:06'),
(813, '2019-06-12', '00:00:00', 'SAR', '3836.04', '3756.04', '3849.15', '3744.15', '3879.00', '3709.00', '2019-06-12 00:00:06'),
(814, '2019-06-12', '00:00:00', 'CNY', '2114.65', '1994.64', '2138.20', '1973.30', '2116.00', '1988.00', '2019-06-12 00:00:06'),
(815, '2019-06-12', '00:00:00', 'MYR', '3458.90', '3378.90', '3466.55', '3366.55', '0.00', '0.00', '2019-06-12 00:00:06'),
(816, '2019-06-12', '00:00:00', 'THB', '458.96', '450.96', '459.90', '449.90', '463.00', '434.00', '2019-06-12 00:00:06'),
(817, '2019-06-13', '00:00:00', 'USD', '14243.00', '14223.00', '14383.00', '14083.00', '14388.00', '14088.00', '2019-06-13 00:00:04'),
(818, '2019-06-13', '00:00:00', 'SGD', '10457.60', '10409.60', '10474.10', '10393.10', '10552.00', '10324.00', '2019-06-13 00:00:04'),
(819, '2019-06-13', '00:00:00', 'EUR', '16179.55', '16079.55', '16331.90', '15948.90', '16341.00', '15923.00', '2019-06-13 00:00:04'),
(820, '2019-06-13', '00:00:00', 'AUD', '9931.93', '9851.93', '10025.95', '9760.95', '10044.00', '9771.00', '2019-06-13 00:00:04'),
(821, '2019-06-13', '00:00:00', 'DKK', '2189.79', '2129.79', '2198.95', '2131.35', '2222.00', '2092.00', '2019-06-13 00:00:04'),
(822, '2019-06-13', '00:00:00', 'SEK', '1526.79', '1486.79', '1536.10', '1483.40', '1564.00', '1446.00', '2019-06-13 00:00:04'),
(823, '2019-06-13', '00:00:00', 'CAD', '10741.51', '10661.51', '10832.10', '10565.10', '10868.00', '10566.00', '2019-06-13 00:00:04'),
(824, '2019-06-13', '00:00:00', 'CHF', '14403.57', '14303.57', '14534.00', '14188.00', '14541.00', '14153.00', '2019-06-13 00:00:04'),
(825, '2019-06-13', '00:00:00', 'NZD', '9408.17', '9328.17', '9508.70', '9225.70', '9496.00', '9229.00', '2019-06-13 00:00:04'),
(826, '2019-06-13', '00:00:00', 'GBP', '18184.26', '18084.26', '18371.25', '17904.25', '18340.00', '17889.00', '2019-06-13 00:00:04'),
(827, '2019-06-13', '00:00:00', 'HKD', '1834.17', '1804.17', '1836.15', '1802.15', '1851.00', '1782.00', '2019-06-13 00:00:04'),
(828, '2019-06-13', '00:00:00', 'JPY', '133.22', '129.82', '134.01', '129.12', '134.61', '127.99', '2019-06-13 00:00:04'),
(829, '2019-06-13', '00:00:00', 'SAR', '3835.07', '3755.07', '3847.90', '3742.90', '3875.00', '3705.00', '2019-06-13 00:00:04'),
(830, '2019-06-13', '00:00:00', 'CNY', '2113.40', '1993.39', '2136.55', '1971.75', '2120.00', '1993.00', '2019-06-13 00:00:04'),
(831, '2019-06-13', '00:00:00', 'MYR', '3461.40', '3381.40', '3469.10', '3369.10', '0.00', '0.00', '2019-06-13 00:00:04'),
(832, '2019-06-13', '00:00:00', 'THB', '459.82', '451.82', '460.80', '450.80', '465.00', '435.00', '2019-06-13 00:00:04'),
(833, '2019-06-14', '00:00:00', 'USD', '14286.00', '14266.00', '14426.00', '14126.00', '14433.00', '14133.00', '2019-06-14 00:00:04'),
(834, '2019-06-14', '00:00:00', 'SGD', '10470.75', '10422.75', '10487.55', '10406.55', '10569.00', '10341.00', '2019-06-14 00:00:04'),
(835, '2019-06-14', '00:00:00', 'EUR', '16169.75', '16069.75', '16324.50', '15942.50', '16341.00', '15924.00', '2019-06-14 00:00:04'),
(836, '2019-06-14', '00:00:00', 'AUD', '9899.72', '9819.72', '9995.40', '9731.40', '10044.00', '9771.00', '2019-06-14 00:00:04'),
(837, '2019-06-14', '00:00:00', 'DKK', '2188.70', '2128.70', '2197.80', '2130.50', '2222.00', '2092.00', '2019-06-14 00:00:04'),
(838, '2019-06-14', '00:00:00', 'SEK', '1525.78', '1485.78', '1534.80', '1482.30', '1565.00', '1448.00', '2019-06-14 00:00:04'),
(839, '2019-06-14', '00:00:00', 'CAD', '10754.50', '10674.50', '10845.75', '10578.75', '10862.00', '10560.00', '2019-06-14 00:00:04'),
(840, '2019-06-14', '00:00:00', 'CHF', '14430.99', '14330.99', '14562.75', '14216.75', '14553.00', '14166.00', '2019-06-14 00:00:04'),
(841, '2019-06-14', '00:00:00', 'NZD', '9412.91', '9332.91', '9514.05', '9231.05', '9534.00', '9267.00', '2019-06-14 00:00:04'),
(842, '2019-06-14', '00:00:00', 'GBP', '18138.40', '18038.40', '18323.85', '17857.85', '18356.00', '17905.00', '2019-06-14 00:00:04'),
(843, '2019-06-14', '00:00:00', 'HKD', '1839.28', '1809.28', '1841.30', '1807.30', '1859.00', '1791.00', '2019-06-14 00:00:04'),
(844, '2019-06-14', '00:00:00', 'JPY', '133.40', '130.00', '134.20', '129.32', '135.13', '128.51', '2019-06-14 00:00:04'),
(845, '2019-06-14', '00:00:00', 'SAR', '3846.53', '3766.53', '3859.40', '3754.40', '3887.00', '3717.00', '2019-06-14 00:00:04'),
(846, '2019-06-14', '00:00:00', 'CNY', '2118.72', '1998.72', '2142.10', '1977.30', '2125.00', '1997.00', '2019-06-14 00:00:04'),
(847, '2019-06-14', '00:00:00', 'MYR', '3468.85', '3388.85', '3476.55', '3376.55', '0.00', '0.00', '2019-06-14 00:00:04'),
(848, '2019-06-14', '00:00:00', 'THB', '461.35', '453.35', '462.30', '452.30', '466.00', '436.00', '2019-06-14 00:00:04'),
(849, '2019-06-15', '00:00:00', 'USD', '14330.00', '14290.00', '14471.00', '14171.00', '14426.00', '14126.00', '2019-06-15 00:00:04'),
(850, '2019-06-15', '00:00:00', 'SGD', '10498.31', '10450.31', '10513.60', '10432.60', '10560.00', '10332.00', '2019-06-15 00:00:04'),
(851, '2019-06-15', '00:00:00', 'EUR', '16199.79', '16099.79', '16350.85', '15968.85', '16312.00', '15895.00', '2019-06-15 00:00:04'),
(852, '2019-06-15', '00:00:00', 'AUD', '9918.63', '9838.63', '10012.15', '9748.15', '10004.00', '9732.00', '2019-06-15 00:00:04'),
(853, '2019-06-15', '00:00:00', 'DKK', '2192.54', '2132.54', '2201.50', '2134.20', '2218.00', '2088.00', '2019-06-15 00:00:04'),
(854, '2019-06-15', '00:00:00', 'SEK', '1536.98', '1496.98', '1546.45', '1493.75', '1563.00', '1445.00', '2019-06-15 00:00:04'),
(855, '2019-06-15', '00:00:00', 'CAD', '10775.38', '10695.38', '10866.50', '10599.50', '10849.00', '10548.00', '2019-06-15 00:00:04'),
(856, '2019-06-15', '00:00:00', 'CHF', '14438.62', '14338.62', '14571.45', '14226.45', '14566.00', '14179.00', '2019-06-15 00:00:04'),
(857, '2019-06-15', '00:00:00', 'NZD', '9403.07', '9323.07', '9503.30', '9221.30', '9486.00', '9220.00', '2019-06-15 00:00:04'),
(858, '2019-06-15', '00:00:00', 'GBP', '18175.38', '18075.38', '18357.90', '17891.90', '18324.00', '17873.00', '2019-06-15 00:00:04'),
(859, '2019-06-15', '00:00:00', 'HKD', '1844.87', '1814.87', '1846.85', '1812.95', '1856.00', '1788.00', '2019-06-15 00:00:04'),
(860, '2019-06-15', '00:00:00', 'JPY', '134.02', '130.61', '134.80', '129.91', '135.22', '128.59', '2019-06-15 00:00:04'),
(861, '2019-06-15', '00:00:00', 'SAR', '3858.53', '3778.53', '3871.45', '3766.45', '3886.00', '3716.00', '2019-06-15 00:00:04'),
(862, '2019-06-15', '00:00:00', 'CNY', '2125.22', '2005.23', '2148.75', '1983.85', '2123.00', '1995.00', '2019-06-15 00:00:04'),
(863, '2019-06-15', '00:00:00', 'MYR', '3475.94', '3395.94', '3484.05', '3384.05', '0.00', '0.00', '2019-06-15 00:00:04'),
(864, '2019-06-15', '00:00:00', 'THB', '463.60', '455.60', '464.65', '454.65', '467.00', '437.00', '2019-06-15 00:00:04'),
(865, '2019-06-16', '00:00:00', 'USD', '14330.00', '14290.00', '14471.00', '14171.00', '14426.00', '14126.00', '2019-06-16 00:00:04'),
(866, '2019-06-16', '00:00:00', 'SGD', '10498.31', '10450.31', '10513.60', '10432.60', '10560.00', '10332.00', '2019-06-16 00:00:04'),
(867, '2019-06-16', '00:00:00', 'EUR', '16199.79', '16099.79', '16350.85', '15968.85', '16312.00', '15895.00', '2019-06-16 00:00:04'),
(868, '2019-06-16', '00:00:00', 'AUD', '9918.63', '9838.63', '10012.15', '9748.15', '10004.00', '9732.00', '2019-06-16 00:00:04'),
(869, '2019-06-16', '00:00:00', 'DKK', '2192.54', '2132.54', '2201.50', '2134.20', '2218.00', '2088.00', '2019-06-16 00:00:04'),
(870, '2019-06-16', '00:00:00', 'SEK', '1536.98', '1496.98', '1546.45', '1493.75', '1563.00', '1445.00', '2019-06-16 00:00:04'),
(871, '2019-06-16', '00:00:00', 'CAD', '10775.38', '10695.38', '10866.50', '10599.50', '10849.00', '10548.00', '2019-06-16 00:00:04'),
(872, '2019-06-16', '00:00:00', 'CHF', '14438.62', '14338.62', '14571.45', '14226.45', '14566.00', '14179.00', '2019-06-16 00:00:04'),
(873, '2019-06-16', '00:00:00', 'NZD', '9403.07', '9323.07', '9503.30', '9221.30', '9486.00', '9220.00', '2019-06-16 00:00:04'),
(874, '2019-06-16', '00:00:00', 'GBP', '18175.38', '18075.38', '18357.90', '17891.90', '18324.00', '17873.00', '2019-06-16 00:00:04'),
(875, '2019-06-16', '00:00:00', 'HKD', '1844.87', '1814.87', '1846.85', '1812.95', '1856.00', '1788.00', '2019-06-16 00:00:04'),
(876, '2019-06-16', '00:00:00', 'JPY', '134.02', '130.61', '134.80', '129.91', '135.22', '128.59', '2019-06-16 00:00:04'),
(877, '2019-06-16', '00:00:00', 'SAR', '3858.53', '3778.53', '3871.45', '3766.45', '3886.00', '3716.00', '2019-06-16 00:00:04'),
(878, '2019-06-16', '00:00:00', 'CNY', '2125.22', '2005.23', '2148.75', '1983.85', '2123.00', '1995.00', '2019-06-16 00:00:04'),
(879, '2019-06-16', '00:00:00', 'MYR', '3475.94', '3395.94', '3484.05', '3384.05', '0.00', '0.00', '2019-06-16 00:00:04'),
(880, '2019-06-16', '00:00:00', 'THB', '463.60', '455.60', '464.65', '454.65', '467.00', '437.00', '2019-06-16 00:00:04'),
(881, '2019-06-17', '00:00:00', 'USD', '14330.00', '14290.00', '14471.00', '14171.00', '14426.00', '14126.00', '2019-06-17 00:00:05'),
(882, '2019-06-17', '00:00:00', 'SGD', '10498.31', '10450.31', '10513.60', '10432.60', '10560.00', '10332.00', '2019-06-17 00:00:05'),
(883, '2019-06-17', '00:00:00', 'EUR', '16199.79', '16099.79', '16350.85', '15968.85', '16312.00', '15895.00', '2019-06-17 00:00:05'),
(884, '2019-06-17', '00:00:00', 'AUD', '9918.63', '9838.63', '10012.15', '9748.15', '10004.00', '9732.00', '2019-06-17 00:00:05'),
(885, '2019-06-17', '00:00:00', 'DKK', '2192.54', '2132.54', '2201.50', '2134.20', '2218.00', '2088.00', '2019-06-17 00:00:05'),
(886, '2019-06-17', '00:00:00', 'SEK', '1536.98', '1496.98', '1546.45', '1493.75', '1563.00', '1445.00', '2019-06-17 00:00:05'),
(887, '2019-06-17', '00:00:00', 'CAD', '10775.38', '10695.38', '10866.50', '10599.50', '10849.00', '10548.00', '2019-06-17 00:00:05'),
(888, '2019-06-17', '00:00:00', 'CHF', '14438.62', '14338.62', '14571.45', '14226.45', '14566.00', '14179.00', '2019-06-17 00:00:05'),
(889, '2019-06-17', '00:00:00', 'NZD', '9403.07', '9323.07', '9503.30', '9221.30', '9486.00', '9220.00', '2019-06-17 00:00:05'),
(890, '2019-06-17', '00:00:00', 'GBP', '18175.38', '18075.38', '18357.90', '17891.90', '18324.00', '17873.00', '2019-06-17 00:00:05'),
(891, '2019-06-17', '00:00:00', 'HKD', '1844.87', '1814.87', '1846.85', '1812.95', '1856.00', '1788.00', '2019-06-17 00:00:05'),
(892, '2019-06-17', '00:00:00', 'JPY', '134.02', '130.61', '134.80', '129.91', '135.22', '128.59', '2019-06-17 00:00:05'),
(893, '2019-06-17', '00:00:00', 'SAR', '3858.53', '3778.53', '3871.45', '3766.45', '3886.00', '3716.00', '2019-06-17 00:00:05'),
(894, '2019-06-17', '00:00:00', 'CNY', '2125.22', '2005.23', '2148.75', '1983.85', '2123.00', '1995.00', '2019-06-17 00:00:05'),
(895, '2019-06-17', '00:00:00', 'MYR', '3475.94', '3395.94', '3484.05', '3384.05', '0.00', '0.00', '2019-06-17 00:00:05'),
(896, '2019-06-17', '00:00:00', 'THB', '463.60', '455.60', '464.65', '454.65', '467.00', '437.00', '2019-06-17 00:00:05'),
(897, '2019-06-18', '00:00:00', 'USD', '14343.00', '14323.00', '14483.00', '14183.00', '14471.00', '14171.00', '2019-06-18 00:00:05'),
(898, '2019-06-18', '00:00:00', 'SGD', '10480.32', '10432.32', '10496.22', '10415.22', '10560.00', '10333.00', '2019-06-18 00:00:05'),
(899, '2019-06-18', '00:00:00', 'EUR', '16123.03', '16023.03', '16270.35', '15890.35', '16278.00', '15864.00', '2019-06-18 00:00:05'),
(900, '2019-06-18', '00:00:00', 'AUD', '9894.66', '9814.66', '9988.60', '9725.60', '9992.00', '9721.00', '2019-06-18 00:00:05'),
(901, '2019-06-18', '00:00:00', 'DKK', '2182.43', '2122.43', '2191.20', '2124.30', '2214.00', '2084.00', '2019-06-18 00:00:05'),
(902, '2019-06-18', '00:00:00', 'SEK', '1530.54', '1490.54', '1539.60', '1487.10', '1568.00', '1450.00', '2019-06-18 00:00:05'),
(903, '2019-06-18', '00:00:00', 'CAD', '10726.69', '10646.69', '10816.30', '10551.30', '10824.00', '10524.00', '2019-06-18 00:00:05'),
(904, '2019-06-18', '00:00:00', 'CHF', '14391.60', '14291.60', '14523.65', '14180.65', '14536.00', '14150.00', '2019-06-18 00:00:05'),
(905, '2019-06-18', '00:00:00', 'NZD', '9375.09', '9295.09', '9475.00', '9193.00', '9441.00', '9176.00', '2019-06-18 00:00:05'),
(906, '2019-06-18', '00:00:00', 'GBP', '18094.54', '17994.54', '18277.65', '17813.65', '18259.00', '17811.00', '2019-06-18 00:00:05'),
(907, '2019-06-18', '00:00:00', 'HKD', '1845.51', '1815.51', '1847.50', '1813.50', '1863.00', '1795.00', '2019-06-18 00:00:05'),
(908, '2019-06-18', '00:00:00', 'JPY', '133.71', '130.31', '134.50', '129.62', '135.31', '128.69', '2019-06-18 00:00:05'),
(909, '2019-06-18', '00:00:00', 'SAR', '3861.72', '3781.72', '3874.65', '3769.65', '3898.00', '3728.00', '2019-06-18 00:00:05'),
(910, '2019-06-18', '00:00:00', 'CNY', '2127.21', '2007.21', '2150.65', '1985.75', '2128.00', '2000.00', '2019-06-18 00:00:05'),
(911, '2019-06-18', '00:00:00', 'MYR', '3471.41', '3391.41', '3479.40', '3379.40', '0.00', '0.00', '2019-06-18 00:00:05'),
(912, '2019-06-18', '00:00:00', 'THB', '462.00', '454.00', '462.95', '452.95', '468.00', '438.00', '2019-06-18 00:00:05'),
(913, '2019-06-19', '00:00:00', 'USD', '14333.00', '14313.00', '14475.00', '14175.00', '14483.00', '14183.00', '2019-06-19 00:00:04'),
(914, '2019-06-19', '00:00:00', 'SGD', '10465.41', '10417.41', '10484.30', '10403.30', '10570.00', '10343.00', '2019-06-19 00:00:05'),
(915, '2019-06-19', '00:00:00', 'EUR', '16070.27', '15970.27', '16220.90', '15841.90', '16293.00', '15879.00', '2019-06-19 00:00:05'),
(916, '2019-06-19', '00:00:00', 'AUD', '9839.08', '9759.08', '9932.45', '9670.45', '9961.00', '9691.00', '2019-06-19 00:00:05'),
(917, '2019-06-19', '00:00:00', 'DKK', '2175.45', '2115.45', '2184.10', '2117.40', '2216.00', '2086.00', '2019-06-19 00:00:05'),
(918, '2019-06-19', '00:00:00', 'SEK', '1527.97', '1487.97', '1537.35', '1484.85', '1570.00', '1452.00', '2019-06-19 00:00:05'),
(919, '2019-06-19', '00:00:00', 'CAD', '10707.32', '10627.32', '10799.05', '10534.05', '10831.00', '10531.00', '2019-06-19 00:00:05'),
(920, '2019-06-19', '00:00:00', 'CHF', '14371.57', '14271.57', '14505.45', '14162.45', '14548.00', '14162.00', '2019-06-19 00:00:05'),
(921, '2019-06-19', '00:00:00', 'NZD', '9359.26', '9279.26', '9459.65', '9177.65', '9443.00', '9178.00', '2019-06-19 00:00:05'),
(922, '2019-06-19', '00:00:00', 'GBP', '17996.00', '17896.00', '18182.25', '17720.25', '18192.00', '17746.00', '2019-06-19 00:00:05'),
(923, '2019-06-19', '00:00:00', 'HKD', '1843.35', '1813.35', '1845.55', '1811.65', '1863.00', '1795.00', '2019-06-19 00:00:05'),
(924, '2019-06-19', '00:00:00', 'JPY', '133.95', '130.55', '134.78', '129.89', '135.51', '128.89', '2019-06-19 00:00:05'),
(925, '2019-06-19', '00:00:00', 'SAR', '3859.17', '3779.17', '3872.60', '3767.60', '3901.00', '3731.00', '2019-06-19 00:00:05'),
(926, '2019-06-19', '00:00:00', 'CNY', '2124.99', '2004.99', '2148.80', '1983.90', '2130.00', '2002.00', '2019-06-19 00:00:05'),
(927, '2019-06-19', '00:00:00', 'MYR', '3465.33', '3385.33', '3473.30', '3373.30', '0.00', '0.00', '2019-06-19 00:00:05'),
(928, '2019-06-19', '00:00:00', 'THB', '460.51', '452.51', '461.50', '451.50', '466.00', '437.00', '2019-06-19 00:00:05'),
(929, '2019-06-20', '00:00:00', 'USD', '14278.00', '14258.00', '14418.00', '14118.00', '14425.00', '14125.00', '2019-06-20 00:00:07'),
(930, '2019-06-20', '00:00:00', 'SGD', '10452.68', '10404.68', '10469.05', '10388.05', '10557.00', '10329.00', '2019-06-20 00:00:07'),
(931, '2019-06-20', '00:00:00', 'EUR', '16028.74', '15928.74', '16178.65', '15799.65', '16195.00', '15781.00', '2019-06-20 00:00:07'),
(932, '2019-06-20', '00:00:00', 'AUD', '9842.11', '9762.11', '9935.15', '9672.15', '9966.00', '9695.00', '2019-06-20 00:00:07'),
(933, '2019-06-20', '00:00:00', 'DKK', '2169.90', '2109.90', '2178.30', '2111.60', '2203.00', '2073.00', '2019-06-20 00:00:07'),
(934, '2019-06-20', '00:00:00', 'SEK', '1514.99', '1474.99', '1523.75', '1471.55', '1555.00', '1438.00', '2019-06-20 00:00:07'),
(935, '2019-06-20', '00:00:00', 'CAD', '10704.08', '10624.08', '10793.50', '10527.50', '10823.00', '10523.00', '2019-06-20 00:00:07'),
(936, '2019-06-20', '00:00:00', 'CHF', '14340.87', '14240.87', '14470.05', '14126.05', '14457.00', '14071.00', '2019-06-20 00:00:07'),
(937, '2019-06-20', '00:00:00', 'NZD', '9345.59', '9265.59', '9444.00', '9162.00', '9463.00', '9197.00', '2019-06-20 00:00:07'),
(938, '2019-06-20', '00:00:00', 'GBP', '17982.03', '17882.03', '18166.80', '17703.80', '18160.00', '17713.00', '2019-06-20 00:00:07'),
(939, '2019-06-20', '00:00:00', 'HKD', '1838.16', '1808.16', '1840.15', '1806.15', '1856.00', '1788.00', '2019-06-20 00:00:07'),
(940, '2019-06-20', '00:00:00', 'JPY', '133.30', '129.90', '134.09', '129.21', '134.91', '128.29', '2019-06-20 00:00:07'),
(941, '2019-06-20', '00:00:00', 'SAR', '3844.60', '3764.60', '3857.50', '3752.50', '3885.00', '3715.00', '2019-06-20 00:00:07'),
(942, '2019-06-20', '00:00:00', 'CNY', '2125.63', '2005.62', '2149.05', '1984.05', '2127.00', '2000.00', '2019-06-20 00:00:07'),
(943, '2019-06-20', '00:00:00', 'MYR', '3456.26', '3376.26', '3463.80', '3363.80', '0.00', '0.00', '2019-06-20 00:00:07'),
(944, '2019-06-20', '00:00:00', 'THB', '460.65', '452.65', '461.60', '451.60', '466.00', '437.00', '2019-06-20 00:00:07'),
(945, '2019-06-21', '00:00:00', 'USD', '14191.00', '14171.00', '14331.00', '14031.00', '14393.00', '14093.00', '2019-06-21 00:00:04'),
(946, '2019-06-21', '00:00:00', 'SGD', '10473.10', '10425.10', '10489.02', '10408.02', '10573.00', '10344.00', '2019-06-21 00:00:04'),
(947, '2019-06-21', '00:00:00', 'EUR', '16058.93', '15958.93', '16209.85', '15827.85', '16234.00', '15819.00', '2019-06-21 00:00:04'),
(948, '2019-06-21', '00:00:00', 'AUD', '9847.58', '9767.58', '9942.45', '9678.45', '9961.00', '9689.00', '2019-06-21 00:00:04'),
(949, '2019-06-21', '00:00:00', 'DKK', '2174.12', '2114.12', '2183.00', '2115.80', '2208.00', '2078.00', '2019-06-21 00:00:04'),
(950, '2019-06-21', '00:00:00', 'SEK', '1526.71', '1486.71', '1536.10', '1483.30', '1562.00', '1444.00', '2019-06-21 00:00:04'),
(951, '2019-06-21', '00:00:00', 'CAD', '10772.61', '10692.61', '10865.70', '10596.70', '10889.00', '10586.00', '2019-06-21 00:00:04'),
(952, '2019-06-21', '00:00:00', 'CHF', '14407.60', '14307.60', '14543.15', '14196.15', '14573.00', '14184.00', '2019-06-21 00:00:04'),
(953, '2019-06-21', '00:00:00', 'NZD', '9378.90', '9298.90', '9480.05', '9197.05', '9488.00', '9221.00', '2019-06-21 00:00:04'),
(954, '2019-06-21', '00:00:00', 'GBP', '18074.05', '17974.05', '18260.45', '17794.45', '18272.00', '17822.00', '2019-06-21 00:00:04'),
(955, '2019-06-21', '00:00:00', 'HKD', '1829.37', '1799.37', '1831.35', '1797.35', '1855.00', '1787.00', '2019-06-21 00:00:04'),
(956, '2019-06-21', '00:00:00', 'JPY', '133.26', '129.86', '134.07', '129.17', '135.63', '128.98', '2019-06-21 00:00:04'),
(957, '2019-06-21', '00:00:00', 'SAR', '3821.40', '3741.40', '3834.25', '3729.25', '3877.00', '3707.00', '2019-06-21 00:00:04'),
(958, '2019-06-21', '00:00:00', 'CNY', '2127.50', '2007.50', '2151.10', '1985.80', '2122.00', '1995.00', '2019-06-21 00:00:04'),
(959, '2019-06-21', '00:00:00', 'MYR', '3455.87', '3375.87', '3463.65', '3363.65', '0.00', '0.00', '2019-06-21 00:00:04'),
(960, '2019-06-21', '00:00:00', 'THB', '462.12', '454.12', '463.10', '453.10', '467.00', '437.00', '2019-06-21 00:00:04'),
(961, '2019-06-22', '00:00:00', 'USD', '14161.00', '14141.00', '14301.00', '14001.00', '14291.00', '13991.00', '2019-06-22 00:00:04'),
(962, '2019-06-22', '00:00:00', 'SGD', '10450.61', '10402.61', '10465.80', '10384.80', '10545.00', '10316.00', '2019-06-22 00:00:04'),
(963, '2019-06-22', '00:00:00', 'EUR', '16045.58', '15945.58', '16194.50', '15812.50', '16182.00', '15765.00', '2019-06-22 00:00:04'),
(964, '2019-06-22', '00:00:00', 'AUD', '9825.42', '9745.42', '9917.35', '9654.35', '9928.00', '9655.00', '2019-06-22 00:00:04'),
(965, '2019-06-22', '00:00:00', 'DKK', '2172.53', '2112.53', '2181.15', '2113.85', '2202.00', '2071.00', '2019-06-22 00:00:04'),
(966, '2019-06-22', '00:00:00', 'SEK', '1527.14', '1487.14', '1535.85', '1482.95', '1561.00', '1443.00', '2019-06-22 00:00:04'),
(967, '2019-06-22', '00:00:00', 'CAD', '10769.40', '10689.40', '10856.95', '10587.95', '10876.00', '10573.00', '2019-06-22 00:00:04'),
(968, '2019-06-22', '00:00:00', 'CHF', '14455.99', '14355.99', '14592.55', '14243.55', '14614.00', '14222.00', '2019-06-22 00:00:04'),
(969, '2019-06-22', '00:00:00', 'NZD', '9334.37', '9254.37', '9435.90', '9153.90', '9451.00', '9183.00', '2019-06-22 00:00:04'),
(970, '2019-06-22', '00:00:00', 'GBP', '17991.35', '17891.35', '18170.80', '17705.80', '18193.00', '17741.00', '2019-06-22 00:00:04'),
(971, '2019-06-22', '00:00:00', 'HKD', '1826.96', '1796.96', '1829.10', '1795.10', '1843.00', '1775.00', '2019-06-22 00:00:04'),
(972, '2019-06-22', '00:00:00', 'JPY', '133.42', '130.02', '134.22', '129.31', '135.24', '128.58', '2019-06-22 00:00:04'),
(973, '2019-06-22', '00:00:00', 'SAR', '3812.99', '3732.99', '3826.00', '3721.00', '3850.00', '3680.00', '2019-06-22 00:00:04'),
(974, '2019-06-22', '00:00:00', 'CNH', '2118.05', '1998.06', '2141.55', '1976.35', '2124.00', '1996.00', '2019-06-22 00:00:04'),
(975, '2019-06-22', '00:00:00', 'MYR', '3448.24', '3368.24', '3458.10', '3358.10', '0.00', '0.00', '2019-06-22 00:00:04'),
(976, '2019-06-22', '00:00:00', 'THB', '462.19', '454.19', '463.05', '453.05', '467.00', '438.00', '2019-06-22 00:00:04'),
(977, '2019-06-23', '00:00:00', 'USD', '14161.00', '14141.00', '14301.00', '14001.00', '14291.00', '13991.00', '2019-06-23 00:00:04'),
(978, '2019-06-23', '00:00:00', 'SGD', '10450.61', '10402.61', '10465.80', '10384.80', '10545.00', '10316.00', '2019-06-23 00:00:04'),
(979, '2019-06-23', '00:00:00', 'EUR', '16045.58', '15945.58', '16194.50', '15812.50', '16182.00', '15765.00', '2019-06-23 00:00:04'),
(980, '2019-06-23', '00:00:00', 'AUD', '9825.42', '9745.42', '9917.35', '9654.35', '9928.00', '9655.00', '2019-06-23 00:00:04'),
(981, '2019-06-23', '00:00:00', 'DKK', '2172.53', '2112.53', '2181.15', '2113.85', '2202.00', '2071.00', '2019-06-23 00:00:04'),
(982, '2019-06-23', '00:00:00', 'SEK', '1527.14', '1487.14', '1535.85', '1482.95', '1561.00', '1443.00', '2019-06-23 00:00:04'),
(983, '2019-06-23', '00:00:00', 'CAD', '10769.40', '10689.40', '10856.95', '10587.95', '10876.00', '10573.00', '2019-06-23 00:00:04'),
(984, '2019-06-23', '00:00:00', 'CHF', '14455.99', '14355.99', '14592.55', '14243.55', '14614.00', '14222.00', '2019-06-23 00:00:04'),
(985, '2019-06-23', '00:00:00', 'NZD', '9334.37', '9254.37', '9435.90', '9153.90', '9451.00', '9183.00', '2019-06-23 00:00:04'),
(986, '2019-06-23', '00:00:00', 'GBP', '17991.35', '17891.35', '18170.80', '17705.80', '18193.00', '17741.00', '2019-06-23 00:00:04'),
(987, '2019-06-23', '00:00:00', 'HKD', '1826.96', '1796.96', '1829.10', '1795.10', '1843.00', '1775.00', '2019-06-23 00:00:04'),
(988, '2019-06-23', '00:00:00', 'JPY', '133.42', '130.02', '134.22', '129.31', '135.24', '128.58', '2019-06-23 00:00:04'),
(989, '2019-06-23', '00:00:00', 'SAR', '3812.99', '3732.99', '3826.00', '3721.00', '3850.00', '3680.00', '2019-06-23 00:00:04'),
(990, '2019-06-23', '00:00:00', 'CNH', '2118.05', '1998.06', '2141.55', '1976.35', '2124.00', '1996.00', '2019-06-23 00:00:04'),
(991, '2019-06-23', '00:00:00', 'MYR', '3448.24', '3368.24', '3458.10', '3358.10', '0.00', '0.00', '2019-06-23 00:00:04'),
(992, '2019-06-23', '00:00:00', 'THB', '462.19', '454.19', '463.05', '453.05', '467.00', '438.00', '2019-06-23 00:00:04'),
(993, '2019-06-24', '00:00:00', 'USD', '14161.00', '14141.00', '14301.00', '14001.00', '14291.00', '13991.00', '2019-06-24 00:00:04'),
(994, '2019-06-24', '00:00:00', 'SGD', '10450.61', '10402.61', '10465.80', '10384.80', '10545.00', '10316.00', '2019-06-24 00:00:04'),
(995, '2019-06-24', '00:00:00', 'EUR', '16045.58', '15945.58', '16194.50', '15812.50', '16182.00', '15765.00', '2019-06-24 00:00:04'),
(996, '2019-06-24', '00:00:00', 'AUD', '9825.42', '9745.42', '9917.35', '9654.35', '9928.00', '9655.00', '2019-06-24 00:00:04'),
(997, '2019-06-24', '00:00:00', 'DKK', '2172.53', '2112.53', '2181.15', '2113.85', '2202.00', '2071.00', '2019-06-24 00:00:04'),
(998, '2019-06-24', '00:00:00', 'SEK', '1527.14', '1487.14', '1535.85', '1482.95', '1561.00', '1443.00', '2019-06-24 00:00:04'),
(999, '2019-06-24', '00:00:00', 'CAD', '10769.40', '10689.40', '10856.95', '10587.95', '10876.00', '10573.00', '2019-06-24 00:00:04'),
(1000, '2019-06-24', '00:00:00', 'CHF', '14455.99', '14355.99', '14592.55', '14243.55', '14614.00', '14222.00', '2019-06-24 00:00:04'),
(1001, '2019-06-24', '00:00:00', 'NZD', '9334.37', '9254.37', '9435.90', '9153.90', '9451.00', '9183.00', '2019-06-24 00:00:04'),
(1002, '2019-06-24', '00:00:00', 'GBP', '17991.35', '17891.35', '18170.80', '17705.80', '18193.00', '17741.00', '2019-06-24 00:00:04'),
(1003, '2019-06-24', '00:00:00', 'HKD', '1826.96', '1796.96', '1829.10', '1795.10', '1843.00', '1775.00', '2019-06-24 00:00:04'),
(1004, '2019-06-24', '00:00:00', 'JPY', '133.42', '130.02', '134.22', '129.31', '135.24', '128.58', '2019-06-24 00:00:04'),
(1005, '2019-06-24', '00:00:00', 'SAR', '3812.99', '3732.99', '3826.00', '3721.00', '3850.00', '3680.00', '2019-06-24 00:00:04'),
(1006, '2019-06-24', '00:00:00', 'CNH', '2118.05', '1998.06', '2141.55', '1976.35', '2124.00', '1996.00', '2019-06-24 00:00:04'),
(1007, '2019-06-24', '00:00:00', 'MYR', '3448.24', '3368.24', '3458.10', '3358.10', '0.00', '0.00', '2019-06-24 00:00:04'),
(1008, '2019-06-24', '00:00:00', 'THB', '462.19', '454.19', '463.05', '453.05', '467.00', '438.00', '2019-06-24 00:00:04'),
(1009, '2019-06-25', '00:00:00', 'USD', '14148.00', '14128.00', '14288.00', '13988.00', '14301.00', '14001.00', '2019-06-25 00:00:03'),
(1010, '2019-06-25', '00:00:00', 'SGD', '10463.34', '10415.34', '10480.90', '10398.90', '10566.00', '10336.00', '2019-06-25 00:00:03'),
(1011, '2019-06-25', '00:00:00', 'EUR', '16147.52', '16047.52', '16301.10', '15917.10', '16318.00', '15898.00', '2019-06-25 00:00:03'),
(1012, '2019-06-25', '00:00:00', 'AUD', '9865.91', '9785.91', '9959.75', '9695.75', '9972.00', '9699.00', '2019-06-25 00:00:03'),
(1013, '2019-06-25', '00:00:00', 'DKK', '2186.05', '2126.05', '2195.35', '2127.55', '2219.00', '2089.00', '2019-06-25 00:00:03'),
(1014, '2019-06-25', '00:00:00', 'SEK', '1536.15', '1496.15', '1545.50', '1492.30', '1572.00', '1454.00', '2019-06-25 00:00:03'),
(1015, '2019-06-25', '00:00:00', 'CAD', '10760.35', '10680.35', '10855.30', '10586.30', '10867.00', '10564.00', '2019-06-25 00:00:03'),
(1016, '2019-06-25', '00:00:00', 'CHF', '14528.98', '14428.98', '14664.50', '14313.50', '14694.00', '14301.00', '2019-06-25 00:00:03'),
(1017, '2019-06-25', '00:00:00', 'NZD', '9388.04', '9308.04', '9487.30', '9204.30', '9465.00', '9197.00', '2019-06-25 00:00:03'),
(1018, '2019-06-25', '00:00:00', 'GBP', '18050.50', '17950.50', '18238.50', '17771.50', '18263.00', '17810.00', '2019-06-25 00:00:03'),
(1019, '2019-06-25', '00:00:00', 'HKD', '1824.75', '1794.75', '1826.75', '1792.75', '1845.00', '1777.00', '2019-06-25 00:00:03'),
(1020, '2019-06-25', '00:00:00', 'JPY', '133.36', '129.96', '134.17', '129.25', '135.26', '128.60', '2019-06-25 00:00:03'),
(1021, '2019-06-25', '00:00:00', 'SAR', '3809.58', '3729.58', '3822.50', '3717.50', '3853.00', '3683.00', '2019-06-25 00:00:03'),
(1022, '2019-06-25', '00:00:00', 'CNH', '2114.63', '1994.63', '2138.00', '1972.90', '2120.00', '1992.00', '2019-06-25 00:00:03'),
(1023, '2019-06-25', '00:00:00', 'MYR', '3451.27', '3371.27', '3459.10', '3359.10', '0.00', '0.00', '2019-06-25 00:00:03'),
(1024, '2019-06-25', '00:00:00', 'THB', '463.40', '455.40', '464.45', '454.45', '469.00', '439.00', '2019-06-25 00:00:03'),
(1025, '2019-06-26', '00:00:00', 'USD', '14133.00', '14113.00', '14273.00', '13973.00', '14288.00', '13988.00', '2019-06-26 00:00:02'),
(1026, '2019-06-26', '00:00:00', 'SGD', '10459.59', '10411.59', '10476.80', '10394.80', '10563.00', '10333.00', '2019-06-26 00:00:02'),
(1027, '2019-06-26', '00:00:00', 'EUR', '16129.03', '16029.03', '16279.70', '15895.70', '16336.00', '15916.00', '2019-06-26 00:00:02'),
(1028, '2019-06-26', '00:00:00', 'AUD', '9881.61', '9801.61', '9976.35', '9711.35', '9992.00', '9718.00', '2019-06-26 00:00:02'),
(1029, '2019-06-26', '00:00:00', 'DKK', '2183.72', '2123.72', '2192.65', '2124.85', '2222.00', '2091.00', '2019-06-26 00:00:02'),
(1030, '2019-06-26', '00:00:00', 'SEK', '1545.97', '1505.96', '1555.35', '1501.85', '1582.00', '1464.00', '2019-06-26 00:00:02'),
(1031, '2019-06-26', '00:00:00', 'CAD', '10759.14', '10679.14', '10852.15', '10582.15', '10878.00', '10574.00', '2019-06-26 00:00:02'),
(1032, '2019-06-26', '00:00:00', 'CHF', '14526.22', '14426.22', '14659.65', '14308.65', '14749.00', '14354.00', '2019-06-26 00:00:02'),
(1033, '2019-06-26', '00:00:00', 'NZD', '9425.44', '9345.44', '9522.95', '9238.95', '9516.00', '9247.00', '2019-06-26 00:00:02'),
(1034, '2019-06-26', '00:00:00', 'GBP', '18075.19', '17975.19', '18262.15', '17795.15', '18240.00', '17788.00', '2019-06-26 00:00:02'),
(1035, '2019-06-26', '00:00:00', 'HKD', '1824.10', '1794.10', '1826.10', '1792.10', '1845.00', '1776.00', '2019-06-26 00:00:02'),
(1036, '2019-06-26', '00:00:00', 'JPY', '133.61', '130.21', '134.40', '129.47', '135.18', '128.53', '2019-06-26 00:00:02'),
(1037, '2019-06-26', '00:00:00', 'SAR', '3805.74', '3725.74', '3818.50', '3713.50', '3849.00', '3679.00', '2019-06-26 00:00:02'),
(1038, '2019-06-26', '00:00:00', 'CNH', '2112.26', '1992.26', '2135.60', '1970.40', '2115.00', '1987.00', '2019-06-26 00:00:02'),
(1039, '2019-06-26', '00:00:00', 'MYR', '3448.47', '3368.47', '3456.30', '3356.30', '0.00', '0.00', '2019-06-26 00:00:02'),
(1040, '2019-06-26', '00:00:00', 'THB', '463.51', '455.51', '464.55', '454.55', '471.00', '441.00', '2019-06-26 00:00:02'),
(1041, '2019-06-27', '00:00:00', 'USD', '14183.00', '14163.00', '14323.00', '14023.00', '14328.00', '14028.00', '2019-06-27 00:00:04'),
(1042, '2019-06-27', '00:00:00', 'SGD', '10488.42', '10440.42', '10504.42', '10422.42', '10571.00', '10341.00', '2019-06-27 00:00:04'),
(1043, '2019-06-27', '00:00:00', 'EUR', '16149.11', '16049.11', '16302.40', '15919.40', '16307.00', '15888.00', '2019-06-27 00:00:04'),
(1044, '2019-06-27', '00:00:00', 'AUD', '9934.17', '9854.17', '10029.50', '9762.50', '10010.00', '9736.00', '2019-06-27 00:00:04'),
(1045, '2019-06-27', '00:00:00', 'DKK', '2186.53', '2126.53', '2195.75', '2128.05', '2218.00', '2088.00', '2019-06-27 00:00:04'),
(1046, '2019-06-27', '00:00:00', 'SEK', '1547.67', '1507.67', '1557.40', '1503.90', '1583.00', '1464.00', '2019-06-27 00:00:04'),
(1047, '2019-06-27', '00:00:00', 'CAD', '10808.93', '10728.93', '10901.75', '10631.75', '10898.00', '10594.00', '2019-06-27 00:00:04'),
(1048, '2019-06-27', '00:00:00', 'CHF', '14577.47', '14477.47', '14715.50', '14363.50', '14723.00', '14330.00', '2019-06-27 00:00:04'),
(1049, '2019-06-27', '00:00:00', 'NZD', '9487.72', '9407.72', '9590.65', '9305.65', '9558.00', '9289.00', '2019-06-27 00:00:04'),
(1050, '2019-06-27', '00:00:00', 'GBP', '18012.87', '17912.87', '18201.65', '17736.65', '18204.00', '17753.00', '2019-06-27 00:00:04'),
(1051, '2019-06-27', '00:00:00', 'HKD', '1829.83', '1799.83', '1831.85', '1797.85', '1849.00', '1781.00', '2019-06-27 00:00:04'),
(1052, '2019-06-27', '00:00:00', 'JPY', '133.61', '130.21', '134.42', '129.50', '135.35', '128.70', '2019-06-27 00:00:04'),
(1053, '2019-06-27', '00:00:00', 'SAR', '3819.07', '3739.07', '3831.90', '3726.90', '3860.00', '3690.00', '2019-06-27 00:00:04'),
(1054, '2019-06-27', '00:00:00', 'CNH', '2118.87', '1998.86', '2142.50', '1977.30', '2118.00', '1990.00', '2019-06-27 00:00:04'),
(1055, '2019-06-27', '00:00:00', 'MYR', '3455.59', '3375.59', '3463.00', '3363.00', '0.00', '0.00', '2019-06-27 00:00:04'),
(1056, '2019-06-27', '00:00:00', 'THB', '465.44', '457.44', '466.60', '456.60', '469.00', '439.00', '2019-06-27 00:00:04'),
(1057, '2019-06-28', '00:00:00', 'USD', '14146.00', '14126.00', '14286.00', '13986.00', '14323.00', '14023.00', '2019-06-28 00:00:05'),
(1058, '2019-06-28', '00:00:00', 'SGD', '10456.86', '10408.86', '10475.92', '10394.92', '10579.00', '10349.00', '2019-06-28 00:00:05'),
(1059, '2019-06-28', '00:00:00', 'EUR', '16121.22', '16021.22', '16271.70', '15887.70', '16323.00', '15904.00', '2019-06-28 00:00:05'),
(1060, '2019-06-28', '00:00:00', 'AUD', '9929.54', '9849.54', '10026.40', '9758.40', '10045.00', '9770.00', '2019-06-28 00:00:05'),
(1061, '2019-06-28', '00:00:00', 'DKK', '2182.97', '2122.97', '2192.30', '2124.60', '2221.00', '2090.00', '2019-06-28 00:00:05'),
(1062, '2019-06-28', '00:00:00', 'SEK', '1544.50', '1504.50', '1554.10', '1500.70', '1588.00', '1470.00', '2019-06-28 00:00:05'),
(1063, '2019-06-28', '00:00:00', 'CAD', '10811.52', '10731.52', '10905.10', '10634.10', '10947.00', '10642.00', '2019-06-28 00:00:05'),
(1064, '2019-06-28', '00:00:00', 'CHF', '14488.49', '14388.49', '14623.50', '14273.50', '14694.00', '14301.00', '2019-06-28 00:00:05'),
(1065, '2019-06-28', '00:00:00', 'NZD', '9495.57', '9415.57', '9597.30', '9312.30', '9605.00', '9335.00', '2019-06-28 00:00:05'),
(1066, '2019-06-28', '00:00:00', 'GBP', '17997.78', '17897.78', '18184.60', '17718.60', '18213.00', '17762.00', '2019-06-28 00:00:05'),
(1067, '2019-06-28', '00:00:00', 'HKD', '1824.35', '1794.35', '1826.30', '1792.30', '1848.00', '1780.00', '2019-06-28 00:00:05'),
(1068, '2019-06-28', '00:00:00', 'JPY', '132.65', '129.25', '133.46', '128.57', '134.98', '128.33', '2019-06-28 00:00:05'),
(1069, '2019-06-28', '00:00:00', 'SAR', '3809.24', '3729.24', '3822.10', '3717.10', '3859.00', '3689.00', '2019-06-28 00:00:05'),
(1070, '2019-06-28', '00:00:00', 'CNH', '2115.49', '1995.49', '2139.00', '1973.80', '2120.00', '1992.00', '2019-06-28 00:00:05'),
(1071, '2019-06-28', '00:00:00', 'MYR', '3450.38', '3370.38', '3458.60', '3358.60', '0.00', '0.00', '2019-06-28 00:00:05'),
(1072, '2019-06-28', '00:00:00', 'THB', '463.19', '455.19', '464.20', '454.20', '470.00', '441.00', '2019-06-28 00:00:05'),
(1073, '2019-06-29', '00:00:00', 'USD', '14136.00', '14116.00', '14276.00', '13976.00', '14286.00', '13986.00', '2019-06-29 00:00:04'),
(1074, '2019-06-29', '00:00:00', 'SGD', '10468.75', '10420.75', '10486.00', '10404.00', '10561.00', '10332.00', '2019-06-29 00:00:04'),
(1075, '2019-06-29', '00:00:00', 'EUR', '16138.81', '16038.81', '16290.25', '15906.25', '16287.00', '15867.00', '2019-06-29 00:00:04'),
(1076, '2019-06-29', '00:00:00', 'AUD', '9945.15', '9865.15', '10043.65', '9775.65', '10046.00', '9771.00', '2019-06-29 00:00:04'),
(1077, '2019-06-29', '00:00:00', 'DKK', '2185.66', '2125.66', '2194.90', '2127.10', '2216.00', '2086.00', '2019-06-29 00:00:04'),
(1078, '2019-06-29', '00:00:00', 'SEK', '1545.10', '1505.10', '1554.75', '1501.25', '1582.00', '1464.00', '2019-06-29 00:00:04'),
(1079, '2019-06-29', '00:00:00', 'CAD', '10826.50', '10746.50', '10919.10', '10648.10', '10947.00', '10642.00', '2019-06-29 00:00:04'),
(1080, '2019-06-29', '00:00:00', 'CHF', '14552.34', '14452.34', '14689.85', '14337.85', '14683.00', '14290.00', '2019-06-29 00:00:04'),
(1081, '2019-06-29', '00:00:00', 'NZD', '9515.01', '9435.01', '9616.25', '9330.25', '9615.00', '9344.00', '2019-06-29 00:00:04'),
(1082, '2019-06-29', '00:00:00', 'GBP', '17964.60', '17864.60', '18153.30', '17688.30', '18143.00', '17693.00', '2019-06-29 00:00:04'),
(1083, '2019-06-29', '00:00:00', 'HKD', '1824.42', '1794.42', '1826.40', '1792.40', '1842.00', '1774.00', '2019-06-29 00:00:04'),
(1084, '2019-06-29', '00:00:00', 'JPY', '132.91', '129.52', '133.73', '128.83', '134.70', '128.05', '2019-06-29 00:00:04'),
(1085, '2019-06-29', '00:00:00', 'SAR', '3806.64', '3726.64', '3819.85', '3714.85', '3849.00', '3679.00', '2019-06-29 00:00:04'),
(1086, '2019-06-29', '00:00:00', 'CNH', '2115.29', '1995.29', '2139.05', '1973.85', '2116.00', '1988.00', '2019-06-29 00:00:04'),
(1087, '2019-06-29', '00:00:00', 'MYR', '3457.44', '3377.44', '3465.40', '3364.40', '0.00', '0.00', '2019-06-29 00:00:04'),
(1088, '2019-06-29', '00:00:00', 'THB', '464.51', '456.51', '465.55', '455.55', '469.00', '439.00', '2019-06-29 00:00:04'),
(1089, '2019-06-30', '00:00:00', 'USD', '14136.00', '14116.00', '14276.00', '13976.00', '14286.00', '13986.00', '2019-06-30 00:00:05'),
(1090, '2019-06-30', '00:00:00', 'SGD', '10468.75', '10420.75', '10486.00', '10404.00', '10561.00', '10332.00', '2019-06-30 00:00:05'),
(1091, '2019-06-30', '00:00:00', 'EUR', '16138.81', '16038.81', '16290.25', '15906.25', '16287.00', '15867.00', '2019-06-30 00:00:05'),
(1092, '2019-06-30', '00:00:00', 'AUD', '9945.15', '9865.15', '10043.65', '9775.65', '10046.00', '9771.00', '2019-06-30 00:00:05'),
(1093, '2019-06-30', '00:00:00', 'DKK', '2185.66', '2125.66', '2194.90', '2127.10', '2216.00', '2086.00', '2019-06-30 00:00:05'),
(1094, '2019-06-30', '00:00:00', 'SEK', '1545.10', '1505.10', '1554.75', '1501.25', '1582.00', '1464.00', '2019-06-30 00:00:05'),
(1095, '2019-06-30', '00:00:00', 'CAD', '10826.50', '10746.50', '10919.10', '10648.10', '10947.00', '10642.00', '2019-06-30 00:00:05'),
(1096, '2019-06-30', '00:00:00', 'CHF', '14552.34', '14452.34', '14689.85', '14337.85', '14683.00', '14290.00', '2019-06-30 00:00:05'),
(1097, '2019-06-30', '00:00:00', 'NZD', '9515.01', '9435.01', '9616.25', '9330.25', '9615.00', '9344.00', '2019-06-30 00:00:05'),
(1098, '2019-06-30', '00:00:00', 'GBP', '17964.60', '17864.60', '18153.30', '17688.30', '18143.00', '17693.00', '2019-06-30 00:00:05'),
(1099, '2019-06-30', '00:00:00', 'HKD', '1824.42', '1794.42', '1826.40', '1792.40', '1842.00', '1774.00', '2019-06-30 00:00:05'),
(1100, '2019-06-30', '00:00:00', 'JPY', '132.91', '129.52', '133.73', '128.83', '134.70', '128.05', '2019-06-30 00:00:05'),
(1101, '2019-06-30', '00:00:00', 'SAR', '3806.64', '3726.64', '3819.85', '3714.85', '3849.00', '3679.00', '2019-06-30 00:00:05'),
(1102, '2019-06-30', '00:00:00', 'CNH', '2115.29', '1995.29', '2139.05', '1973.85', '2116.00', '1988.00', '2019-06-30 00:00:05'),
(1103, '2019-06-30', '00:00:00', 'MYR', '3457.44', '3377.44', '3465.40', '3364.40', '0.00', '0.00', '2019-06-30 00:00:05'),
(1104, '2019-06-30', '00:00:00', 'THB', '464.51', '456.51', '465.55', '455.55', '469.00', '439.00', '2019-06-30 00:00:05'),
(1105, '2019-07-01', '00:00:00', 'USD', '14136.00', '14116.00', '14276.00', '13976.00', '14286.00', '13986.00', '2019-07-01 00:00:04'),
(1106, '2019-07-01', '00:00:00', 'SGD', '10468.75', '10420.75', '10486.00', '10404.00', '10561.00', '10332.00', '2019-07-01 00:00:04'),
(1107, '2019-07-01', '00:00:00', 'EUR', '16138.81', '16038.81', '16290.25', '15906.25', '16287.00', '15867.00', '2019-07-01 00:00:04'),
(1108, '2019-07-01', '00:00:00', 'AUD', '9945.15', '9865.15', '10043.65', '9775.65', '10046.00', '9771.00', '2019-07-01 00:00:04'),
(1109, '2019-07-01', '00:00:00', 'DKK', '2185.66', '2125.66', '2194.90', '2127.10', '2216.00', '2086.00', '2019-07-01 00:00:04'),
(1110, '2019-07-01', '00:00:00', 'SEK', '1545.10', '1505.10', '1554.75', '1501.25', '1582.00', '1464.00', '2019-07-01 00:00:04'),
(1111, '2019-07-01', '00:00:00', 'CAD', '10826.50', '10746.50', '10919.10', '10648.10', '10947.00', '10642.00', '2019-07-01 00:00:04'),
(1112, '2019-07-01', '00:00:00', 'CHF', '14552.34', '14452.34', '14689.85', '14337.85', '14683.00', '14290.00', '2019-07-01 00:00:04'),
(1113, '2019-07-01', '00:00:00', 'NZD', '9515.01', '9435.01', '9616.25', '9330.25', '9615.00', '9344.00', '2019-07-01 00:00:04'),
(1114, '2019-07-01', '00:00:00', 'GBP', '17964.60', '17864.60', '18153.30', '17688.30', '18143.00', '17693.00', '2019-07-01 00:00:04'),
(1115, '2019-07-01', '00:00:00', 'HKD', '1824.42', '1794.42', '1826.40', '1792.40', '1842.00', '1774.00', '2019-07-01 00:00:04'),
(1116, '2019-07-01', '00:00:00', 'JPY', '132.91', '129.52', '133.73', '128.83', '134.70', '128.05', '2019-07-01 00:00:04'),
(1117, '2019-07-01', '00:00:00', 'SAR', '3806.64', '3726.64', '3819.85', '3714.85', '3849.00', '3679.00', '2019-07-01 00:00:04'),
(1118, '2019-07-01', '00:00:00', 'CNH', '2115.29', '1995.29', '2139.05', '1973.85', '2116.00', '1988.00', '2019-07-01 00:00:04'),
(1119, '2019-07-01', '00:00:00', 'MYR', '3457.44', '3377.44', '3465.40', '3364.40', '0.00', '0.00', '2019-07-01 00:00:04'),
(1120, '2019-07-01', '00:00:00', 'THB', '464.51', '456.51', '465.55', '455.55', '469.00', '439.00', '2019-07-01 00:00:04'),
(1121, '2019-07-02', '00:00:00', 'USD', '14123.00', '14103.00', '14263.00', '13963.00', '14276.00', '13976.00', '2019-07-02 00:00:07'),
(1122, '2019-07-02', '00:00:00', 'SGD', '10445.65', '10397.65', '10462.42', '10380.42', '10562.00', '10332.00', '2019-07-02 00:00:07'),
(1123, '2019-07-02', '00:00:00', 'EUR', '16040.03', '15940.03', '16188.50', '15806.50', '16259.00', '15841.00', '2019-07-02 00:00:07'),
(1124, '2019-07-02', '00:00:00', 'AUD', '9918.40', '9838.40', '10013.10', '9746.10', '10051.00', '9775.00', '2019-07-02 00:00:07'),
(1125, '2019-07-02', '00:00:00', 'DKK', '2172.32', '2112.32', '2181.00', '2113.60', '2212.00', '2081.00', '2019-07-02 00:00:07'),
(1126, '2019-07-02', '00:00:00', 'SEK', '1537.50', '1497.50', '1546.20', '1492.90', '1577.00', '1459.00', '2019-07-02 00:00:07'),
(1127, '2019-07-02', '00:00:00', 'CAD', '10812.87', '10732.87', '10905.80', '10634.80', '10936.00', '10631.00', '2019-07-02 00:00:07'),
(1128, '2019-07-02', '00:00:00', 'CHF', '14393.21', '14293.21', '14524.20', '14176.20', '14631.00', '14239.00', '2019-07-02 00:00:07'),
(1129, '2019-07-02', '00:00:00', 'NZD', '9507.00', '9427.00', '9607.45', '9321.45', '9627.00', '9355.00', '2019-07-02 00:00:07'),
(1130, '2019-07-02', '00:00:00', 'GBP', '17890.24', '17790.24', '18074.10', '17610.10', '18156.00', '17706.00', '2019-07-02 00:00:07'),
(1131, '2019-07-02', '00:00:00', 'HKD', '1822.41', '1792.41', '1824.35', '1790.35', '1843.00', '1774.00', '2019-07-02 00:00:07'),
(1132, '2019-07-02', '00:00:00', 'JPY', '132.06', '128.66', '132.82', '127.94', '133.97', '127.34', '2019-07-02 00:00:07'),
(1133, '2019-07-02', '00:00:00', 'SAR', '3803.22', '3723.22', '3815.95', '3710.95', '3846.00', '3676.00', '2019-07-02 00:00:07'),
(1134, '2019-07-02', '00:00:00', 'CNH', '2121.92', '2001.92', '2145.40', '1980.00', '2117.00', '1989.00', '2019-07-02 00:00:07'),
(1135, '2019-07-02', '00:00:00', 'MYR', '3453.89', '3373.89', '3461.85', '3360.85', '0.00', '0.00', '2019-07-02 00:00:07'),
(1136, '2019-07-02', '00:00:00', 'THB', '465.44', '457.44', '466.50', '456.50', '470.00', '440.00', '2019-07-02 00:00:07'),
(1137, '2019-07-03', '00:00:00', 'USD', '14147.00', '14127.00', '14287.00', '13987.00', '14263.00', '13963.00', '2019-07-03 00:00:04'),
(1138, '2019-07-03', '00:00:00', 'SGD', '10448.75', '10400.75', '10465.10', '10384.10', '10521.00', '10292.00', '2019-07-03 00:00:04'),
(1139, '2019-07-03', '00:00:00', 'EUR', '16015.62', '15915.62', '16165.80', '15783.80', '16136.00', '15720.00', '2019-07-03 00:00:04'),
(1140, '2019-07-03', '00:00:00', 'AUD', '9911.17', '9831.17', '10008.55', '9741.55', '9970.00', '9696.00', '2019-07-03 00:00:04'),
(1141, '2019-07-03', '00:00:00', 'DKK', '2169.09', '2109.09', '2177.75', '2110.55', '2196.00', '2066.00', '2019-07-03 00:00:04'),
(1142, '2019-07-03', '00:00:00', 'SEK', '1533.03', '1493.03', '1542.55', '1489.45', '1565.00', '1447.00', '2019-07-03 00:00:04'),
(1143, '2019-07-03', '00:00:00', 'CAD', '10809.82', '10729.82', '10900.85', '10629.85', '10890.00', '10585.00', '2019-07-03 00:00:04'),
(1144, '2019-07-03', '00:00:00', 'CHF', '14360.15', '14260.15', '14494.05', '14147.05', '14494.00', '14104.00', '2019-07-03 00:00:04'),
(1145, '2019-07-03', '00:00:00', 'NZD', '9458.77', '9378.77', '9558.00', '9273.00', '9551.00', '9281.00', '2019-07-03 00:00:04'),
(1146, '2019-07-03', '00:00:00', 'GBP', '17888.06', '17788.06', '18068.80', '17605.80', '18069.00', '17619.00', '2019-07-03 00:00:04'),
(1147, '2019-07-03', '00:00:00', 'HKD', '1826.55', '1796.55', '1828.55', '1794.55', '1840.00', '1772.00', '2019-07-03 00:00:04'),
(1148, '2019-07-03', '00:00:00', 'JPY', '132.32', '128.92', '133.11', '128.23', '133.70', '127.07', '2019-07-03 00:00:04'),
(1149, '2019-07-03', '00:00:00', 'SAR', '3809.62', '3729.62', '3822.35', '3717.35', '3843.00', '3673.00', '2019-07-03 00:00:04'),
(1150, '2019-07-03', '00:00:00', 'CNH', '2114.47', '1994.47', '2137.95', '1972.75', '2119.00', '1991.00', '2019-07-03 00:00:04'),
(1151, '2019-07-03', '00:00:00', 'MYR', '3456.80', '3376.80', '3464.70', '3364.70', '0.00', '0.00', '2019-07-03 00:00:04'),
(1152, '2019-07-03', '00:00:00', 'THB', '464.57', '456.57', '465.60', '455.60', '469.00', '439.00', '2019-07-03 00:00:04'),
(1153, '2019-07-04', '00:00:00', 'USD', '14128.00', '14108.00', '14268.00', '13968.00', '14287.00', '13987.00', '2019-07-04 00:00:04'),
(1154, '2019-07-04', '00:00:00', 'SGD', '10438.58', '10390.58', '10455.70', '10374.70', '10547.00', '10318.00', '2019-07-04 00:00:04'),
(1155, '2019-07-04', '00:00:00', 'EUR', '15983.58', '15883.58', '16137.15', '15756.15', '16173.00', '15757.00', '2019-07-04 00:00:04'),
(1156, '2019-07-04', '00:00:00', 'AUD', '9941.66', '9861.66', '10040.85', '9772.85', '10025.00', '9751.00', '2019-07-04 00:00:04'),
(1157, '2019-07-04', '00:00:00', 'DKK', '2164.71', '2104.71', '2173.75', '2106.55', '2201.00', '2070.00', '2019-07-04 00:00:04'),
(1158, '2019-07-04', '00:00:00', 'SEK', '1533.56', '1493.56', '1543.50', '1490.30', '1573.00', '1454.00', '2019-07-04 00:00:04'),
(1159, '2019-07-04', '00:00:00', 'CAD', '10813.81', '10733.81', '10911.30', '10640.30', '10939.00', '10634.00', '2019-07-04 00:00:04'),
(1160, '2019-07-04', '00:00:00', 'CHF', '14372.10', '14272.10', '14505.65', '14157.65', '14559.00', '14169.00', '2019-07-04 00:00:04'),
(1161, '2019-07-04', '00:00:00', 'NZD', '9487.77', '9407.77', '9588.05', '9303.05', '9578.00', '9308.00', '2019-07-04 00:00:04'),
(1162, '2019-07-04', '00:00:00', 'GBP', '17804.80', '17704.80', '17989.20', '17527.20', '18033.00', '17585.00', '2019-07-04 00:00:04'),
(1163, '2019-07-04', '00:00:00', 'HKD', '1825.83', '1795.83', '1827.85', '1793.85', '1846.00', '1778.00', '2019-07-04 00:00:04'),
(1164, '2019-07-04', '00:00:00', 'JPY', '132.80', '129.39', '133.57', '128.67', '134.66', '128.01', '2019-07-04 00:00:04'),
(1165, '2019-07-04', '00:00:00', 'SAR', '3804.55', '3724.55', '3817.30', '3712.30', '3849.00', '3679.00', '2019-07-04 00:00:04'),
(1166, '2019-07-04', '00:00:00', 'CNH', '2110.34', '1990.33', '2133.85', '1968.75', '2117.00', '1989.00', '2019-07-04 00:00:04'),
(1167, '2019-07-04', '00:00:00', 'MYR', '3452.21', '3372.21', '3460.10', '3360.10', '0.00', '0.00', '2019-07-04 00:00:04'),
(1168, '2019-07-04', '00:00:00', 'THB', '466.20', '458.20', '467.25', '457.25', '472.00', '443.00', '2019-07-04 00:00:04'),
(1169, '2019-07-05', '00:00:00', 'USD', '14146.00', '14126.00', '14286.00', '13986.00', '14268.00', '13968.00', '2019-07-05 00:00:03'),
(1170, '2019-07-05', '00:00:00', 'SGD', '10444.93', '10396.93', '10462.42', '10381.42', '10526.00', '10296.00', '2019-07-05 00:00:03'),
(1171, '2019-07-05', '00:00:00', 'EUR', '15991.87', '15891.87', '16144.70', '15763.70', '16146.00', '15730.00', '2019-07-05 00:00:03'),
(1172, '2019-07-05', '00:00:00', 'AUD', '9962.06', '9882.06', '10057.80', '9789.80', '10072.00', '9796.00', '2019-07-05 00:00:03'),
(1173, '2019-07-05', '00:00:00', 'DKK', '2166.11', '2106.11', '2175.50', '2108.40', '2198.00', '2067.00', '2019-07-05 00:00:03'),
(1174, '2019-07-05', '00:00:00', 'SEK', '1534.22', '1494.22', '1543.95', '1490.85', '1575.00', '1457.00', '2019-07-05 00:00:03'),
(1175, '2019-07-05', '00:00:00', 'CAD', '10849.00', '10769.00', '10945.15', '10673.15', '10959.00', '10653.00', '2019-07-05 00:00:03'),
(1176, '2019-07-05', '00:00:00', 'CHF', '14377.27', '14277.27', '14509.25', '14162.25', '14515.00', '14125.00', '2019-07-05 00:00:03'),
(1177, '2019-07-05', '00:00:00', 'NZD', '9482.14', '9402.14', '9584.45', '9299.45', '9601.00', '9330.00', '2019-07-05 00:00:03'),
(1178, '2019-07-05', '00:00:00', 'GBP', '17831.67', '17731.67', '18016.20', '17553.20', '17982.00', '17535.00', '2019-07-05 00:00:03'),
(1179, '2019-07-05', '00:00:00', 'HKD', '1829.95', '1799.95', '1832.05', '1798.05', '1844.00', '1776.00', '2019-07-05 00:00:03'),
(1180, '2019-07-05', '00:00:00', 'JPY', '132.81', '129.41', '133.60', '128.70', '134.43', '127.79', '2019-07-05 00:00:03'),
(1181, '2019-07-05', '00:00:00', 'SAR', '3809.30', '3729.30', '3822.10', '3717.10', '3844.00', '3674.00', '2019-07-05 00:00:03'),
(1182, '2019-07-05', '00:00:00', 'CNH', '2115.62', '1995.62', '2139.20', '1974.00', '2112.00', '1984.00', '2019-07-05 00:00:03'),
(1183, '2019-07-05', '00:00:00', 'MYR', '3458.21', '3378.21', '3466.15', '3365.15', '0.00', '0.00', '2019-07-05 00:00:03'),
(1184, '2019-07-05', '00:00:00', 'THB', '464.98', '456.98', '466.05', '456.05', '470.00', '440.00', '2019-07-05 00:00:03'),
(1185, '2019-07-06', '00:00:00', 'USD', '14091.00', '14071.00', '14231.00', '13931.00', '14286.00', '13986.00', '2019-07-06 00:00:03'),
(1186, '2019-07-06', '00:00:00', 'SGD', '10400.18', '10352.18', '9679.00', '9598.00', '10538.00', '10309.00', '2019-07-06 00:00:03'),
(1187, '2019-07-06', '00:00:00', 'EUR', '15907.32', '15807.32', '16055.50', '15675.50', '16155.00', '15739.00', '2019-07-06 00:00:03'),
(1188, '2019-07-06', '00:00:00', 'AUD', '9920.63', '9840.63', '10017.65', '9749.65', '10066.00', '9790.00', '2019-07-06 00:00:03'),
(1189, '2019-07-06', '00:00:00', 'DKK', '2154.64', '2094.64', '2163.25', '2096.25', '2199.00', '2068.00', '2019-07-06 00:00:03'),
(1190, '2019-07-06', '00:00:00', 'SEK', '1523.85', '1483.85', '1533.15', '1480.15', '1574.00', '1455.00', '2019-07-06 00:00:03'),
(1191, '2019-07-06', '00:00:00', 'CAD', '10816.01', '10736.01', '10913.20', '10639.20', '10975.00', '10670.00', '2019-07-06 00:00:03'),
(1192, '2019-07-06', '00:00:00', 'CHF', '14312.13', '14212.13', '14444.60', '14097.60', '14547.00', '14156.00', '2019-07-06 00:00:03'),
(1193, '2019-07-06', '00:00:00', 'NZD', '9432.03', '9352.03', '9531.90', '9246.90', '9589.00', '9318.00', '2019-07-06 00:00:03'),
(1194, '2019-07-06', '00:00:00', 'GBP', '17732.92', '17632.92', '17915.55', '17453.55', '18003.00', '17556.00', '2019-07-06 00:00:03'),
(1195, '2019-07-06', '00:00:00', 'HKD', '1822.19', '1792.19', '1695.85', '1661.85', '1849.00', '1781.00', '2019-07-06 00:00:03');
INSERT INTO `kurs_all` (`id`, `kursDate`, `kursTime`, `currency`, `rate_jual`, `rate_beli`, `tt_jual`, `tt_beli`, `notes_jual`, `notes_beli`, `inputDate`) VALUES
(1196, '2019-07-06', '00:00:00', 'JPY', '132.06', '128.66', '132.85', '127.96', '134.52', '127.88', '2019-07-06 00:00:03'),
(1197, '2019-07-06', '00:00:00', 'SAR', '3794.53', '3714.53', '3807.30', '3702.30', '3849.00', '3679.00', '2019-07-06 00:00:03'),
(1198, '2019-07-06', '00:00:00', 'CNH', '2105.89', '1985.88', '2129.45', '1964.25', '2117.00', '1989.00', '2019-07-06 00:00:03'),
(1199, '2019-07-06', '00:00:00', 'MYR', '3445.73', '3365.73', '3453.65', '3353.65', '0.00', '0.00', '2019-07-06 00:00:03'),
(1200, '2019-07-06', '00:00:00', 'THB', '463.34', '455.34', '464.40', '454.40', '470.00', '441.00', '2019-07-06 00:00:03'),
(1201, '2019-07-07', '00:00:00', 'USD', '14091.00', '14071.00', '14231.00', '13931.00', '14286.00', '13986.00', '2019-07-07 00:00:04'),
(1202, '2019-07-07', '00:00:00', 'SGD', '10400.18', '10352.18', '9679.00', '9598.00', '10538.00', '10309.00', '2019-07-07 00:00:04'),
(1203, '2019-07-07', '00:00:00', 'EUR', '15907.32', '15807.32', '16055.50', '15675.50', '16155.00', '15739.00', '2019-07-07 00:00:04'),
(1204, '2019-07-07', '00:00:00', 'AUD', '9920.63', '9840.63', '10017.65', '9749.65', '10066.00', '9790.00', '2019-07-07 00:00:04'),
(1205, '2019-07-07', '00:00:00', 'DKK', '2154.64', '2094.64', '2163.25', '2096.25', '2199.00', '2068.00', '2019-07-07 00:00:04'),
(1206, '2019-07-07', '00:00:00', 'SEK', '1523.85', '1483.85', '1533.15', '1480.15', '1574.00', '1455.00', '2019-07-07 00:00:04'),
(1207, '2019-07-07', '00:00:00', 'CAD', '10816.01', '10736.01', '10913.20', '10639.20', '10975.00', '10670.00', '2019-07-07 00:00:04'),
(1208, '2019-07-07', '00:00:00', 'CHF', '14312.13', '14212.13', '14444.60', '14097.60', '14547.00', '14156.00', '2019-07-07 00:00:04'),
(1209, '2019-07-07', '00:00:00', 'NZD', '9432.03', '9352.03', '9531.90', '9246.90', '9589.00', '9318.00', '2019-07-07 00:00:04'),
(1210, '2019-07-07', '00:00:00', 'GBP', '17732.92', '17632.92', '17915.55', '17453.55', '18003.00', '17556.00', '2019-07-07 00:00:04'),
(1211, '2019-07-07', '00:00:00', 'HKD', '1822.19', '1792.19', '1695.85', '1661.85', '1849.00', '1781.00', '2019-07-07 00:00:04'),
(1212, '2019-07-07', '00:00:00', 'JPY', '132.06', '128.66', '132.85', '127.96', '134.52', '127.88', '2019-07-07 00:00:04'),
(1213, '2019-07-07', '00:00:00', 'SAR', '3794.53', '3714.53', '3807.30', '3702.30', '3849.00', '3679.00', '2019-07-07 00:00:04'),
(1214, '2019-07-07', '00:00:00', 'CNH', '2105.89', '1985.88', '2129.45', '1964.25', '2117.00', '1989.00', '2019-07-07 00:00:04'),
(1215, '2019-07-07', '00:00:00', 'MYR', '3445.73', '3365.73', '3453.65', '3353.65', '0.00', '0.00', '2019-07-07 00:00:04'),
(1216, '2019-07-07', '00:00:00', 'THB', '463.34', '455.34', '464.40', '454.40', '470.00', '441.00', '2019-07-07 00:00:04'),
(1217, '2019-07-08', '00:00:00', 'USD', '14091.00', '14071.00', '14231.00', '13931.00', '14286.00', '13986.00', '2019-07-08 00:00:04'),
(1218, '2019-07-08', '00:00:00', 'SGD', '10400.18', '10352.18', '9679.00', '9598.00', '10538.00', '10309.00', '2019-07-08 00:00:04'),
(1219, '2019-07-08', '00:00:00', 'EUR', '15907.32', '15807.32', '16055.50', '15675.50', '16155.00', '15739.00', '2019-07-08 00:00:04'),
(1220, '2019-07-08', '00:00:00', 'AUD', '9920.63', '9840.63', '10017.65', '9749.65', '10066.00', '9790.00', '2019-07-08 00:00:04'),
(1221, '2019-07-08', '00:00:00', 'DKK', '2154.64', '2094.64', '2163.25', '2096.25', '2199.00', '2068.00', '2019-07-08 00:00:04'),
(1222, '2019-07-08', '00:00:00', 'SEK', '1523.85', '1483.85', '1533.15', '1480.15', '1574.00', '1455.00', '2019-07-08 00:00:04'),
(1223, '2019-07-08', '00:00:00', 'CAD', '10816.01', '10736.01', '10913.20', '10639.20', '10975.00', '10670.00', '2019-07-08 00:00:04'),
(1224, '2019-07-08', '00:00:00', 'CHF', '14312.13', '14212.13', '14444.60', '14097.60', '14547.00', '14156.00', '2019-07-08 00:00:04'),
(1225, '2019-07-08', '00:00:00', 'NZD', '9432.03', '9352.03', '9531.90', '9246.90', '9589.00', '9318.00', '2019-07-08 00:00:04'),
(1226, '2019-07-08', '00:00:00', 'GBP', '17732.92', '17632.92', '17915.55', '17453.55', '18003.00', '17556.00', '2019-07-08 00:00:04'),
(1227, '2019-07-08', '00:00:00', 'HKD', '1822.19', '1792.19', '1695.85', '1661.85', '1849.00', '1781.00', '2019-07-08 00:00:04'),
(1228, '2019-07-08', '00:00:00', 'JPY', '132.06', '128.66', '132.85', '127.96', '134.52', '127.88', '2019-07-08 00:00:04'),
(1229, '2019-07-08', '00:00:00', 'SAR', '3794.53', '3714.53', '3807.30', '3702.30', '3849.00', '3679.00', '2019-07-08 00:00:04'),
(1230, '2019-07-08', '00:00:00', 'CNH', '2105.89', '1985.88', '2129.45', '1964.25', '2117.00', '1989.00', '2019-07-08 00:00:04'),
(1231, '2019-07-08', '00:00:00', 'MYR', '3445.73', '3365.73', '3453.65', '3353.65', '0.00', '0.00', '2019-07-08 00:00:04'),
(1232, '2019-07-08', '00:00:00', 'THB', '463.34', '455.34', '464.40', '454.40', '470.00', '441.00', '2019-07-08 00:00:04'),
(1233, '2019-07-09', '00:00:00', 'USD', '14118.00', '14098.00', '14258.00', '13958.00', '14231.00', '13931.00', '2019-07-09 00:00:04'),
(1234, '2019-07-09', '00:00:00', 'SGD', '10400.20', '10352.20', '10417.20', '10336.20', '10467.00', '10238.00', '2019-07-09 00:00:04'),
(1235, '2019-07-09', '00:00:00', 'EUR', '15896.10', '15796.10', '16043.25', '15663.25', '16019.00', '15604.00', '2019-07-09 00:00:04'),
(1236, '2019-07-09', '00:00:00', 'AUD', '9899.38', '9819.38', '9995.35', '9728.35', '9972.00', '9698.00', '2019-07-09 00:00:04'),
(1237, '2019-07-09', '00:00:00', 'DKK', '2153.23', '2093.23', '2161.40', '2094.60', '2180.00', '2050.00', '2019-07-09 00:00:04'),
(1238, '2019-07-09', '00:00:00', 'SEK', '1516.15', '1476.15', '1525.05', '1472.45', '1549.00', '1431.00', '2019-07-09 00:00:04'),
(1239, '2019-07-09', '00:00:00', 'CAD', '10834.18', '10754.18', '10926.20', '10654.20', '10917.00', '10611.00', '2019-07-09 00:00:04'),
(1240, '2019-07-09', '00:00:00', 'CHF', '14283.26', '14183.26', '14415.00', '14069.00', '14411.00', '14022.00', '2019-07-09 00:00:04'),
(1241, '2019-07-09', '00:00:00', 'NZD', '9416.88', '9336.88', '9517.20', '9233.20', '9477.00', '9208.00', '2019-07-09 00:00:04'),
(1242, '2019-07-09', '00:00:00', 'GBP', '17730.85', '17630.85', '17912.50', '17451.50', '17866.00', '17420.00', '2019-07-09 00:00:04'),
(1243, '2019-07-09', '00:00:00', 'HKD', '1824.12', '1794.12', '1826.10', '1792.10', '1840.00', '1771.00', '2019-07-09 00:00:04'),
(1244, '2019-07-09', '00:00:00', 'JPY', '131.86', '128.46', '132.64', '127.77', '133.30', '126.67', '2019-07-09 00:00:04'),
(1245, '2019-07-09', '00:00:00', 'SAR', '3801.84', '3721.84', '3814.50', '3709.50', '3834.00', '3664.00', '2019-07-09 00:00:04'),
(1246, '2019-07-09', '00:00:00', 'CNH', '2108.74', '1988.74', '2132.10', '1967.00', '2103.00', '1975.00', '2019-07-09 00:00:04'),
(1247, '2019-07-09', '00:00:00', 'MYR', '3447.32', '3367.32', '3455.20', '3355.20', '0.00', '0.00', '2019-07-09 00:00:04'),
(1248, '2019-07-09', '00:00:00', 'THB', '462.43', '454.43', '463.45', '453.45', '467.00', '437.00', '2019-07-09 00:00:04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kurs_markup`
--

CREATE TABLE `kurs_markup` (
  `id` int(11) NOT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `lastDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kurs_markup`
--

INSERT INTO `kurs_markup` (`id`, `currency`, `amount`, `lastDate`) VALUES
(1, 'IDR', 200, '2019-05-06 12:51:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_admin`
--

CREATE TABLE `ms_admin` (
  `id` int(11) NOT NULL,
  `userID` varchar(100) DEFAULT NULL,
  `userPassword` varchar(100) DEFAULT NULL,
  `userName` varchar(100) DEFAULT NULL,
  `flag` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_admin`
--

INSERT INTO `ms_admin` (`id`, `userID`, `userPassword`, `userName`, `flag`) VALUES
(1, 'riaabdul', '25d55ad283aa400af464c76d713c07ad', 'Ria Abdul', 0),
(2, 'freddy', 'd0970714757783e6cf17b26fb8e2298f', 'Freddy', 0),
(3, 'ddn', 'e10adc3949ba59abbe56e057f20f883e', 'ddn', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_location`
--

CREATE TABLE `ms_location` (
  `id` int(11) NOT NULL,
  `location` varchar(50) DEFAULT NULL,
  `flag` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_location`
--

INSERT INTO `ms_location` (`id`, `location`, `flag`) VALUES
(1, 'JOHOR', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_product_category`
--

CREATE TABLE `ms_product_category` (
  `id` int(11) NOT NULL,
  `product_category_name` varchar(100) DEFAULT NULL,
  `flag` char(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_product_category`
--

INSERT INTO `ms_product_category` (`id`, `product_category_name`, `flag`) VALUES
(1, 'medical', '0'),
(2, 'hiking', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_room_setting`
--

CREATE TABLE `ms_room_setting` (
  `id` int(11) NOT NULL,
  `settingName` varchar(100) DEFAULT NULL,
  `as` int(11) DEFAULT '0',
  `atw` int(11) DEFAULT '0',
  `atp` int(11) DEFAULT '0',
  `cwa` int(11) DEFAULT '0',
  `cwb` int(11) DEFAULT '0',
  `cnb` int(11) DEFAULT '0',
  `flag` char(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_room_setting`
--

INSERT INTO `ms_room_setting` (`id`, `settingName`, `as`, `atw`, `atp`, `cwa`, `cwb`, `cnb`, `flag`) VALUES
(1, 'Adult Single', 1, 0, 0, 0, 0, 0, '0'),
(2, 'Adult + Adult (TwinSharing)', 0, 2, 0, 0, 0, 0, '0'),
(3, 'Adult + Adult + Adult (Triple)', 0, 0, 3, 0, 0, 0, '0'),
(4, 'Adult + Adult + Child No Bed', 0, 2, 0, 0, 0, 1, '0'),
(5, 'Adult + Adult + Child With Bed', 0, 2, 0, 0, 1, 0, '0'),
(6, 'Adult + Adult + Child No Bed + Child No Bed', 0, 2, 0, 0, 0, 2, '0'),
(7, 'Adult + Adult + Child With Bed + Child No Bed', 0, 2, 0, 0, 1, 1, '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `package_fne`
--

CREATE TABLE `package_fne` (
  `id` int(11) NOT NULL,
  `productID` int(11) DEFAULT NULL,
  `packageName` varchar(100) DEFAULT NULL,
  `hotelName` varchar(100) DEFAULT NULL,
  `hotelStars` int(11) DEFAULT NULL,
  `priceSell` float DEFAULT NULL,
  `priceDiscount` float DEFAULT NULL,
  `priceNett` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `package_fne`
--

INSERT INTO `package_fne` (`id`, `productID`, `packageName`, `hotelName`, `hotelStars`, `priceSell`, `priceDiscount`, `priceNett`) VALUES
(1, 1, 'SUPER SAVE 1', 'Arenaa Star Hotel', 3, 295.22, 0, 295.22),
(2, 1, 'SUPER SAVE 2', 'Pacific Express Hotel', 4, 295.97, 0, 295.97),
(3, 1, 'SUPER SAVE 3', 'Pudu Plaza Hotel', 3, 316.81, 0, 316.81),
(4, 1, 'BEST DEAL 1', 'Hotel Royal Kuala Lumpur', 4, 458.54, 0, 458.54),
(5, 1, 'BEST DEAL 2', 'Dorsett Regency', 5, 475.22, 0, 475.22),
(6, 1, 'BEST DEAL 3', 'Le Apple Boutique Hotel @ KLCC', 3, 483.55, 0, 483.55),
(7, 1, 'DELUXE 1', 'Pacific Regency Hotel Suites', 5, 441.87, 0, 441.87),
(8, 1, 'DELUXE 2', 'Grand Millennium Kuala Lumpur', 5, 842.05, 0, 842.05),
(9, 2, 'SUPER SAVE 1', 'Fave Hotel Cenang Beach', 2, 498.98, 0, 498.98),
(10, 2, 'SUPER SAVE 2', 'The Villa Langkawi', 4, 570.26, 0, 570.26),
(11, 2, 'BEST DEAL 1', 'Bayview Langkawi Kuah Town', 4, 617.78, 0, 617.78),
(12, 2, 'BEST DEAL 2', 'Century Langkasuka Resort Langkawi', 4, 693.03, 0, 693.03),
(13, 2, 'DELUXE 1', 'Century Langkawi Beach Resort', 4, 740.55, 0, 740.55),
(14, 2, 'DELUXE 2', 'Berjaya Langkawi Beach & Spa Resort', 3, 934.6, 0, 934.6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_fne`
--

CREATE TABLE `product_fne` (
  `id` int(11) NOT NULL,
  `productName` varchar(200) DEFAULT NULL,
  `highlight` text,
  `image` varchar(100) DEFAULT NULL,
  `fileItin` varchar(100) DEFAULT NULL,
  `noofdays` int(11) DEFAULT NULL,
  `noofnights` int(11) DEFAULT NULL,
  `validDays` varchar(50) DEFAULT NULL,
  `validPeriodeFrom` date DEFAULT NULL,
  `validPeriodeTo` date DEFAULT NULL,
  `minpax` int(11) DEFAULT NULL,
  `include` text,
  `exclude` text,
  `tnc` text,
  `agentID` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `lastDate` datetime DEFAULT NULL,
  `lastBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product_fne`
--

INSERT INTO `product_fne` (`id`, `productName`, `highlight`, `image`, `fileItin`, `noofdays`, `noofnights`, `validDays`, `validPeriodeFrom`, `validPeriodeTo`, `minpax`, `include`, `exclude`, `tnc`, `agentID`, `createdDate`, `createdBy`, `lastDate`, `lastBy`) VALUES
(1, '3D/2N Favourite Kuala Lumpur', 'Paket tour Malaysia 2 malam di tengah kota Kuala Lumpur, ditambah dengan mengunjungi King`s Palace, Independence Square, National Museum, Cocoa House Boutique, Twin Tower.', '01.jpg', NULL, 3, 2, 'mon,tue,wed,thu,fri,sat,sun', '2019-04-01', '2019-05-31', 2, '<ul>\r\n<li>2 Nights stay at Kuala Lumpur Hotel</li>\r\n<li>Daily Breakfast</li>\r\n<li>Return Airport Transfer (KLIA/KLIA2 - Hotel - KLIA/KLIA2) based on SIC</li>\r\n<li>Half Day City Tour (SIC)</li>\r\n</ul>', '<ul>\r\n<li>International Air ticket</li>\r\n<li>Tips guide & driver</li>\r\n<li>Personal expenses</li>\r\n<li>Lunch and Dinner</li>\r\n<li>Tourism Tax (Guest pay directly upon check in, RM 10.00 per room per night)</li>\r\n<li>Midnight transfer (between 2300-0700 hours) will subject to additional surcharge of USD 20 per person per way</li>\r\n<li>Fare quoted base on SIC if guest last minute change flight details we will charge additional USD 43 per Van per Way (7am – 11pm) and USD 60 per Van per Way (11pm – 7am)</li>\r\n<li>High Season Surcharge</li>\r\n</ul>', '<ul>\r\n<li>Applicable for Domestic & KIMS/KITAS holder only</li>\r\n<li>Rates are subject to change with or without prior notice</li>\r\n<li>Above tour fare quoted on SIC basis, any changes or amendment will consider private tour and subjects to additional charge</li>\r\n<li>For arrival pick-up at the Airport based on the flight`s Estimated Time of Arrival (ETA), the maximum waiting period shall be 1.5 hours from the flight landing time</li>\r\n<li>For departure transfer, the maximum waiting period shall be 15 minutes from the agreed pick-up time</li>\r\n<li>Unused services is not transferable and cannot be redeemed or exchanged for cash, other products or discount</li>\r\n<li>Cancellation policy apply</li>\r\n</ul>', 1, '2019-04-09 19:08:37', 1, '2019-04-09 19:08:44', 1),
(2, '3D/2N Favourite Langkawi Tour', 'Paket tour Malaysia ini mengajak Anda melihat keindahan Pulau Langkawi yang memukau dengan didukung oleh duty free diseluruh toko perbelanjaan di Pulau Langkawi ini, yang akan menambahkan kesenangan saat berlibur di Pulau Elang merah ini. Kami suguhkan Special Promo Paket untuk Anda agar dapat berkesempatan untuk menikmati keindahan pulau Langkawi yang menakjubkan.', '02.jpg', NULL, 3, 2, 'mon,tue,wed,thu,fri,sat,sun', '2019-04-01', '2019-05-31', 2, '<ul>\r\n<li>2 Nights accommodation in Langkawi</li>\r\n<li>Daily breakfast</li>\r\n<li>Return airport transfer based on SIC</li>\r\n<li>Half day Langkawi city tour based on (SIC) EXCLUDE ADMISSION TICKET</li>\r\n</ul>', '<ul>\r\n<li>International Air ticket</li>\r\n<li>Tips guide & driver</li>\r\n<li>Personal expenses</li>\r\n<li>Lunch and Dinner</li>\r\n<li>Tourism Tax (Guest pay directly upon check in, RM 10.00 per room per night)</li>\r\n<li>Midnight transfer (between 2300-0700 hours) will subject to additional surcharge of USD 20 per person per way</li>\r\n<li>Fare quoted base on SIC if guest last minute change flight details we will charge additional USD 43 per Van per Way (7am – 11pm) and USD 60 per Van per Way (11pm – 7am)</li>\r\n<li>High Season Surcharge for stay at Berjaya Langkawi Resort : TBA</li>\r\n<li>Compulsory Dinner on 24 & 31 Dec 2019 for stay at Bayview Langkawi or Century Langkasuka or Century Langkawi Beach Resort or Berjaya Langkawi Resort : TBA</li>\r\n</ul>', '<ul>\r\n<li>Applicable for Domestic & KIMS/KITAS holder only</li>\r\n<li>Rates are subject to change with or without prior notice</li>\r\n<li>Above tour fare quoted on SIC basis, any changes or amendment will consider private tour and subjects to additional charge</li>\r\n<li>For arrival pick-up at the Airport based on the flight`s Estimated Time of Arrival (ETA), the maximum waiting period shall be 1.5 hours from the flight landing time</li>\r\n<li>For departure transfer, the maximum waiting period shall be 15 minutes from the agreed pick-up time</li>\r\n<li>Unused services is not transferable and cannot be redeemed or exchanged for cash, other products or discounts</li>\r\n<li>Cancellation policy apply</li>\r\n</ul>', 1, '2019-04-09 19:31:43', 1, '2019-04-09 19:31:47', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `bookingCode` varchar(10) DEFAULT NULL,
  `bookingDate` date DEFAULT NULL,
  `bookingTime` time DEFAULT NULL,
  `agentID` int(11) DEFAULT NULL,
  `productID` int(11) DEFAULT NULL,
  `inDate` date DEFAULT NULL,
  `outDate` date DEFAULT NULL,
  `noofpax` int(11) DEFAULT NULL,
  `contactName` varchar(100) DEFAULT NULL,
  `contactMobile` varchar(50) DEFAULT NULL,
  `isStatus` int(11) DEFAULT '0' COMMENT '0 Waiting list 1 Confirm 2 Guarantee 3 Complete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `reservation`
--

INSERT INTO `reservation` (`id`, `bookingCode`, `bookingDate`, `bookingTime`, `agentID`, `productID`, `inDate`, `outDate`, `noofpax`, `contactName`, `contactMobile`, `isStatus`) VALUES
(1, '2YLMJ5', '2019-05-07', '10:43:59', 6, 3, '2019-05-23', '2019-05-26', 2, 'RIA ABDUL', '085607719219', 0),
(2, 'B5VA1A', '2019-05-07', '11:14:04', 6, 12, '2019-05-19', '2019-05-23', 2, 'NOVERIA ANGGRAENI', '085607719219', 0),
(3, 'TPYDFN', '2019-05-07', '11:32:48', 6, 4, '2019-06-02', '2019-06-06', 1, 'LINA PARAMITHA', '08560771921', 0),
(4, 'N5QZMD', '2019-05-07', '11:46:06', 6, 5, '2019-08-18', '2019-08-21', 2, 'LINA', '0808080808', 0),
(5, 'MQN9KQ', '2019-05-07', '12:00:54', 6, 10, '2019-06-04', '2019-06-08', 2, 'PARK CHAN YEOL', '085607719219', 0),
(6, '4W3ADN', '2019-07-04', '22:24:14', 19, 1, '2019-08-18', '2019-08-21', 2, 'FF', 'fff', 0),
(7, 'NHBKHB', '2019-07-08', '11:48:43', 19, 14, '2019-09-08', '2019-09-12', 2, 'BAMBANG', '081555566667777', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `reservation_details`
--

CREATE TABLE `reservation_details` (
  `id` int(11) NOT NULL,
  `reservationID` int(11) DEFAULT NULL,
  `gCategory` varchar(50) DEFAULT NULL,
  `gFname` varchar(50) DEFAULT NULL,
  `gLname` varchar(50) DEFAULT NULL,
  `gPassport` varchar(50) DEFAULT NULL,
  `gExpired` varchar(50) DEFAULT NULL,
  `gDob` varchar(50) DEFAULT NULL,
  `OpriceNett` float DEFAULT NULL,
  `priceNett` float DEFAULT NULL,
  `OagentComm` float DEFAULT NULL,
  `agentComm` float DEFAULT NULL,
  `OadminComm` float DEFAULT NULL,
  `adminComm` float DEFAULT NULL,
  `exchangeRate` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `reservation_details`
--

INSERT INTO `reservation_details` (`id`, `reservationID`, `gCategory`, `gFname`, `gLname`, `gPassport`, `gExpired`, `gDob`, `OpriceNett`, `priceNett`, `OagentComm`, `agentComm`, `OadminComm`, `adminComm`, `exchangeRate`) VALUES
(1, 1, 'twin', 'RIA', 'ABDUL', '', '1993-09-10', '', 1657.23, 6106840, 115, 423772, 30, 110549, 3684.97),
(2, 1, 'cwa', 'DO', 'KYUNGSOO', '', '', '', 1657.23, 6106840, 115, 423772, 30, 110549, 3684.97),
(3, 2, 'twin', 'NOVERIA', 'ANGGRAENI', 'B159357', '22 NOV 2019', '10 NOV 1993', 2002.89, 7380590, 115, 423772, 30, 110549, 3684.97),
(4, 2, 'cwa', 'KIM', 'MIN SEOK', 'A456622', '19 SEP 2020', '05 MEI 1990', 2002.89, 7380590, 115, 423772, 30, 110549, 3684.97),
(5, 3, 'single', 'LINA', 'PARAMITHA', 'C159357', '21 JAN 2020', '21 SEP 1982', 2687.63, 9903840, 115, 423772, 30, 110549, 3684.97),
(6, 4, 'single', 'LINA', 'PARAMITHA', 'A123456', '21 DESEMBER 2020', '21 DEC 1990', 2430.03, 8954590, 115, 423772, 30, 110549, 3684.97),
(7, 4, 'cwb', 'ZOE', 'PARAMITHA', 'B123456', '21 DESEMBER 2020', '20 DEC 2010', 1943.45, 7161560, 115, 423772, 30, 110549, 3684.97),
(8, 5, 'twin', 'RIA', 'ABDUL', 'A159357', '', '', 1715.94, 6323190, 115, 423772, 30, 110549, 3684.97),
(9, 5, 'twin', 'PARK', 'CHAN YEOL', 'B159357', '', '', 1715.94, 6323190, 115, 423772, 30, 110549, 3684.97),
(10, 6, 'triple', 'FF', 'FF', 'FF', 'FF', 'FF', 1571.36, 5738940, 115, 420004, 30, 109566, 3652.21),
(11, 6, 'triple', 'FF', 'FF', 'FF', 'FF', 'FF', 1571.36, 5738940, 115, 420004, 30, 109566, 3652.21),
(12, 7, 'twin', 'BAMBANG', 'SUSSANTO', '', '', '', 1773.33, 6465080, 115, 419259, 30, 109372, 3645.73),
(13, 7, 'twin', 'SUSANTO', 'BAMBANG', '', '', '', 1773.33, 6465080, 115, 419259, 30, 109372, 3645.73);

-- --------------------------------------------------------

--
-- Struktur dari tabel `seller`
--

CREATE TABLE `seller` (
  `id` int(11) NOT NULL,
  `sellerID` varchar(100) DEFAULT NULL,
  `sellerEmail` varchar(100) DEFAULT NULL,
  `companyName` varchar(100) DEFAULT NULL,
  `companyAddress` varchar(100) DEFAULT NULL,
  `companyCity` varchar(100) DEFAULT NULL,
  `companyCountry` varchar(100) DEFAULT NULL,
  `companyZipcode` varchar(100) DEFAULT NULL,
  `companyPhone` varchar(100) DEFAULT NULL,
  `companyFax` varchar(100) DEFAULT NULL,
  `companyEmail` varchar(100) DEFAULT NULL,
  `companyLicence` varchar(200) DEFAULT NULL,
  `contactName` varchar(100) DEFAULT NULL,
  `contactPosition` varchar(100) DEFAULT NULL,
  `contactMobile` varchar(100) DEFAULT NULL,
  `contactWhatsapp` varchar(100) DEFAULT NULL,
  `contactEmail` varchar(100) DEFAULT NULL,
  `isApproved` char(1) DEFAULT '0' COMMENT '0 Belum approve 1 Sudah',
  `flag` char(1) DEFAULT '0',
  `createdDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `approvedDate` datetime DEFAULT NULL,
  `approvedBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `seller`
--

INSERT INTO `seller` (`id`, `sellerID`, `sellerEmail`, `companyName`, `companyAddress`, `companyCity`, `companyCountry`, `companyZipcode`, `companyPhone`, `companyFax`, `companyEmail`, `companyLicence`, `contactName`, `contactPosition`, `contactMobile`, `contactWhatsapp`, `contactEmail`, `isApproved`, `flag`, `createdDate`, `approvedDate`, `approvedBy`) VALUES
(1, 'ahindragiri', 'tba@gmail.com', 'ASIA HOLIDAY ', 'JL INDRAGIRI', 'SBY', 'TBA', '123', '123', '123', 'tba@gmail.com', '123', 'YUKE', 'IT', '123', '123', 'tba@gmail.com', '1', '0', '2019-06-23 22:25:21', '2019-06-24 12:26:43', 1),
(4, NULL, 'dudinabcde@gmail.com', 'TES COMPANY SELLER', 'TES ADDRESS SELLER', 'KL', 'MALAYSIA', '1231', '1', '1', 'dudinabcde@gmail.com', 'tes licence', 'TES CONTACT SELLER', 'TES POSITION', '1', '1', 'dudinabcde@gmail.com', '0', '0', '2019-07-04 03:38:46', '2019-07-04 03:38:46', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `seller_log`
--

CREATE TABLE `seller_log` (
  `id` int(11) NOT NULL,
  `sellerID` int(11) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `createdDate` date DEFAULT NULL,
  `createdTime` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `seller_staff`
--

CREATE TABLE `seller_staff` (
  `id` int(11) NOT NULL,
  `sellerID` int(11) DEFAULT NULL,
  `staffName` varchar(100) DEFAULT NULL,
  `staffUsername` varchar(100) DEFAULT NULL,
  `staffPassword` varchar(100) DEFAULT NULL,
  `flag` char(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `seller_staff`
--

INSERT INTO `seller_staff` (`id`, `sellerID`, `staffName`, `staffUsername`, `staffPassword`, `flag`) VALUES
(1, 1, 'yuke', 'SUPERID', 'd0970714757783e6cf17b26fb8e2298f', '0'),
(4, 1, 'yukedhelilah', 'yuke', 'e10adc3949ba59abbe56e057f20f883e', '0');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `buyer`
--
ALTER TABLE `buyer`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `buyer_log`
--
ALTER TABLE `buyer_log`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `buyer_staff`
--
ALTER TABLE `buyer_staff`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `fne_bod`
--
ALTER TABLE `fne_bod`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `fne_category`
--
ALTER TABLE `fne_category`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `fne_product`
--
ALTER TABLE `fne_product`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `fne_product_pricing`
--
ALTER TABLE `fne_product_pricing`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `fne_room`
--
ALTER TABLE `fne_room`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `itinerary_fne`
--
ALTER TABLE `itinerary_fne`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kurs`
--
ALTER TABLE `kurs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kurs_all`
--
ALTER TABLE `kurs_all`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kurs_markup`
--
ALTER TABLE `kurs_markup`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ms_admin`
--
ALTER TABLE `ms_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ms_location`
--
ALTER TABLE `ms_location`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ms_product_category`
--
ALTER TABLE `ms_product_category`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ms_room_setting`
--
ALTER TABLE `ms_room_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `package_fne`
--
ALTER TABLE `package_fne`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `product_fne`
--
ALTER TABLE `product_fne`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `reservation_details`
--
ALTER TABLE `reservation_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `seller`
--
ALTER TABLE `seller`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `seller_log`
--
ALTER TABLE `seller_log`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `seller_staff`
--
ALTER TABLE `seller_staff`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `buyer`
--
ALTER TABLE `buyer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `buyer_log`
--
ALTER TABLE `buyer_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `buyer_staff`
--
ALTER TABLE `buyer_staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `fne_bod`
--
ALTER TABLE `fne_bod`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `fne_category`
--
ALTER TABLE `fne_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `fne_product`
--
ALTER TABLE `fne_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `fne_product_pricing`
--
ALTER TABLE `fne_product_pricing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `fne_room`
--
ALTER TABLE `fne_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT untuk tabel `itinerary_fne`
--
ALTER TABLE `itinerary_fne`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `kurs`
--
ALTER TABLE `kurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT untuk tabel `kurs_all`
--
ALTER TABLE `kurs_all`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1249;

--
-- AUTO_INCREMENT untuk tabel `kurs_markup`
--
ALTER TABLE `kurs_markup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `ms_admin`
--
ALTER TABLE `ms_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `ms_location`
--
ALTER TABLE `ms_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `ms_product_category`
--
ALTER TABLE `ms_product_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `ms_room_setting`
--
ALTER TABLE `ms_room_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `package_fne`
--
ALTER TABLE `package_fne`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `product_fne`
--
ALTER TABLE `product_fne`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `reservation_details`
--
ALTER TABLE `reservation_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `seller`
--
ALTER TABLE `seller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `seller_log`
--
ALTER TABLE `seller_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `seller_staff`
--
ALTER TABLE `seller_staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
